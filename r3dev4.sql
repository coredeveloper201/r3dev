-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2019 at 10:53 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `r3dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_ship_methods`
--

CREATE TABLE `admin_ship_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courier_id` int(11) NOT NULL,
  `fee` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_ship_methods`
--

INSERT INTO `admin_ship_methods` (`id`, `name`, `courier_id`, `fee`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'UPS Ground', 1, 0, '2018-06-01 03:13:50', '2018-06-01 03:13:50', NULL),
(2, 'UPS 23', 1, 0, '2018-06-01 03:15:23', '2018-06-01 03:20:10', NULL),
(3, 'UPS 3', 1, 0, '2018-06-01 03:15:34', '2018-06-01 03:15:34', NULL),
(4, 'erw', 1, 0, '2018-06-01 03:20:58', '2018-06-01 03:21:00', '2018-06-01 03:21:00'),
(5, 'Truck', 3, 0, '2018-06-01 03:38:12', '2018-07-02 09:19:53', '2018-07-02 09:19:53'),
(6, 'asdf3', 4, 0, '2018-07-02 09:28:36', '2018-07-02 09:33:19', '2018-07-02 09:33:19'),
(7, 'with fee', 1, 35, '2018-07-20 09:44:36', '2018-07-20 09:45:01', NULL),
(8, 'Pore', 4, NULL, '2018-08-01 12:17:06', '2018-08-01 12:17:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `value`, `link`, `status`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1', '5b2cb5c0-416c-11e9-b881-3be2f47ec26f.png', 'https://r3all.com', 1, 1, NULL, '2019-03-04 06:16:58', '2019-03-08 00:35:25'),
(2, '1', '792240e0-416c-11e9-b7e5-e5babdbdd355.jpeg', 'https://r3all.com', 1, 1, NULL, '2019-03-04 06:18:35', '2019-03-08 00:36:15'),
(3, '1', '81e2f240-416c-11e9-a9a6-6b601292e956.jpeg', 'https://r3all.com', 1, 1, NULL, '2019-03-04 06:18:49', '2019-03-08 00:36:30'),
(6, '111', '<h2><strong>#Coveted</strong></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Our Favorite Polish Minis&nbsp;12-Piece Mini Nail Set</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"https://r3all.com/admin/setting/bannerone#\">SHOP NOW</a></p>', NULL, 1, 1, NULL, '2019-03-04 06:45:35', '2019-03-04 06:47:32'),
(7, '2', 'c3f86ee0-416c-11e9-a3b3-d39872325dca.png', 'https://r3all.com', 1, 1, NULL, '2019-03-04 07:04:51', '2019-03-08 00:38:21'),
(8, '222', '<h2><strong>#Coveted</strong></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Our Favorite Polish Minis&nbsp;12-Piece Mini Nail Set</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"https://r3all.com/admin/setting/bannerone#\">SHOP NOW</a></p>', NULL, 1, 1, NULL, '2019-03-04 07:06:32', '2019-03-04 07:06:32'),
(9, '2', 'ca360c90-416c-11e9-a276-a5b186be889b.jpeg', 'https://r3all.com', 1, 1, NULL, '2019-03-05 00:31:12', '2019-03-08 00:38:31'),
(10, '2', 'cef07050-416c-11e9-b8c6-2d30f27f0fa8.jpeg', 'https://r3all.com', 1, 1, NULL, '2019-03-05 00:31:20', '2019-03-08 00:38:39');

-- --------------------------------------------------------

--
-- Table structure for table `block_users`
--

CREATE TABLE `block_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `block_users`
--

INSERT INTO `block_users` (`id`, `user_id`, `vendor_meta_id`, `created_at`, `updated_at`) VALUES
(3, 14, 1, '2018-06-26 05:31:46', '2018-06-26 05:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `body_sizes`
--

CREATE TABLE `body_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `body_sizes`
--

INSERT INTO `body_sizes` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Junior', 1, '2018-05-17 04:13:39', '2018-05-22 14:18:18', NULL),
(2, 'sdf', 1, '2018-05-17 05:00:37', '2018-05-17 05:00:40', '2018-05-17 05:00:40'),
(3, 'Young Contemporary', 1, '2018-05-17 06:39:49', '2018-05-22 14:18:31', NULL),
(4, '222', 0, '2018-05-17 06:42:29', '2018-05-17 06:42:35', '2018-05-17 06:42:35'),
(5, 'Missy', 1, '2018-05-22 14:18:43', '2018-05-22 14:18:43', NULL),
(6, 'Plus Size', 1, '2018-05-22 14:18:51', '2018-05-22 14:18:51', NULL),
(7, 'Maternity', 1, '2018-05-22 14:18:59', '2018-05-22 14:18:59', NULL),
(8, 'tt', 3, '2018-06-22 03:13:35', '2018-06-22 03:36:26', '2018-06-22 03:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `buyer_shipping_addresses`
--

CREATE TABLE `buyer_shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `store_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commercial` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buyer_shipping_addresses`
--

INSERT INTO `buyer_shipping_addresses` (`id`, `user_id`, `default`, `store_no`, `location`, `address`, `unit`, `city`, `state_id`, `state_text`, `zip`, `country_id`, `phone`, `fax`, `commercial`, `created_at`, `updated_at`, `deleted_at`) VALUES
(25, 27, 1, NULL, NULL, 'Address', NULL, 'City', NULL, 'State', '1234', 20, '123123123', NULL, 0, '2019-03-07 12:02:07', '2019-03-07 23:03:47', NULL),
(23, 26, 1, NULL, NULL, 'Address', NULL, 'City Name', NULL, 'State', '1234', 20, NULL, NULL, 0, '2019-03-05 04:27:11', '2019-03-08 03:43:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

CREATE TABLE `cart_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` bigint(191) NOT NULL,
  `color_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Jewelry', 0, 1, '2018-05-15 03:22:30', '2018-12-01 09:25:01', '2018-12-01 09:25:01'),
(2, 'SHOES', 9, 1, '2018-05-15 03:22:39', '2018-05-22 13:35:29', '2018-05-22 13:35:29'),
(3, 'ACCESSORIES', 0, 3, '2018-05-15 03:22:42', '2018-06-30 12:52:55', '2018-06-30 12:52:55'),
(4, 'Body Jewelry', 43, 2, '2018-05-15 03:22:44', '2018-06-29 09:32:59', NULL),
(5, 'HANDBAGS', 0, 4, '2018-05-15 03:22:47', '2018-06-28 04:12:25', '2018-06-28 04:12:25'),
(6, 'Bracelets', 43, 3, '2018-05-15 03:22:50', '2018-06-29 09:32:59', NULL),
(7, 'Bagpack', 44, 1, '2018-05-15 03:22:52', '2018-06-07 13:55:05', NULL),
(8, 'Belts', 43, 1, '2018-05-15 03:22:54', '2018-06-29 09:32:59', NULL),
(9, 'SHOES', 0, 2, '2018-05-15 03:22:57', '2018-06-30 12:52:46', '2018-06-30 12:52:46'),
(10, 'Necklaces', 1, 1, '2018-05-15 03:59:50', '2018-07-09 07:39:55', NULL),
(11, 'Bucket', 44, 2, '2018-05-15 04:00:29', '2018-06-07 13:55:05', NULL),
(12, 'Long Length', 10, 1, '2018-05-22 02:39:30', '2018-07-09 07:39:55', NULL),
(13, 'Made in Korea', 10, 2, '2018-05-22 02:39:47', '2018-07-09 07:39:55', NULL),
(14, 'Earrings', 1, 2, '2018-05-22 02:40:09', '2018-07-09 07:39:55', NULL),
(15, 'Dangle/Pendant', 14, 1, '2018-05-22 02:40:18', '2018-07-09 07:39:55', NULL),
(16, 'Adjustable Clip On', 14, 2, '2018-05-22 02:40:26', '2018-07-09 07:39:55', NULL),
(17, 'Mid Length', 10, 3, '2018-05-22 13:17:29', '2018-07-09 07:39:55', NULL),
(18, 'Chokers', 10, 4, '2018-05-22 13:17:42', '2018-07-09 07:39:55', NULL),
(19, 'Statement Necklaces', 10, 5, '2018-05-22 13:17:58', '2018-07-09 07:39:55', NULL),
(20, 'Seamless', 10, 6, '2018-05-22 13:18:10', '2018-07-09 07:36:15', '2018-07-09 07:36:15'),
(21, 'Shirt & Blouse', 10, 7, '2018-05-22 13:18:25', '2018-07-09 07:36:19', '2018-07-09 07:36:19'),
(22, 'Tank & Tube Tops', 10, 8, '2018-05-22 13:19:07', '2018-07-09 07:36:22', '2018-07-09 07:36:22'),
(23, 'T-Shirt & Polo', 10, 9, '2018-05-22 13:19:28', '2018-07-09 07:36:25', '2018-07-09 07:36:25'),
(24, 'Tunics', 10, 10, '2018-05-22 13:19:40', '2018-07-09 07:36:27', '2018-07-09 07:36:27'),
(25, 'Stud', 14, 3, '2018-05-22 13:20:17', '2018-07-09 07:39:55', NULL),
(26, 'Hoop', 14, 4, '2018-05-22 13:20:30', '2018-07-09 07:39:55', NULL),
(27, 'Earcuffs', 14, 5, '2018-05-22 13:20:42', '2018-07-09 07:39:55', NULL),
(28, 'Earring Sets', 14, 6, '2018-05-22 13:21:00', '2018-07-09 07:39:55', NULL),
(29, 'JACKETS / OUTWEAR', 1, 3, '2018-05-22 13:30:21', '2018-07-09 07:39:35', '2018-07-09 07:39:35'),
(30, 'Cape & Poncho', 29, 1, '2018-05-22 13:30:47', '2018-07-09 07:39:02', NULL),
(31, 'Coats', 29, 2, '2018-05-22 13:31:00', '2018-07-09 07:39:02', NULL),
(32, 'Hoodies', 29, 3, '2018-05-22 13:31:10', '2018-07-09 07:39:02', NULL),
(33, 'Jackets & Blazer', 29, 4, '2018-05-22 13:31:29', '2018-07-09 07:39:02', NULL),
(34, 'Shrugs & Cardigans', 29, 5, '2018-05-22 13:31:46', '2018-07-09 07:39:02', NULL),
(35, 'Sweaters', 29, 6, '2018-05-22 13:32:00', '2018-07-09 07:39:02', NULL),
(36, 'Vests', 29, 7, '2018-05-22 13:32:17', '2018-06-30 12:52:49', '2018-06-30 12:52:49'),
(37, 'Booties', 42, 1, '2018-05-22 13:33:58', '2018-06-29 09:32:59', NULL),
(38, 'Boots', 42, 2, '2018-05-22 13:34:11', '2018-06-29 09:32:59', NULL),
(39, 'Dress Shoes', 42, 3, '2018-05-22 13:34:25', '2018-06-29 09:32:59', NULL),
(40, 'Flats', 42, 4, '2018-05-22 13:34:40', '2018-06-29 09:32:59', NULL),
(41, 'Crossbody Bags', 44, 3, '2018-05-22 13:39:00', '2018-06-07 13:55:05', NULL),
(42, 'SHOES', 9, 1, '2018-05-22 13:45:19', '2018-06-29 09:32:59', NULL),
(43, 'ACCESSORIES', 3, 1, '2018-05-22 13:45:58', '2018-06-29 09:32:59', NULL),
(44, 'HANDBAGS', 5, 1, '2018-05-22 13:46:15', '2018-06-07 13:55:05', NULL),
(45, 'MEN', 0, 4, '2018-05-22 13:46:33', '2018-06-30 12:52:43', '2018-06-30 12:52:43'),
(46, 'MEN', 45, 1, '2018-05-22 13:46:53', '2018-06-29 09:32:59', NULL),
(47, 'Casual Shirts', 46, 1, '2018-05-22 13:47:15', '2018-06-29 09:32:59', NULL),
(48, 'Denim', 46, 2, '2018-05-22 13:47:36', '2018-06-29 09:32:59', NULL),
(49, 'Dress Shirts', 46, 3, '2018-05-22 13:47:50', '2018-06-29 09:32:59', NULL),
(50, 'Graphic', 46, 4, '2018-05-22 13:48:04', '2018-06-29 09:32:59', NULL),
(51, 'KIDS', 0, 5, '2018-05-22 13:48:21', '2018-06-30 12:52:23', '2018-06-30 12:52:23'),
(52, 'KIDS', 51, 1, '2018-05-22 13:48:26', '2018-06-29 09:32:59', NULL),
(53, 'Boys', 52, 1, '2018-05-22 13:48:56', '2018-06-29 09:32:59', NULL),
(54, 'Girls', 52, 2, '2018-05-22 13:49:03', '2018-06-29 09:32:59', NULL),
(55, 'Infants', 52, 3, '2018-05-22 13:49:21', '2018-06-29 09:32:59', NULL),
(56, 'MORE', 0, 7, '2018-05-22 13:50:00', '2018-06-30 12:52:14', '2018-06-30 12:52:14'),
(57, 'MORE', 56, 1, '2018-05-22 13:50:04', '2018-06-29 09:32:59', NULL),
(58, 'Cosmetics', 57, 1, '2018-05-22 13:50:22', '2018-06-29 09:32:59', NULL),
(59, 'Fixtures/Display', 57, 2, '2018-05-22 13:50:33', '2018-06-29 09:32:59', NULL),
(60, 'Key Chains', 57, 3, '2018-05-22 13:50:43', '2018-06-29 09:32:59', NULL),
(61, 'test2', 0, 8, '2018-06-29 09:32:30', '2018-06-29 09:33:09', '2018-06-29 09:33:09'),
(62, 'Accessories', 0, 2, '2018-06-30 12:52:36', '2018-12-01 09:25:04', '2018-12-01 09:25:04'),
(63, 'Second Level', 0, 3, '2018-06-30 12:53:11', '2018-07-09 07:42:20', '2018-07-09 07:42:20'),
(64, 'Second Level Item', 63, 1, '2018-06-30 12:53:22', '2018-07-09 07:39:55', NULL),
(65, 'Made in Korea', 14, 7, '2018-07-09 07:39:28', '2018-07-09 07:39:55', NULL),
(66, 'Beach Towels', 62, 1, '2018-07-09 07:41:16', '2018-07-09 07:41:16', NULL),
(67, 'Ponchos and Vests', 62, 2, '2018-07-09 07:41:29', '2018-07-09 07:41:29', NULL),
(68, 'Bags and Clutches', 62, 3, '2018-07-09 07:41:46', '2018-07-09 07:41:46', NULL),
(69, 'Handbags', 68, 1, '2018-07-09 07:42:35', '2018-07-09 07:42:35', NULL),
(70, 'Backpacks', 68, 2, '2018-07-09 07:42:44', '2018-07-09 07:42:44', NULL),
(71, 'Clutches', 68, 3, '2018-07-09 07:42:57', '2018-07-09 07:42:57', NULL),
(72, 'Cosmetic Bags', 68, 4, '2018-07-09 07:43:08', '2018-07-09 07:43:08', NULL),
(73, 'Coin Purse', 68, 5, '2018-07-09 07:43:17', '2018-07-09 07:43:17', NULL),
(74, 'Hand Made Handbags', 68, 6, '2018-07-09 07:43:35', '2018-07-09 07:43:35', NULL),
(75, 'Crossbody Bags', 68, 7, '2018-07-09 07:43:53', '2018-07-09 07:43:53', NULL),
(76, 'Fanny Packs', 68, 8, '2018-07-09 07:44:04', '2018-07-09 07:44:04', NULL),
(77, 'Parent', 0, 3, '2018-07-09 09:29:40', '2018-12-01 09:25:07', '2018-12-01 09:25:07'),
(78, 'Clothing', 0, 4, '2018-08-09 05:21:45', '2018-12-01 09:25:09', '2018-12-01 09:25:09'),
(79, 'Dresses', 78, 1, '2018-08-09 05:21:50', '2018-08-09 05:21:50', NULL),
(80, 'Peptide Skin Care', 0, 6, '2018-12-01 09:25:31', '2019-03-08 03:30:34', NULL),
(81, 'Sun/Make-Up', 0, 7, '2018-12-01 09:25:38', '2019-03-08 03:31:07', NULL),
(82, 'Health', 0, 8, '2018-12-01 09:25:45', '2019-03-08 03:31:32', NULL),
(83, 'KIDS', 0, 4, '2018-12-01 09:25:51', '2019-03-08 03:31:38', '2019-03-08 03:31:38'),
(84, 'SHOES', 0, 5, '2018-12-01 09:26:00', '2019-03-08 03:31:42', '2019-03-08 03:31:42'),
(85, 'COATS', 80, 2, '2018-12-01 09:26:24', '2019-03-04 03:50:21', NULL),
(86, 'Skin Toner/Emulsion', 80, 6, '2018-12-01 09:26:32', '2019-03-08 03:34:54', NULL),
(87, 'BLAZERS', 80, 3, '2018-12-01 09:26:45', '2019-03-04 03:50:21', NULL),
(88, 'DRESSES', 80, 4, '2018-12-01 09:26:54', '2019-03-04 03:50:21', NULL),
(89, 'JUMPSUITS', 80, 5, '2018-12-01 09:27:05', '2019-03-04 03:50:21', NULL),
(90, 'OUTERWEAR', 81, 1, '2018-12-01 09:27:27', '2019-03-04 03:50:21', NULL),
(91, 'Point Make-Up', 81, 6, '2018-12-01 09:27:42', '2019-03-08 03:33:25', NULL),
(92, 'Base Make-Up', 81, 7, '2018-12-01 09:27:51', '2019-03-08 03:33:45', NULL),
(93, 'Health Supplement Food', 82, 1, '2018-12-01 09:28:13', '2019-03-08 03:32:15', NULL),
(94, 'T-SHIRTS', 82, 2, '2018-12-01 09:28:22', '2019-03-08 03:32:07', '2019-03-08 03:32:07'),
(95, 'POLOS', 82, 3, '2018-12-01 09:28:35', '2019-03-08 03:32:10', '2019-03-08 03:32:10');

-- --------------------------------------------------------

--
-- Table structure for table `category_banners`
--

CREATE TABLE `category_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_banners`
--

INSERT INTO `category_banners` (`id`, `type`, `vendor_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2018-06-07 15:19:59', '2018-06-07 15:19:59'),
(2, 2, 2, 1, '2018-06-07 15:25:45', '2018-06-07 15:25:45'),
(3, 3, 1, 1, '2018-06-07 15:25:49', '2018-06-07 15:25:49'),
(6, 3, 2, 1, '2018-06-07 15:30:23', '2018-06-07 15:30:23'),
(8, 2, 1, 1, '2018-06-08 10:28:20', '2018-06-08 10:28:20'),
(7, 1, 2, 1, '2018-06-08 09:09:28', '2018-06-08 09:09:28'),
(10, 1, 1, 0, '2018-06-11 05:49:31', '2018-06-11 05:49:31'),
(11, 1, 2, 0, '2018-06-11 12:52:37', '2018-06-11 12:52:37'),
(12, 2, 1, 0, '2018-06-11 12:52:40', '2018-06-11 12:52:40'),
(13, 2, 2, 0, '2018-06-11 12:52:44', '2018-06-11 12:52:44'),
(14, 3, 1, 0, '2018-06-11 12:52:48', '2018-06-11 12:52:48'),
(15, 3, 2, 0, '2018-06-11 12:52:51', '2018-06-11 12:52:51');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `master_color_id` int(11) DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbs_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `status`, `master_color_id`, `image_path`, `thumbs_image_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BEIGE', 1, 14, NULL, NULL, '2018-05-11 09:11:15', '2018-06-25 07:55:30', NULL),
(5, 'BLACK MULTI', 1, 8, NULL, NULL, '2018-05-22 14:38:46', '2018-06-10 03:53:16', NULL),
(2, 'tet2', 1, 1, NULL, NULL, '2018-05-12 02:29:35', '2018-05-12 03:15:09', '2018-05-12 03:15:09'),
(3, 'DeepBlue2', 1, 4, NULL, NULL, '2018-05-17 10:36:52', '2018-05-17 10:36:59', '2018-05-17 10:36:59'),
(4, 'BLACK', 1, 8, NULL, NULL, '2018-05-18 10:55:03', '2018-06-10 03:34:39', NULL),
(6, 'BLUE', 1, 9, NULL, NULL, '2018-05-22 14:38:59', '2018-06-10 03:53:20', NULL),
(7, 'BLUE MULTI', 1, 9, NULL, NULL, '2018-05-22 14:39:12', '2018-06-10 03:53:25', NULL),
(8, 'BLUE/YELLOW', 1, 15, NULL, NULL, '2018-05-22 14:39:26', '2018-06-10 03:53:30', NULL),
(9, 'BLUSH', 1, 17, NULL, NULL, '2018-05-22 14:39:36', '2018-07-07 10:00:00', '2018-07-07 10:00:00'),
(10, 'BRICK', 1, 10, NULL, NULL, '2018-05-22 14:39:47', '2018-05-22 14:39:47', NULL),
(11, 'BROWN', 1, 10, NULL, NULL, '2018-05-22 14:40:01', '2018-06-10 03:53:39', NULL),
(12, 'CAMEL', 1, 15, NULL, NULL, '2018-05-22 14:40:09', '2018-06-10 03:53:43', NULL),
(13, 'CHARCOAL', 1, 8, NULL, NULL, '2018-05-22 14:40:18', '2018-05-22 14:41:13', NULL),
(14, 'CORAL', 1, 16, NULL, NULL, '2018-05-22 14:40:31', '2018-06-10 03:53:51', NULL),
(15, 'GREEN', 1, 13, NULL, NULL, '2018-05-23 04:14:51', '2018-06-10 03:53:56', NULL),
(16, 'RED', 1, 7, NULL, NULL, '2018-05-23 04:14:57', '2018-06-10 03:54:07', NULL),
(17, 'WHITE', 1, 21, NULL, NULL, '2018-05-23 04:15:04', '2018-05-23 04:15:04', NULL),
(18, 'PINK', 1, 17, NULL, NULL, '2018-05-23 04:24:10', '2018-06-10 03:54:02', NULL),
(19, 'JADE', 1, 14, NULL, NULL, '2018-05-23 15:23:45', '2018-05-23 15:23:45', NULL),
(20, 'WHITE', 1, 21, NULL, NULL, '2018-05-23 15:24:04', '2018-05-23 15:24:04', NULL),
(21, 'YELLOW', 1, 22, NULL, NULL, '2018-05-23 15:24:18', '2018-05-23 15:24:18', NULL),
(22, 'PINK', 1, 17, NULL, NULL, '2018-05-23 15:27:43', '2018-05-23 15:27:43', NULL),
(23, 'MINT', 1, 13, NULL, NULL, '2018-05-23 15:28:02', '2018-05-23 15:28:02', NULL),
(24, '122', 1, 9, NULL, NULL, '2018-05-26 09:47:53', '2018-05-26 09:53:27', '2018-05-26 09:53:27'),
(25, '3', 1, 10, NULL, NULL, '2018-05-26 09:52:58', '2018-05-26 09:53:24', '2018-05-26 09:53:24'),
(26, 'test', 1, 9, NULL, NULL, '2018-06-10 03:15:20', '2018-06-22 05:36:56', '2018-06-22 05:36:56'),
(27, 'tttt', 1, 8, '/images/vendors/1/colors/19af7a90-6f29-11e8-8d9e-99bd1237045b.jpg', '/images/vendors/1/colors/thumbs/19af7a90-6f29-11e8-8d9e-99bd1237045b.jpg', '2018-06-13 10:40:13', '2018-06-13 15:46:47', '2018-06-13 15:46:47'),
(28, 'fdf', 1, 8, NULL, NULL, '2018-06-22 05:37:02', '2018-06-22 05:37:10', '2018-06-22 05:37:10'),
(29, 'sdf', 1, 9, '/images/vendors/1/colors/9fb374f0-7610-11e8-9d3a-cf28bc1263a7.jpg', '/images/vendors/1/colors/thumbs/9fb374f0-7610-11e8-9d3a-cf28bc1263a7.jpg', '2018-06-22 05:37:20', '2018-06-22 05:37:24', '2018-06-22 05:37:24'),
(30, 'rt', 1, 8, NULL, NULL, '2018-06-25 07:56:25', '2018-06-25 07:56:28', '2018-06-25 07:56:28'),
(31, 'trt', 1, 8, NULL, NULL, '2018-06-25 10:52:40', '2018-06-25 10:52:43', '2018-06-25 10:52:43'),
(35, 'sadf', 1, 9, NULL, NULL, '2018-06-28 09:40:47', '2018-06-28 09:40:47', NULL),
(36, 'asdf', 1, 20, NULL, NULL, '2018-06-28 09:43:58', '2018-06-28 09:43:58', NULL),
(37, 'bsdf', 1, 19, NULL, NULL, '2018-06-28 09:55:47', '2018-06-28 09:55:47', NULL),
(38, 'v', 1, 20, NULL, NULL, '2018-06-28 10:42:02', '2018-06-28 10:42:02', NULL),
(39, 'fasd', 1, 8, NULL, NULL, '2018-06-28 12:21:52', '2018-06-28 12:21:52', NULL),
(40, 'bl', 1, 13, NULL, NULL, '2018-06-28 13:26:09', '2018-06-28 13:26:09', NULL),
(41, 'gfsad22', 1, 8, '/images/colors/38155b90-7bb6-11e8-b7ad-a176e0cfc740.jpg', '/images/colors/thumbs/38155b90-7bb6-11e8-b7ad-a176e0cfc740.jpg', '2018-06-29 10:05:08', '2018-06-29 10:05:44', '2018-06-29 10:05:44'),
(42, 'asdfdf', 1, 20, NULL, NULL, '2018-06-30 10:28:36', '2018-06-30 10:28:36', NULL),
(43, 'GOLD', 1, 11, NULL, NULL, '2018-07-09 07:50:58', '2018-07-09 07:50:58', NULL),
(44, 'SILVER', 1, 19, NULL, NULL, '2018-07-09 07:51:04', '2018-07-09 07:51:04', NULL),
(45, 'AMBER', 1, 8, NULL, NULL, '2018-07-09 07:53:03', '2018-07-09 07:53:03', NULL),
(46, 'GREY', 1, 12, NULL, NULL, '2018-07-09 07:53:14', '2018-07-09 07:53:14', NULL),
(47, 'GUNMETAL', 1, 19, NULL, NULL, '2018-07-09 07:53:22', '2018-07-09 07:53:22', NULL),
(48, 'Navy', 1, NULL, NULL, NULL, '2018-08-09 05:23:15', '2018-08-09 05:23:15', NULL),
(49, 'JHAKANAKA', 1, NULL, NULL, NULL, '2018-09-24 06:48:42', '2018-09-24 06:48:42', NULL),
(50, 'TAN', 1, 10, NULL, NULL, '2018-12-01 09:32:16', '2018-12-01 09:32:16', NULL),
(51, 'BURGUNDY', 1, 10, NULL, NULL, '2018-12-01 09:32:41', '2018-12-01 09:32:41', NULL),
(52, 'MID-CAMEL', 1, 11, NULL, NULL, '2018-12-01 09:42:53', '2018-12-01 09:42:53', NULL),
(53, 'CHEETAH', 1, NULL, NULL, NULL, '2018-12-13 02:43:45', '2018-12-13 02:43:45', NULL),
(54, 'asdfsdf', 1, 20, NULL, NULL, '2018-12-27 06:10:39', '2018-12-27 06:10:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `color_item`
--

CREATE TABLE `color_item` (
  `item_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `color_item`
--

INSERT INTO `color_item` (`item_id`, `color_id`, `available`) VALUES
(3, 4, 1),
(3, 40, 1),
(4, 4, 1),
(5, 43, 1),
(5, 44, 1),
(6, 45, 1),
(6, 46, 1),
(6, 47, 1),
(7, 46, 1),
(7, 44, 1),
(8, 4, 1),
(9, 46, 1),
(9, 44, 1),
(10, 4, 1),
(10, 20, 1),
(12, 4, 0),
(12, 20, 0),
(13, 4, 1),
(13, 20, 0),
(14, 4, 1),
(14, 20, 0),
(14, 23, 0),
(15, 4, 0),
(15, 23, 1),
(15, 20, 1),
(16, 4, 0),
(16, 23, 1),
(16, 20, 1),
(17, 17, 1),
(17, 4, 1),
(18, 45, 1),
(18, 46, 1),
(18, 47, 1),
(19, 48, 1),
(19, 13, 1),
(19, 4, 1),
(20, 49, 1),
(20, 17, 1),
(20, 16, 1),
(20, 15, 1),
(21, 51, 0),
(21, 50, 1),
(22, 11, 1),
(23, 52, 1),
(24, 4, 1),
(25, 53, 1),
(26, 4, 1),
(27, 4, 1),
(28, 4, 1),
(29, 51, 0),
(29, 50, 1),
(29, 4, 0),
(30, 52, 1),
(31, 52, 1),
(34, 52, 1),
(35, 16, 1),
(36, 16, 1),
(37, 16, 0),
(38, 16, 0),
(39, 16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'US', 'United States'),
(2, 'CA', 'Canada'),
(3, 'AF', 'Afghanistan'),
(4, 'AL', 'Albania'),
(5, 'DZ', 'Algeria'),
(6, 'AS', 'American Samoa'),
(7, 'AD', 'Andorra'),
(8, 'AO', 'Angola'),
(9, 'AI', 'Anguilla'),
(10, 'AQ', 'Antarctica'),
(11, 'AG', 'Antigua and/or Barbuda'),
(12, 'AR', 'Argentina'),
(13, 'AM', 'Armenia'),
(14, 'AW', 'Aruba'),
(15, 'AU', 'Australia'),
(16, 'AT', 'Austria'),
(17, 'AZ', 'Azerbaijan'),
(18, 'BS', 'Bahamas'),
(19, 'BH', 'Bahrain'),
(20, 'BD', 'Bangladesh'),
(21, 'BB', 'Barbados'),
(22, 'BY', 'Belarus'),
(23, 'BE', 'Belgium'),
(24, 'BZ', 'Belize'),
(25, 'BJ', 'Benin'),
(26, 'BM', 'Bermuda'),
(27, 'BT', 'Bhutan'),
(28, 'BO', 'Bolivia'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British lndian Ocean Territory'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CV', 'Cape Verde'),
(41, 'KY', 'Cayman Islands'),
(42, 'CF', 'Central African Republic'),
(43, 'TD', 'Chad'),
(44, 'CL', 'Chile'),
(45, 'CN', 'China'),
(46, 'CX', 'Christmas Island'),
(47, 'CC', 'Cocos (Keeling) Islands'),
(48, 'CO', 'Colombia'),
(49, 'KM', 'Comoros'),
(50, 'CG', 'Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'CD', 'Democratic Republic of Congo'),
(58, 'DK', 'Denmark'),
(59, 'DJ', 'Djibouti'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Dominican Republic'),
(62, 'TP', 'East Timor'),
(63, 'EC', 'Ecudaor'),
(64, 'EG', 'Egypt'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Equatorial Guinea'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Ethiopia'),
(70, 'FK', 'Falkland Islands (Malvinas)'),
(71, 'FO', 'Faroe Islands'),
(72, 'FJ', 'Fiji'),
(73, 'FI', 'Finland'),
(74, 'FR', 'France'),
(75, 'FX', 'France, Metropolitan'),
(76, 'GF', 'French Guiana'),
(77, 'PF', 'French Polynesia'),
(78, 'TF', 'French Southern Territories'),
(79, 'GA', 'Gabon'),
(80, 'GM', 'Gambia'),
(81, 'GE', 'Georgia'),
(82, 'DE', 'Germany'),
(83, 'GH', 'Ghana'),
(84, 'GI', 'Gibraltar'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'JO', 'Jordan'),
(111, 'KZ', 'Kazakhstan'),
(112, 'KE', 'Kenya'),
(113, 'KI', 'Kiribati'),
(114, 'KP', 'Korea, Democratic People\'s Republic of'),
(115, 'KR', 'Korea, Republic of'),
(116, 'KW', 'Kuwait'),
(117, 'KG', 'Kyrgyzstan'),
(118, 'LA', 'Lao People\'s Democratic Republic'),
(119, 'LV', 'Latvia'),
(120, 'LB', 'Lebanon'),
(121, 'LS', 'Lesotho'),
(122, 'LR', 'Liberia'),
(123, 'LY', 'Libyan Arab Jamahiriya'),
(124, 'LI', 'Liechtenstein'),
(125, 'LT', 'Lithuania'),
(126, 'LU', 'Luxembourg'),
(127, 'MO', 'Macau'),
(128, 'MK', 'Macedonia'),
(129, 'MG', 'Madagascar'),
(130, 'MW', 'Malawi'),
(131, 'MY', 'Malaysia'),
(132, 'MV', 'Maldives'),
(133, 'ML', 'Mali'),
(134, 'MT', 'Malta'),
(135, 'MH', 'Marshall Islands'),
(136, 'MQ', 'Martinique'),
(137, 'MR', 'Mauritania'),
(138, 'MU', 'Mauritius'),
(139, 'TY', 'Mayotte'),
(140, 'MX', 'Mexico'),
(141, 'FM', 'Micronesia, Federated States of'),
(142, 'MD', 'Moldova, Republic of'),
(143, 'MC', 'Monaco'),
(144, 'MN', 'Mongolia'),
(145, 'MS', 'Montserrat'),
(146, 'MA', 'Morocco'),
(147, 'MZ', 'Mozambique'),
(148, 'MM', 'Myanmar'),
(149, 'NA', 'Namibia'),
(150, 'NR', 'Nauru'),
(151, 'NP', 'Nepal'),
(152, 'NL', 'Netherlands'),
(153, 'AN', 'Netherlands Antilles'),
(154, 'NC', 'New Caledonia'),
(155, 'NZ', 'New Zealand'),
(156, 'NI', 'Nicaragua'),
(157, 'NE', 'Niger'),
(158, 'NG', 'Nigeria'),
(159, 'NU', 'Niue'),
(160, 'NF', 'Norfork Island'),
(161, 'MP', 'Northern Mariana Islands'),
(162, 'NO', 'Norway'),
(163, 'OM', 'Oman'),
(164, 'PK', 'Pakistan'),
(165, 'PW', 'Palau'),
(166, 'PA', 'Panama'),
(167, 'PG', 'Papua New Guinea'),
(168, 'PY', 'Paraguay'),
(169, 'PE', 'Peru'),
(170, 'PH', 'Philippines'),
(171, 'PN', 'Pitcairn'),
(172, 'PL', 'Poland'),
(173, 'PT', 'Portugal'),
(174, 'PR', 'Puerto Rico'),
(175, 'QA', 'Qatar'),
(176, 'SS', 'Republic of South Sudan'),
(177, 'RE', 'Reunion'),
(178, 'RO', 'Romania'),
(179, 'RU', 'Russian Federation'),
(180, 'RW', 'Rwanda'),
(181, 'KN', 'Saint Kitts and Nevis'),
(182, 'LC', 'Saint Lucia'),
(183, 'VC', 'Saint Vincent and the Grenadines'),
(184, 'WS', 'Samoa'),
(185, 'SM', 'San Marino'),
(186, 'ST', 'Sao Tome and Principe'),
(187, 'SA', 'Saudi Arabia'),
(188, 'SN', 'Senegal'),
(189, 'RS', 'Serbia'),
(190, 'SC', 'Seychelles'),
(191, 'SL', 'Sierra Leone'),
(192, 'SG', 'Singapore'),
(193, 'SK', 'Slovakia'),
(194, 'SI', 'Slovenia'),
(195, 'SB', 'Solomon Islands'),
(196, 'SO', 'Somalia'),
(197, 'ZA', 'South Africa'),
(198, 'GS', 'South Georgia South Sandwich Islands'),
(199, 'ES', 'Spain'),
(200, 'LK', 'Sri Lanka'),
(201, 'SH', 'St. Helena'),
(202, 'PM', 'St. Pierre and Miquelon'),
(203, 'SD', 'Sudan'),
(204, 'SR', 'Suriname'),
(205, 'SJ', 'Svalbarn and Jan Mayen Islands'),
(206, 'SZ', 'Swaziland'),
(207, 'SE', 'Sweden'),
(208, 'CH', 'Switzerland'),
(209, 'SY', 'Syrian Arab Republic'),
(210, 'TW', 'Taiwan'),
(211, 'TJ', 'Tajikistan'),
(212, 'TZ', 'Tanzania, United Republic of'),
(213, 'TH', 'Thailand'),
(214, 'TG', 'Togo'),
(215, 'TK', 'Tokelau'),
(216, 'TO', 'Tonga'),
(217, 'TT', 'Trinidad and Tobago'),
(218, 'TN', 'Tunisia'),
(219, 'TR', 'Turkey'),
(220, 'TM', 'Turkmenistan'),
(221, 'TC', 'Turks and Caicos Islands'),
(222, 'TV', 'Tuvalu'),
(223, 'UG', 'Uganda'),
(224, 'UA', 'Ukraine'),
(225, 'AE', 'United Arab Emirates'),
(226, 'GB', 'United Kingdom'),
(227, 'UM', 'United States minor outlying islands'),
(228, 'UY', 'Uruguay'),
(229, 'UZ', 'Uzbekistan'),
(230, 'VU', 'Vanuatu'),
(231, 'VA', 'Vatican City State'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Virgin Islands (British)'),
(235, 'VI', 'Virgin Islands (U.S.)'),
(236, 'WF', 'Wallis and Futuna Islands'),
(237, 'EH', 'Western Sahara'),
(238, 'YE', 'Yemen'),
(239, 'YU', 'Yugoslavia'),
(240, 'ZR', 'Zaire'),
(241, 'ZM', 'Zambia'),
(242, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

CREATE TABLE `couriers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `couriers`
--

INSERT INTO `couriers` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'UPS', '2018-05-31 13:43:21', '2018-05-31 13:46:34', NULL),
(2, 'tyty', '2018-05-31 13:48:22', '2018-05-31 13:48:25', '2018-05-31 13:48:25'),
(3, 'Others', '2018-06-01 03:38:03', '2018-07-02 09:20:12', '2018-07-02 09:20:12'),
(4, 'dfdf2', '2018-07-02 09:20:16', '2018-07-02 09:20:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fabrics`
--

CREATE TABLE `fabrics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_fabric_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fabrics`
--

INSERT INTO `fabrics` (`id`, `name`, `master_fabric_id`, `status`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, '100% COTTON', 5, 1, 0, '2018-05-12 11:02:30', '2018-06-30 09:57:35', NULL),
(10, 'er', 2, 1, 0, '2018-05-12 11:02:54', '2018-05-12 11:02:57', '2018-05-12 11:02:57'),
(8, '100% POLYAMIDE', 37, 1, 0, '2018-05-12 09:49:52', '2018-06-30 09:57:35', NULL),
(7, '53% COTTON 47% ACETATE', 5, 1, 0, '2018-05-12 09:49:28', '2018-06-30 09:57:35', NULL),
(6, '100% ACRYLIC', 43, 1, 0, '2018-05-12 09:48:02', '2018-06-30 09:57:35', NULL),
(11, 'dd2', 3, 1, 0, '2018-05-17 01:25:56', '2018-05-17 01:26:07', '2018-05-17 01:26:07'),
(12, '59% POLYESTER 35% POLYAMIDE 6% SPANDEX', 28, 1, 0, '2018-05-23 15:22:05', '2018-06-30 09:57:35', NULL),
(13, '60% COTTON 37% POLYAMIDE 3% SPANDEX', 3, 1, 0, '2018-05-23 15:22:16', '2018-06-30 09:57:35', NULL),
(14, '64% COTTON 36% POLYESTER', 5, 1, 0, '2018-05-23 15:22:25', '2018-06-30 09:57:35', NULL),
(15, '122', 3, 1, 1, '2018-05-26 11:42:54', '2018-05-26 12:03:45', '2018-05-26 12:03:45'),
(16, '3334', 43, 1, 1, '2018-06-30 09:55:59', '2018-06-30 09:57:39', '2018-06-30 09:57:39');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'erd', '2018-05-14 04:26:39', '2018-05-14 04:28:21', '2018-05-14 04:28:21'),
(2, 'df', '2018-05-14 04:27:04', '2018-05-14 05:14:22', '2018-05-14 05:14:22'),
(3, 'Women\'s Clothing', '2018-05-14 05:14:32', '2018-05-14 05:14:32', NULL),
(4, 'Men\'s Clothing', '2018-05-14 05:14:44', '2018-05-14 05:14:44', NULL),
(5, 'Children\'s Clothings', '2018-05-14 05:14:59', '2018-05-14 05:14:59', NULL),
(6, 'Accessories', '2018-05-14 05:15:09', '2018-05-14 05:15:09', NULL),
(7, 'Shoes', '2018-05-14 05:15:19', '2018-05-14 05:15:19', NULL),
(8, 'Fixure', '2018-05-14 05:15:28', '2018-05-14 05:15:28', NULL),
(9, 'Handbag', '2018-05-14 05:15:38', '2018-05-14 05:15:38', NULL),
(10, 'Cosmetic', '2018-05-14 05:15:47', '2018-05-14 05:15:47', NULL),
(11, 'Lingerie', '2018-05-14 05:15:58', '2018-05-14 05:15:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `industry_user`
--

CREATE TABLE `industry_user` (
  `user_id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industry_user`
--

INSERT INTO `industry_user` (`user_id`, `industry_id`) VALUES
(1, 3),
(1, 5),
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `style_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) NOT NULL,
  `orig_price` double(8,2) DEFAULT NULL,
  `pack_id` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` int(11) DEFAULT NULL,
  `advantage` text COLLATE utf8mb4_unicode_ci,
  `details` longtext COLLATE utf8mb4_unicode_ci,
  `available_on` date DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_parent_category` int(11) DEFAULT NULL,
  `default_second_category` int(11) DEFAULT NULL,
  `default_third_category` int(11) DEFAULT NULL,
  `min_qty` int(11) DEFAULT NULL,
  `fabric` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `made_in_id` int(11) DEFAULT NULL,
  `labeled` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `status`, `style_no`, `video`, `price`, `orig_price`, `pack_id`, `sorting`, `description`, `featured`, `advantage`, `details`, `available_on`, `availability`, `name`, `default_parent_category`, `default_second_category`, `default_third_category`, `min_qty`, `fabric`, `made_in_id`, `labeled`, `memo`, `activated_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '800 Series', NULL, 30.00, NULL, NULL, NULL, NULL, 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/izJvqsAT1Fvh5nLLtdvwEc9bzETPnCl4LISVUjzE.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>Peptide total anti-aging</h2>\r\n\r\n<h1><strong>800 MOISTURIZING AMPOULE </strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>펩타이드 800 ppm 함유.</strong></p>\r\n\r\n<p><strong>보습 펩타이드 원료를 가득 담아 즉각적인 효과.</strong></p>\r\n\r\n<p><strong>히알루론산 및 엘라스틴 합성 촉진으로 보습과 피부재생에 효과.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>● 사용방법 : 아침, 저녁 세안 후 스킨 및 로션과 같은 어떠한 제품도 사용하지 마시고 본 제품만 사용하기를 권장하며, 한 번의 사용만으로도 변화된 피부를 느끼실 수 있습니다.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>전성분 :</strong></p>\r\n\r\n<p><strong>정제수, 글리세린, 에탄올, 1,2-헥산다이올, 소듐하이알루로네이트, 카프릴릴글라이콜, 피피지-26-부테스-26,</strong></p>\r\n\r\n<p><strong>카퍼트라이펩타이드-1, 베르가모트오일, 아세틸헥사펩타이드-8, 트라이펩타이드-1, 다이펩타이드-15,</strong></p>\r\n\r\n<p><strong>카르노신, 에스에이치-올리고펩타이드-1, 에스에이치-폴리펩타이드-1</strong></p>', NULL, 2, '800 Moisturizing Ampoule', 80, NULL, NULL, 900, NULL, NULL, NULL, NULL, NULL, '2019-03-08 00:50:47', '2019-03-08 02:51:27', NULL),
(2, 1, '800 Series', NULL, 30.00, NULL, NULL, NULL, NULL, 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/izJvqsAT1Fvh5nLLtdvwEc9bzETPnCl4LISVUjzE.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>Peptide total anti-aging</h2>\r\n\r\n<h1><strong>800 MOISTURIZING AMPOULE </strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>펩타이드 800 ppm 함유.</strong></p>\r\n\r\n<p><strong>보습 펩타이드 원료를 가득 담아 즉각적인 효과.</strong></p>\r\n\r\n<p><strong>히알루론산 및 엘라스틴 합성 촉진으로 보습과 피부재생에 효과.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>● 사용방법 : 아침, 저녁 세안 후 스킨 및 로션과 같은 어떠한 제품도 사용하지 마시고 본 제품만 사용하기를 권장하며, 한 번의 사용만으로도 변화된 피부를 느끼실 수 있습니다.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>전성분 :</strong></p>\r\n\r\n<p><strong>정제수, 글리세린, 에탄올, 1,2-헥산다이올, 소듐하이알루로네이트, 카프릴릴글라이콜, 피피지-26-부테스-26,</strong></p>\r\n\r\n<p><strong>카퍼트라이펩타이드-1, 베르가모트오일, 아세틸헥사펩타이드-8, 트라이펩타이드-1, 다이펩타이드-15,</strong></p>\r\n\r\n<p><strong>카르노신, 에스에이치-올리고펩타이드-1, 에스에이치-폴리펩타이드-1</strong></p>', NULL, 2, '800 Anti-Wrinkle Ampoule', 80, NULL, NULL, 900, NULL, NULL, NULL, NULL, NULL, '2019-03-08 00:59:09', '2019-03-08 02:52:06', NULL),
(3, 0, '800 Series-delete-465558821', NULL, 30.00, NULL, NULL, NULL, NULL, 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/izJvqsAT1Fvh5nLLtdvwEc9bzETPnCl4LISVUjzE.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>Peptide total anti-aging</h2>\r\n\r\n<h1><strong>800 MOISTURIZING AMPOULE </strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>펩타이드 800 ppm 함유.</strong></p>\r\n\r\n<p><strong>보습 펩타이드 원료를 가득 담아 즉각적인 효과.</strong></p>\r\n\r\n<p><strong>히알루론산 및 엘라스틴 합성 촉진으로 보습과 피부재생에 효과.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>● 사용방법 : 아침, 저녁 세안 후 스킨 및 로션과 같은 어떠한 제품도 사용하지 마시고 본 제품만 사용하기를 권장하며, 한 번의 사용만으로도 변화된 피부를 느끼실 수 있습니다.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>전성분 :</strong></p>\r\n\r\n<p><strong>정제수, 글리세린, 에탄올, 1,2-헥산다이올, 소듐하이알루로네이트, 카프릴릴글라이콜, 피피지-26-부테스-26,</strong></p>\r\n\r\n<p><strong>카퍼트라이펩타이드-1, 베르가모트오일, 아세틸헥사펩타이드-8, 트라이펩타이드-1, 다이펩타이드-15,</strong></p>\r\n\r\n<p><strong>카르노신, 에스에이치-올리고펩타이드-1, 에스에이치-폴리펩타이드-1</strong></p>', NULL, 2, '800 Bright Ampoule', 80, NULL, NULL, 900, NULL, NULL, NULL, NULL, NULL, '2019-03-08 01:03:46', '2019-03-08 01:05:25', '2019-03-08 01:05:25'),
(4, 0, '800 Series-delete-868989272', NULL, 30.00, NULL, NULL, NULL, NULL, 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/izJvqsAT1Fvh5nLLtdvwEc9bzETPnCl4LISVUjzE.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>Peptide total anti-aging</h2>\r\n\r\n<h1><strong>800 MOISTURIZING AMPOULE </strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>펩타이드 800 ppm 함유.</strong></p>\r\n\r\n<p><strong>보습 펩타이드 원료를 가득 담아 즉각적인 효과.</strong></p>\r\n\r\n<p><strong>히알루론산 및 엘라스틴 합성 촉진으로 보습과 피부재생에 효과.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>● 사용방법 : 아침, 저녁 세안 후 스킨 및 로션과 같은 어떠한 제품도 사용하지 마시고 본 제품만 사용하기를 권장하며, 한 번의 사용만으로도 변화된 피부를 느끼실 수 있습니다.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>전성분 :</strong></p>\r\n\r\n<p><strong>정제수, 글리세린, 에탄올, 1,2-헥산다이올, 소듐하이알루로네이트, 카프릴릴글라이콜, 피피지-26-부테스-26,</strong></p>\r\n\r\n<p><strong>카퍼트라이펩타이드-1, 베르가모트오일, 아세틸헥사펩타이드-8, 트라이펩타이드-1, 다이펩타이드-15,</strong></p>\r\n\r\n<p><strong>카르노신, 에스에이치-올리고펩타이드-1, 에스에이치-폴리펩타이드-1</strong></p>', NULL, 2, '800 Bright Ampoule', 80, NULL, NULL, 900, NULL, NULL, NULL, NULL, NULL, '2019-03-08 01:04:20', '2019-03-08 01:05:25', '2019-03-08 01:05:25'),
(5, 0, '800 Series-delete-1672522959', NULL, 30.00, NULL, NULL, NULL, NULL, 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/izJvqsAT1Fvh5nLLtdvwEc9bzETPnCl4LISVUjzE.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>Peptide total anti-aging</h2>\r\n\r\n<h1><strong>800 MOISTURIZING AMPOULE </strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>펩타이드 800 ppm 함유.</strong></p>\r\n\r\n<p><strong>보습 펩타이드 원료를 가득 담아 즉각적인 효과.</strong></p>\r\n\r\n<p><strong>히알루론산 및 엘라스틴 합성 촉진으로 보습과 피부재생에 효과.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>● 사용방법 : 아침, 저녁 세안 후 스킨 및 로션과 같은 어떠한 제품도 사용하지 마시고 본 제품만 사용하기를 권장하며, 한 번의 사용만으로도 변화된 피부를 느끼실 수 있습니다.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>전성분 :</strong></p>\r\n\r\n<p><strong>정제수, 글리세린, 에탄올, 1,2-헥산다이올, 소듐하이알루로네이트, 카프릴릴글라이콜, 피피지-26-부테스-26,</strong></p>\r\n\r\n<p><strong>카퍼트라이펩타이드-1, 베르가모트오일, 아세틸헥사펩타이드-8, 트라이펩타이드-1, 다이펩타이드-15,</strong></p>\r\n\r\n<p><strong>카르노신, 에스에이치-올리고펩타이드-1, 에스에이치-폴리펩타이드-1</strong></p>', NULL, 2, '800 Bright Ampoule', 80, NULL, NULL, 900, NULL, NULL, NULL, NULL, NULL, '2019-03-08 01:04:41', '2019-03-08 01:05:25', '2019-03-08 01:05:25'),
(6, 1, '800 Series', NULL, 30.00, NULL, NULL, NULL, NULL, 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/izJvqsAT1Fvh5nLLtdvwEc9bzETPnCl4LISVUjzE.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>Peptide total anti-aging</h2>\r\n\r\n<h1><strong>800 MOISTURIZING AMPOULE </strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>펩타이드 800 ppm 함유.</strong></p>\r\n\r\n<p><strong>보습 펩타이드 원료를 가득 담아 즉각적인 효과.</strong></p>\r\n\r\n<p><strong>히알루론산 및 엘라스틴 합성 촉진으로 보습과 피부재생에 효과.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>● 사용방법 : 아침, 저녁 세안 후 스킨 및 로션과 같은 어떠한 제품도 사용하지 마시고 본 제품만 사용하기를 권장하며, 한 번의 사용만으로도 변화된 피부를 느끼실 수 있습니다.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>전성분 :</strong></p>\r\n\r\n<p><strong>정제수, 글리세린, 에탄올, 1,2-헥산다이올, 소듐하이알루로네이트, 카프릴릴글라이콜, 피피지-26-부테스-26,</strong></p>\r\n\r\n<p><strong>카퍼트라이펩타이드-1, 베르가모트오일, 아세틸헥사펩타이드-8, 트라이펩타이드-1, 다이펩타이드-15,</strong></p>\r\n\r\n<p><strong>카르노신, 에스에이치-올리고펩타이드-1, 에스에이치-폴리펩타이드-1</strong></p>', NULL, 2, '800 Bright Ampoule', 80, NULL, NULL, 900, NULL, NULL, NULL, NULL, NULL, '2019-03-08 01:06:51', '2019-03-08 02:51:58', NULL),
(7, 1, 'Peptide Line', NULL, 26.00, NULL, NULL, NULL, NULL, NULL, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/JG9NVPMD5ryb2U2608xClRsWWZKsajZOGiolqc18.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Peptide total anti-aging</h2>\r\n\r\n<h1><strong>100 C.C CREAM</strong></h1>\r\n\r\n<h2>COVER UP</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>펩타이드 100 ppm 함유.</strong></p>\r\n\r\n<p><strong>부드러운 발림성과 피부 톤에 맞는 컬러 체인지 효과로 화사한 피부 연출.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>● 사용방법 : 기초 마지막 단계 혹은 선블록 사용 후 적당량을 취하여 피부결을 따라 부드럽게 펴 발라줍니다.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>전성분 : </strong></p>\r\n\r\n<p><strong>정제수, 에칠헥실메톡시신나메이트, 프로판다이올, 사이클로펜타실록세인, 카프릴릭/카프릭트라이글리세라이드, 글리세린, 에칠헥실살리실레이트,</strong></p>\r\n\r\n<p><strong>티타늄디옥사이드, 포타슘세틸포스페이트, 사이클로헥사실록세인, 1,2-헥산다이올, 소르비탄세스퀴올리에이트, 글리세릴스테아레이트,</strong></p>\r\n\r\n<p><strong>피이지-100스테아레이트, C12-15알킬벤조에이트, 스테아릭애씨드, 오조케라이트, 세틸알코올, 스테아릴알코올, 다이프로필렌글라이콜다이벤조에이트,</strong></p>\r\n\r\n<p><strong>알루미늄하이드록사이드, 다이메티콘크로스폴리머, 트라이에톡시카프릴릴실레인, 토코페릴아세테이트, 피피지-15스테아릴에터벤조에이트,</strong></p>\r\n\r\n<p><strong>잔탄검, 아크릴레이트/C10-30알킬아크릴레이트크로스폴리머, 카퍼트라이펩타이드-1, 아세틸헥사펩타이드-8,</strong></p>\r\n\r\n<p><strong>트라이펩타이드-1, 카르노신, 다이펩타이드-15, 에스에이치-올리고펩타이드-1, 에스에이치-폴리펩타이드-1, 트로메타민, 황색산화철,</strong></p>\r\n\r\n<p><strong>적색산화철, 카프릴릴글라이콜, 다이소듐이디티에이, 향료</strong></p>', NULL, 2, '100 C.C Cream', NULL, NULL, NULL, 1500, NULL, NULL, NULL, NULL, NULL, '2019-03-08 01:10:49', '2019-03-08 02:51:47', NULL),
(8, 1, 'Peptide Line', NULL, 57.00, NULL, NULL, NULL, '고 기능성 원료인 펩타이드를 200ppm의 고농도로\r\n함유하여 손상된 피부를 빠르게 개선.\r\n1. 고 기능성 원료인 펩타이드를 200ppm의 고농도로\r\n    함유하여 손상된 피부를 빠르게 개선시켜 드립니다. \r\n2. 피부에 보습막을 형성하여 하루종일 촉촉한 피부로\r\n     유지시켜 드립니다.\r\n3. 피부에 유.수분 발란스를 조절하여 건강한 피부로\r\n     가꾸어 줍니다.\r\n4. 유효한 나노 성분들이 피부에 빠르게 흡수되어 탄력\r\n     있는 피부로 가꾸어 줍니다.\r\n* 사용방법 : 에센스 사용 후 바르는 제품으로 적당량을 덜어 얼굴에\r\n부드럽게 펴 바른 다음 톡톡 두드려서 흡수시켜 줍니다.', 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/mQbrkD79OunqC4J4QuRqmyLbuyGtrGK0HmRn09pb.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/hXwI1pG4DF64Srqc71AWXbRwhqgePqMqvkI1GnFG.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/6RHPifnaQ2BvW4yNVDZGZdvhsYkJH0ChoSGDWLEY.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/I6CVbzVYkJUEUjXWF3ObuH249hPPuFk6ORkAVlt9.jpeg\" /></p>', NULL, 2, '200 Emulsion', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, '2019-03-08 02:14:42', '2019-03-08 02:13:54', '2019-03-08 02:50:32', NULL),
(9, 1, 'Peptide Line', NULL, 26.00, NULL, NULL, NULL, NULL, 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/uCvcXgUKcz9GY48CiTgXMdcQyGs9HLan0FLJdGUF.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Peptide total anti-aging</h2>\r\n\r\n<h1><strong>R&middot;3 100 B.B CREAM</strong></h1>\r\n\r\n<h2>COVER UP</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>펩타이드 100 ppm 함유.</strong></p>\r\n\r\n<p><strong>피부를 예쁜 빛으로 가꾸어주는 촉촉하고 쫀쫀한 사용감의 커버 비비크림.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>● 사용방법</strong></h3>\r\n\r\n<p><strong>기초 마지막 단계 혹은 선블록 사용 후 적당량을 취하여 피부결을 따라 부드럽게 펴 발라줍니다.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>● 전성분</strong></h3>\r\n\r\n<p><strong>정제수, 카프릴릭/카프릭트라이글리세라이드, 티타늄디옥사이드, 에칠헥실메톡시신나메이트, 프로필렌글라이콜, 사이클로펜타실록세인,</strong></p>\r\n\r\n<p><strong>마이크로크리스탈린왁스, 에칠헥실살리실레이트, 세틸에틸헥사노에이트, 세틸피이지/피피지-10/1다이메티콘, 옥수수전분,</strong></p>\r\n\r\n<p><strong>솔비탄세스퀴올리에이트, 1,2-헥산다이올, 부틸렌글라이콜다이카프릴레이트/다이카프레이트, 오조케라이트,</strong></p>\r\n\r\n<p><strong>카퍼트라이펩타이드-1, 아세틸헥사펩타이드-8, 트라이펩타이드-1, 카르노신,</strong></p>\r\n\r\n<p><strong>다이펩타이드-15, 에스에이치-올리고펩타이드-1, 에스에이치-폴리펩타이드-1, 알란토인,</strong></p>\r\n\r\n<p><strong>다이스테아다이모늄헥토라이트, 소듐클로라이드, 마이카, 트라이에틸시트레이트,</strong></p>\r\n\r\n<p><strong>트라이에톡시카프릴릴실레인, 트로메타민, 적색산화철, 흑색산화철, 황색산화철, 카프릴릴글라이콜, 다이소듐이디티에이, 향료</strong></p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '100 B.B Cream', NULL, NULL, NULL, 1500, NULL, NULL, NULL, NULL, NULL, '2019-03-08 02:17:10', '2019-03-08 02:51:38', NULL),
(10, 1, 'Peptide Line', NULL, 66.00, NULL, NULL, NULL, NULL, 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/QmvX7AAjhXTcy6Smdy55MIcijuEn2nIsHdrJh0nj.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>미백 및 주름개선 이중 기능성 화장품</strong></p>\r\n\r\n<h1><strong>R&middot;3 AMPOULE MIST</strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>보습과 수분에 탁월한 뷰티 펩타이드를 500ppm 함유하여</strong></p>\r\n\r\n<p><strong>하루종일 촉촉하고 건강한 피부로 가꾸어 주는 고보습 앰플 미스트</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>나노 입자로 피부속 깊숙이 저자극 매일 보습 솔루션</strong></p>\r\n\r\n<h2><strong>나노 펩타이드 미스트</strong></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>● 피부 깊숙이 스며들어 촉촉함과 생기있는 피부로 가꾸어 줍니다.</strong></p>\r\n\r\n<p><strong>펩타이드와 천연 오일의 황금 비율로 건조함을 강력하게 해결해 줍니다 .</strong></p>\r\n\r\n<p><strong>● 들뜨기 쉬운 메이크업을 강하게 밀착시켜 하루종일 윤기있는 피부를 연출 시켜줍니다.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>● 사용방법</strong></h3>\r\n\r\n<p><strong>얼굴에서 15~25cm 정도 거리를 두고 눈을 감고 얼굴 전체 고르게 분사 해줍니다.&nbsp;</strong></p>\r\n\r\n<p><strong>가볍게 톡톡 두드려 피부에 흡수시켜 줍니다.</strong></p>\r\n\r\n<p><strong>메이크업&nbsp;전, 후 모두 사용이 가능합니다.</strong></p>\r\n\r\n<p><strong>세안 후 스킨 대용으로 사용이 가능합니다.</strong></p>\r\n\r\n<p><strong>촉촉한 보습과 맑은 피부톤 관리를 위해 수시로 사용해 주셔도 좋습니다.&nbsp;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>● 전성분 :</strong></h3>\r\n\r\n<p><strong>Water, Portulaca Oleracea Extract, Camellia Sinensis Leaf Extract, Niacinamide, Hydrolyzed Collagen, Aloe Barbadensis Leaf Juice,</strong></p>\r\n\r\n<p><strong>Yeast Beta-Glucan, Caprylyl Glycol, Zanthoxylum Piperitum Fruit Extract, Usnea Barbata (Lichen) Extract, PPG-26-Buteth-26,</strong></p>\r\n\r\n<p><strong>Citrus Aurantium Bergamia (Bergamot) Fruit Oil, Adenosine, Copper Tripeptide-1, Acetyl Hexapeptide-8, Tripeptide-1, Carnosine,</strong></p>\r\n\r\n<p><strong>Dipeptide-15, sh-Oligopeptide-1, sh-Polypeptide-1</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&nbsp;</strong></p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '500 Ampoule Mist', NULL, NULL, NULL, 300, NULL, NULL, NULL, NULL, '2019-03-08 02:18:41', '2019-03-08 02:18:36', '2019-03-08 02:50:58', NULL),
(11, 1, 'Peptide Line', NULL, 79.00, NULL, NULL, NULL, NULL, NULL, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/AcowIuqKpFz8dGF6Oddw2cL8rr2OSChXwVRtKk7W.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Peptide total anti-aging</h2>\r\n\r\n<h1><strong>R&middot;3 2000 TOTAL SOLUTION AMPOULE</strong></h1>\r\n\r\n<p><strong>[미백 및 주름개선 기능성 화장품]</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>펩타이드 2000 ppm 함유.</strong></p>\r\n\r\n<p><strong>고 함량의 펩타이드가 콜라겐 형성을 도와 피부에 눈에 띄는 탄력을 부여하는 기능성 토탈 영양 앰플.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>● 사용방법</strong></h3>\r\n\r\n<p><strong>아침, 저녁 세안 후 제일 먼저 사용하는 제품으로 적당량을 피부에 펴 발라 톡톡 두드리며 흡수시켜 주세요.</strong></p>\r\n\r\n<p><strong>알코올이 들어 있는 제품과 함께 사용하면 효과가 떨어질 수 있으니 가급적 알코올이 들어 있는 제품은 피해주세요.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>● 전성분</strong></h3>\r\n\r\n<p><strong>Water, Glycerin, Niacinamide, 1,2-Hexanediol, Taurine, Caprylyl Glycol, Hydroxyethylcellulose, Copper Tripeptide-1,</strong></p>\r\n\r\n<p><strong>Arginine, Adenosine, Acetyl Hexapeptide-8, Tripeptide-1, Carnosine, Dipeptide-15, Citric Acid, sh-Oligopeptide-1, sh-Polypeptide-1</strong></p>', NULL, 2, '2000 Total Solution Ampoule', NULL, NULL, NULL, 300, NULL, NULL, NULL, NULL, NULL, '2019-03-08 02:24:34', '2019-03-08 02:50:44', NULL),
(12, 1, 'Peptide Line', NULL, 79.00, NULL, NULL, NULL, '[주름개선 기능성 화장품]\r\n1. 피부의 주름개선에 도움을 줍니다.\r\n2. 고 기능성 원료인 펩타이드를 400ppm의 고농도로 함유하여 피부의 노화를 방지하며 항상 건강하고 탄력있는 피부로 가꾸어줍니다.\r\n3. 미세소구체 전달시스템기술을 적용하여 모든 유효성분을 피부에 빠르게 흡수 시켜줍니다.\r\n4. 고함량의 펩타이드가 피부 깊숙이 작용해 콜라겐 생성을 도와주어 손상된 피부를 빠르고 효과적으로 개선시켜 드립니다.\r\n* 사용방법 : 에멀전 사용 후 바르는 제품으로 적당량을 덜어 얼굴에 부드럽게 펴 바른 다음 톡톡 두드려서 흡수시켜 줍니다.', NULL, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/JmLIZIODRXZ2TKJ6od7nFaGon7n2Ik32gvTKwemx.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/zSp42kcNg3WXSXRveOtM7R77NVDwPdOHmIAujSIn.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/8LcUhjICZwe3ShhF7YxNOFAwL4wD8mOwhe28T27U.jpeg\" /></p>', NULL, 2, '400 Wrinkle Cream', NULL, NULL, NULL, 300, NULL, NULL, NULL, NULL, NULL, '2019-03-08 02:26:20', '2019-03-08 02:50:12', NULL),
(13, 1, 'Peptide Line', NULL, 78.00, NULL, NULL, NULL, '[미백 기능성 화장품]\r\n고 기능성 원료인 펩타이드를 400ppm의\r\n고농도로 함유하여 피부의 노화 방지와 탄력 부여.\r\n1. 피부의 미백에 도움을 줍니다.\r\n2. 고 기능성 원료인 펩타이드를 400ppm의 고\r\n    농도로 함유하여 피부의 노화를 방지하며 항상\r\n    건강하고 탄력 있는 피부로 가꾸어 줍니다.\r\n3. 미세소구체 전달시스템 기술을 적용하여 모든\r\n    유효성분을 피부에 빠르게 흡수 시켜줍니다.\r\n4. 미용성분의 효과적인 피부흡수, 보습력을 강\r\n    화한 피부 유.수분 조절 제품으로 피부에 안전\r\n    하면서도 효과가 뛰어난 자연화장품입니다.\r\n* 사용방법 : 아이크림 사용 후 바르는 제품으로 적당량을\r\n덜어 얼굴에 부드럽게 펴 바른 다음 톡톡 두드\r\n려서 흡수시켜 줍니다.', 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/AnBo6kkGEqGRwY6XZPxd4x1RuMep0pKTksHaKa8U.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/9xaHdLOC9A2KC29s3dlTVElmOJLRa7Lpmo1ENkQV.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/63jQ5Nf929XAkveR41x2vo6p3rEf9oBjdJaFCZ5D.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/8cf3sXPdP5CNDlfi1FYRfayOTbpa6vaQbJK0Zu0c.jpeg\" /></p>', NULL, 2, '400 Essence', NULL, NULL, NULL, 300, NULL, NULL, NULL, NULL, NULL, '2019-03-08 02:28:19', '2019-03-08 02:50:03', NULL),
(14, 1, 'Base Cosmetics', NULL, 35.00, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2, 'Peal Powder Foundation', NULL, NULL, NULL, 200, NULL, NULL, NULL, NULL, NULL, '2019-03-08 02:29:56', '2019-03-08 02:49:49', NULL),
(15, 1, 'Peptide Line', NULL, 39.00, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2, '200 B.B Stick Foundation', NULL, NULL, NULL, 200, NULL, NULL, NULL, NULL, '2019-03-08 02:32:00', '2019-03-08 02:31:04', '2019-03-08 02:49:38', NULL),
(16, 1, 'Peptide Line', NULL, 39.00, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2, '200 Cushion Foundation', NULL, NULL, NULL, 200, NULL, NULL, NULL, NULL, '2019-03-08 02:32:00', '2019-03-08 02:31:53', '2019-03-08 02:49:28', NULL),
(17, 1, 'Peptide Line', NULL, 75.00, NULL, NULL, NULL, '[주름개선 기능성 화장품]\r\n1. 피부의 주름개선에 도움을 줍니다.\r\n2. 고 기능성 원료인 펩타이드가 800ppm의 고농도로 농축되어 탄력을 잃은 눈가를 건강하게 가꾸어줍니다.\r\n3. 자극받은 눈가에 고 농도의 펩타이드가 효과적으로 작용해 편안하게 진정시켜줍니다.\r\n4. 거칠어진 눈가에 농축된 펩타이드가 콜라겐을 형성을 도와 탄력있는 피부로 만들어드립니다.\r\n* 사용방법 : 스킨토너 사용 후 제일 먼저 사용하는 제품으로 적당량을 덜어 눈가에 부드럽게 펴 바른 다음 톡톡 두드려서 흡수시켜 줍니다.', 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/tV1ZjdSb8sVbFhhu6megfNvHZeSg5sqE4TOhZwW8.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/1hlJGAMdJsv8Hqjj6paSlZp5rxfMRxoeAzdIZZRF.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/6VUl9csbXE98mAoAMZJo4izkm5NLtyWbC0sIFSMP.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/YZ6Q8YA1b8xbVvua2slJymaziL7nPGDQH1mDTBWv.jpeg\" /></p>', NULL, 2, '800 Eye Cream', NULL, NULL, NULL, 300, NULL, NULL, NULL, NULL, NULL, '2019-03-08 02:38:18', '2019-03-08 02:46:20', NULL),
(18, 1, 'Peptide Line', NULL, 48.00, NULL, NULL, NULL, '고 기능성 원료인 펩타이드를 100ppm으로 함유.\r\n1. 고 기능성 원료인 펩타이드를 100ppm으로 함유하여 일반적인 스킨토너에 비해 효과가 뛰어납니다.\r\n2. 피지분비를 조절하여 피부의 손상을 방지해줍니다.\r\n3. 건조한 피부에 깊숙히 작용하여 피부결을 정돈하고 항상 촉촉한 피부로 유지시켜 드립니다.\r\n* 사용방법 : 세안 후 적당량을 덜어 얼굴 안쪽에서 바깥쪽으로 펴 발라 흡수시켜줍니다.', 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/W1Aw98KxFqrxTi2Mt8nStDvKRpk7IGt0XQ4N5goA.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/V7a4wLdwMvUGmXRrOR8hszkGWAmyRnRKniYrwU4A.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/rdBtf7eZWHJHIJetclWAWkEnQrqDs4plUBpuzt5x.jpeg\" /><img src=\"https://r3all.com/storage/tinyeditor/Pkcb0ta0eoylizAxxF2XsTIkbeRifz4Gf4spPJBD.jpeg\" /></p>', NULL, 2, '100 Skin Toner', NULL, NULL, NULL, 300, NULL, NULL, NULL, NULL, NULL, '2019-03-08 02:42:48', '2019-03-08 02:46:06', NULL),
(19, 0, 'Peptide Line Copy', NULL, 48.00, NULL, NULL, NULL, '고 기능성 원료인 펩타이드를 100ppm으로 함유.\r\n1. 고 기능성 원료인 펩타이드를 100ppm으로 함유하여 일반적인 스킨토너에 비해 효과가 뛰어납니다.\r\n2. 피지분비를 조절하여 피부의 손상을 방지해줍니다.\r\n3. 건조한 피부에 깊숙히 작용하여 피부결을 정돈하고 항상 촉촉한 피부로 유지시켜 드립니다.\r\n* 사용방법 : 세안 후 적당량을 덜어 얼굴 안쪽에서 바깥쪽으로 펴 발라 흡수시켜줍니다.', NULL, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/dseWQEVsVvamyu1oW4JtWdhW3eZFdbkvf0bXZBQV.jpeg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '100 Skin Toner', NULL, NULL, NULL, 300, NULL, NULL, NULL, NULL, NULL, '2019-03-08 02:44:28', '2019-03-08 02:45:45', NULL),
(20, 0, 'Peptide Line Copy Copy Copy Copy', NULL, 35.00, NULL, NULL, NULL, '[주름개선 기능성 화장품]\r\n1. 피부의 주름개선에 도움을 줍니다.\r\n2. 고 기능성 원료인 펩타이드가 800ppm의 고농도로 농축되어 탄력을 잃은 눈가를 건강하게 가꾸어줍니다.\r\n3. 자극받은 눈가에 고 농도의 펩타이드가 효과적으로 작용해 편안하게 진정시켜줍니다.\r\n4. 거칠어진 눈가에 농축된 펩타이드가 콜라겐을 형성을 도와 탄력있는 피부로 만들어드립니다.\r\n* 사용방법 : 스킨토너 사용 후 제일 먼저 사용하는 제품으로 적당량을 덜어 눈가에 부드럽게 펴 바른 다음 톡톡 두드려서 흡수시켜 줍니다.', 1, NULL, '<p><img src=\"https://r3all.com/storage/tinyeditor/mWQ5UhuPDxWzkZ8Vptpgz4w5z8Fi5NGxh005UVSo.jpeg\" /></p>\r\n\r\n<p><img src=\"https://r3all.com/storage/tinyeditor/CQItax5AHlIuluyD69z8MZOviU0XOilgGAEJiZuv.jpeg\" /></p>\r\n\r\n<p><img src=\"https://r3all.com/storage/tinyeditor/YaQfFPDsubovolb6UOeoSfn2B4Q5zV3kD0T6Ad48.jpeg\" /></p>\r\n\r\n<p><img src=\"https://r3all.com/storage/tinyeditor/N6QeQCZVaZPhwQrpRKGcj84asID3MvMeJreGA6Pv.jpeg\" /></p>', NULL, 2, '800 Eye Cream', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, '2019-03-08 02:45:36', '2019-03-08 02:45:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_images`
--

CREATE TABLE `item_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `list_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbs_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_images`
--

INSERT INTO `item_images` (`id`, `item_id`, `color_id`, `sort`, `image_path`, `list_image_path`, `thumbs_image_path`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, '/images/item/6e54eed0-7c88-11e8-97da-8ff2bf00b99c.jpg', NULL, NULL, '2018-06-30 11:10:04', '2018-06-30 11:10:04'),
(3, NULL, NULL, NULL, '/images/item/31fda5b0-7c91-11e8-9394-19058d004339.jpg', NULL, NULL, '2018-06-30 12:12:48', '2018-06-30 12:12:48'),
(246, 7, NULL, 3, 'images/item/original/4de33140-4171-11e9-852c-f765185f0ed8.jpg', 'images/item/list/4de33140-4171-11e9-852c-f765185f0ed8.jpg', 'images/item/thumbs/4de33140-4171-11e9-852c-f765185f0ed8.jpg', '2019-03-08 01:09:06', '2019-03-08 01:10:51'),
(247, 7, NULL, 2, 'images/item/original/4d9b1a90-4171-11e9-865c-9f6ec7d59ad3.jpg', 'images/item/list/4d9b1a90-4171-11e9-865c-9f6ec7d59ad3.jpg', 'images/item/thumbs/4d9b1a90-4171-11e9-865c-9f6ec7d59ad3.jpg', '2019-03-08 01:09:07', '2019-03-08 01:10:50'),
(248, 7, NULL, 1, 'images/item/original/4d488060-4171-11e9-9a27-11e5c26d56d6.jpg', 'images/item/list/4d488060-4171-11e9-9a27-11e5c26d56d6.jpg', 'images/item/thumbs/4d488060-4171-11e9-9a27-11e5c26d56d6.jpg', '2019-03-08 01:09:07', '2019-03-08 01:10:50'),
(249, 8, NULL, 3, 'images/item/original/1de8f020-417a-11e9-8ff4-4ffbc13bf0c7.jpg', 'images/item/list/1de8f020-417a-11e9-8ff4-4ffbc13bf0c7.jpg', 'images/item/thumbs/1de8f020-417a-11e9-8ff4-4ffbc13bf0c7.jpg', '2019-03-08 02:13:48', '2019-03-08 02:13:56'),
(250, 8, NULL, 2, 'images/item/original/1d9ac3e0-417a-11e9-a15b-d78b486985a3.jpg', 'images/item/list/1d9ac3e0-417a-11e9-a15b-d78b486985a3.jpg', 'images/item/thumbs/1d9ac3e0-417a-11e9-a15b-d78b486985a3.jpg', '2019-03-08 02:13:48', '2019-03-08 02:13:55'),
(251, 8, NULL, 1, 'images/item/original/1d48f7e0-417a-11e9-9300-6db401476966.jpg', 'images/item/list/1d48f7e0-417a-11e9-9300-6db401476966.jpg', 'images/item/thumbs/1d48f7e0-417a-11e9-9300-6db401476966.jpg', '2019-03-08 02:13:49', '2019-03-08 02:13:54'),
(252, 9, NULL, 2, 'images/item/original/92202d50-417a-11e9-a16b-4dc466408382.jpg', 'images/item/list/92202d50-417a-11e9-a16b-4dc466408382.jpg', 'images/item/thumbs/92202d50-417a-11e9-a16b-4dc466408382.jpg', '2019-03-08 02:17:05', '2019-03-08 02:17:10'),
(253, 9, NULL, 1, 'images/item/original/91f4bc00-417a-11e9-9ecf-59e7c9308771.jpg', 'images/item/list/91f4bc00-417a-11e9-9ecf-59e7c9308771.jpg', 'images/item/thumbs/91f4bc00-417a-11e9-9ecf-59e7c9308771.jpg', '2019-03-08 02:17:05', '2019-03-08 02:17:10'),
(254, 10, NULL, 1, 'images/item/original/c5973740-417a-11e9-b89e-913909dd5875.jpg', 'images/item/list/c5973740-417a-11e9-b89e-913909dd5875.jpg', 'images/item/thumbs/c5973740-417a-11e9-b89e-913909dd5875.jpg', '2019-03-08 02:18:34', '2019-03-08 02:18:37'),
(255, 11, NULL, 1, 'images/item/original/9af68a20-417b-11e9-bbcf-69377506ae38.jpg', 'images/item/list/9af68a20-417b-11e9-bbcf-69377506ae38.jpg', 'images/item/thumbs/9af68a20-417b-11e9-bbcf-69377506ae38.jpg', '2019-03-08 02:24:33', '2019-03-08 02:24:35'),
(256, 12, NULL, 1, 'images/item/original/da3ac340-417b-11e9-b309-b9afd4bd0516.jpg', 'images/item/list/da3ac340-417b-11e9-b309-b9afd4bd0516.jpg', 'images/item/thumbs/da3ac340-417b-11e9-b309-b9afd4bd0516.jpg', '2019-03-08 02:26:14', '2019-03-08 02:26:21'),
(257, 12, NULL, 2, 'images/item/original/da649cc0-417b-11e9-ab3a-27530605cd19.jpg', 'images/item/list/da649cc0-417b-11e9-ab3a-27530605cd19.jpg', 'images/item/thumbs/da649cc0-417b-11e9-ab3a-27530605cd19.jpg', '2019-03-08 02:26:14', '2019-03-08 02:26:21'),
(258, 12, NULL, 3, 'images/item/original/da8b45e0-417b-11e9-aecb-61541375d8b5.jpg', 'images/item/list/da8b45e0-417b-11e9-aecb-61541375d8b5.jpg', 'images/item/thumbs/da8b45e0-417b-11e9-aecb-61541375d8b5.jpg', '2019-03-08 02:26:14', '2019-03-08 02:26:21'),
(259, 13, NULL, 1, 'images/item/original/20c4ba60-417c-11e9-a62b-57bd83efa353.jpg', 'images/item/list/20c4ba60-417c-11e9-a62b-57bd83efa353.jpg', 'images/item/thumbs/20c4ba60-417c-11e9-a62b-57bd83efa353.jpg', '2019-03-08 02:28:17', '2019-03-08 02:28:19'),
(260, 13, NULL, 2, 'images/item/original/20eca5d0-417c-11e9-95c3-335e19673a07.jpg', 'images/item/list/20eca5d0-417c-11e9-95c3-335e19673a07.jpg', 'images/item/thumbs/20eca5d0-417c-11e9-95c3-335e19673a07.jpg', '2019-03-08 02:28:17', '2019-03-08 02:28:19'),
(261, 14, NULL, 1, 'images/item/original/5abc9780-417c-11e9-8864-cdd2d03a8ca4.jpg', 'images/item/list/5abc9780-417c-11e9-8864-cdd2d03a8ca4.jpg', 'images/item/thumbs/5abc9780-417c-11e9-8864-cdd2d03a8ca4.jpg', '2019-03-08 02:29:53', '2019-03-08 02:29:56'),
(262, 15, NULL, 1, 'images/item/original/83570f30-417c-11e9-b8e0-1714c60cf573.jpg', 'images/item/list/83570f30-417c-11e9-b8e0-1714c60cf573.jpg', 'images/item/thumbs/83570f30-417c-11e9-b8e0-1714c60cf573.jpg', '2019-03-08 02:31:02', '2019-03-08 02:31:04'),
(263, 16, NULL, 1, 'images/item/original/a0361560-417c-11e9-b3ee-83a84c8810ec.jpg', 'images/item/list/a0361560-417c-11e9-b3ee-83a84c8810ec.jpg', 'images/item/thumbs/a0361560-417c-11e9-b3ee-83a84c8810ec.jpg', '2019-03-08 02:31:51', '2019-03-08 02:31:53'),
(264, 17, NULL, 1, 'images/item/original/860f9170-417d-11e9-8707-77f18b7ab212.jpg', 'images/item/list/860f9170-417d-11e9-8707-77f18b7ab212.jpg', 'images/item/thumbs/860f9170-417d-11e9-8707-77f18b7ab212.jpg', '2019-03-08 02:38:15', '2019-03-08 02:38:19'),
(265, 17, NULL, 3, 'images/item/original/86608ee0-417d-11e9-bd21-1bfbefa58710.jpg', 'images/item/list/86608ee0-417d-11e9-bd21-1bfbefa58710.jpg', 'images/item/thumbs/86608ee0-417d-11e9-bd21-1bfbefa58710.jpg', '2019-03-08 02:38:15', '2019-03-08 02:38:19'),
(266, 17, NULL, 2, 'images/item/original/863ad9a0-417d-11e9-afdf-9bdfac7b10ef.jpg', 'images/item/list/863ad9a0-417d-11e9-afdf-9bdfac7b10ef.jpg', 'images/item/thumbs/863ad9a0-417d-11e9-afdf-9bdfac7b10ef.jpg', '2019-03-08 02:38:15', '2019-03-08 02:38:19'),
(267, 18, NULL, 1, 'images/item/original/26a14020-417e-11e9-ac44-1d266c0ab7a6.jpg', 'images/item/list/26a14020-417e-11e9-ac44-1d266c0ab7a6.jpg', 'images/item/thumbs/26a14020-417e-11e9-ac44-1d266c0ab7a6.jpg', '2019-03-08 02:42:44', '2019-03-08 02:42:48'),
(268, 18, NULL, 2, 'images/item/original/26c8c4e0-417e-11e9-a223-cf7f8eedea35.jpg', 'images/item/list/26c8c4e0-417e-11e9-a223-cf7f8eedea35.jpg', 'images/item/thumbs/26c8c4e0-417e-11e9-a223-cf7f8eedea35.jpg', '2019-03-08 02:42:44', '2019-03-08 02:42:48'),
(269, 19, NULL, 2, 'images/item/original/6291b300-417e-11e9-a6ee-55a9f7978378.jpg', 'images/item/list/6291b300-417e-11e9-a6ee-55a9f7978378.jpg', 'images/item/thumbs/6291b300-417e-11e9-a6ee-55a9f7978378.jpg', '2019-03-08 02:44:22', '2019-03-08 02:44:28'),
(270, 19, NULL, 1, 'images/item/original/62681640-417e-11e9-b883-71d75f05934b.jpg', 'images/item/list/62681640-417e-11e9-b883-71d75f05934b.jpg', 'images/item/thumbs/62681640-417e-11e9-b883-71d75f05934b.jpg', '2019-03-08 02:44:22', '2019-03-08 02:44:28'),
(271, 20, NULL, 1, 'images/item/original/8acd54c0-417e-11e9-a597-2dc560371199.jpg', 'images/item/list/8acd54c0-417e-11e9-a597-2dc560371199.jpg', 'images/item/thumbs/8acd54c0-417e-11e9-a597-2dc560371199.jpg', '2019-03-08 02:45:34', '2019-03-08 02:45:36'),
(132, NULL, NULL, NULL, '/images/item/0fb2bab0-f7e9-11e8-9cb7-d5f0e7723cb0.jpg', NULL, NULL, '2018-12-04 11:21:39', '2018-12-04 11:21:39'),
(145, NULL, NULL, NULL, '/images/item/5231dbb0-feec-11e8-be13-7d00736a7da1.gif', NULL, NULL, '2018-12-13 09:32:37', '2018-12-13 09:32:37'),
(146, NULL, NULL, NULL, '/images/item/90e01570-feec-11e8-b0f9-ef077fd52639.gif', NULL, NULL, '2018-12-13 09:34:23', '2018-12-13 09:34:23'),
(148, NULL, NULL, NULL, '/images/item/f276dd80-ff01-11e8-8ed5-7bf766ee5e67.gif', NULL, NULL, '2018-12-13 12:07:26', '2018-12-13 12:07:26'),
(182, NULL, NULL, NULL, '/images/item/f704e010-3e5d-11e9-a8b4-e1e1e1601343.jpg', NULL, NULL, '2019-03-04 03:14:50', '2019-03-04 03:14:50'),
(185, NULL, NULL, NULL, '/images/item/43d295c0-3e5e-11e9-a950-d5bef0e67bc1.jpg', NULL, NULL, '2019-03-04 03:16:59', '2019-03-04 03:16:59'),
(186, NULL, NULL, NULL, '/images/item/5247a430-3e5e-11e9-a2e0-67aff8765401.jpg', NULL, NULL, '2019-03-04 03:17:23', '2019-03-04 03:17:23'),
(187, NULL, NULL, NULL, '/images/item/2249bfc0-3e66-11e9-bc33-21cef094ffe2.jpg', NULL, NULL, '2019-03-04 04:13:19', '2019-03-04 04:13:19'),
(188, NULL, NULL, NULL, '/images/item/27446ae0-3e66-11e9-901a-e76cc2f5ea42.jpg', NULL, NULL, '2019-03-04 04:13:27', '2019-03-04 04:13:27'),
(184, NULL, NULL, NULL, '/images/item/1ef88020-3e5e-11e9-b70b-99f4d36d435a.jpg', NULL, NULL, '2019-03-04 03:15:57', '2019-03-04 03:15:57'),
(189, NULL, NULL, NULL, '/images/item/27476580-3e66-11e9-88e2-07d6ded67da1.png', NULL, NULL, '2019-03-04 04:13:27', '2019-03-04 04:13:27'),
(190, NULL, NULL, NULL, '/images/item/2c4109d0-3e66-11e9-aaee-51cac120d60e.jpg', NULL, NULL, '2019-03-04 04:13:36', '2019-03-04 04:13:36'),
(191, NULL, NULL, NULL, '/images/item/2c41f1a0-3e66-11e9-ad1b-d109364fbb3b.png', NULL, NULL, '2019-03-04 04:13:36', '2019-03-04 04:13:36'),
(192, NULL, NULL, NULL, '/images/item/cd806260-3e66-11e9-89f2-e9bc25e2bf89.jpg', NULL, NULL, '2019-03-04 04:18:06', '2019-03-04 04:18:06'),
(193, NULL, NULL, NULL, '/images/item/eb63f170-3e66-11e9-83cc-3bf69c8258ce.jpg', NULL, NULL, '2019-03-04 04:18:56', '2019-03-04 04:18:56'),
(194, NULL, NULL, NULL, '/images/item/c086f1d0-3e67-11e9-a361-db43e8694a45.jpg', NULL, NULL, '2019-03-04 04:24:54', '2019-03-04 04:24:54'),
(195, NULL, NULL, NULL, '/images/item/fffc54a0-3e67-11e9-bd31-dbbc7380bbf1.jpg', NULL, NULL, '2019-03-04 04:26:40', '2019-03-04 04:26:40'),
(196, NULL, NULL, NULL, '/images/item/81322f40-3e68-11e9-84fd-bfdfd690a4f0.jpg', NULL, NULL, '2019-03-04 04:30:17', '2019-03-04 04:30:17'),
(197, NULL, NULL, NULL, '/images/item/8b8394a0-3e68-11e9-9803-eda37812e343.jpg', NULL, NULL, '2019-03-04 04:30:34', '2019-03-04 04:30:34'),
(198, NULL, NULL, NULL, '/images/item/9b7ec520-3e68-11e9-be87-81393779472b.png', NULL, NULL, '2019-03-04 04:31:01', '2019-03-04 04:31:01'),
(199, NULL, NULL, NULL, '/images/item/b1f257e0-3e68-11e9-98f8-3deb64e3350f.jpg', NULL, NULL, '2019-03-04 04:31:39', '2019-03-04 04:31:39'),
(200, NULL, NULL, NULL, '/images/item/b1f592e0-3e68-11e9-ae50-079cadd553fa.jpeg', NULL, NULL, '2019-03-04 04:31:39', '2019-03-04 04:31:39'),
(201, NULL, NULL, NULL, '/images/item/b1fff620-3e68-11e9-9e24-716d61ebd416.JPG', NULL, NULL, '2019-03-04 04:31:39', '2019-03-04 04:31:39'),
(202, NULL, NULL, NULL, '/images/item/b2099810-3e68-11e9-acd1-5125c76b1ad1.jpg', NULL, NULL, '2019-03-04 04:31:39', '2019-03-04 04:31:39'),
(203, NULL, NULL, NULL, '/images/item/a8178a20-3e69-11e9-b5d8-3d6ac0097764.png', NULL, NULL, '2019-03-04 04:38:32', '2019-03-04 04:38:32'),
(204, NULL, NULL, NULL, '/images/item/a8178a80-3e69-11e9-8626-bf5961ce0479.jpg', NULL, NULL, '2019-03-04 04:38:32', '2019-03-04 04:38:32'),
(205, NULL, NULL, NULL, '/images/item/a8187c60-3e69-11e9-acf9-711f049acf2e.jpg', NULL, NULL, '2019-03-04 04:38:32', '2019-03-04 04:38:32'),
(206, NULL, NULL, NULL, '/images/item/a8274ee0-3e69-11e9-a6ab-49a21e2a9107.jpg', NULL, NULL, '2019-03-04 04:38:32', '2019-03-04 04:38:32'),
(207, NULL, NULL, NULL, '/images/item/a82b2a10-3e69-11e9-b934-b7598f103182.jpeg', NULL, NULL, '2019-03-04 04:38:32', '2019-03-04 04:38:32'),
(208, NULL, NULL, NULL, '/images/item/a82b9da0-3e69-11e9-88d0-378a64107841.JPG', NULL, NULL, '2019-03-04 04:38:32', '2019-03-04 04:38:32'),
(209, NULL, NULL, NULL, '/images/item/d51e6fe0-3e69-11e9-a9e9-712732046da3.png', NULL, NULL, '2019-03-04 04:39:47', '2019-03-04 04:39:47'),
(210, NULL, NULL, NULL, '/images/item/d51f4ce0-3e69-11e9-9278-ad12e309d4af.jpeg', NULL, NULL, '2019-03-04 04:39:47', '2019-03-04 04:39:47'),
(211, NULL, NULL, NULL, '/images/item/d5277a40-3e69-11e9-8d94-0bc4bcfbf69a.jpg', NULL, NULL, '2019-03-04 04:39:48', '2019-03-04 04:39:48'),
(212, NULL, NULL, NULL, '/images/item/d52896f0-3e69-11e9-9088-33a487aabaf2.jpg', NULL, NULL, '2019-03-04 04:39:48', '2019-03-04 04:39:48'),
(213, NULL, NULL, NULL, '/images/item/d52b27e0-3e69-11e9-9bd6-e58597584a91.jpg', NULL, NULL, '2019-03-04 04:39:48', '2019-03-04 04:39:48'),
(214, NULL, NULL, NULL, '/images/item/d534c340-3e69-11e9-99ea-456a1c339a23.JPG', NULL, NULL, '2019-03-04 04:39:48', '2019-03-04 04:39:48'),
(221, NULL, NULL, NULL, '/images/item/73670ae0-3e6b-11e9-88af-5b487f9ae796.jpg', NULL, NULL, '2019-03-04 04:51:23', '2019-03-04 04:51:23'),
(222, NULL, NULL, NULL, '/images/item/2ec435b0-3e6c-11e9-bd13-0991756ce10f.jpg', NULL, NULL, '2019-03-04 04:56:37', '2019-03-04 04:56:37'),
(223, NULL, NULL, NULL, '/images/item/65bb0d20-3e6c-11e9-871f-e5dea38f8f79.png', NULL, NULL, '2019-03-04 04:58:09', '2019-03-04 04:58:09'),
(224, NULL, NULL, NULL, '/images/item/6c84c490-3e6c-11e9-87e3-db34863d207a.png', NULL, NULL, '2019-03-04 04:58:20', '2019-03-04 04:58:20'),
(244, 6, NULL, 1, 'images/item/original/bf71ac30-4170-11e9-aaa3-a3e9d9d2e687.jpg', 'images/item/list/bf71ac30-4170-11e9-aaa3-a3e9d9d2e687.jpg', 'images/item/thumbs/bf71ac30-4170-11e9-aaa3-a3e9d9d2e687.jpg', '2019-03-08 01:06:52', '2019-03-08 01:06:52'),
(245, 6, NULL, 2, 'images/item/original/bfc28d30-4170-11e9-a520-198158acc0b0.jpg', 'images/item/list/bfc28d30-4170-11e9-a520-198158acc0b0.jpg', 'images/item/thumbs/bfc28d30-4170-11e9-a520-198158acc0b0.jpg', '2019-03-08 01:06:52', '2019-03-08 01:06:52'),
(239, 1, NULL, 2, 'images/item/original/f299ab20-416e-11e9-80c0-8386b83dd4b4.jpg', 'images/item/list/f299ab20-416e-11e9-80c0-8386b83dd4b4.jpg', 'images/item/thumbs/f299ab20-416e-11e9-80c0-8386b83dd4b4.jpg', '2019-03-08 00:53:51', '2019-03-08 00:53:58'),
(240, 2, NULL, 1, 'images/item/original/ac329580-416f-11e9-be02-530b291d59b0.jpg', 'images/item/list/ac329580-416f-11e9-be02-530b291d59b0.jpg', 'images/item/thumbs/ac329580-416f-11e9-be02-530b291d59b0.jpg', '2019-03-08 00:59:10', '2019-03-08 00:59:10'),
(238, 1, NULL, 1, 'images/item/original/f242ce20-416e-11e9-b91f-057979a296e5.jpg', 'images/item/list/f242ce20-416e-11e9-b91f-057979a296e5.jpg', 'images/item/thumbs/f242ce20-416e-11e9-b91f-057979a296e5.jpg', '2019-03-08 00:53:48', '2019-03-08 00:53:58'),
(241, 2, NULL, 2, 'images/item/original/ac8206b0-416f-11e9-925f-3f5a355daf16.jpg', 'images/item/list/ac8206b0-416f-11e9-925f-3f5a355daf16.jpg', 'images/item/thumbs/ac8206b0-416f-11e9-925f-3f5a355daf16.jpg', '2019-03-08 00:59:10', '2019-03-08 00:59:10'),
(234, 39, NULL, 1, 'images/item/original/9871e120-416d-11e9-84c2-db650008f903.jpg', 'images/item/list/9871e120-416d-11e9-84c2-db650008f903.jpg', 'images/item/thumbs/9871e120-416d-11e9-84c2-db650008f903.jpg', '2019-03-08 00:43:03', '2019-03-08 00:44:18'),
(235, 39, NULL, 2, 'images/item/original/993a64e0-416d-11e9-83db-2de9a075dd08.jpg', 'images/item/list/993a64e0-416d-11e9-83db-2de9a075dd08.jpg', 'images/item/thumbs/993a64e0-416d-11e9-83db-2de9a075dd08.jpg', '2019-03-08 00:43:06', '2019-03-08 00:44:19');

-- --------------------------------------------------------

--
-- Table structure for table `item_views`
--

CREATE TABLE `item_views` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lengths`
--

CREATE TABLE `lengths` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lengths`
--

INSERT INTO `lengths` (`id`, `name`, `sub_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'sdf2', 8, '2018-05-17 07:02:23', '2018-05-17 07:02:52', '2018-05-17 07:02:52'),
(2, '1/2 Sleeve', 10, '2018-05-17 07:02:50', '2018-05-22 14:23:02', NULL),
(3, '3/4 Sleeve', 10, '2018-05-22 14:23:10', '2018-05-22 14:23:10', NULL),
(4, 'Asymmetrical', 10, '2018-05-22 14:23:18', '2018-05-22 14:23:18', NULL),
(5, 'Bell Sleeves', 10, '2018-05-22 14:23:24', '2018-05-22 14:23:24', NULL),
(6, 'Bubble Sleeve', 10, '2018-05-22 14:23:32', '2018-05-22 14:23:32', NULL),
(7, 'Cap Sleeve', 10, '2018-05-22 14:23:42', '2018-05-22 14:23:42', NULL),
(8, 'Dolman Sleeve', 10, '2018-05-22 14:23:49', '2018-05-22 14:23:49', NULL),
(9, 'Flyaway Sleeve', 10, '2018-05-22 14:23:57', '2018-05-22 14:23:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_history`
--

CREATE TABLE `login_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `login_history`
--

INSERT INTO `login_history` (`id`, `user_id`, `ip`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2018-07-26 02:37:44', '2018-07-26 02:37:44'),
(2, 14, '::1', '2018-07-26 07:58:34', '2018-07-26 07:58:34'),
(3, 1, '::1', '2018-07-26 08:07:24', '2018-07-26 08:07:24'),
(4, 1, '::1', '2018-07-26 08:07:33', '2018-07-26 08:07:33'),
(5, 1, '::1', '2018-07-26 08:37:44', '2018-07-26 08:37:44'),
(6, 14, '::1', '2018-07-26 12:13:40', '2018-07-26 12:13:40'),
(7, 14, '::1', '2018-07-26 12:28:12', '2018-07-26 12:28:12'),
(8, 14, '::1', '2018-07-26 12:38:18', '2018-07-26 12:38:18'),
(9, 14, '::1', '2018-07-26 12:40:29', '2018-07-26 12:40:29'),
(10, 14, '::1', '2018-07-27 02:23:18', '2018-07-27 02:23:18'),
(11, 14, '::1', '2018-07-27 09:19:07', '2018-07-27 09:19:07'),
(12, 14, '::1', '2018-07-27 09:24:21', '2018-07-27 09:24:21'),
(13, 14, '::1', '2018-07-27 09:41:17', '2018-07-27 09:41:17'),
(14, 14, '::1', '2018-07-27 09:56:03', '2018-07-27 09:56:03'),
(15, 14, '::1', '2018-07-27 10:14:53', '2018-07-27 10:14:53'),
(16, 1, '::1', '2018-07-27 13:18:56', '2018-07-27 13:18:56'),
(17, 14, '::1', '2018-07-28 05:38:13', '2018-07-28 05:38:13'),
(18, 14, '::1', '2018-07-28 11:14:46', '2018-07-28 11:14:46'),
(19, 1, '::1', '2018-07-30 08:55:10', '2018-07-30 08:55:10'),
(20, 14, '::1', '2018-07-30 10:59:43', '2018-07-30 10:59:43'),
(21, 14, '::1', '2018-07-30 11:01:25', '2018-07-30 11:01:25'),
(22, 14, '::1', '2018-07-31 05:23:43', '2018-07-31 05:23:43'),
(23, 1, '::1', '2018-07-31 05:37:01', '2018-07-31 05:37:01'),
(24, 1, '::1', '2018-07-31 05:37:46', '2018-07-31 05:37:46'),
(25, 1, '::1', '2018-07-31 05:57:04', '2018-07-31 05:57:04'),
(26, 14, '::1', '2018-07-31 11:15:15', '2018-07-31 11:15:15'),
(27, 1, '::1', '2018-07-31 11:16:13', '2018-07-31 11:16:13'),
(28, 1, '::1', '2018-07-31 11:16:37', '2018-07-31 11:16:37'),
(29, 14, '::1', '2018-07-31 11:25:01', '2018-07-31 11:25:01'),
(30, 1, '::1', '2018-07-31 11:26:41', '2018-07-31 11:26:41'),
(31, 1, '::1', '2018-08-01 04:23:12', '2018-08-01 04:23:12'),
(32, 1, '::1', '2018-08-01 10:03:52', '2018-08-01 10:03:52'),
(33, 14, '::1', '2018-08-01 12:18:30', '2018-08-01 12:18:30'),
(34, 1, '::1', '2018-08-01 12:23:34', '2018-08-01 12:23:34'),
(35, 1, '::1', '2018-08-01 12:42:38', '2018-08-01 12:42:38'),
(36, 1, '::1', '2018-08-01 12:47:07', '2018-08-01 12:47:07'),
(37, 14, '::1', '2018-08-02 03:25:52', '2018-08-02 03:25:52'),
(38, 1, '::1', '2018-08-03 02:24:00', '2018-08-03 02:24:00'),
(39, 1, '::1', '2018-08-03 10:02:12', '2018-08-03 10:02:12'),
(40, 1, '::1', '2018-08-03 10:44:19', '2018-08-03 10:44:19'),
(41, 1, '::1', '2018-08-04 05:49:09', '2018-08-04 05:49:09'),
(42, 1, '::1', '2018-08-04 07:49:38', '2018-08-04 07:49:38'),
(43, 1, '::1', '2018-08-07 05:19:33', '2018-08-07 05:19:33'),
(44, 1, '::1', '2018-08-09 05:21:15', '2018-08-09 05:21:15'),
(45, 1, '::1', '2018-08-09 05:43:23', '2018-08-09 05:43:23'),
(46, 14, '::1', '2018-08-09 10:14:47', '2018-08-09 10:14:47'),
(47, 1, '::1', '2018-08-10 05:13:09', '2018-08-10 05:13:09'),
(48, 1, '::1', '2018-08-13 08:40:49', '2018-08-13 08:40:49'),
(49, 14, '::1', '2018-08-13 09:10:18', '2018-08-13 09:10:18'),
(50, 1, '::1', '2018-08-13 10:49:09', '2018-08-13 10:49:09'),
(51, 14, '::1', '2018-08-13 11:04:59', '2018-08-13 11:04:59'),
(52, 1, '::1', '2018-08-14 02:16:41', '2018-08-14 02:16:41'),
(53, 1, '::1', '2018-08-14 02:32:27', '2018-08-14 02:32:27'),
(54, 14, '::1', '2018-08-16 02:19:37', '2018-08-16 02:19:37'),
(55, 14, '::1', '2018-08-16 05:59:29', '2018-08-16 05:59:29'),
(56, 1, '::1', '2018-08-16 06:45:57', '2018-08-16 06:45:57'),
(57, 1, '::1', '2018-08-16 06:47:48', '2018-08-16 06:47:48'),
(58, 14, '::1', '2018-08-16 11:20:15', '2018-08-16 11:20:15'),
(59, 1, '::1', '2018-08-17 05:08:37', '2018-08-17 05:08:37'),
(60, 14, '::1', '2018-08-17 05:21:14', '2018-08-17 05:21:14'),
(61, 1, '::1', '2018-08-17 05:28:17', '2018-08-17 05:28:17'),
(62, 14, '::1', '2018-08-17 09:46:34', '2018-08-17 09:46:34'),
(63, 1, '::1', '2018-08-17 11:05:37', '2018-08-17 11:05:37'),
(64, 14, '::1', '2018-08-17 11:09:30', '2018-08-17 11:09:30'),
(65, 14, '::1', '2018-08-18 03:34:49', '2018-08-18 03:34:49'),
(66, 1, '::1', '2018-08-18 03:38:20', '2018-08-18 03:38:20'),
(67, 14, '::1', '2018-08-18 04:07:52', '2018-08-18 04:07:52'),
(68, 1, '::1', '2018-09-06 04:23:00', '2018-09-06 04:23:00'),
(69, 1, '::1', '2018-09-06 07:26:46', '2018-09-06 07:26:46'),
(70, 1, '::1', '2018-09-11 08:57:45', '2018-09-11 08:57:45'),
(71, 1, '::1', '2018-09-11 09:42:08', '2018-09-11 09:42:08'),
(72, 1, '::1', '2018-09-16 05:37:28', '2018-09-16 05:37:28'),
(73, 14, '::1', '2018-09-17 07:07:23', '2018-09-17 07:07:23'),
(74, 1, '::1', '2018-09-17 07:23:35', '2018-09-17 07:23:35'),
(75, 1, '::1', '2018-09-17 07:55:00', '2018-09-17 07:55:00'),
(76, 14, '::1', '2018-09-17 08:01:06', '2018-09-17 08:01:06'),
(77, 1, '::1', '2018-09-17 08:01:40', '2018-09-17 08:01:40'),
(78, 1, '::1', '2018-09-18 02:06:31', '2018-09-18 02:06:31'),
(79, 14, '::1', '2018-09-18 08:17:38', '2018-09-18 08:17:38'),
(80, 1, '::1', '2018-09-18 10:22:01', '2018-09-18 10:22:01'),
(81, 1, '::1', '2018-09-19 05:59:59', '2018-09-19 05:59:59'),
(82, 14, '::1', '2018-09-19 09:26:44', '2018-09-19 09:26:44'),
(83, 1, '::1', '2018-09-20 09:18:21', '2018-09-20 09:18:21'),
(84, 1, '::1', '2018-09-20 13:07:47', '2018-09-20 13:07:47'),
(85, 1, '::1', '2018-09-24 02:04:27', '2018-09-24 02:04:27'),
(86, 14, '::1', '2018-09-24 02:11:57', '2018-09-24 02:11:57'),
(87, 14, '::1', '2018-09-24 04:58:25', '2018-09-24 04:58:25'),
(88, 1, '::1', '2018-09-24 06:00:01', '2018-09-24 06:00:01'),
(89, 1, '::1', '2018-09-24 06:48:22', '2018-09-24 06:48:22'),
(90, 14, '::1', '2018-09-24 08:48:28', '2018-09-24 08:48:28'),
(91, 1, '::1', '2018-09-24 08:48:44', '2018-09-24 08:48:44'),
(92, 1, '::1', '2018-09-25 02:02:41', '2018-09-25 02:02:41'),
(93, 1, '::1', '2018-09-25 04:19:27', '2018-09-25 04:19:27'),
(94, 14, '::1', '2018-09-25 04:31:25', '2018-09-25 04:31:25'),
(95, 1, '::1', '2018-09-25 07:50:54', '2018-09-25 07:50:54'),
(96, 14, '::1', '2018-09-25 08:03:38', '2018-09-25 08:03:38'),
(97, 1, '::1', '2018-09-25 08:05:39', '2018-09-25 08:05:39'),
(98, 14, '::1', '2018-09-25 08:20:33', '2018-09-25 08:20:33'),
(99, 1, '::1', '2018-09-25 08:33:40', '2018-09-25 08:33:40'),
(100, 14, '::1', '2018-09-25 09:08:31', '2018-09-25 09:08:31'),
(101, 14, '::1', '2018-09-25 09:33:15', '2018-09-25 09:33:15'),
(102, 14, '::1', '2018-09-25 09:48:48', '2018-09-25 09:48:48'),
(103, 14, '::1', '2018-09-25 10:49:10', '2018-09-25 10:49:10'),
(104, 1, '::1', '2018-09-26 05:32:25', '2018-09-26 05:32:25'),
(105, 14, '::1', '2018-09-26 07:13:45', '2018-09-26 07:13:45'),
(106, 1, '::1', '2018-09-26 07:34:48', '2018-09-26 07:34:48'),
(107, 14, '::1', '2018-09-28 04:12:45', '2018-09-28 04:12:45'),
(108, 1, '::1', '2018-09-28 04:36:51', '2018-09-28 04:36:51'),
(109, 1, '::1', '2018-10-03 02:10:55', '2018-10-03 02:10:55'),
(110, 1, '::1', '2018-10-09 03:27:38', '2018-10-09 03:27:38'),
(111, 1, '::1', '2018-10-09 03:45:01', '2018-10-09 03:45:01'),
(112, 1, '::1', '2018-10-09 08:51:21', '2018-10-09 08:51:21'),
(113, 1, '::1', '2018-10-09 08:51:42', '2018-10-09 08:51:42'),
(114, 1, '::1', '2018-10-10 02:41:17', '2018-10-10 02:41:17'),
(115, 14, '::1', '2018-10-11 02:27:00', '2018-10-11 02:27:00'),
(116, 1, '::1', '2018-10-11 07:19:41', '2018-10-11 07:19:41'),
(117, 1, '::1', '2018-10-30 08:24:36', '2018-10-30 08:24:36'),
(118, 1, '::1', '2018-11-21 04:31:23', '2018-11-21 04:31:23'),
(119, 1, '::1', '2018-11-21 07:20:43', '2018-11-21 07:20:43'),
(120, 14, '::1', '2018-11-21 09:00:44', '2018-11-21 09:00:44'),
(121, 1, '::1', '2018-11-21 09:09:41', '2018-11-21 09:09:41'),
(122, 14, '::1', '2018-11-21 09:27:39', '2018-11-21 09:27:39'),
(123, 1, '::1', '2018-11-24 10:59:04', '2018-11-24 10:59:04'),
(124, 14, '::1', '2018-11-30 08:09:58', '2018-11-30 08:09:58'),
(125, 14, '::1', '2018-11-30 08:17:38', '2018-11-30 08:17:38'),
(126, 14, '::1', '2018-11-30 12:34:32', '2018-11-30 12:34:32'),
(127, 14, '::1', '2018-12-01 02:05:51', '2018-12-01 02:05:51'),
(128, 14, '::1', '2018-12-01 04:26:38', '2018-12-01 04:26:38'),
(129, 14, '::1', '2018-12-01 04:44:08', '2018-12-01 04:44:08'),
(130, 1, '::1', '2018-12-01 07:55:54', '2018-12-01 07:55:54'),
(131, 14, '::1', '2018-12-01 08:03:05', '2018-12-01 08:03:05'),
(132, 1, '::1', '2018-12-01 09:14:20', '2018-12-01 09:14:20'),
(133, 1, '::1', '2018-12-01 10:34:29', '2018-12-01 10:34:29'),
(134, 14, '::1', '2018-12-03 03:24:11', '2018-12-03 03:24:11'),
(135, 1, '::1', '2018-12-03 07:11:35', '2018-12-03 07:11:35'),
(136, 14, '::1', '2018-12-03 07:34:14', '2018-12-03 07:34:14'),
(137, 1, '::1', '2018-12-03 07:54:24', '2018-12-03 07:54:24'),
(138, 1, '::1', '2018-12-04 04:19:52', '2018-12-04 04:19:52'),
(139, 1, '::1', '2018-12-04 07:30:39', '2018-12-04 07:30:39'),
(140, 1, '::1', '2018-12-04 08:49:28', '2018-12-04 08:49:28'),
(141, 1, '::1', '2018-12-04 11:00:20', '2018-12-04 11:00:20'),
(142, 1, '::1', '2018-12-05 07:11:04', '2018-12-05 07:11:04'),
(143, 1, '::1', '2018-12-05 08:59:44', '2018-12-05 08:59:44'),
(144, 14, '::1', '2018-12-05 10:32:11', '2018-12-05 10:32:11'),
(145, 1, '::1', '2018-12-13 02:18:36', '2018-12-13 02:18:36'),
(146, 1, '::1', '2018-12-13 09:23:51', '2018-12-13 09:23:51'),
(147, 1, '::1', '2018-12-13 09:37:20', '2018-12-13 09:37:20'),
(148, 1, '::1', '2018-12-13 12:02:29', '2018-12-13 12:02:29'),
(149, 1, '::1', '2018-12-17 05:00:16', '2018-12-17 05:00:16'),
(150, 1, '::1', '2018-12-17 05:16:45', '2018-12-17 05:16:45'),
(151, 1, '::1', '2018-12-17 05:25:30', '2018-12-17 05:25:30'),
(152, 1, '::1', '2018-12-17 09:44:13', '2018-12-17 09:44:13'),
(153, 1, '::1', '2018-12-17 22:07:27', '2018-12-17 22:07:27'),
(154, 1, '::1', '2018-12-17 22:13:32', '2018-12-17 22:13:32'),
(155, 14, '::1', '2018-12-18 00:05:02', '2018-12-18 00:05:02'),
(156, 1, '::1', '2018-12-18 00:06:15', '2018-12-18 00:06:15'),
(157, 1, '::1', '2018-12-18 03:37:05', '2018-12-18 03:37:05'),
(158, 1, '::1', '2018-12-19 03:07:00', '2018-12-19 03:07:00'),
(159, 1, '::1', '2018-12-19 04:27:21', '2018-12-19 04:27:21'),
(160, 1, '::1', '2018-12-19 04:55:36', '2018-12-19 04:55:36'),
(161, 1, '::1', '2018-12-19 05:51:57', '2018-12-19 05:51:57'),
(162, 1, '::1', '2018-12-19 06:44:36', '2018-12-19 06:44:36'),
(163, 1, '::1', '2018-12-19 07:54:36', '2018-12-19 07:54:36'),
(164, 1, '::1', '2018-12-19 09:56:03', '2018-12-19 09:56:03'),
(165, 1, '::1', '2018-12-20 06:52:10', '2018-12-20 06:52:10'),
(166, 1, '::1', '2018-12-27 02:15:45', '2018-12-27 02:15:45'),
(167, 1, '::1', '2018-12-27 05:24:14', '2018-12-27 05:24:14'),
(168, 1, '::1', '2018-12-27 06:27:12', '2018-12-27 06:27:12'),
(169, 1, '::1', '2018-12-28 02:35:30', '2018-12-28 02:35:30'),
(170, 1, '::1', '2018-12-29 02:03:33', '2018-12-29 02:03:33'),
(171, 1, '::1', '2019-01-01 04:01:29', '2019-01-01 04:01:29'),
(172, 1, '::1', '2019-01-01 10:17:40', '2019-01-01 10:17:40'),
(173, 1, '::1', '2019-01-02 02:07:14', '2019-01-02 02:07:14'),
(174, 1, '::1', '2019-01-02 02:41:57', '2019-01-02 02:41:57'),
(175, 14, '::1', '2019-01-02 10:17:16', '2019-01-02 10:17:16'),
(176, 14, '::1', '2019-01-02 11:01:22', '2019-01-02 11:01:22'),
(177, 1, '::1', '2019-01-03 08:42:26', '2019-01-03 08:42:26'),
(178, 14, '::1', '2019-01-04 02:04:27', '2019-01-04 02:04:27'),
(179, 1, '::1', '2019-01-04 02:18:32', '2019-01-04 02:18:32'),
(180, 1, '::1', '2019-01-04 03:17:22', '2019-01-04 03:17:22'),
(181, 1, '::1', '2019-01-05 02:04:57', '2019-01-05 02:04:57'),
(182, 14, '::1', '2019-01-05 02:06:57', '2019-01-05 02:06:57'),
(183, 1, '::1', '2019-01-05 03:09:03', '2019-01-05 03:09:03'),
(184, 14, '::1', '2019-01-05 05:24:56', '2019-01-05 05:24:56'),
(185, 14, '::1', '2019-01-05 05:34:54', '2019-01-05 05:34:54'),
(186, 14, '::1', '2019-01-05 05:43:57', '2019-01-05 05:43:57'),
(187, 1, '::1', '2019-01-07 02:02:22', '2019-01-07 02:02:22'),
(188, 1, '::1', '2019-01-08 03:31:33', '2019-01-08 03:31:33'),
(189, 1, '::1', '2019-01-08 07:02:01', '2019-01-08 07:02:01'),
(190, 1, '::1', '2019-01-08 07:29:23', '2019-01-08 07:29:23'),
(191, 1, '::1', '2019-01-10 10:25:32', '2019-01-10 10:25:32'),
(192, 14, '::1', '2019-01-10 10:53:58', '2019-01-10 10:53:58'),
(193, 1, '::1', '2019-01-30 03:22:35', '2019-01-30 03:22:35'),
(194, 1, '::1', '2019-03-04 02:57:58', '2019-03-04 02:57:58'),
(195, 14, '::1', '2019-03-04 03:40:38', '2019-03-04 03:40:38'),
(196, 1, '::1', '2019-03-04 03:45:07', '2019-03-04 03:45:07'),
(197, 1, '::1', '2019-03-05 00:30:51', '2019-03-05 00:30:51'),
(198, 1, '::1', '2019-03-05 03:04:35', '2019-03-05 03:04:35'),
(199, 19, '::1', '2019-03-05 03:19:45', '2019-03-05 03:19:45'),
(200, 19, '::1', '2019-03-05 03:23:16', '2019-03-05 03:23:16'),
(201, 26, '::1', '2019-03-05 04:55:42', '2019-03-05 04:55:42'),
(202, 1, '::1', '2019-03-05 05:23:12', '2019-03-05 05:23:12'),
(203, 1, '::1', '2019-03-05 05:29:42', '2019-03-05 05:29:42'),
(204, 26, '::1', '2019-03-05 07:41:09', '2019-03-05 07:41:09'),
(205, 1, '::1', '2019-03-05 23:05:10', '2019-03-05 23:05:10'),
(206, 26, '::1', '2019-03-06 08:58:43', '2019-03-06 08:58:43'),
(207, 26, '::1', '2019-03-06 09:14:54', '2019-03-06 09:14:54'),
(208, 26, '::1', '2019-03-06 09:15:19', '2019-03-06 09:15:19'),
(209, 26, '::1', '2019-03-06 22:21:07', '2019-03-06 22:21:07'),
(210, 1, '::1', '2019-03-07 07:08:22', '2019-03-07 07:08:22'),
(211, 26, '::1', '2019-03-07 07:22:54', '2019-03-07 07:22:54'),
(212, 1, '::1', '2019-03-07 07:29:54', '2019-03-07 07:29:54'),
(213, 1, '::1', '2019-03-07 07:30:07', '2019-03-07 07:30:07'),
(214, 1, '::1', '2019-03-07 23:05:06', '2019-03-07 23:05:06'),
(215, 1, '::1', '2019-03-07 23:45:57', '2019-03-07 23:45:57'),
(216, 1, '::1', '2019-03-08 00:03:30', '2019-03-08 00:03:30'),
(217, 1, '::1', '2019-03-08 00:12:29', '2019-03-08 00:12:29'),
(218, 26, '::1', '2019-03-08 03:01:04', '2019-03-08 03:01:04'),
(219, 1, '::1', '2019-03-08 03:29:39', '2019-03-08 03:29:39'),
(220, 26, '::1', '2019-03-08 03:38:17', '2019-03-08 03:38:17');

-- --------------------------------------------------------

--
-- Table structure for table `made_in_countries`
--

CREATE TABLE `made_in_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `made_in_countries`
--

INSERT INTO `made_in_countries` (`id`, `name`, `status`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'IMPORT', 1, 1, '2018-05-12 05:03:14', '2018-06-30 09:57:51', NULL),
(11, 'ee', 1, 0, '2018-05-12 06:08:15', '2018-05-12 06:28:46', '2018-05-12 06:28:46'),
(12, 'd', 0, 0, '2018-05-12 06:20:36', '2018-05-22 14:43:53', '2018-05-22 14:43:53'),
(13, '2', 0, 0, '2018-05-12 06:43:58', '2018-05-12 06:44:11', '2018-05-12 06:44:11'),
(8, '4', 1, 0, '2018-05-12 05:42:22', '2018-05-22 14:43:49', '2018-05-22 14:43:49'),
(10, 'aa', 1, 0, '2018-05-12 05:46:40', '2018-05-22 14:43:51', '2018-05-22 14:43:51'),
(14, 'dfd', 1, 0, '2018-05-12 09:27:19', '2018-05-22 14:43:54', '2018-05-22 14:43:54'),
(15, 'dda3', 0, 0, '2018-05-17 01:30:11', '2018-05-17 01:30:28', '2018-05-17 01:30:28'),
(16, 'USA', 1, 0, '2018-05-22 14:44:10', '2018-06-30 09:57:51', NULL),
(17, 'IMPORT', 1, 0, '2018-05-23 15:22:38', '2018-06-30 09:57:51', NULL),
(18, 'USA', 1, 0, '2018-05-23 15:22:47', '2018-06-30 09:57:51', NULL),
(19, 'd', 1, 0, '2018-06-25 08:01:29', '2018-06-25 08:01:33', '2018-06-25 08:01:33'),
(20, 'erer', 0, 0, '2018-06-30 09:57:44', '2018-06-30 09:57:56', '2018-06-30 09:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `master_colors`
--

CREATE TABLE `master_colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_colors`
--

INSERT INTO `master_colors` (`id`, `name`, `image_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 'Black', '/images/master_color/4e77ad10-5df2-11e8-9866-35f0a9aad927.jpg', '2018-05-22 12:59:51', '2018-05-22 12:59:51', NULL),
(7, 'Red', '/images/master_color/3586a0b0-5df2-11e8-8423-fbb0e7848eaf.jpg', '2018-05-22 12:59:09', '2018-05-22 12:59:09', NULL),
(9, 'Blue', '/images/master_color/5e75f940-5df2-11e8-8b31-53e1d9dda377.jpg', '2018-05-22 13:00:18', '2018-05-22 13:00:18', NULL),
(10, 'Brown', '/images/master_color/791e7f20-5df2-11e8-b6ea-f9806acbee22.jpg', '2018-05-22 13:01:03', '2018-05-22 13:01:03', NULL),
(11, 'Gold', '/images/master_color/871c7b10-5df2-11e8-bd06-5d3d96e22977.jpg', '2018-05-22 13:01:26', '2018-05-22 13:01:26', NULL),
(12, 'Gray', '/images/master_color/97251680-5df2-11e8-80d0-0b45ed04fbf9.jpg', '2018-05-22 13:01:53', '2018-05-22 13:01:53', NULL),
(13, 'Green', '/images/master_color/ad0764b0-5df2-11e8-bf69-49778d6fff9a.jpg', '2018-05-22 13:02:30', '2018-05-22 13:02:30', NULL),
(14, 'Ivory', '/images/master_color/c1f2e920-5df2-11e8-8da5-5b7f0944a348.jpg', '2018-05-22 13:03:05', '2018-05-22 13:03:05', NULL),
(15, 'Multi', '/images/master_color/d3727d00-5df2-11e8-8efd-e3a8ede6be2e.jpg', '2018-05-22 13:03:34', '2018-05-22 13:03:34', NULL),
(16, 'Orange', '/images/master_color/e5b44320-5df2-11e8-8331-275f5efbdb75.jpg', '2018-05-22 13:04:05', '2018-05-22 13:04:05', NULL),
(17, 'Pink', '/images/master_color/f9303bb0-5df2-11e8-a50f-230282c65397.jpg', '2018-05-22 13:04:37', '2018-05-22 13:04:37', NULL),
(18, 'Purple', '/images/master_color/12f03b80-5df3-11e8-a134-07210ad6c7cb.jpg', '2018-05-22 13:05:21', '2018-05-22 13:05:21', NULL),
(19, 'Silver', '/images/master_color/283ad410-5df3-11e8-878c-15abac44d755.jpg', '2018-05-22 13:05:56', '2018-05-22 13:05:56', NULL),
(20, 'Beige', '/images/master_color/3bbf2f50-5df3-11e8-861a-ff67af98860e.jpg', '2018-05-22 13:06:29', '2018-05-22 13:06:29', NULL),
(21, 'White', '/images/master_color/4b694870-5df3-11e8-a72e-7702ab347774.jpg', '2018-05-22 13:06:55', '2018-05-22 13:06:55', NULL),
(22, 'Yellow', '/images/master_color/5b5a3520-5df3-11e8-a6bc-bd02725fcab8.jpg', '2018-05-22 13:07:22', '2018-06-29 09:43:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_fabrics`
--

CREATE TABLE `master_fabrics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_fabrics`
--

INSERT INTO `master_fabrics` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Canvas', '2018-05-12 08:04:56', '2018-05-22 13:10:03', NULL),
(2, 'Chiffon', '2018-05-12 08:05:13', '2018-05-22 13:10:12', NULL),
(3, 'Acid Wash', '2018-05-12 08:08:01', '2018-05-22 13:09:48', NULL),
(4, 'Corduroy', '2018-05-22 13:10:23', '2018-05-22 13:10:23', NULL),
(5, 'Cotton', '2018-05-22 13:10:32', '2018-05-22 13:10:32', NULL),
(6, 'Crystal', '2018-05-22 13:10:37', '2018-05-22 13:10:37', NULL),
(7, 'Dark Wash', '2018-05-22 13:10:49', '2018-05-22 13:10:49', NULL),
(8, 'Denim', '2018-05-22 13:11:21', '2018-05-22 13:11:21', NULL),
(9, 'Faux Leather', '2018-05-22 13:11:27', '2018-05-22 13:11:27', NULL),
(10, 'Faux Pearl', '2018-05-22 13:11:32', '2018-05-22 13:11:32', NULL),
(11, 'Genuine Stones', '2018-05-22 13:11:38', '2018-05-22 13:11:38', NULL),
(12, 'Jersey', '2018-05-22 13:11:43', '2018-05-22 13:11:43', NULL),
(13, 'Lace', '2018-05-22 13:11:49', '2018-05-22 13:11:49', NULL),
(14, 'Leather', '2018-05-22 13:12:06', '2018-05-22 13:12:06', NULL),
(15, 'Leather/Pleather', '2018-05-22 13:12:11', '2018-05-22 13:12:11', NULL),
(16, 'Light Wash', '2018-05-22 13:12:18', '2018-05-22 13:12:18', NULL),
(17, 'Linen', '2018-05-22 13:12:23', '2018-05-22 13:12:23', NULL),
(18, 'Mesh', '2018-05-22 13:12:28', '2018-05-22 13:12:28', NULL),
(19, 'Metal', '2018-05-22 13:12:34', '2018-05-22 13:12:34', NULL),
(20, 'Patent Leather', '2018-05-22 13:12:38', '2018-05-22 13:12:38', NULL),
(21, 'Pearl', '2018-05-22 13:12:44', '2018-05-22 13:12:44', NULL),
(22, 'Plastic', '2018-05-22 13:12:51', '2018-05-22 13:12:51', NULL),
(23, 'Pleather', '2018-05-22 13:12:55', '2018-05-22 13:12:55', NULL),
(24, 'Polyester', '2018-05-22 13:12:59', '2018-05-22 13:12:59', NULL),
(25, 'Rhinestones', '2018-05-22 13:13:04', '2018-05-22 13:13:04', NULL),
(26, 'Satin', '2018-05-22 13:13:08', '2018-05-22 13:13:08', NULL),
(27, 'Silk', '2018-05-22 13:13:15', '2018-05-22 13:13:15', NULL),
(28, 'Spandex', '2018-05-22 13:13:23', '2018-05-22 13:13:23', NULL),
(29, 'Stones', '2018-05-22 13:13:28', '2018-05-22 13:13:28', NULL),
(30, 'Straw', '2018-05-22 13:13:33', '2018-05-22 13:13:33', NULL),
(31, 'Suede', '2018-05-22 13:13:37', '2018-05-22 13:13:37', NULL),
(32, 'Taffeta', '2018-05-22 13:13:43', '2018-05-22 13:13:43', NULL),
(33, 'Terry/Velour', '2018-05-22 13:13:47', '2018-05-22 13:13:47', NULL),
(34, 'Wood', '2018-05-22 13:13:55', '2018-05-22 13:13:55', NULL),
(35, 'Wool', '2018-05-22 13:14:01', '2018-05-22 13:14:01', NULL),
(36, 'Rayon', '2018-05-22 13:14:06', '2018-05-22 13:14:06', NULL),
(37, 'Cotton/Poly', '2018-05-22 13:14:10', '2018-05-22 13:14:10', NULL),
(38, 'Viscose', '2018-05-22 13:14:17', '2018-05-22 13:14:17', NULL),
(39, 'Nylon', '2018-05-22 13:14:22', '2018-05-22 13:14:22', NULL),
(40, 'Medium Wash', '2018-05-22 13:14:27', '2018-05-22 13:14:27', NULL),
(41, 'White Denim', '2018-05-22 13:14:31', '2018-05-22 13:14:31', NULL),
(42, 'Black and Grey', '2018-05-22 13:14:36', '2018-05-22 13:14:36', NULL),
(43, 'Acrylic', '2018-05-22 13:14:42', '2018-05-22 13:14:42', NULL),
(44, 'Tencel', '2018-05-22 13:14:53', '2018-05-22 13:14:53', NULL),
(45, 'Cashmere', '2018-05-22 13:14:57', '2018-05-22 13:14:57', NULL),
(46, 'Modal', '2018-05-22 13:15:01', '2018-05-22 13:15:01', NULL),
(47, 'Velvet', '2018-05-22 13:15:06', '2018-05-22 13:15:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `metas`
--

CREATE TABLE `metas` (
  `id` int(10) UNSIGNED NOT NULL,
  `page` int(11) NOT NULL,
  `category` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `metas`
--

INSERT INTO `metas` (`id`, `page`, `category`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 6, NULL, 'R3 All - Home', NULL, '2018-07-31 05:39:19', '2019-03-04 03:35:51'),
(2, 7, 1, NULL, NULL, '2018-07-31 11:16:18', '2018-07-31 11:16:18'),
(3, 4, NULL, NULL, NULL, '2018-10-09 09:09:06', '2018-10-09 09:09:06'),
(4, 8, NULL, NULL, NULL, '2018-10-09 09:09:09', '2018-10-09 09:09:09'),
(5, 7, 80, NULL, NULL, '2019-03-04 03:46:17', '2019-03-04 03:46:17');

-- --------------------------------------------------------

--
-- Table structure for table `meta_buyers`
--

CREATE TABLE `meta_buyers` (
  `id` int(10) UNSIGNED NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user.png',
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `block` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_customer_market` int(11) DEFAULT NULL,
  `seller_permit_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sell_online` tinyint(1) DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attention` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country_id` int(11) DEFAULT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_commercial` tinyint(1) DEFAULT NULL,
  `hear_about_us` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hear_about_us_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_offers` tinyint(1) DEFAULT NULL,
  `sales2_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales1_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_order` tinyint(1) NOT NULL DEFAULT '1',
  `ein_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_buyers`
--

INSERT INTO `meta_buyers` (`id`, `avatar`, `mobile`, `address1`, `address2`, `city`, `state`, `zip`, `country`, `verified`, `active`, `block`, `user_id`, `company_name`, `primary_customer_market`, `seller_permit_number`, `sell_online`, `website`, `attention`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state_id`, `billing_state`, `billing_zip`, `billing_country_id`, `billing_phone`, `billing_fax`, `billing_commercial`, `hear_about_us`, `hear_about_us_other`, `receive_offers`, `sales2_path`, `sales1_path`, `min_order`, `ein_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(13, 'Xfe5dC3ODwVlSqGNNkvoduwlw5UVC7kWxkMTmZGQ.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Address', NULL, 'City', NULL, NULL, '1234', 20, '123123123', NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-03-05 04:27:11', '2019-03-08 03:23:08', NULL),
(14, 'user.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Address', NULL, 'City', NULL, NULL, '1234', 20, '123123123', NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-03-07 12:02:06', '2019-03-07 23:03:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meta_vendors`
--

CREATE TABLE `meta_vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country_id` int(11) NOT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_alternate_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_commercial` tinyint(1) NOT NULL,
  `factory_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_state_id` int(11) DEFAULT NULL,
  `factory_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_country_id` int(11) NOT NULL,
  `factory_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_alternate_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_evening_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_commercial` tinyint(1) DEFAULT NULL,
  `company_info` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hear_about_us` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hear_about_us_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_notice` varchar(3000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_chart` varchar(3000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_offers` tinyint(1) NOT NULL,
  `setting_estimated_shipping_charge` tinyint(1) NOT NULL DEFAULT '0',
  `setting_consolidation` tinyint(1) NOT NULL DEFAULT '0',
  `setting_sort_activation_date` tinyint(1) NOT NULL DEFAULT '0',
  `setting_unverified_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `setting_unverified_user` tinyint(1) NOT NULL DEFAULT '0',
  `setting_not_logged` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_main_slider` tinyint(1) NOT NULL DEFAULT '0',
  `sp_vendor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp_password` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp_default_category` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_order` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_vendors`
--

INSERT INTO `meta_vendors` (`id`, `verified`, `active`, `company_name`, `business_name`, `website`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state_id`, `billing_state`, `billing_zip`, `billing_country_id`, `billing_phone`, `billing_alternate_phone`, `billing_fax`, `billing_commercial`, `factory_location`, `factory_address`, `factory_unit`, `factory_city`, `factory_state_id`, `factory_state`, `factory_zip`, `factory_country_id`, `factory_phone`, `factory_alternate_phone`, `factory_evening_phone`, `factory_fax`, `factory_commercial`, `company_info`, `hear_about_us`, `hear_about_us_other`, `order_notice`, `size_chart`, `receive_offers`, `setting_estimated_shipping_charge`, `setting_consolidation`, `setting_sort_activation_date`, `setting_unverified_checkout`, `setting_unverified_user`, `setting_not_logged`, `show_in_main_slider`, `sp_vendor`, `sp_password`, `sp_category`, `sp_default_category`, `min_order`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Binjar', 'Binjar Inc.', 'www.binjar.com', 'CA', 'Banashree', '2', 'Dhaka', 68, 'Rampura', '1219', 2, '01670985422', NULL, '123456', 1, 'US', 'cuet', '2', 'Chittagong', 11, 'Raozan', '3152', 1, '123456789', '45664', NULL, '4555', NULL, 'Company Info in details2', 'other', 'Bola jabena', '<p>&nbsp;asdf<strong> asd ORDER6</strong></p>\n\n<p><strong>asdfasdf</strong></p>', '<p>&nbsp;asdfa<em> ds2</em></p>', 1, 0, 0, 1, 0, 0, 0, 1, 'vendor', '123456', 'SALES', 'df', 500, '2018-05-08 09:03:20', '2019-01-05 03:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(46, '2014_10_12_000000_create_users_table', 1),
(47, '2014_10_12_100000_create_password_resets_table', 1),
(48, '2018_05_05_133752_create_default_categories_table', 1),
(49, '2018_05_08_080401_create_countries_table', 1),
(50, '2018_05_08_083857_create_states_table', 1),
(51, '2018_05_08_134438_create_meta_vendors_table', 1),
(52, '2018_05_09_103818_create_sizes_table', 2),
(53, '2018_05_11_105225_create_packs_table', 3),
(54, '2018_05_11_120421_create_master_colors_table', 4),
(56, '2018_05_11_135750_create_colors_table', 5),
(57, '2018_05_12_103732_create_made_in_countries_table', 6),
(58, '2018_05_12_134524_create_master_fabrics_table', 7),
(59, '2018_05_12_151609_create_fabrics_table', 8),
(60, '2018_05_14_101752_create_industries_table', 9),
(62, '2018_05_14_122651_add_year_establish_in_meta_vendors_table', 10),
(63, '2018_05_14_133652_create_industry_user_table', 11),
(64, '2018_05_14_152938_add_size_chart_column_in_meta_vendors_table', 12),
(65, '2018_05_15_092930_add_sort_column_in_default_categories_table', 13),
(66, '2018_05_15_110154_add_user_id_column_in_users_table', 14),
(67, '2018_05_15_110903_remove_user_id_column_in_meta_vendors_table', 15),
(68, '2018_05_15_125606_add_status_column_in_users_table', 16),
(69, '2018_05_15_131551_add_last_login_column_in_users_table', 17),
(70, '2018_05_15_135817_remove_verified_column_in_users_table', 18),
(71, '2018_05_15_135945_add_verified_column_in_meta_vendors_table', 19),
(72, '2018_05_15_164157_create_user_permission_table', 20),
(73, '2018_05_16_081643_add_settings_columns_in_meta_vendors_table', 21),
(74, '2018_05_16_091300_create_login_history_table', 22),
(79, '2018_05_16_121659_create_categories_table', 23),
(80, '2018_05_16_173514_change_user_id_to_meta_vendors_in_packs_table', 24),
(81, '2018_05_17_072246_change_user_id_to_meta_vendor_id_in_fabrics_table', 25),
(82, '2018_05_17_072801_change_user_id_to_meta_vendor_id_in_made_in_countries_table', 26),
(83, '2018_05_17_084204_create_body_sizes_table', 27),
(84, '2018_05_17_113905_create_patterns_table', 28),
(85, '2018_05_17_124941_create_lengths_table', 29),
(86, '2018_05_17_133027_create_styles_table', 30),
(87, '2018_05_17_163110_change_user_id_to_meta_vendor_id_in_made_in_colors_table', 31),
(88, '2018_05_18_142050_create_item_images_table', 32),
(89, '2018_05_18_180811_create_items_table', 33),
(90, '2018_05_18_182509_create_color_item_table', 34),
(91, '2018_05_18_194317_add_color_id_column_in_item_images_table', 35),
(92, '2018_05_19_150357_add_activation_at_column_in_items_table', 36),
(93, '2018_05_21_085041_add_meta_vendor_id_in_items_column', 37),
(94, '2018_05_21_104611_change_descrtiption_to_nullable_in_items_table', 38),
(95, '2018_05_21_181848_add_sort_column_in_item_images_table', 39),
(96, '2018_05_22_183025_add_image_path_column_in_master_colors_table', 40),
(97, '2018_05_24_155751_create_meta_buyers_table', 41),
(99, '2018_05_25_052549_create_cart_items_table', 42),
(100, '2018_05_25_150654_add_active_column_in_meta_vendors_table', 43),
(101, '2018_05_26_115243_add_location_columns_in_meta_vendors_table', 44),
(103, '2018_05_28_135447_create_vendor_images_table', 45),
(105, '2018_05_28_182550_create_orders_table', 46),
(106, '2018_05_28_200922_add_vendor_meta_id_column_in_cart_items_table', 47),
(109, '2018_05_29_085525_create_order_items_table', 48),
(111, '2018_05_29_095042_add_order_number_column_in_orders_table', 49),
(112, '2018_05_29_164751_add_per_unit_price_column_in_order_items_table', 50),
(113, '2018_05_30_083752_create_notifications_table', 51),
(114, '2018_05_30_181718_add_verify_active_column_in_meta_buyers_table', 52),
(115, '2018_05_30_183944_add_block_column_in_meta_buyers_table', 53),
(116, '2018_05_31_055248_add_shipping_location_column_in_meta_buyers_table', 54),
(118, '2018_05_31_060140_add_store_no_column_in_meta_buyers_table', 55),
(119, '2018_05_31_192012_create_couriers_table', 56),
(120, '2018_06_01_083044_create_admin_ship_methods_table', 57),
(121, '2018_06_01_094139_create_shipping_methods_table', 58),
(122, '2018_06_01_191253_add_shipping_method_id_in_orders_table', 59),
(123, '2018_06_04_090630_add_show_main_slider_column_in_meta_vendors_table', 60),
(124, '2018_06_05_090127_create_visitors_table', 61),
(125, '2018_06_07_204324_create_category_banners_table', 62),
(126, '2018_06_08_101221_add_main_slider_column_in_items_table', 63),
(127, '2018_06_08_180010_add_soft_delete_in_orders_table', 64),
(128, '2018_06_09_153920_add_shipping_location_column_in_orders_table', 65),
(129, '2018_06_09_155854_add_shipping_country_id_column_in_orders_table', 66),
(130, '2018_06_10_085852_add_code_column_in_colors_table', 67),
(131, '2018_06_10_111156_add_tracking_number_column_in_orders_table', 68),
(133, '2018_06_11_090147_add_user_id_column_in_meta_buyers_table', 69),
(134, '2018_06_11_154425_change_card_number_length_in_orders_table', 70),
(135, '2018_06_11_190820_add_new_top_slider_column_in_items_table', 71),
(136, '2018_06_12_151328_create_wish_list_items_table', 72),
(138, '2018_06_12_181955_add_list_image_path_in_item_images_table', 73),
(139, '2018_06_12_190655_add_thumbs_image_path_in_item_images_table', 74),
(140, '2018_06_13_090427_add_style_no_column_in_order_items_table', 75),
(141, '2018_06_13_093013_add_company_name_column_in_orders_table', 76),
(142, '2018_06_13_094203_add_name_column_in_orders_table', 77),
(143, '2018_06_13_094740_add_shipping_column_in_orders_table', 78),
(144, '2018_06_13_105024_add_notes_column_in_orders_table', 79),
(145, '2018_06_13_160933_add_image_path_column_in_colors_table', 80),
(146, '2018_06_13_180012_create_slider_items_table', 81),
(147, '2018_06_22_090108_remove_sub_category_from_body_sizes_table', 82),
(148, '2018_06_22_090425_add_parent_category_id_in_body_sizes_table', 83),
(149, '2018_06_22_101056_change_sub_category_id_to_parent_category_id_in_patterns_table', 84),
(150, '2018_06_22_102143_change_sub_category_id_to_parent_category_id_in_styles_table', 85),
(152, '2018_06_22_124629_create_buyer_shipping_addresses_table', 86),
(153, '2018_06_22_130248_remove_shipping_address_from_meta_buyers_table', 87),
(154, '2018_06_22_141254_rename_state_to_state_text_in_buyer_shipping_address_table', 88),
(155, '2018_06_22_175526_add_shipping_address_id_in_orders_table', 89),
(156, '2018_06_26_095141_add_user_id_column_in_visitors_table', 90),
(157, '2018_06_26_103908_create_block_users_table', 91),
(158, '2018_06_28_164609_add_fabric_column_in_items_table', 92);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `order_id`, `text`, `link`, `view`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, NULL, 'G9QUFXMHTMLG0 need approval.', 'http://localhost/fourgirls/public/order/34', 0, '2018-09-25 08:01:29', '2018-09-25 08:01:29', NULL),
(2, 14, NULL, 'G9QUFXMHTMLG0 need approval.', 'http://localhost/fourgirls/public/order/34', 0, '2018-09-25 08:02:10', '2018-09-25 08:02:10', NULL),
(3, 14, NULL, 'C1CM9B506UFZR-BO2 need approval.', 'http://localhost/fourgirls/public/order/36', 0, '2018-09-25 08:33:47', '2018-09-25 08:33:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL,
  `rejected` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address_id` int(11) DEFAULT NULL,
  `shipping_method_id` int(11) DEFAULT NULL,
  `shipping_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_id` int(11) DEFAULT NULL,
  `shipping_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country_id` int(11) DEFAULT NULL,
  `shipping_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country_id` int(11) DEFAULT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_full_name` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_expire` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_cvc` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_payment_info` text COLLATE utf8mb4_unicode_ci,
  `subtotal` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `store_credit` float NOT NULL DEFAULT '0',
  `shipping_cost` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `note` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `can_call` tinyint(1) NOT NULL DEFAULT '0',
  `authorize_info` text COLLATE utf8mb4_unicode_ci,
  `paypal_payment_id` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_token` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_payer_id` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirm_at` timestamp NULL DEFAULT NULL,
  `partially_shipped_at` timestamp NULL DEFAULT NULL,
  `fully_shipped_at` timestamp NULL DEFAULT NULL,
  `back_ordered_at` timestamp NULL DEFAULT NULL,
  `cancel_at` timestamp NULL DEFAULT NULL,
  `return_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `status`, `rejected`, `user_id`, `order_number`, `invoice_number`, `tracking_number`, `name`, `email`, `company_name`, `shipping`, `shipping_address_id`, `shipping_method_id`, `shipping_location`, `shipping_address`, `shipping_unit`, `shipping_city`, `shipping_state`, `shipping_state_text`, `shipping_state_id`, `shipping_zip`, `shipping_country`, `shipping_country_id`, `shipping_phone`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state`, `billing_state_text`, `billing_state_id`, `billing_zip`, `billing_country`, `billing_country_id`, `billing_phone`, `card_number`, `card_full_name`, `card_expire`, `card_cvc`, `payment_type`, `stripe_payment_info`, `subtotal`, `discount`, `store_credit`, `shipping_cost`, `total`, `note`, `can_call`, `authorize_info`, `paypal_payment_id`, `paypal_token`, `paypal_payer_id`, `confirm_at`, `partially_shipped_at`, `fully_shipped_at`, `back_ordered_at`, `cancel_at`, `return_at`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 0, 26, 'AN94VD4VHBKJM', 'AN94VD4VHBKJM', NULL, 'Ridwanul Hafiz', 'ridwan@mailinator.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Address', NULL, 'city', NULL, 'state', NULL, '1234', NULL, NULL, '23142341234', NULL, NULL, NULL, NULL, NULL, NULL, 36.00, 0.00, 0, 0.00, 36.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-07 07:17:39', NULL, NULL, NULL, 1, '2019-03-07 07:07:09', '2019-03-07 07:17:39', NULL),
(2, 2, 0, 27, 'JYZP7AHI2PJIE', 'STDJGTXP8GV3E', NULL, 'Test User', 'test@mailinator.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Address', NULL, 'City', NULL, 'State', NULL, '1234', NULL, NULL, '123123123', NULL, NULL, NULL, NULL, NULL, NULL, 12.00, 0.00, 0, 0.00, 12.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-07 12:02:08', '2019-03-07 12:02:08', NULL),
(3, 2, 0, 27, 'OQWND7O9U215H', 'U000U3UN3KY4W', NULL, 'Test User', 'test@mailinator.com', NULL, NULL, NULL, 7, NULL, 'Address', NULL, 'City', NULL, 'State', NULL, '1234', NULL, 20, '123123123', NULL, 'Address', NULL, 'City', NULL, 'State', NULL, '1234', NULL, 20, '123123123', 'eyJpdiI6IldRWVlETDNzUDlDWVRVa1JqUjlUSlE9PSIsInZhbHVlIjoiT01kanVwbXFZU0c3WDBoTEVXaGNkQmJlVGVRV2xnXC9vR3FWXC9peERjRDQ4PSIsIm1hYyI6IjgyOGU2MzY1OTZlMDBkZmU2ZDhiNTA0MWI1MDhkN2NjYjljZTcwYTAzYmI0OGEyZDFiOTU2ZmYwZjcxZDg1YTcifQ==', 'eyJpdiI6IjlDWnhPekRQN2lwSGdKKzhZbVVib3c9PSIsInZhbHVlIjoiVnBVcjJ4NHMwb2JXcGZQTnlHZUFzVDVoXC8zYkVLa3JSMmZpb1dQcVlGSUU9IiwibWFjIjoiNTA1MzliZmViYmVmYTkxNDQwZDIwYzgwYmNkYjVjNjBkNGY4NTVlNzY1M2QwNTQyNmJjOGQyYjI3MTJkY2ZmNiJ9', 'eyJpdiI6ImR5S1hOTVhnYVVITk9LaXpnODdobXc9PSIsInZhbHVlIjoidjYweXE1K0RQWUYyeUlIMGlzbnNcL2c9PSIsIm1hYyI6ImM1M2QyMjRlY2EyZjk5NDIyOGI4ZjE2Y2E0YjZhYTNhMzE2NGEwZjMyZjY1MzcwOTI1YzViMWI3MzVkZmU1N2QifQ==', 'eyJpdiI6ImpPS2xXVzljWHFGTGhlRDhtSndET0E9PSIsInZhbHVlIjoieDF0Y2gzcEg2UkRnaVRjZWduaDh6UT09IiwibWFjIjoiNmZjZjM2MjlkMThhZWNmMzFjY2JjMjExNzY4NDQ3Y2E0NGU2ODYzN2M0MjY4NjY0NTc0NGYwMjg5YzI4YzMyMCJ9', 'stripe', NULL, 12.00, 5.00, 0, 35.00, 42.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-07 23:03:48', '2019-03-07 23:13:53', NULL),
(4, 2, 0, 26, 'VRP77OUVCWYPH', 'L6ZQ4Q179V3S9', NULL, 'Ridwanul Hafiz', 'ridwan@mailinator.com', NULL, NULL, NULL, 1, NULL, 'Address', NULL, 'City', NULL, 'State', NULL, '1234', NULL, 20, '123123123', NULL, 'Address', NULL, 'City', NULL, 'State', NULL, '1234', NULL, 20, '123123123', 'eyJpdiI6ImFnYW42TDFRTVwvS0UzXC9yVzNMb1BUUT09IiwidmFsdWUiOiIxUlR1Z2xFVHYwTjZaQ25RcWhjUlJqU210UXVHZTRpXC9XT09XXC9mcEhmdUk9IiwibWFjIjoiY2NmNGQxNzc5NmVmMWIwMWY4NWY3Mzk1NjRkOTliMjQ5MzNhZjRiZWE5OTVkOWQ5ZGFmM2M1MDU1NjRkZDk1ZSJ9', 'eyJpdiI6IktCYmZ2WHhzTEc1clptZTVseXBTbmc9PSIsInZhbHVlIjoieXd6MzVWTVwvZGxOMXZvOForcmpsVmc9PSIsIm1hYyI6IjYxMDBkYjU2NWE3Y2Y5YzJiNDM0ZmEwMDMzZmQ0ODYzMzk2MjY3ODE4YTJmMGUyMTE2NmZjZWI5MDdmOWIwZDEifQ==', 'eyJpdiI6Ik91OFN2dUM4NGc1eUpOc0VGVWk5akE9PSIsInZhbHVlIjoiQzJGNHpXaUhudlZwVTIxYjdnbmUrQT09IiwibWFjIjoiMTIwMjY1N2ZiYjU2ZDViZGU5ZThkNjhkZWM3ZjIxOWU5ZGJlYTAwOWQ0YTAzNWVlYjA5MTVkYjEzMzc0ZjJmMiJ9', 'eyJpdiI6ImF4b3hiZG1VMTdlVXpVcmdpM1dmQlE9PSIsInZhbHVlIjoiVGRWXC9cL0tDc011N200ZGZoZ0Y3K0tnPT0iLCJtYWMiOiIzM2IzNzY4MTAyYjdjY2FiYTUwZWQyNWM2YmZhYWFiNDJkNzFjZTM1NWUwZDA4ZjQ1NjMwNjM0ZTZmZjM1NmRkIn0=', 'stripe', NULL, 30.00, 5.00, 0, 0.00, 25.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-08 03:02:00', '2019-03-08 03:02:05', NULL),
(5, 2, 0, 26, '4T3NEPWX9V4GQ', '7JR8H6P9O2WXK', NULL, 'Ridwanul Hafiz', 'ridwan@mailinator.com', NULL, NULL, NULL, 7, NULL, 'Address', NULL, 'City', NULL, 'State', NULL, '1234', NULL, 20, '123123123', NULL, 'Address', NULL, 'City', NULL, 'State', NULL, '1234', NULL, 20, '123123123', 'eyJpdiI6IjFpQ1cwZW5ZZmxPU0xyamlvWkczZFE9PSIsInZhbHVlIjoiTzdwYWJ1Y0E2VnJYb2VzYmRxbFYwalZJN25vSjJydnNUUlZ5c3JLN1prYz0iLCJtYWMiOiIxZDNkMTBkNTVhZThhNjM0M2IyYTgyYjM3NmJlOTBhYjhmMjU2ZDIyYTE2ZmM4Y2E4NThiNGI4MGY4NjJiY2E2In0=', 'eyJpdiI6IkdcL1V6cjdcL3hLVnplNUZUVjN6cW85QT09IiwidmFsdWUiOiJSelp3XC9QUGptblpzcWs5V1c0bEh5ejRSYXFZR1wvV3FZcjYrWHZEUDdmcVk9IiwibWFjIjoiZjhlYzYxZGEyNDRjOGQ1OTcyNTA3ODhlZjBmODc3MDkxMTlhZWMxMTNkOWU3MThjOTVjMWUyMjYxNTU4ZDg1YiJ9', 'eyJpdiI6IkVkQlZqM0ZaTDNjVUV6RUpsWlh4alE9PSIsInZhbHVlIjoiUHU2dm5KdXVLTHAxaXNWdHB4V1lqUT09IiwibWFjIjoiZjcwMTg4MDRjNzAzZThlMzM2YTZkMjc0ZDYyZjUxZjg5ZTE0Njc0YTViNzNiNTI5MDEwNTdkNWNhZWU3YzFiNCJ9', 'eyJpdiI6ImpZUldwWEtWVEJqS1l1cHlibFpwMkE9PSIsInZhbHVlIjoiaTM1NndPdFRDWGFwT3ZxUFJ1XC9Bd2c9PSIsIm1hYyI6Ijg5NDY0YTQxNTk1Y2ZhNWM5ZjAyNzVmMTc2YTA0ZWVjZWQ3NDJlNWYzMGQxODYxNzM3OTM5NGM0NzNjODJiNjAifQ==', 'stripe', '{\"id\":\"ch_1EBev3DqSSVhZA597nHhg64q\",\"object\":\"charge\",\"amount\":9500,\"amount_refunded\":0,\"balance_transaction\":\"txn_1EBev3DqSSVhZA59quyA2oFp\",\"captured\":true,\"currency\":\"usd\",\"description\":\"Charge for R3 All - Order Number 4T3NEPWX9V4GQ\",\"receipt_url\":\"https:\\/\\/pay.stripe.com\\/receipts\\/acct_1E0d7cDqSSVhZA59\\/ch_1EBev3DqSSVhZA597nHhg64q\\/rcpt_EeysJ0JSEimOAUNIjvLjZbPLYVB2quL\",\"refunds\":{\"object\":\"list\",\"data\":[],\"has_more\":false,\"total_count\":0,\"url\":\"\\/v1\\/charges\\/ch_1EBev3DqSSVhZA597nHhg64q\\/refunds\"},\"source\":{\"id\":\"card_1EBev1DqSSVhZA59zpTZtAX3\",\"object\":\"card\",\"address_city\":null,\"address_country\":null,\"address_line1\":null,\"address_line1_check\":null,\"address_line2\":null,\"address_state\":null,\"address_zip\":null,\"address_zip_check\":null,\"brand\":\"MasterCard\",\"country\":\"US\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":1,\"exp_year\":2020,\"fingerprint\":\"Tm6m9brhOkQfHbHx\",\"funding\":\"credit\",\"last4\":\"4444\",\"metadata\":[],\"name\":null,\"tokenization_method\":null},\"status\":{\"id\":\"card_1EBev1DqSSVhZA59zpTZtAX3\",\"object\":\"card\",\"address_city\":null,\"address_country\":null,\"address_line1\":null,\"address_line1_check\":null,\"address_line2\":null,\"address_state\":null,\"address_zip\":null,\"address_zip_check\":null,\"brand\":\"MasterCard\",\"country\":\"US\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":1,\"exp_year\":2020,\"fingerprint\":\"Tm6m9brhOkQfHbHx\",\"funding\":\"credit\",\"last4\":\"4444\",\"metadata\":[],\"name\":null,\"tokenization_method\":null}}', 60.00, 0.00, 0, 35.00, 95.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-08 03:23:08', '2019-03-08 03:23:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `style_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_per_pack` int(11) NOT NULL,
  `pack` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `total_qty` int(11) NOT NULL,
  `dispatch` int(11) NOT NULL DEFAULT '0',
  `per_unit_price` double(8,2) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `item_id`, `style_no`, `color`, `size`, `item_per_pack`, `pack`, `qty`, `total_qty`, `dispatch`, `per_unit_price`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 'N01', '', '', 0, '', 1, 1, 0, 12.00, 12.00, '2019-03-07 07:07:10', '2019-03-07 07:07:10'),
(2, 1, 2, 'TEST!2', '', '', 0, '', 2, 2, 0, 12.00, 24.00, '2019-03-07 07:07:10', '2019-03-07 07:07:10'),
(3, 2, 5, 'N01', '', '', 0, '', 1, 1, 0, 12.00, 12.00, '2019-03-07 12:02:08', '2019-03-07 12:02:08'),
(4, 3, 2, 'TEST!2', '', '', 0, '', 1, 1, 0, 12.00, 12.00, '2019-03-07 23:03:48', '2019-03-07 23:03:48'),
(5, 4, 1, '800 Series', '', '', 0, '', 1, 1, 0, 30.00, 30.00, '2019-03-08 03:02:00', '2019-03-08 03:02:00'),
(6, 5, 1, '800 Series', '', '', 0, '', 2, 2, 0, 30.00, 60.00, '2019-03-08 03:23:08', '2019-03-08 03:23:08');

-- --------------------------------------------------------

--
-- Table structure for table `packs`
--

CREATE TABLE `packs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `pack1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pack2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packs`
--

INSERT INTO `packs` (`id`, `name`, `status`, `default`, `pack1`, `pack2`, `pack3`, `pack4`, `pack5`, `pack6`, `pack7`, `pack8`, `pack9`, `pack10`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'S-M-L', 1, 0, '2', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-11 05:16:28', '2018-06-29 10:20:48', NULL),
(2, '3-3', 1, 0, '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-11 05:30:54', '2018-07-27 13:31:37', NULL),
(3, 'ONE SIZE', 1, 0, '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-16 11:39:34', '2018-06-29 10:20:48', NULL),
(4, '2/2/2', 1, 0, '2', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:18', '2018-06-29 10:20:48', NULL),
(5, '3/3', 1, 0, '3', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:28', '2018-06-29 10:20:48', NULL),
(6, 'ONE SIZE', 1, 0, '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:38', '2018-06-29 10:20:48', NULL),
(7, 'dsfd', 1, 1, '1', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-26 08:59:37', '2018-06-29 10:20:48', NULL),
(8, '123', 1, 0, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-26 10:34:42', '2018-05-26 10:42:56', '2018-05-26 10:42:56'),
(9, 'a-u', 0, 0, '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-29 10:19:56', '2018-06-29 10:20:52', '2018-06-29 10:20:52'),
(10, '2-3-e-5', 1, 0, '34', '54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-30 09:20:07', '2018-06-30 09:27:20', NULL),
(11, '1X-2X-3X', 1, 0, '3', '4', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-09 05:22:42', '2018-08-09 05:22:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `content` varchar(10000) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, '<p>Official R3 Store - Korean Cosmetics</p>', '2018-07-26 08:37:48', '2019-03-08 00:39:15'),
(2, 4, NULL, '2018-08-13 10:53:55', '2018-08-13 10:53:55'),
(3, 12, '<p>asdfa</p>', '2018-09-26 05:34:58', '2018-09-26 05:35:03'),
(4, 2, '<h2>contactasdfasdfadfasfdsadvdsdsagagdsdagsdgasg</h2>', '2018-10-03 02:10:58', '2019-03-08 00:40:06'),
(5, 3, NULL, '2018-10-09 09:09:19', '2018-10-09 09:09:19');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patterns`
--

CREATE TABLE `patterns` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patterns`
--

INSERT INTO `patterns` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 1, '2018-05-17 06:44:46', '2018-05-17 06:44:52', '2018-05-17 06:44:52'),
(2, 'Burnout', 1, '2018-05-17 06:44:57', '2018-05-22 14:19:31', NULL),
(3, 'Check', 1, '2018-05-22 14:19:38', '2018-05-22 14:19:38', NULL),
(4, 'Color block', 1, '2018-05-22 14:19:46', '2018-05-22 14:19:46', NULL),
(5, 'Crochet', 1, '2018-05-22 14:19:55', '2018-05-22 14:19:55', NULL),
(6, 'Digital Print', 1, '2018-05-22 14:20:02', '2018-05-22 14:20:02', NULL),
(7, 'Embroidered', 1, '2018-05-22 14:20:11', '2018-05-22 14:20:11', NULL),
(8, 'Glitter/Sequins', 1, '2018-05-22 14:20:17', '2018-05-22 14:20:17', NULL),
(9, 'Lace', 1, '2018-05-22 14:20:27', '2018-05-22 14:20:27', NULL),
(10, 'Lettering', 1, '2018-05-22 14:20:33', '2018-05-22 14:20:33', NULL),
(11, 'Metallic', 1, '2018-05-22 14:20:41', '2018-05-22 14:20:41', NULL),
(12, 'Multi-Color', 1, '2018-05-22 14:20:47', '2018-05-22 14:20:47', NULL),
(13, 'Multicolor', 1, '2018-05-22 14:20:54', '2018-05-22 14:20:54', NULL),
(14, 'Ombre', 1, '2018-05-22 14:21:02', '2018-05-22 14:21:02', NULL),
(15, 'Plaid', 1, '2018-05-22 14:21:10', '2018-05-22 14:21:10', NULL),
(16, 'Polka Dot', 1, '2018-05-22 14:21:17', '2018-05-22 14:21:17', NULL),
(17, 'Print Screen', 1, '2018-05-22 14:21:24', '2018-05-22 14:21:24', NULL),
(18, 'Print Sublimation', 1, '2018-05-22 14:21:31', '2018-05-22 14:21:31', NULL),
(19, 'Print-Animal-Floral-Skull-Butterfly', 1, '2018-05-22 14:21:37', '2018-05-22 14:21:37', NULL),
(20, 'Rhinestones', 1, '2018-05-22 14:21:44', '2018-05-22 14:21:44', NULL),
(21, 'Ribbon/Bows', 1, '2018-05-22 14:21:51', '2018-05-22 14:21:51', NULL),
(22, 'Solid', 1, '2018-05-22 14:21:59', '2018-05-22 14:21:59', NULL),
(23, 'Striped', 1, '2018-05-22 14:22:07', '2018-05-22 14:22:07', NULL),
(24, 'Striped Sublimation', 1, '2018-05-22 14:22:15', '2018-05-22 14:22:15', NULL),
(27, 'tt', 5, '2018-06-22 04:18:28', '2018-06-22 04:18:41', '2018-06-22 04:18:41');

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `rating` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `product_id`, `customer_id`, `title`, `comment`, `rating`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'test 3', 'Lorem ipsamd  akldsfls dfhalk sdhf;klas df;klasjdf;kl jsdkl f', 3, '2019-03-06 10:13:07', '2019-03-06 10:13:07'),
(3, 2, 26, 'Good Title', 'Writing review', 2, '2019-03-06 04:25:43', '2019-03-06 04:25:43');

-- --------------------------------------------------------

--
-- Table structure for table `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` int(191) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promo_codes`
--

INSERT INTO `promo_codes` (`id`, `title`, `code`, `discount`, `start_date`, `end_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'r3all', 5, '2019-03-01 00:00:00', '2019-03-30 00:00:00', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `star` int(11) NOT NULL DEFAULT '0',
  `review` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `order_id`, `user_id`, `star`, `review`, `reply`, `created_at`, `updated_at`) VALUES
(8, 6, 14, 0, 'werwr', 'd', '2018-07-17 11:09:16', '2018-07-17 11:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(5, 'welcome_notification', '<p>Welcome to CQBYCQ</p>', '2018-07-16 11:19:12', '2018-12-03 08:59:46'),
(4, 'buyer_home', '<p>WELCOME TO CQBYCQ</p>\r\n\r\n<p>&nbsp;</p>', '2018-07-16 11:19:04', '2018-12-03 09:00:37'),
(6, 'logo-white', 'images/logo/f245aca0-3e5b-11e9-b961-57ce8d1cf9d5.jpg', NULL, NULL),
(7, 'logo-black', 'images/logo/9b6f1400-3e70-11e9-ac80-b5295c6542b4.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_methods`
--

CREATE TABLE `shipping_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `ship_method_id` int(11) NOT NULL,
  `list_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_methods`
--

INSERT INTO `shipping_methods` (`id`, `status`, `default`, `courier_id`, `ship_method_id`, `list_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 1, 1, '2018-06-01 04:41:24', '2018-06-25 10:20:16', NULL),
(2, 1, 0, 1, 3, 2, '2018-06-01 04:41:42', '2018-06-25 10:20:16', NULL),
(3, 1, 0, 3, 5, 1, '2018-06-04 09:22:27', '2018-07-02 09:20:12', '2018-07-02 09:20:12'),
(4, 1, 0, 1, 1, 3, '2018-07-02 11:08:02', '2018-07-02 11:08:12', '2018-07-02 11:08:12');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `size1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `status`, `default`, `user_id`, `size1`, `size2`, `size3`, `size4`, `size5`, `size6`, `size7`, `size8`, `size9`, `size10`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test Size', 1, 0, 1, 'S', 'M', 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-09 06:42:40', '2018-05-11 08:16:24', NULL),
(2, 'Test Size 2', 0, 1, 1, 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-10 03:25:13', '2018-05-11 08:16:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider_items`
--

CREATE TABLE `slider_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_items`
--

INSERT INTO `slider_items` (`id`, `item_id`, `sort`, `type`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, '2018-07-02 07:34:35', '2018-07-02 07:34:40'),
(2, 4, 2, 1, '2018-07-02 07:34:37', '2018-07-02 07:34:40'),
(3, 2, 1, 4, '2018-07-02 07:34:46', '2018-07-02 07:34:46');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `name`, `code`) VALUES
(1, 1, 'Alaska', 'AK'),
(2, 1, 'Alabama', 'AL'),
(3, 1, 'American Samoa', 'AS'),
(4, 1, 'Arizona', 'AZ'),
(5, 1, 'Arkansas', 'AR'),
(6, 1, 'California', 'CA'),
(7, 1, 'Colorado', 'CO'),
(8, 1, 'Connecticut', 'CT'),
(9, 1, 'Delaware', 'DE'),
(10, 1, 'District of Columbia', 'DC'),
(11, 1, 'Federated States of Micronesia', 'FM'),
(12, 1, 'Florida', 'FL'),
(13, 1, 'Georgia', 'GA'),
(14, 1, 'Guam', 'GU'),
(15, 1, 'Hawaii', 'HI'),
(16, 1, 'Idaho', 'ID'),
(17, 1, 'Illinois', 'IL'),
(18, 1, 'Indiana', 'IN'),
(19, 1, 'Iowa', 'IA'),
(20, 1, 'Kansas', 'KS'),
(21, 1, 'Kentucky', 'KY'),
(22, 1, 'Louisiana', 'LA'),
(23, 1, 'Maine', 'ME'),
(24, 1, 'Marshall Islands', 'MH'),
(25, 1, 'Maryland', 'MD'),
(26, 1, 'Massachusetts', 'MA'),
(27, 1, 'Michigan', 'MI'),
(28, 1, 'Minnesota', 'MN'),
(29, 1, 'Mississippi', 'MS'),
(30, 1, 'Missouri', 'MO'),
(31, 1, 'Montana', 'MT'),
(32, 1, 'Nebraska', 'NE'),
(33, 1, 'Nevada', 'NV'),
(34, 1, 'New Hampshire', 'NH'),
(35, 1, 'New Jersey', 'NJ'),
(36, 1, 'New Mexico', 'NM'),
(37, 1, 'New York', 'NY'),
(38, 1, 'North Carolina', 'NC'),
(39, 1, 'North Dakota', 'ND'),
(40, 1, 'Northern Mariana Islands', 'MP'),
(41, 1, 'Ohio', 'OH'),
(42, 1, 'Oklahoma', 'OK'),
(43, 1, 'Oregon', 'OR'),
(44, 1, 'Palau', 'PW'),
(45, 1, 'Pennsylvania', 'PA'),
(46, 1, 'Puerto Rico', 'PR'),
(47, 1, 'Rhode Island', 'RI'),
(48, 1, 'South Carolina', 'SC'),
(49, 1, 'South Dakota', 'SD'),
(50, 1, 'Tennessee', 'TN'),
(51, 1, 'Texas', 'TX'),
(52, 1, 'Utah', 'UT'),
(53, 1, 'Vermont', 'VT'),
(54, 1, 'Virgin Islands', 'VI'),
(55, 1, 'Virginia', 'VA'),
(56, 1, 'Washington', 'WA'),
(57, 1, 'West Virginia', 'WV'),
(58, 1, 'Wisconsin', 'WI'),
(59, 1, 'Wyoming', 'WY'),
(60, 1, 'Armed Forces Africa', 'AE'),
(61, 1, 'Armed Forces Americas (except Canada)', 'AA'),
(62, 1, 'Armed Forces Canada', 'AE'),
(63, 1, 'Armed Forces Europe', 'AE'),
(64, 1, 'Armed Forces Middle East', 'AE'),
(65, 1, 'Armed Forces Pacific', 'AP'),
(66, 2, 'Alberta', 'AB'),
(67, 2, 'British Columbia', 'BC'),
(68, 2, 'Manitoba', 'MB'),
(69, 2, 'New Brunswick', 'NB'),
(70, 2, 'Newfoundland and Labrador', 'NL'),
(71, 2, 'Northwest Territories', 'NT'),
(72, 2, 'Nova Scotia', 'NS'),
(73, 2, 'Nunavut', 'NU'),
(74, 2, 'Ontario', 'ON'),
(75, 2, 'Prince Edward Island', 'PE'),
(76, 2, 'Quebec', 'QC'),
(77, 2, 'Saskatchewan', 'SK'),
(78, 2, 'Yukon', 'YT');

-- --------------------------------------------------------

--
-- Table structure for table `store_credits`
--

CREATE TABLE `store_credits` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_credits`
--

INSERT INTO `store_credits` (`id`, `user_id`, `amount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 637.00, NULL, '2019-03-04 03:19:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_credit_transections`
--

CREATE TABLE `store_credit_transections` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_credit_transections`
--

INSERT INTO `store_credit_transections` (`id`, `user_id`, `order_id`, `reason`, `amount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 20, 'qweqwe', 12.00, '2018-07-31 11:27:04', '2018-07-31 11:27:04', NULL),
(2, 14, 52, 'test', 125.00, '2019-01-30 03:27:51', '2019-01-30 03:27:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE `styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'A-line', 1, '2018-05-17 07:34:48', '2018-05-22 14:24:20', NULL),
(2, 'asdfa', 1, '2018-05-17 07:34:57', '2018-05-17 07:35:00', '2018-05-17 07:35:00'),
(3, 'Asymmetrical', 1, '2018-05-22 14:24:27', '2018-05-22 14:24:27', NULL),
(4, 'Babydoll', 1, '2018-05-22 14:24:33', '2018-05-22 14:24:33', NULL),
(5, 'Beaded', 1, '2018-05-22 14:24:40', '2018-05-22 14:24:40', NULL),
(6, 'Belted', 1, '2018-05-22 14:24:48', '2018-05-22 14:24:48', NULL),
(7, 'Blouse', 1, '2018-05-22 14:24:56', '2018-05-22 14:24:56', NULL),
(8, 'fd', 3, '2018-06-22 04:36:09', '2018-06-22 04:36:20', '2018-06-22 04:36:20');

-- --------------------------------------------------------

--
-- Table structure for table `top_banners`
--

CREATE TABLE `top_banners` (
  `id` int(11) NOT NULL,
  `page` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `image_path` varchar(500) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `top_banners`
--

INSERT INTO `top_banners` (`id`, `page`, `category_id`, `url`, `image_path`, `created_at`, `updated_at`) VALUES
(8, 10, NULL, 'ty', '/images/banner/b6f98e60-95ba-11e8-b58d-b7d25d4f29a0.jpg', '2018-08-01 12:43:00', '2018-08-01 12:43:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `buyer_meta_id` int(11) DEFAULT NULL,
  `vendor_meta_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_token` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `order_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `user_id`, `password`, `social`, `provider_id`, `role`, `active`, `buyer_meta_id`, `vendor_meta_id`, `remember_token`, `reset_token`, `last_login`, `order_count`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super', 'Admin', 'superadmin@gmail.com', 'superadmin', '$2y$10$Uumgr3En9yFT5fEUBw4OFeulreuynvKWNP2SnuYI2ic2vU8WpUFaG', NULL, NULL, 1, 1, NULL, 1, 'fNPcWW9ZMRW4bDkFxr5wpscWXaNyFYbPRHtNEx98doOJ7uiF0fyt6cusKcwH', NULL, '2019-03-08 03:29:39', 0, '2018-05-08 09:03:20', '2019-03-08 03:29:39', NULL),
(26, 'Test', 'User', 'ridwan@mailinator.com', NULL, '$2y$10$GCo2RiZvup.zxjFD/XSmV.ingnya3vyTHvBQopw9iu3idGGBK0stW', NULL, NULL, 3, 1, 13, NULL, 'u7oHZTG9F4wF94KhcovNQNwz2HGcJqXnA2RQ8RTphgX5YduCGyq7Yb6aCnaw', NULL, '2019-03-08 03:38:17', 0, '2019-03-05 04:27:11', '2019-03-08 03:42:26', NULL),
(27, 'Test', 'User', 'test@mailinator.com', NULL, '$2y$10$sJcbEz92NoYUrBuq2NJ3Iu61/XWyUPz3pVd3.XSdDhSShoyklc4I2', NULL, NULL, 3, 1, 14, NULL, 'Brynj0xBDT6jG1SeDOsH40cw5KlnQ3FmEdraAlPd5ehXWQQb4RsiGZuU7klO', NULL, NULL, 0, '2019-03-07 12:02:07', '2019-03-07 12:02:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

CREATE TABLE `user_permission` (
  `user_id` int(11) NOT NULL,
  `permission` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_permission`
--

INSERT INTO `user_permission` (`user_id`, `permission`) VALUES
(9, 3),
(9, 4),
(10, 2),
(10, 6),
(11, 10),
(11, 4),
(11, 3),
(20, 3),
(20, 4);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_images`
--

CREATE TABLE `vendor_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendor_images`
--

INSERT INTO `vendor_images` (`id`, `type`, `status`, `color`, `image_path`, `sort`, `url`, `created_at`, `updated_at`) VALUES
(26, 7, 1, NULL, '/images/banner/863d0cc0-3e7f-11e9-969e-33ac4b13c402.jpeg', 1, 'https://r3all.com', '2019-03-04 07:15:04', '2019-03-04 07:15:04'),
(13, 8, 1, NULL, '/images/banner/cd322e20-83a2-11e8-a6e3-bb1ef1eb06af.jpg', 3, '4545', '2018-07-09 12:06:28', '2018-07-30 08:57:27'),
(14, 8, 1, NULL, '/images/banner/d03fbc20-83a2-11e8-a3de-e31afaa0ced3.jpg', 1, NULL, '2018-07-09 12:06:33', '2018-07-09 12:09:36'),
(15, 8, 1, NULL, '/images/banner/d2c10540-83a2-11e8-a969-9d7108a652e3.jpg', 2, NULL, '2018-07-09 12:06:37', '2018-07-09 12:09:36'),
(24, 1, 1, NULL, '/images/banner/5b42d7b0-f57c-11e8-a12b-ff9f73d5eee4.png', NULL, NULL, '2018-12-01 09:18:29', '2018-12-01 09:18:31'),
(27, 7, 1, NULL, '/images/banner/92f063c0-4165-11e9-9698-3d38a305c23c.jpeg', 2, 'https://r3all.com', '2019-03-07 23:46:52', '2019-03-07 23:46:52'),
(28, 7, 1, NULL, '/images/banner/98c733a0-4165-11e9-887d-1f04035c32d9.jpeg', 3, 'https://r3all.com', '2019-03-07 23:47:02', '2019-03-07 23:47:02');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `user_id`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 02:37:40', '2018-07-26 02:37:40'),
(2, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 02:39:54', '2018-07-26 02:39:54'),
(3, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 07:58:17', '2018-07-26 07:58:17'),
(4, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:07:40', '2018-07-26 08:07:40'),
(5, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:13:24', '2018-07-26 08:13:24'),
(6, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:13:46', '2018-07-26 08:13:46'),
(7, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:14:16', '2018-07-26 08:14:16'),
(8, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:14:41', '2018-07-26 08:14:41'),
(9, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:15:00', '2018-07-26 08:15:00'),
(10, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:15:51', '2018-07-26 08:15:51'),
(11, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:26:05', '2018-07-26 08:26:05'),
(12, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:27:23', '2018-07-26 08:27:23'),
(13, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:27:43', '2018-07-26 08:27:43'),
(14, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:28:43', '2018-07-26 08:28:43'),
(15, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:29:22', '2018-07-26 08:29:22'),
(16, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:39:15', '2018-07-26 08:39:15'),
(17, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:39:27', '2018-07-26 08:39:27'),
(18, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:39:31', '2018-07-26 08:39:31'),
(19, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:44:05', '2018-07-26 08:44:05'),
(20, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 09:07:20', '2018-07-26 09:07:20'),
(21, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 09:15:11', '2018-07-26 09:15:11'),
(22, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:23:21', '2018-07-26 11:23:21'),
(23, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:29:00', '2018-07-26 11:29:00'),
(24, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:34:35', '2018-07-26 11:34:35'),
(25, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:34:37', '2018-07-26 11:34:37'),
(26, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:34:56', '2018-07-26 11:34:56'),
(27, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:35:36', '2018-07-26 11:35:36'),
(28, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:35:37', '2018-07-26 11:35:37'),
(29, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:03', '2018-07-26 11:36:03'),
(30, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:06', '2018-07-26 11:36:06'),
(31, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:07', '2018-07-26 11:36:07'),
(32, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:08', '2018-07-26 11:36:08'),
(33, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:09', '2018-07-26 11:36:09'),
(34, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:15', '2018-07-26 11:36:15'),
(35, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:16', '2018-07-26 11:36:16'),
(36, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:16', '2018-07-26 11:36:16'),
(37, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:36', '2018-07-26 11:36:36'),
(38, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:37', '2018-07-26 11:36:37'),
(39, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:39', '2018-07-26 11:36:39'),
(40, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:39', '2018-07-26 11:36:39'),
(41, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:37:50', '2018-07-26 11:37:50'),
(42, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:37:51', '2018-07-26 11:37:51'),
(43, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:37:52', '2018-07-26 11:37:52'),
(44, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:37', '2018-07-26 11:40:37'),
(45, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:38', '2018-07-26 11:40:38'),
(46, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:38', '2018-07-26 11:40:38'),
(47, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:38', '2018-07-26 11:40:38'),
(48, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:39', '2018-07-26 11:40:39'),
(49, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:39', '2018-07-26 11:40:39'),
(50, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:39', '2018-07-26 11:40:39'),
(51, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:40', '2018-07-26 11:40:40'),
(52, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:40', '2018-07-26 11:40:40'),
(53, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:44:17', '2018-07-26 11:44:17'),
(54, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:11', '2018-07-26 11:47:11'),
(55, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:13', '2018-07-26 11:47:13'),
(56, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:14', '2018-07-26 11:47:14'),
(57, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:14', '2018-07-26 11:47:14'),
(58, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:26', '2018-07-26 11:47:26'),
(59, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:27', '2018-07-26 11:47:27'),
(60, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:27', '2018-07-26 11:47:27'),
(61, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:27', '2018-07-26 11:47:27'),
(62, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:48:44', '2018-07-26 11:48:44'),
(63, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:21', '2018-07-26 11:49:21'),
(64, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:22', '2018-07-26 11:49:22'),
(65, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:22', '2018-07-26 11:49:22'),
(66, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:23', '2018-07-26 11:49:23'),
(67, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:23', '2018-07-26 11:49:23'),
(68, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:27', '2018-07-26 11:49:27'),
(69, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:51:37', '2018-07-26 11:51:37'),
(70, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:08', '2018-07-26 11:54:08'),
(71, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:29', '2018-07-26 11:54:29'),
(72, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:30', '2018-07-26 11:54:30'),
(73, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:31', '2018-07-26 11:54:31'),
(74, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:31', '2018-07-26 11:54:31'),
(75, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:32', '2018-07-26 11:54:32'),
(76, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:32', '2018-07-26 11:54:32'),
(77, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:49', '2018-07-26 11:54:49'),
(78, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:55:22', '2018-07-26 11:55:22'),
(79, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:56:12', '2018-07-26 11:56:12'),
(80, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:57:01', '2018-07-26 11:57:01'),
(81, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:57:04', '2018-07-26 11:57:04'),
(82, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:57:49', '2018-07-26 11:57:49'),
(83, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:01:20', '2018-07-26 12:01:20'),
(84, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:07:39', '2018-07-26 12:07:39'),
(85, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:11:13', '2018-07-26 12:11:13'),
(86, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:11:42', '2018-07-26 12:11:42'),
(87, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:11:53', '2018-07-26 12:11:53'),
(88, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:11:58', '2018-07-26 12:11:58'),
(89, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:00', '2018-07-26 12:12:00'),
(90, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:03', '2018-07-26 12:12:03'),
(91, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:19', '2018-07-26 12:12:19'),
(92, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:39', '2018-07-26 12:12:39'),
(93, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:51', '2018-07-26 12:12:51'),
(94, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:13:04', '2018-07-26 12:13:04'),
(95, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:13:44', '2018-07-26 12:13:44'),
(96, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:28:08', '2018-07-26 12:28:08'),
(97, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:30:04', '2018-07-26 12:30:04'),
(98, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:37:13', '2018-07-26 12:37:13'),
(99, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:38:15', '2018-07-26 12:38:15'),
(100, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:38:27', '2018-07-26 12:38:27'),
(101, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:43:31', '2018-07-26 12:43:31'),
(102, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:57:19', '2018-07-26 12:57:19'),
(103, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:07:26', '2018-07-26 13:07:26'),
(104, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:13:43', '2018-07-26 13:13:43'),
(105, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:14:06', '2018-07-26 13:14:06'),
(106, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:22:54', '2018-07-26 13:22:54'),
(107, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:39:44', '2018-07-26 13:39:44'),
(108, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 02:22:15', '2018-07-27 02:22:15'),
(109, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 02:47:25', '2018-07-27 02:47:25'),
(110, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 02:47:45', '2018-07-27 02:47:45'),
(111, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 02:48:40', '2018-07-27 02:48:40'),
(112, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 03:02:59', '2018-07-27 03:02:59'),
(113, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 04:26:35', '2018-07-27 04:26:35'),
(114, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:13:22', '2018-07-27 06:13:22'),
(115, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:19:02', '2018-07-27 06:19:02'),
(116, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:20:33', '2018-07-27 06:20:33'),
(117, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:20:52', '2018-07-27 06:20:52'),
(118, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:20:53', '2018-07-27 06:20:53'),
(119, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:20:54', '2018-07-27 06:20:54'),
(120, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:21:20', '2018-07-27 06:21:20'),
(121, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:22:02', '2018-07-27 06:22:02'),
(122, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:23:14', '2018-07-27 06:23:14'),
(123, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:23:42', '2018-07-27 06:23:42'),
(124, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:25:30', '2018-07-27 06:25:30'),
(125, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:28:09', '2018-07-27 06:28:09'),
(126, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:28:48', '2018-07-27 06:28:48'),
(127, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:30:01', '2018-07-27 06:30:01'),
(128, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:30:18', '2018-07-27 06:30:18'),
(129, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:31:14', '2018-07-27 06:31:14'),
(130, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:31:27', '2018-07-27 06:31:27'),
(131, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:29', '2018-07-27 06:32:29'),
(132, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:30', '2018-07-27 06:32:30'),
(133, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:33', '2018-07-27 06:32:33'),
(134, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:38', '2018-07-27 06:32:38'),
(135, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:55', '2018-07-27 06:32:55'),
(136, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:33:58', '2018-07-27 06:33:58'),
(137, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:37:58', '2018-07-27 06:37:58'),
(138, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:38:40', '2018-07-27 06:38:40'),
(139, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:39:49', '2018-07-27 06:39:49'),
(140, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:41:06', '2018-07-27 06:41:06'),
(141, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:41:07', '2018-07-27 06:41:07'),
(142, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:41:09', '2018-07-27 06:41:09'),
(143, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:41:41', '2018-07-27 06:41:41'),
(144, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:42:47', '2018-07-27 06:42:47'),
(145, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:43:18', '2018-07-27 06:43:18'),
(146, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:45:14', '2018-07-27 06:45:14'),
(147, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:46:40', '2018-07-27 06:46:40'),
(148, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:47:10', '2018-07-27 06:47:10'),
(149, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:47:20', '2018-07-27 06:47:20'),
(150, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:48:12', '2018-07-27 06:48:12'),
(151, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:48:34', '2018-07-27 06:48:34'),
(152, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:48:57', '2018-07-27 06:48:57'),
(153, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:50:50', '2018-07-27 06:50:50'),
(154, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:52:03', '2018-07-27 06:52:03'),
(155, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:52:11', '2018-07-27 06:52:11'),
(156, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:53:19', '2018-07-27 06:53:19'),
(157, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:53:20', '2018-07-27 06:53:20'),
(158, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:54:23', '2018-07-27 06:54:23'),
(159, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:54:50', '2018-07-27 06:54:50'),
(160, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:09', '2018-07-27 06:55:09'),
(161, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:25', '2018-07-27 06:55:25'),
(162, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:25', '2018-07-27 06:55:25'),
(163, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:26', '2018-07-27 06:55:26'),
(164, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:53', '2018-07-27 06:55:53'),
(165, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:56:55', '2018-07-27 06:56:55'),
(166, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:57:43', '2018-07-27 06:57:43'),
(167, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:57:53', '2018-07-27 06:57:53'),
(168, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:58:02', '2018-07-27 06:58:02'),
(169, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:58:07', '2018-07-27 06:58:07'),
(170, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:58:11', '2018-07-27 06:58:11'),
(171, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:58:16', '2018-07-27 06:58:16'),
(172, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:59:16', '2018-07-27 06:59:16'),
(173, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:24', '2018-07-27 07:02:24'),
(174, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:26', '2018-07-27 07:02:26'),
(175, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:26', '2018-07-27 07:02:26'),
(176, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:27', '2018-07-27 07:02:27'),
(177, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:27', '2018-07-27 07:02:27'),
(178, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:27', '2018-07-27 07:02:27'),
(179, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:28', '2018-07-27 07:02:28'),
(180, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:28', '2018-07-27 07:02:28'),
(181, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:28', '2018-07-27 07:02:28'),
(182, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:29', '2018-07-27 07:02:29'),
(183, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:29', '2018-07-27 07:02:29'),
(184, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:29', '2018-07-27 07:02:29'),
(185, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:52', '2018-07-27 07:02:52'),
(186, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:52', '2018-07-27 07:02:52'),
(187, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:53', '2018-07-27 07:02:53'),
(188, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:06:43', '2018-07-27 07:06:43'),
(189, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:06:44', '2018-07-27 07:06:44'),
(190, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:06:45', '2018-07-27 07:06:45'),
(191, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:14', '2018-07-27 07:07:14'),
(192, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:15', '2018-07-27 07:07:15'),
(193, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:15', '2018-07-27 07:07:15'),
(194, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:15', '2018-07-27 07:07:15'),
(195, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:16', '2018-07-27 07:07:16'),
(196, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:07', '2018-07-27 07:08:07'),
(197, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:08', '2018-07-27 07:08:08'),
(198, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:08', '2018-07-27 07:08:08'),
(199, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:10', '2018-07-27 07:08:10'),
(200, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:10', '2018-07-27 07:08:10'),
(201, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:39', '2018-07-27 07:08:39'),
(202, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:42', '2018-07-27 07:08:42'),
(203, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:42', '2018-07-27 07:08:42'),
(204, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:50', '2018-07-27 07:08:50'),
(205, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:51', '2018-07-27 07:08:51'),
(206, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:09:16', '2018-07-27 07:09:16'),
(207, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:09:33', '2018-07-27 07:09:33'),
(208, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:09:45', '2018-07-27 07:09:45'),
(209, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:10:14', '2018-07-27 07:10:14'),
(210, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:12:57', '2018-07-27 07:12:57'),
(211, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:13:46', '2018-07-27 07:13:46'),
(212, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:14:40', '2018-07-27 07:14:40'),
(213, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:14:41', '2018-07-27 07:14:41'),
(214, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:14:42', '2018-07-27 07:14:42'),
(215, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:15:29', '2018-07-27 07:15:29'),
(216, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:15:44', '2018-07-27 07:15:44'),
(217, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:16:07', '2018-07-27 07:16:07'),
(218, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:19:44', '2018-07-27 07:19:44'),
(219, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:20:26', '2018-07-27 07:20:26'),
(220, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:22:02', '2018-07-27 07:22:02'),
(221, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:22:03', '2018-07-27 07:22:03'),
(222, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:23:45', '2018-07-27 07:23:45'),
(223, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 08:59:36', '2018-07-27 08:59:36'),
(224, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:00:38', '2018-07-27 09:00:38'),
(225, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:13:24', '2018-07-27 09:13:24'),
(226, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:13:31', '2018-07-27 09:13:31'),
(227, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:16:25', '2018-07-27 09:16:25'),
(228, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:16:26', '2018-07-27 09:16:26'),
(229, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:20:38', '2018-07-27 09:20:38'),
(230, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:24:19', '2018-07-27 09:24:19'),
(231, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:26:42', '2018-07-27 09:26:42'),
(232, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:27:36', '2018-07-27 09:27:36'),
(233, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:41:11', '2018-07-27 09:41:11'),
(234, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:54:22', '2018-07-27 09:54:22'),
(235, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:55:07', '2018-07-27 09:55:07'),
(236, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:55:51', '2018-07-27 09:55:51'),
(237, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:07:49', '2018-07-27 10:07:49'),
(238, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:08:03', '2018-07-27 10:08:03'),
(239, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:08:36', '2018-07-27 10:08:36'),
(240, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:09:11', '2018-07-27 10:09:11'),
(241, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:09:43', '2018-07-27 10:09:43'),
(242, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:09:49', '2018-07-27 10:09:49'),
(243, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:10:20', '2018-07-27 10:10:20'),
(244, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:20', '2018-07-27 10:11:20'),
(245, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:21', '2018-07-27 10:11:21'),
(246, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:21', '2018-07-27 10:11:21'),
(247, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:21', '2018-07-27 10:11:21'),
(248, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:22', '2018-07-27 10:11:22'),
(249, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:36', '2018-07-27 10:11:36'),
(250, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:21', '2018-07-27 10:12:21'),
(251, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:21', '2018-07-27 10:12:21'),
(252, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:22', '2018-07-27 10:12:22'),
(253, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:28', '2018-07-27 10:12:28'),
(254, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:35', '2018-07-27 10:12:35'),
(255, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:59', '2018-07-27 10:12:59'),
(256, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:13:40', '2018-07-27 10:13:40'),
(257, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:13:52', '2018-07-27 10:13:52'),
(258, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:13:55', '2018-07-27 10:13:55'),
(259, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:07', '2018-07-27 10:14:07'),
(260, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:08', '2018-07-27 10:14:08'),
(261, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:10', '2018-07-27 10:14:10'),
(262, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:23', '2018-07-27 10:14:23'),
(263, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:45', '2018-07-27 10:14:45'),
(264, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:48', '2018-07-27 10:14:48'),
(265, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:55', '2018-07-27 10:14:55'),
(266, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:15:31', '2018-07-27 10:15:31'),
(267, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:16:01', '2018-07-27 10:16:01'),
(268, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:13:08', '2018-07-27 11:13:08'),
(269, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:27:06', '2018-07-27 11:27:06'),
(270, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:27:24', '2018-07-27 11:27:24'),
(271, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:35:34', '2018-07-27 11:35:34'),
(272, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:36:31', '2018-07-27 11:36:31'),
(273, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:39:31', '2018-07-27 11:39:31'),
(274, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:39:40', '2018-07-27 11:39:40'),
(275, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:47:40', '2018-07-27 11:47:40'),
(276, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:49:17', '2018-07-27 11:49:17'),
(277, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:49:21', '2018-07-27 11:49:21'),
(278, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:50:15', '2018-07-27 11:50:15'),
(279, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:50:37', '2018-07-27 11:50:37'),
(280, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:51:11', '2018-07-27 11:51:11'),
(281, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:51:17', '2018-07-27 11:51:17'),
(282, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:51:54', '2018-07-27 11:51:54'),
(283, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:29', '2018-07-27 11:54:29'),
(284, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:31', '2018-07-27 11:54:31'),
(285, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:33', '2018-07-27 11:54:33'),
(286, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:35', '2018-07-27 11:54:35'),
(287, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:37', '2018-07-27 11:54:37'),
(288, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 12:28:10', '2018-07-27 12:28:10'),
(289, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 12:28:34', '2018-07-27 12:28:34'),
(290, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 12:30:00', '2018-07-27 12:30:00'),
(291, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 12:32:13', '2018-07-27 12:32:13'),
(292, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 13:41:14', '2018-07-27 13:41:14'),
(293, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-28 05:37:46', '2018-07-28 05:37:46'),
(294, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 05:39:52', '2018-07-28 05:39:52'),
(295, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 05:55:23', '2018-07-28 05:55:23'),
(296, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:14:04', '2018-07-28 11:14:04'),
(297, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:14:47', '2018-07-28 11:14:47'),
(298, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:19:32', '2018-07-28 11:19:32'),
(299, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:20:45', '2018-07-28 11:20:45'),
(300, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:20:52', '2018-07-28 11:20:52'),
(301, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:21:00', '2018-07-28 11:21:00'),
(302, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:21:59', '2018-07-28 11:21:59'),
(303, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:22:14', '2018-07-28 11:22:14'),
(304, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:24:29', '2018-07-28 11:24:29'),
(305, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:38:21', '2018-07-28 11:38:21'),
(306, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:38:59', '2018-07-28 11:38:59'),
(307, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:40:16', '2018-07-28 11:40:16'),
(308, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:00:36', '2018-07-28 12:00:36'),
(309, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:02:48', '2018-07-28 12:02:48'),
(310, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:04:45', '2018-07-28 12:04:45'),
(311, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:06:21', '2018-07-28 12:06:21'),
(312, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:23:50', '2018-07-28 12:23:50'),
(313, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 08:54:36', '2018-07-30 08:54:36'),
(314, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:00:08', '2018-07-30 09:00:08'),
(315, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:12', '2018-07-30 09:02:12'),
(316, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:12', '2018-07-30 09:02:12'),
(317, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:13', '2018-07-30 09:02:13'),
(318, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:13', '2018-07-30 09:02:13'),
(319, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:14', '2018-07-30 09:02:14'),
(320, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:14', '2018-07-30 09:02:14'),
(321, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:03', '2018-07-30 09:03:03'),
(322, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:19', '2018-07-30 09:03:19'),
(323, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:20', '2018-07-30 09:03:20'),
(324, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:20', '2018-07-30 09:03:20'),
(325, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:20', '2018-07-30 09:03:20'),
(326, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:20', '2018-07-30 09:03:20'),
(327, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:32', '2018-07-30 09:03:32'),
(328, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:33', '2018-07-30 09:03:33'),
(329, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:38', '2018-07-30 09:03:38'),
(330, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:43', '2018-07-30 09:03:43'),
(331, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:51', '2018-07-30 09:03:51'),
(332, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:52', '2018-07-30 09:03:52'),
(333, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:04:27', '2018-07-30 09:04:27'),
(334, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:04:27', '2018-07-30 09:04:27'),
(335, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:02', '2018-07-30 09:05:02'),
(336, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:02', '2018-07-30 09:05:02'),
(337, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:03', '2018-07-30 09:05:03'),
(338, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:53', '2018-07-30 09:05:53'),
(339, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:54', '2018-07-30 09:05:54'),
(340, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:54', '2018-07-30 09:05:54'),
(341, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:56', '2018-07-30 09:05:56'),
(342, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:57', '2018-07-30 09:05:57'),
(343, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:06:03', '2018-07-30 09:06:03'),
(344, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:06:04', '2018-07-30 09:06:04'),
(345, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:06:04', '2018-07-30 09:06:04'),
(346, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:06:10', '2018-07-30 09:06:10'),
(347, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:07:00', '2018-07-30 09:07:00'),
(348, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:07:02', '2018-07-30 09:07:02'),
(349, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:07:14', '2018-07-30 09:07:14'),
(350, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:08:48', '2018-07-30 09:08:48'),
(351, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:09:27', '2018-07-30 09:09:27'),
(352, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:09:41', '2018-07-30 09:09:41'),
(353, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:14:35', '2018-07-30 09:14:35'),
(354, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:42:38', '2018-07-30 09:42:38'),
(355, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:43:49', '2018-07-30 09:43:49'),
(356, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:44:59', '2018-07-30 09:44:59'),
(357, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:45:09', '2018-07-30 09:45:09'),
(358, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:45:18', '2018-07-30 09:45:18'),
(359, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:45:34', '2018-07-30 09:45:34'),
(360, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:45:46', '2018-07-30 09:45:46'),
(361, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:46:23', '2018-07-30 09:46:23'),
(362, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:46:30', '2018-07-30 09:46:30'),
(363, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:46:43', '2018-07-30 09:46:43'),
(364, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:46:51', '2018-07-30 09:46:51'),
(365, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:47:35', '2018-07-30 09:47:35'),
(366, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:47:47', '2018-07-30 09:47:47'),
(367, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:47:57', '2018-07-30 09:47:57'),
(368, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:50:21', '2018-07-30 09:50:21'),
(369, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:51:05', '2018-07-30 09:51:05'),
(370, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:51:09', '2018-07-30 09:51:09'),
(371, NULL, '//', '3', '2018-07-30 09:51:26', '2018-07-30 09:51:26'),
(372, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:52:00', '2018-07-30 09:52:00'),
(373, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:56:22', '2018-07-30 09:56:22'),
(374, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:09', '2018-07-30 09:57:09'),
(375, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:10', '2018-07-30 09:57:10'),
(376, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:11', '2018-07-30 09:57:11'),
(377, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:11', '2018-07-30 09:57:11'),
(378, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:54', '2018-07-30 09:57:54'),
(379, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:03', '2018-07-30 09:58:03'),
(380, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:03', '2018-07-30 09:58:03'),
(381, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:04', '2018-07-30 09:58:04'),
(382, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:10', '2018-07-30 09:58:10'),
(383, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:39', '2018-07-30 09:58:39'),
(384, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:39', '2018-07-30 09:58:39'),
(385, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:59:05', '2018-07-30 09:59:05'),
(386, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:59:15', '2018-07-30 09:59:15'),
(387, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:59:58', '2018-07-30 09:59:58'),
(388, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:00:01', '2018-07-30 10:00:01'),
(389, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:00:03', '2018-07-30 10:00:03'),
(390, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:00:23', '2018-07-30 10:00:23'),
(391, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:00:27', '2018-07-30 10:00:27'),
(392, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:01:01', '2018-07-30 10:01:01'),
(393, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:01:13', '2018-07-30 10:01:13'),
(394, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:01:18', '2018-07-30 10:01:18'),
(395, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:03:25', '2018-07-30 10:03:25'),
(396, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:03:47', '2018-07-30 10:03:47'),
(397, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:03:56', '2018-07-30 10:03:56'),
(398, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:04:14', '2018-07-30 10:04:14'),
(399, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:19:23', '2018-07-30 10:19:23'),
(400, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:34:14', '2018-07-30 10:34:14'),
(401, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:35:48', '2018-07-30 10:35:48'),
(402, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:36:56', '2018-07-30 10:36:56'),
(403, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:37:04', '2018-07-30 10:37:04'),
(404, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:37:18', '2018-07-30 10:37:18'),
(405, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 11:00:47', '2018-07-30 11:00:47'),
(406, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 11:01:04', '2018-07-30 11:01:04'),
(407, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 11:01:21', '2018-07-30 11:01:21'),
(408, 14, 'http://localhost/fourgirls/public', '3', '2018-07-30 11:51:46', '2018-07-30 11:51:46'),
(409, 14, 'http://localhost/fourgirls/public', '3', '2018-07-30 12:09:45', '2018-07-30 12:09:45'),
(410, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 03:21:05', '2018-07-31 03:21:05'),
(411, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 03:29:41', '2018-07-31 03:29:41'),
(412, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 05:23:33', '2018-07-31 05:23:33'),
(413, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 05:39:32', '2018-07-31 05:39:32'),
(414, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:11:09', '2018-07-31 06:11:09'),
(415, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:11:09', '2018-07-31 06:11:09'),
(416, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:11:30', '2018-07-31 06:11:30'),
(417, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:31', '2018-07-31 06:35:31'),
(418, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:32', '2018-07-31 06:35:32'),
(419, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:33', '2018-07-31 06:35:33'),
(420, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:34', '2018-07-31 06:35:34'),
(421, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:34', '2018-07-31 06:35:34'),
(422, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:35', '2018-07-31 06:35:35'),
(423, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:36', '2018-07-31 06:35:36'),
(424, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:38', '2018-07-31 06:35:38'),
(425, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:38', '2018-07-31 06:35:38'),
(426, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:39', '2018-07-31 06:35:39'),
(427, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:40', '2018-07-31 06:35:40'),
(428, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:40', '2018-07-31 06:35:40'),
(429, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:41', '2018-07-31 06:35:41'),
(430, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:11', '2018-07-31 06:37:11'),
(431, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:12', '2018-07-31 06:37:12'),
(432, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:13', '2018-07-31 06:37:13'),
(433, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:14', '2018-07-31 06:37:14'),
(434, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:15', '2018-07-31 06:37:15'),
(435, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:16', '2018-07-31 06:37:16'),
(436, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:16', '2018-07-31 06:37:16'),
(437, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:17', '2018-07-31 06:37:17'),
(438, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:00', '2018-07-31 06:40:00'),
(439, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:01', '2018-07-31 06:40:01'),
(440, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:02', '2018-07-31 06:40:02'),
(441, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:02', '2018-07-31 06:40:02'),
(442, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:02', '2018-07-31 06:40:02'),
(443, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:03', '2018-07-31 06:40:03'),
(444, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:03', '2018-07-31 06:40:03'),
(445, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:05', '2018-07-31 06:40:05'),
(446, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:05', '2018-07-31 06:40:05'),
(447, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:06', '2018-07-31 06:40:06'),
(448, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:07', '2018-07-31 06:40:07'),
(449, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:07', '2018-07-31 06:40:07'),
(450, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:08', '2018-07-31 06:40:08'),
(451, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:08', '2018-07-31 06:40:08'),
(452, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:45', '2018-07-31 06:47:45'),
(453, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:46', '2018-07-31 06:47:46'),
(454, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:46', '2018-07-31 06:47:46'),
(455, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:47', '2018-07-31 06:47:47'),
(456, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:48', '2018-07-31 06:47:48'),
(457, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:48', '2018-07-31 06:47:48'),
(458, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:49', '2018-07-31 06:47:49'),
(459, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:49', '2018-07-31 06:47:49'),
(460, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:50', '2018-07-31 06:47:50'),
(461, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:50', '2018-07-31 06:47:50'),
(462, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:50', '2018-07-31 06:47:50'),
(463, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:51', '2018-07-31 06:47:51'),
(464, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:51', '2018-07-31 06:47:51'),
(465, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:52', '2018-07-31 06:47:52'),
(466, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:52', '2018-07-31 06:47:52'),
(467, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:52', '2018-07-31 06:47:52'),
(468, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:53', '2018-07-31 06:47:53'),
(469, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:53', '2018-07-31 06:47:53'),
(470, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:54', '2018-07-31 06:47:54'),
(471, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:54', '2018-07-31 06:47:54'),
(472, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:55', '2018-07-31 06:47:55'),
(473, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:56', '2018-07-31 06:47:56'),
(474, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:56', '2018-07-31 06:47:56'),
(475, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:48:16', '2018-07-31 06:48:16'),
(476, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:02', '2018-07-31 06:51:02'),
(477, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:03', '2018-07-31 06:51:03'),
(478, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:03', '2018-07-31 06:51:03'),
(479, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:04', '2018-07-31 06:51:04'),
(480, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:04', '2018-07-31 06:51:04'),
(481, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:05', '2018-07-31 06:51:05'),
(482, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:05', '2018-07-31 06:51:05'),
(483, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:38', '2018-07-31 06:51:38'),
(484, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:39', '2018-07-31 06:51:39'),
(485, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:39', '2018-07-31 06:51:39'),
(486, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:40', '2018-07-31 06:51:40'),
(487, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:40', '2018-07-31 06:51:40'),
(488, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:41', '2018-07-31 06:51:41'),
(489, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:41', '2018-07-31 06:51:41'),
(490, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:42', '2018-07-31 06:51:42'),
(491, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:42', '2018-07-31 06:51:42'),
(492, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:43', '2018-07-31 06:51:43'),
(493, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:52', '2018-07-31 06:51:52'),
(494, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:52', '2018-07-31 06:51:52'),
(495, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:53', '2018-07-31 06:51:53'),
(496, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:15', '2018-07-31 06:52:15'),
(497, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:16', '2018-07-31 06:52:16'),
(498, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:16', '2018-07-31 06:52:16'),
(499, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:17', '2018-07-31 06:52:17'),
(500, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:17', '2018-07-31 06:52:17'),
(501, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:54:40', '2018-07-31 06:54:40'),
(502, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:54:42', '2018-07-31 06:54:42'),
(503, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:57:51', '2018-07-31 06:57:51'),
(504, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:57:53', '2018-07-31 06:57:53'),
(505, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:58:41', '2018-07-31 06:58:41'),
(506, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:58:43', '2018-07-31 06:58:43'),
(507, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:58:45', '2018-07-31 06:58:45'),
(508, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 08:37:21', '2018-07-31 08:37:21'),
(509, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 08:48:06', '2018-07-31 08:48:06');
INSERT INTO `visitors` (`id`, `user_id`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(510, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:16:14', '2018-07-31 10:16:14'),
(511, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:16:20', '2018-07-31 10:16:20'),
(512, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:16:27', '2018-07-31 10:16:27'),
(513, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:17:20', '2018-07-31 10:17:20'),
(514, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:18:04', '2018-07-31 10:18:04'),
(515, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:19:19', '2018-07-31 10:19:19'),
(516, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:21:02', '2018-07-31 10:21:02'),
(517, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:21:05', '2018-07-31 10:21:05'),
(518, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:21:05', '2018-07-31 10:21:05'),
(519, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:21:06', '2018-07-31 10:21:06'),
(520, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:13:23', '2018-07-31 11:13:23'),
(521, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:14:03', '2018-07-31 11:14:03'),
(522, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:14:53', '2018-07-31 11:14:53'),
(523, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:01', '2018-07-31 11:15:01'),
(524, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:16', '2018-07-31 11:15:16'),
(525, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:21', '2018-07-31 11:15:21'),
(526, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:22', '2018-07-31 11:15:22'),
(527, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:22', '2018-07-31 11:15:22'),
(528, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:22', '2018-07-31 11:15:22'),
(529, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:23', '2018-07-31 11:15:23'),
(530, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:24', '2018-07-31 11:15:24'),
(531, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:24', '2018-07-31 11:15:24'),
(532, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:25', '2018-07-31 11:15:25'),
(533, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:25', '2018-07-31 11:15:25'),
(534, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:26', '2018-07-31 11:15:26'),
(535, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:26', '2018-07-31 11:15:26'),
(536, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:26', '2018-07-31 11:15:26'),
(537, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:26', '2018-07-31 11:15:26'),
(538, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:16:08', '2018-07-31 11:16:08'),
(539, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:17:53', '2018-07-31 11:17:53'),
(540, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:19:27', '2018-07-31 11:19:27'),
(541, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:21:21', '2018-07-31 11:21:21'),
(542, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:44:42', '2018-07-31 11:44:42'),
(543, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 12:08:31', '2018-07-31 12:08:31'),
(544, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 02:06:50', '2018-08-01 02:06:50'),
(545, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:23:03', '2018-08-01 04:23:03'),
(546, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:23:28', '2018-08-01 04:23:28'),
(547, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:03', '2018-08-01 04:24:03'),
(548, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:09', '2018-08-01 04:24:09'),
(549, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:10', '2018-08-01 04:24:10'),
(550, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:11', '2018-08-01 04:24:11'),
(551, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:11', '2018-08-01 04:24:11'),
(552, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:15', '2018-08-01 04:24:15'),
(553, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:25:07', '2018-08-01 04:25:07'),
(554, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:25:17', '2018-08-01 04:25:17'),
(555, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 10:03:45', '2018-08-01 10:03:45'),
(556, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 11:15:12', '2018-08-01 11:15:12'),
(557, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 12:18:26', '2018-08-01 12:18:26'),
(558, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 12:42:33', '2018-08-01 12:42:33'),
(559, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 12:46:45', '2018-08-01 12:46:45'),
(560, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 12:47:05', '2018-08-01 12:47:05'),
(561, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-02 03:25:46', '2018-08-02 03:25:46'),
(562, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 02:19:57', '2018-08-03 02:19:57'),
(563, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:02:00', '2018-08-03 10:02:00'),
(564, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:21:30', '2018-08-03 10:21:30'),
(565, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:30:45', '2018-08-03 10:30:45'),
(566, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:34:03', '2018-08-03 10:34:03'),
(567, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:43:29', '2018-08-03 10:43:29'),
(568, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:43:31', '2018-08-03 10:43:31'),
(569, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 11:28:02', '2018-08-03 11:28:02'),
(570, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 11:38:05', '2018-08-03 11:38:05'),
(571, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 12:08:15', '2018-08-03 12:08:15'),
(572, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 12:08:33', '2018-08-03 12:08:33'),
(573, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-04 05:48:56', '2018-08-04 05:48:56'),
(574, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-04 07:49:31', '2018-08-04 07:49:31'),
(575, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-07 05:19:22', '2018-08-07 05:19:22'),
(576, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-09 05:21:07', '2018-08-09 05:21:07'),
(577, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-09 05:43:15', '2018-08-09 05:43:15'),
(578, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-09 10:14:40', '2018-08-09 10:14:40'),
(579, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:10:19', '2018-08-13 02:10:19'),
(580, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:17:51', '2018-08-13 02:17:51'),
(581, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:23:12', '2018-08-13 02:23:12'),
(582, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:36:44', '2018-08-13 02:36:44'),
(583, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:36:47', '2018-08-13 02:36:47'),
(584, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:39:04', '2018-08-13 02:39:04'),
(585, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:39:33', '2018-08-13 02:39:33'),
(586, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:41:03', '2018-08-13 02:41:03'),
(587, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:41:16', '2018-08-13 02:41:16'),
(588, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:42:52', '2018-08-13 02:42:52'),
(589, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:46', '2018-08-13 02:47:46'),
(590, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:47', '2018-08-13 02:47:47'),
(591, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:48', '2018-08-13 02:47:48'),
(592, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:49', '2018-08-13 02:47:49'),
(593, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:50', '2018-08-13 02:47:50'),
(594, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:07:34', '2018-08-13 03:07:34'),
(595, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:07:53', '2018-08-13 03:07:53'),
(596, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:08:45', '2018-08-13 03:08:45'),
(597, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:09:23', '2018-08-13 03:09:23'),
(598, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:09:54', '2018-08-13 03:09:54'),
(599, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:11:22', '2018-08-13 03:11:22'),
(600, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:12:30', '2018-08-13 03:12:30'),
(601, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:15:11', '2018-08-13 03:15:11'),
(602, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:19:56', '2018-08-13 03:19:56'),
(603, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:19:58', '2018-08-13 03:19:58'),
(604, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:19:58', '2018-08-13 03:19:58'),
(605, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:01', '2018-08-13 03:20:01'),
(606, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:06', '2018-08-13 03:20:06'),
(607, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:06', '2018-08-13 03:20:06'),
(608, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:36', '2018-08-13 03:20:36'),
(609, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:37', '2018-08-13 03:20:37'),
(610, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:21:22', '2018-08-13 03:21:22'),
(611, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:21:23', '2018-08-13 03:21:23'),
(612, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:21:36', '2018-08-13 03:21:36'),
(613, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:09', '2018-08-13 03:22:09'),
(614, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:18', '2018-08-13 03:22:18'),
(615, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:20', '2018-08-13 03:22:20'),
(616, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:33', '2018-08-13 03:22:33'),
(617, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:34', '2018-08-13 03:22:34'),
(618, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:57', '2018-08-13 03:22:57'),
(619, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:23:46', '2018-08-13 03:23:46'),
(620, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:23:48', '2018-08-13 03:23:48'),
(621, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:24:12', '2018-08-13 03:24:12'),
(622, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:24:13', '2018-08-13 03:24:13'),
(623, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:24:13', '2018-08-13 03:24:13'),
(624, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:24:13', '2018-08-13 03:24:13'),
(625, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:26:07', '2018-08-13 03:26:07'),
(626, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:26:57', '2018-08-13 03:26:57'),
(627, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:27:21', '2018-08-13 03:27:21'),
(628, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:27:32', '2018-08-13 03:27:32'),
(629, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:27:57', '2018-08-13 03:27:57'),
(630, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:28:14', '2018-08-13 03:28:14'),
(631, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:28:35', '2018-08-13 03:28:35'),
(632, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:29:44', '2018-08-13 03:29:44'),
(633, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:05', '2018-08-13 03:31:05'),
(634, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:16', '2018-08-13 03:31:16'),
(635, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:19', '2018-08-13 03:31:19'),
(636, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:19', '2018-08-13 03:31:19'),
(637, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:35', '2018-08-13 03:31:35'),
(638, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:32:19', '2018-08-13 03:32:19'),
(639, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:32:37', '2018-08-13 03:32:37'),
(640, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:33:04', '2018-08-13 03:33:04'),
(641, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:33:46', '2018-08-13 03:33:46'),
(642, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:33:52', '2018-08-13 03:33:52'),
(643, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:35:29', '2018-08-13 03:35:29'),
(644, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:35:30', '2018-08-13 03:35:30'),
(645, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:37:35', '2018-08-13 03:37:35'),
(646, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:37:36', '2018-08-13 03:37:36'),
(647, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:38:59', '2018-08-13 03:38:59'),
(648, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:39:00', '2018-08-13 03:39:00'),
(649, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:41:49', '2018-08-13 03:41:49'),
(650, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:44:44', '2018-08-13 03:44:44'),
(651, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:45:56', '2018-08-13 03:45:56'),
(652, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:46:46', '2018-08-13 03:46:46'),
(653, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:48:34', '2018-08-13 03:48:34'),
(654, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:49:07', '2018-08-13 03:49:07'),
(655, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:51:38', '2018-08-13 03:51:38'),
(656, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:52:08', '2018-08-13 03:52:08'),
(657, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:54:23', '2018-08-13 03:54:23'),
(658, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:54:53', '2018-08-13 03:54:53'),
(659, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:58:50', '2018-08-13 03:58:50'),
(660, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:58:53', '2018-08-13 03:58:53'),
(661, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 04:10:12', '2018-08-13 04:10:12'),
(662, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 04:11:43', '2018-08-13 04:11:43'),
(663, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:34:25', '2018-08-13 07:34:25'),
(664, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:41:43', '2018-08-13 07:41:43'),
(665, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:43:41', '2018-08-13 07:43:41'),
(666, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:44:14', '2018-08-13 07:44:14'),
(667, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:44:34', '2018-08-13 07:44:34'),
(668, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:44:55', '2018-08-13 07:44:55'),
(669, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:44:57', '2018-08-13 07:44:57'),
(670, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:45:18', '2018-08-13 07:45:18'),
(671, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:45:37', '2018-08-13 07:45:37'),
(672, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:45:42', '2018-08-13 07:45:42'),
(673, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:51:37', '2018-08-13 07:51:37'),
(674, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:54:14', '2018-08-13 07:54:14'),
(675, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:55:52', '2018-08-13 07:55:52'),
(676, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:55:53', '2018-08-13 07:55:53'),
(677, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:57:16', '2018-08-13 07:57:16'),
(678, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:59:01', '2018-08-13 07:59:01'),
(679, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:01:14', '2018-08-13 08:01:14'),
(680, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:01:40', '2018-08-13 08:01:40'),
(681, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:01:44', '2018-08-13 08:01:44'),
(682, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:02:12', '2018-08-13 08:02:12'),
(683, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:03:00', '2018-08-13 08:03:00'),
(684, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:03:03', '2018-08-13 08:03:03'),
(685, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:03:30', '2018-08-13 08:03:30'),
(686, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:03:39', '2018-08-13 08:03:39'),
(687, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:04:27', '2018-08-13 08:04:27'),
(688, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:04:47', '2018-08-13 08:04:47'),
(689, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:19:36', '2018-08-13 08:19:36'),
(690, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:21:41', '2018-08-13 08:21:41'),
(691, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:22:01', '2018-08-13 08:22:01'),
(692, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:35:56', '2018-08-13 08:35:56'),
(693, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:10:11', '2018-08-13 09:10:11'),
(694, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:15:19', '2018-08-13 09:15:19'),
(695, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:26:27', '2018-08-13 09:26:27'),
(696, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:36:10', '2018-08-13 09:36:10'),
(697, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:36:18', '2018-08-13 09:36:18'),
(698, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:41:33', '2018-08-13 09:41:33'),
(699, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:44:21', '2018-08-13 09:44:21'),
(700, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 10:04:17', '2018-08-13 10:04:17'),
(701, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 10:06:47', '2018-08-13 10:06:47'),
(702, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 10:23:07', '2018-08-13 10:23:07'),
(703, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:04:16', '2018-08-13 11:04:16'),
(704, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:08:47', '2018-08-13 11:08:47'),
(705, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:17:05', '2018-08-13 11:17:05'),
(706, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:17:52', '2018-08-13 11:17:52'),
(707, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:18:12', '2018-08-13 11:18:12'),
(708, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:18:28', '2018-08-13 11:18:28'),
(709, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:19:00', '2018-08-13 11:19:00'),
(710, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:19:42', '2018-08-13 11:19:42'),
(711, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:20:31', '2018-08-13 11:20:31'),
(712, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:21:18', '2018-08-13 11:21:18'),
(713, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:21:42', '2018-08-13 11:21:42'),
(714, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:22:11', '2018-08-13 11:22:11'),
(715, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:22:46', '2018-08-13 11:22:46'),
(716, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:02:27', '2018-08-14 02:02:27'),
(717, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:47:07', '2018-08-14 02:47:07'),
(718, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:48:44', '2018-08-14 02:48:44'),
(719, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:49:45', '2018-08-14 02:49:45'),
(720, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:50:15', '2018-08-14 02:50:15'),
(721, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:50:41', '2018-08-14 02:50:41'),
(722, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:50:49', '2018-08-14 02:50:49'),
(723, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 03:19:59', '2018-08-14 03:19:59'),
(724, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 03:23:06', '2018-08-14 03:23:06'),
(725, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 03:23:20', '2018-08-14 03:23:20'),
(726, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 03:30:11', '2018-08-14 03:30:11'),
(727, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 08:34:03', '2018-08-14 08:34:03'),
(728, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 09:19:41', '2018-08-14 09:19:41'),
(729, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 02:19:31', '2018-08-16 02:19:31'),
(730, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:28:46', '2018-08-16 05:28:46'),
(731, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:30:39', '2018-08-16 05:30:39'),
(732, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:33:23', '2018-08-16 05:33:23'),
(733, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:35:09', '2018-08-16 05:35:09'),
(734, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:35:26', '2018-08-16 05:35:26'),
(735, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:36:05', '2018-08-16 05:36:05'),
(736, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:40:37', '2018-08-16 05:40:37'),
(737, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:40:49', '2018-08-16 05:40:49'),
(738, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:41:18', '2018-08-16 05:41:18'),
(739, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:41:34', '2018-08-16 05:41:34'),
(740, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:10:53', '2018-08-16 06:10:53'),
(741, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:13:55', '2018-08-16 06:13:55'),
(742, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:43:32', '2018-08-16 06:43:32'),
(743, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:43:33', '2018-08-16 06:43:33'),
(744, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:44:43', '2018-08-16 06:44:43'),
(745, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:46:26', '2018-08-16 06:46:26'),
(746, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:46:31', '2018-08-16 06:46:31'),
(747, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:47:30', '2018-08-16 06:47:30'),
(748, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 10:22:57', '2018-08-16 10:22:57'),
(749, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-16 11:39:44', '2018-08-16 11:39:44'),
(750, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 02:53:57', '2018-08-17 02:53:57'),
(751, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 03:23:12', '2018-08-17 03:23:12'),
(752, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 03:39:21', '2018-08-17 03:39:21'),
(753, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 03:40:01', '2018-08-17 03:40:01'),
(754, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 04:47:40', '2018-08-17 04:47:40'),
(755, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:02:21', '2018-08-17 05:02:21'),
(756, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:02:26', '2018-08-17 05:02:26'),
(757, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:21:09', '2018-08-17 05:21:09'),
(758, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:28:10', '2018-08-17 05:28:10'),
(759, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:38:32', '2018-08-17 05:38:32'),
(760, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:38:46', '2018-08-17 05:38:46'),
(761, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 07:10:20', '2018-08-17 07:10:20'),
(762, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 09:46:24', '2018-08-17 09:46:24'),
(763, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-17 10:21:16', '2018-08-17 10:21:16'),
(764, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 11:09:25', '2018-08-17 11:09:25'),
(765, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-18 03:33:52', '2018-08-18 03:33:52'),
(766, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-18 03:35:13', '2018-08-18 03:35:13'),
(767, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-18 06:14:21', '2018-08-18 06:14:21'),
(768, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-18 11:33:50', '2018-08-18 11:33:50'),
(769, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-05 02:19:39', '2018-09-05 02:19:39'),
(770, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-06 05:56:06', '2018-09-06 05:56:06'),
(771, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-06 05:57:05', '2018-09-06 05:57:05'),
(772, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-06 06:09:34', '2018-09-06 06:09:34'),
(773, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-06 11:07:39', '2018-09-06 11:07:39'),
(774, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 05:32:07', '2018-09-11 05:32:07'),
(775, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 06:13:22', '2018-09-11 06:13:22'),
(776, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 06:22:21', '2018-09-11 06:22:21'),
(777, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 07:21:59', '2018-09-11 07:21:59'),
(778, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 07:24:08', '2018-09-11 07:24:08'),
(779, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 07:55:19', '2018-09-11 07:55:19'),
(780, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 07:58:31', '2018-09-11 07:58:31'),
(781, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 08:02:00', '2018-09-11 08:02:00'),
(782, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 08:51:31', '2018-09-11 08:51:31'),
(783, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 08:51:32', '2018-09-11 08:51:32'),
(784, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 08:51:38', '2018-09-11 08:51:38'),
(785, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 09:09:33', '2018-09-11 09:09:33'),
(786, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-17 07:07:16', '2018-09-17 07:07:16'),
(787, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-17 07:54:53', '2018-09-17 07:54:53'),
(788, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-17 08:01:01', '2018-09-17 08:01:01'),
(789, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-18 02:06:25', '2018-09-18 02:06:25'),
(790, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-18 07:22:19', '2018-09-18 07:22:19'),
(791, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-18 07:25:55', '2018-09-18 07:25:55'),
(792, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-18 07:48:19', '2018-09-18 07:48:19'),
(793, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-18 08:29:37', '2018-09-18 08:29:37'),
(794, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 04:16:29', '2018-09-19 04:16:29'),
(795, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 04:21:19', '2018-09-19 04:21:19'),
(796, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 04:30:33', '2018-09-19 04:30:33'),
(797, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 05:38:35', '2018-09-19 05:38:35'),
(798, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 07:33:17', '2018-09-19 07:33:17'),
(799, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 09:13:53', '2018-09-19 09:13:53'),
(800, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 09:32:19', '2018-09-19 09:32:19'),
(801, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 10:54:06', '2018-09-19 10:54:06'),
(802, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 11:36:13', '2018-09-19 11:36:13'),
(803, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 12:35:11', '2018-09-19 12:35:11'),
(804, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 05:47:18', '2018-09-20 05:47:18'),
(805, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:11:24', '2018-09-20 08:11:24'),
(806, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:17:06', '2018-09-20 08:17:06'),
(807, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:17:21', '2018-09-20 08:17:21'),
(808, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:17:29', '2018-09-20 08:17:29'),
(809, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:17:44', '2018-09-20 08:17:44'),
(810, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:45:14', '2018-09-20 08:45:14'),
(811, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:47:02', '2018-09-20 08:47:02'),
(812, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:47:32', '2018-09-20 08:47:32'),
(813, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:48:36', '2018-09-20 08:48:36'),
(814, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:49:14', '2018-09-20 08:49:14'),
(815, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:53:00', '2018-09-20 08:53:00'),
(816, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:53:47', '2018-09-20 08:53:47'),
(817, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:01:39', '2018-09-20 09:01:39'),
(818, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:02:05', '2018-09-20 09:02:05'),
(819, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:03:48', '2018-09-20 09:03:48'),
(820, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:04:25', '2018-09-20 09:04:25'),
(821, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:18:42', '2018-09-20 09:18:42'),
(822, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:23:38', '2018-09-20 09:23:38'),
(823, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 13:07:43', '2018-09-20 13:07:43'),
(824, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 13:52:06', '2018-09-20 13:52:06'),
(825, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 02:04:25', '2018-09-24 02:04:25'),
(826, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 02:11:41', '2018-09-24 02:11:41'),
(827, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 02:11:52', '2018-09-24 02:11:52'),
(828, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 04:57:24', '2018-09-24 04:57:24'),
(829, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:05:19', '2018-09-24 05:05:19'),
(830, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:22:48', '2018-09-24 05:22:48'),
(831, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:23:41', '2018-09-24 05:23:41'),
(832, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:30:11', '2018-09-24 05:30:11'),
(833, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:59:44', '2018-09-24 05:59:44'),
(834, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 08:48:24', '2018-09-24 08:48:24'),
(835, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 08:48:39', '2018-09-24 08:48:39'),
(836, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 02:02:24', '2018-09-25 02:02:24'),
(837, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 04:19:19', '2018-09-25 04:19:19'),
(838, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 04:29:28', '2018-09-25 04:29:28'),
(839, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-25 06:00:58', '2018-09-25 06:00:58'),
(840, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-25 07:41:26', '2018-09-25 07:41:26'),
(841, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 08:03:34', '2018-09-25 08:03:34'),
(842, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 08:20:29', '2018-09-25 08:20:29'),
(843, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 08:20:59', '2018-09-25 08:20:59'),
(844, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:03:10', '2018-09-25 09:03:10'),
(845, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:30:13', '2018-09-25 09:30:13'),
(846, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:32:10', '2018-09-25 09:32:10'),
(847, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:32:11', '2018-09-25 09:32:11'),
(848, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:32:45', '2018-09-25 09:32:45'),
(849, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:32:47', '2018-09-25 09:32:47'),
(850, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:34:58', '2018-09-25 09:34:58'),
(851, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:36:43', '2018-09-25 09:36:43'),
(852, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 10:16:09', '2018-09-25 10:16:09'),
(853, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 10:47:56', '2018-09-25 10:47:56'),
(854, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-26 02:07:47', '2018-09-26 02:07:47'),
(855, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:19:08', '2018-09-26 05:19:08'),
(856, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:27:07', '2018-09-26 05:27:07'),
(857, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:30:32', '2018-09-26 05:30:32'),
(858, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:31:03', '2018-09-26 05:31:03'),
(859, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:33:23', '2018-09-26 05:33:23'),
(860, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:34:48', '2018-09-26 05:34:48'),
(861, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:35:07', '2018-09-26 05:35:07'),
(862, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 06:32:51', '2018-09-26 06:32:51'),
(863, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-26 07:29:33', '2018-09-26 07:29:33'),
(864, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-26 07:34:45', '2018-09-26 07:34:45'),
(865, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-28 04:12:36', '2018-09-28 04:12:36'),
(866, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-30 03:20:54', '2018-09-30 03:20:54'),
(867, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-30 03:21:43', '2018-09-30 03:21:43'),
(868, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-03 01:58:57', '2018-10-03 01:58:57'),
(869, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-03 01:59:00', '2018-10-03 01:59:00'),
(870, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-03 02:10:34', '2018-10-03 02:10:34'),
(871, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-03 02:11:24', '2018-10-03 02:11:24'),
(872, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-09 08:51:17', '2018-10-09 08:51:17'),
(873, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 02:41:13', '2018-10-10 02:41:13'),
(874, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 04:05:56', '2018-10-10 04:05:56'),
(875, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 04:23:45', '2018-10-10 04:23:45'),
(876, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 04:37:59', '2018-10-10 04:37:59'),
(877, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 08:40:18', '2018-10-10 08:40:18'),
(878, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 11:12:54', '2018-10-10 11:12:54'),
(879, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 11:23:51', '2018-10-10 11:23:51'),
(880, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-11 02:22:53', '2018-10-11 02:22:53'),
(881, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-11 07:19:31', '2018-10-11 07:19:31'),
(882, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-30 11:24:54', '2018-10-30 11:24:54'),
(883, NULL, 'http://localhost/fourgirls/public', '::1', '2018-11-21 04:31:06', '2018-11-21 04:31:06'),
(884, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:32:57', '2018-11-21 07:32:57'),
(885, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:33:40', '2018-11-21 07:33:40'),
(886, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:50:59', '2018-11-21 07:50:59'),
(887, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:51:00', '2018-11-21 07:51:00'),
(888, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:53:06', '2018-11-21 07:53:06'),
(889, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:14:27', '2018-11-21 08:14:27'),
(890, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:14:30', '2018-11-21 08:14:30'),
(891, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:14:31', '2018-11-21 08:14:31'),
(892, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:14:32', '2018-11-21 08:14:32'),
(893, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:16:37', '2018-11-21 08:16:37'),
(894, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:13', '2018-11-21 08:17:13'),
(895, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:17', '2018-11-21 08:17:17'),
(896, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:18', '2018-11-21 08:17:18'),
(897, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:19', '2018-11-21 08:17:19'),
(898, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:21', '2018-11-21 08:17:21'),
(899, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:22', '2018-11-21 08:17:22'),
(900, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:23', '2018-11-21 08:17:23'),
(901, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:20:12', '2018-11-21 08:20:12'),
(902, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:20:30', '2018-11-21 08:20:30'),
(903, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:20:32', '2018-11-21 08:20:32'),
(904, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:20:33', '2018-11-21 08:20:33'),
(905, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:21:15', '2018-11-21 08:21:15'),
(906, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:21:22', '2018-11-21 08:21:22'),
(907, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:21:29', '2018-11-21 08:21:29'),
(908, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:21:34', '2018-11-21 08:21:34'),
(909, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:24:36', '2018-11-21 08:24:36'),
(910, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:24:37', '2018-11-21 08:24:37'),
(911, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:24:50', '2018-11-21 08:24:50'),
(912, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:25:04', '2018-11-21 08:25:04'),
(913, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:54:28', '2018-11-21 08:54:28'),
(914, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:54:40', '2018-11-21 08:54:40'),
(915, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:00:57', '2018-11-21 09:00:57'),
(916, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:02:03', '2018-11-21 09:02:03'),
(917, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:09:50', '2018-11-21 09:09:50'),
(918, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:12:13', '2018-11-21 09:12:13'),
(919, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:19:39', '2018-11-21 09:19:39'),
(920, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:33:47', '2018-11-21 09:33:47'),
(921, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 10:12:47', '2018-11-21 10:12:47'),
(922, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-24 10:58:54', '2018-11-24 10:58:54'),
(923, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-27 08:43:37', '2018-11-27 08:43:37'),
(924, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:20:41', '2018-11-29 07:20:41'),
(925, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:26:25', '2018-11-29 07:26:25'),
(926, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:31:25', '2018-11-29 07:31:25'),
(927, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:31:28', '2018-11-29 07:31:28'),
(928, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:32:41', '2018-11-29 07:32:41'),
(929, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:33:00', '2018-11-29 07:33:00'),
(930, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:33:03', '2018-11-29 07:33:03'),
(931, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:04', '2018-11-29 07:34:04'),
(932, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:20', '2018-11-29 07:34:20'),
(933, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:40', '2018-11-29 07:34:40'),
(934, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:41', '2018-11-29 07:34:41'),
(935, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:42', '2018-11-29 07:34:42'),
(936, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:45', '2018-11-29 07:34:45'),
(937, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:48', '2018-11-29 07:34:48'),
(938, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:35:39', '2018-11-29 07:35:39'),
(939, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:37:02', '2018-11-29 07:37:02'),
(940, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:38:36', '2018-11-29 07:38:36'),
(941, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:38:55', '2018-11-29 07:38:55'),
(942, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:40:27', '2018-11-29 07:40:27'),
(943, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:40:51', '2018-11-29 07:40:51'),
(944, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:40:54', '2018-11-29 07:40:54'),
(945, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:06', '2018-11-29 07:41:06'),
(946, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:13', '2018-11-29 07:41:13'),
(947, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:44', '2018-11-29 07:41:44'),
(948, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:45', '2018-11-29 07:41:45'),
(949, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:46', '2018-11-29 07:41:46'),
(950, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:56:16', '2018-11-29 07:56:16'),
(951, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:01:16', '2018-11-29 08:01:16'),
(952, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:02:54', '2018-11-29 08:02:54'),
(953, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:04:22', '2018-11-29 08:04:22'),
(954, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:08:59', '2018-11-29 08:08:59'),
(955, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:09:08', '2018-11-29 08:09:08'),
(956, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:10:12', '2018-11-29 08:10:12'),
(957, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:18:25', '2018-11-29 08:18:25'),
(958, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:25:06', '2018-11-29 08:25:06'),
(959, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:25:35', '2018-11-29 08:25:35'),
(960, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:09', '2018-11-29 08:29:09'),
(961, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:10', '2018-11-29 08:29:10'),
(962, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:30', '2018-11-29 08:29:30'),
(963, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:31', '2018-11-29 08:29:31'),
(964, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:38', '2018-11-29 08:29:38'),
(965, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:53', '2018-11-29 08:29:53'),
(966, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:30:38', '2018-11-29 08:30:38'),
(967, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:30:55', '2018-11-29 08:30:55'),
(968, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:31:37', '2018-11-29 08:31:37'),
(969, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:31:59', '2018-11-29 08:31:59'),
(970, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:32:13', '2018-11-29 08:32:13'),
(971, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:32:34', '2018-11-29 08:32:34'),
(972, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:32:48', '2018-11-29 08:32:48'),
(973, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:32:58', '2018-11-29 08:32:58'),
(974, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:33:00', '2018-11-29 08:33:00'),
(975, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:33:02', '2018-11-29 08:33:02'),
(976, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:34:09', '2018-11-29 08:34:09'),
(977, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:34:31', '2018-11-29 08:34:31'),
(978, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:34:39', '2018-11-29 08:34:39'),
(979, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:39:47', '2018-11-29 08:39:47'),
(980, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:42:51', '2018-11-29 08:42:51'),
(981, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 09:46:27', '2018-11-29 09:46:27'),
(982, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 02:05:13', '2018-11-30 02:05:13'),
(983, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 02:14:51', '2018-11-30 02:14:51'),
(984, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 07:38:25', '2018-11-30 07:38:25'),
(985, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:00:15', '2018-11-30 08:00:15'),
(986, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:21:18', '2018-11-30 08:21:18'),
(987, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:33:20', '2018-11-30 08:33:20'),
(988, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:33:39', '2018-11-30 08:33:39'),
(989, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:34:58', '2018-11-30 08:34:58'),
(990, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:35:06', '2018-11-30 08:35:06'),
(991, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:40:18', '2018-11-30 08:40:18'),
(992, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 12:34:25', '2018-11-30 12:34:25'),
(993, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 13:11:12', '2018-11-30 13:11:12'),
(994, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 02:05:45', '2018-12-01 02:05:45'),
(995, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 02:44:13', '2018-12-01 02:44:13'),
(996, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:26:35', '2018-12-01 04:26:35'),
(997, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:43:22', '2018-12-01 04:43:22'),
(998, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:43:59', '2018-12-01 04:43:59'),
(999, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:44:01', '2018-12-01 04:44:01'),
(1000, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:44:01', '2018-12-01 04:44:01'),
(1001, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 06:51:13', '2018-12-01 06:51:13'),
(1002, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 07:38:42', '2018-12-01 07:38:42'),
(1003, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 07:51:23', '2018-12-01 07:51:23'),
(1004, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 08:58:54', '2018-12-01 08:58:54'),
(1005, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 08:58:59', '2018-12-01 08:58:59'),
(1006, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:05:07', '2018-12-01 09:05:07'),
(1007, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:12:24', '2018-12-01 09:12:24'),
(1008, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:12:32', '2018-12-01 09:12:32'),
(1009, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:14:55', '2018-12-01 09:14:55'),
(1010, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:18:35', '2018-12-01 09:18:35');
INSERT INTO `visitors` (`id`, `user_id`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(1011, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:18:49', '2018-12-01 09:18:49'),
(1012, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:20:42', '2018-12-01 09:20:42'),
(1013, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:22:26', '2018-12-01 09:22:26'),
(1014, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:28:42', '2018-12-01 09:28:42'),
(1015, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 10:33:04', '2018-12-01 10:33:04'),
(1016, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 10:34:22', '2018-12-01 10:34:22'),
(1017, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 10:55:03', '2018-12-01 10:55:03'),
(1018, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 02:04:46', '2018-12-03 02:04:46'),
(1019, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 02:18:41', '2018-12-03 02:18:41'),
(1020, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 02:51:24', '2018-12-03 02:51:24'),
(1021, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:08:32', '2018-12-03 03:08:32'),
(1022, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:24:34', '2018-12-03 03:24:34'),
(1023, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:42:42', '2018-12-03 03:42:42'),
(1024, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:43:09', '2018-12-03 03:43:09'),
(1025, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:43:11', '2018-12-03 03:43:11'),
(1026, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:43:12', '2018-12-03 03:43:12'),
(1027, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 04:50:17', '2018-12-03 04:50:17'),
(1028, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 04:50:54', '2018-12-03 04:50:54'),
(1029, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 04:52:39', '2018-12-03 04:52:39'),
(1030, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 05:11:12', '2018-12-03 05:11:12'),
(1031, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 05:38:23', '2018-12-03 05:38:23'),
(1032, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 07:06:27', '2018-12-03 07:06:27'),
(1033, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 07:28:28', '2018-12-03 07:28:28'),
(1034, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 08:54:43', '2018-12-03 08:54:43'),
(1035, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 09:15:13', '2018-12-03 09:15:13'),
(1036, 14, 'http://localhost/cqbycq/public', '3', '2018-12-04 02:03:03', '2018-12-04 02:03:03'),
(1037, 14, 'http://localhost/cqbycq/public', '3', '2018-12-04 02:24:17', '2018-12-04 02:24:17'),
(1038, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:01:19', '2018-12-04 04:01:19'),
(1039, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:19:43', '2018-12-04 04:19:43'),
(1040, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:34:35', '2018-12-04 04:34:35'),
(1041, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:35:14', '2018-12-04 04:35:14'),
(1042, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:56:17', '2018-12-04 04:56:17'),
(1043, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:57:40', '2018-12-04 04:57:40'),
(1044, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:00:08', '2018-12-04 05:00:08'),
(1045, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:12:45', '2018-12-04 05:12:45'),
(1046, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:12:54', '2018-12-04 05:12:54'),
(1047, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:13:02', '2018-12-04 05:13:02'),
(1048, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:14:23', '2018-12-04 05:14:23'),
(1049, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:14:26', '2018-12-04 05:14:26'),
(1050, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:16:24', '2018-12-04 05:16:24'),
(1051, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:16:32', '2018-12-04 05:16:32'),
(1052, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:18:32', '2018-12-04 05:18:32'),
(1053, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:20:18', '2018-12-04 05:20:18'),
(1054, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:21:56', '2018-12-04 05:21:56'),
(1055, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:22:07', '2018-12-04 05:22:07'),
(1056, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:22:21', '2018-12-04 05:22:21'),
(1057, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:23:15', '2018-12-04 05:23:15'),
(1058, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:23:38', '2018-12-04 05:23:38'),
(1059, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:43:28', '2018-12-04 06:43:28'),
(1060, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:43:56', '2018-12-04 06:43:56'),
(1061, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:44:20', '2018-12-04 06:44:20'),
(1062, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:44:23', '2018-12-04 06:44:23'),
(1063, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:44:31', '2018-12-04 06:44:31'),
(1064, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:20', '2018-12-04 06:45:20'),
(1065, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:22', '2018-12-04 06:45:22'),
(1066, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:25', '2018-12-04 06:45:25'),
(1067, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:40', '2018-12-04 06:45:40'),
(1068, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:58', '2018-12-04 06:45:58'),
(1069, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:46:44', '2018-12-04 06:46:44'),
(1070, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:47:26', '2018-12-04 06:47:26'),
(1071, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:08:26', '2018-12-04 07:08:26'),
(1072, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:09:33', '2018-12-04 07:09:33'),
(1073, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:22:57', '2018-12-04 07:22:57'),
(1074, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:23:10', '2018-12-04 07:23:10'),
(1075, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:23:15', '2018-12-04 07:23:15'),
(1076, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:23:30', '2018-12-04 07:23:30'),
(1077, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:23:35', '2018-12-04 07:23:35'),
(1078, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:24:26', '2018-12-04 07:24:26'),
(1079, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:41:09', '2018-12-04 07:41:09'),
(1080, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:53:21', '2018-12-04 07:53:21'),
(1081, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:53:35', '2018-12-04 07:53:35'),
(1082, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:55:20', '2018-12-04 07:55:20'),
(1083, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:58:48', '2018-12-04 07:58:48'),
(1084, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:58:52', '2018-12-04 07:58:52'),
(1085, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:59:08', '2018-12-04 07:59:08'),
(1086, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:59:32', '2018-12-04 07:59:32'),
(1087, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:59:57', '2018-12-04 07:59:57'),
(1088, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:00:35', '2018-12-04 08:00:35'),
(1089, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:07:25', '2018-12-04 08:07:25'),
(1090, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:07:30', '2018-12-04 08:07:30'),
(1091, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:07:39', '2018-12-04 08:07:39'),
(1092, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:28:47', '2018-12-04 08:28:47'),
(1093, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:32:41', '2018-12-04 08:32:41'),
(1094, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:32:55', '2018-12-04 08:32:55'),
(1095, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:33:32', '2018-12-04 08:33:32'),
(1096, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:34:47', '2018-12-04 08:34:47'),
(1097, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:35:56', '2018-12-04 08:35:56'),
(1098, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:36:19', '2018-12-04 08:36:19'),
(1099, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:48:09', '2018-12-04 08:48:09'),
(1100, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 09:37:48', '2018-12-04 09:37:48'),
(1101, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 09:44:23', '2018-12-04 09:44:23'),
(1102, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 10:20:20', '2018-12-04 10:20:20'),
(1103, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 10:24:47', '2018-12-04 10:24:47'),
(1104, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 10:45:27', '2018-12-04 10:45:27'),
(1105, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 11:26:19', '2018-12-04 11:26:19'),
(1106, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 04:20:24', '2018-12-05 04:20:24'),
(1107, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 07:14:07', '2018-12-05 07:14:07'),
(1108, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 07:14:48', '2018-12-05 07:14:48'),
(1109, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 08:23:03', '2018-12-05 08:23:03'),
(1110, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 08:23:17', '2018-12-05 08:23:17'),
(1111, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 09:00:25', '2018-12-05 09:00:25'),
(1112, 14, 'http://localhost/cqbycq/public', '3', '2018-12-05 10:53:18', '2018-12-05 10:53:18'),
(1113, 14, 'http://localhost/cqbycq/public', '3', '2018-12-05 11:35:11', '2018-12-05 11:35:11'),
(1114, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:30:55', '2018-12-11 04:30:55'),
(1115, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:31:02', '2018-12-11 04:31:02'),
(1116, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:33:51', '2018-12-11 04:33:51'),
(1117, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:33:51', '2018-12-11 04:33:51'),
(1118, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:38:10', '2018-12-11 04:38:10'),
(1119, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:54:42', '2018-12-11 04:54:42'),
(1120, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 02:17:38', '2018-12-13 02:17:38'),
(1121, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 09:23:13', '2018-12-13 09:23:13'),
(1122, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 09:36:05', '2018-12-13 09:36:05'),
(1123, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 12:02:19', '2018-12-13 12:02:19'),
(1124, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 12:20:41', '2018-12-13 12:20:41'),
(1125, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-17 05:00:06', '2018-12-17 05:00:06'),
(1126, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-17 09:44:04', '2018-12-17 09:44:04'),
(1127, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-17 22:07:20', '2018-12-17 22:07:20'),
(1128, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-18 00:04:49', '2018-12-18 00:04:49'),
(1129, 14, 'http://localhost/cqbycq/public', '3', '2018-12-18 00:05:35', '2018-12-18 00:05:35'),
(1130, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 03:06:55', '2018-12-19 03:06:55'),
(1131, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 03:08:58', '2018-12-19 03:08:58'),
(1132, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:09:28', '2018-12-19 04:09:28'),
(1133, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:13:17', '2018-12-19 04:13:17'),
(1134, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:13:44', '2018-12-19 04:13:44'),
(1135, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:14:21', '2018-12-19 04:14:21'),
(1136, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:14:48', '2018-12-19 04:14:48'),
(1137, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:15:03', '2018-12-19 04:15:03'),
(1138, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:16:09', '2018-12-19 04:16:09'),
(1139, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:16:14', '2018-12-19 04:16:14'),
(1140, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:16:32', '2018-12-19 04:16:32'),
(1141, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:16:36', '2018-12-19 04:16:36'),
(1142, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:39:47', '2018-12-19 04:39:47'),
(1143, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:50:50', '2018-12-19 04:50:50'),
(1144, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 06:44:28', '2018-12-19 06:44:28'),
(1145, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-20 06:51:59', '2018-12-20 06:51:59'),
(1146, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-27 05:24:09', '2018-12-27 05:24:09'),
(1147, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-27 06:18:44', '2018-12-27 06:18:44'),
(1148, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-28 02:35:24', '2018-12-28 02:35:24'),
(1149, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 02:11:59', '2019-01-01 02:11:59'),
(1150, NULL, 'http://127.0.0.1:8000', '3', '2019-01-01 03:03:58', '2019-01-01 03:03:58'),
(1151, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 03:21:16', '2019-01-01 03:21:16'),
(1152, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 09:02:04', '2019-01-01 09:02:04'),
(1153, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 09:02:27', '2019-01-01 09:02:27'),
(1154, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 10:17:33', '2019-01-01 10:17:33'),
(1155, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 02:03:11', '2019-01-02 02:03:11'),
(1156, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 03:45:40', '2019-01-02 03:45:40'),
(1157, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 04:57:58', '2019-01-02 04:57:58'),
(1158, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 09:08:01', '2019-01-02 09:08:01'),
(1159, 14, 'http://localhost/cqbycq/public', '3', '2019-01-02 10:17:20', '2019-01-02 10:17:20'),
(1160, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 10:17:25', '2019-01-02 10:17:25'),
(1161, 14, 'http://localhost/cqbycq/public', '3', '2019-01-02 11:01:26', '2019-01-02 11:01:26'),
(1162, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 11:37:20', '2019-01-02 11:37:20'),
(1163, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-03 08:12:00', '2019-01-03 08:12:00'),
(1164, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-03 09:21:01', '2019-01-03 09:21:01'),
(1165, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-03 09:35:33', '2019-01-03 09:35:33'),
(1166, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-04 02:04:15', '2019-01-04 02:04:15'),
(1167, 14, 'http://localhost/cqbycq/public', '3', '2019-01-04 02:04:30', '2019-01-04 02:04:30'),
(1168, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-04 03:14:27', '2019-01-04 03:14:27'),
(1169, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-04 03:34:11', '2019-01-04 03:34:11'),
(1170, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 02:04:51', '2019-01-05 02:04:51'),
(1171, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 02:06:51', '2019-01-05 02:06:51'),
(1172, 14, 'http://localhost/cqbycq/public', '3', '2019-01-05 02:07:46', '2019-01-05 02:07:46'),
(1173, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 05:16:08', '2019-01-05 05:16:08'),
(1174, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 05:25:02', '2019-01-05 05:25:02'),
(1175, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 05:38:04', '2019-01-05 05:38:04'),
(1176, 14, 'http://localhost/cqbycq/public', '3', '2019-01-05 05:49:11', '2019-01-05 05:49:11'),
(1177, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 06:03:15', '2019-01-05 06:03:15'),
(1178, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-07 02:02:15', '2019-01-07 02:02:15'),
(1179, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 03:31:11', '2019-01-08 03:31:11'),
(1180, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 03:58:48', '2019-01-08 03:58:48'),
(1181, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 07:01:54', '2019-01-08 07:01:54'),
(1182, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 07:29:18', '2019-01-08 07:29:18'),
(1183, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 09:29:29', '2019-01-08 09:29:29'),
(1184, NULL, 'http://localhost/projects/mediusware/cqbycq/public', '3', '2019-01-10 10:22:45', '2019-01-10 10:22:45'),
(1185, NULL, 'http://localhost/projects/mediusware/cqbycq/public', '3', '2019-01-10 10:53:24', '2019-01-10 10:53:24'),
(1186, 14, 'http://localhost/projects/mediusware/cqbycq/public', '3', '2019-01-10 10:55:33', '2019-01-10 10:55:33'),
(1187, 14, 'http://localhost/projects/mediusware/cqbycq/public', '3', '2019-01-11 05:33:32', '2019-01-11 05:33:32'),
(1188, 14, 'http://localhost/projects/mediusware/cqbycq/public', '3', '2019-01-11 05:42:01', '2019-01-11 05:42:01'),
(1189, NULL, 'http://localhost/projects/mediusware/r3Dev/public', '3', '2019-03-04 02:55:35', '2019-03-04 02:55:35'),
(1190, NULL, 'http://localhost/projects/mediusware/r3Dev/public', '3', '2019-03-04 02:57:21', '2019-03-04 02:57:21'),
(1191, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 02:57:34', '2019-03-04 02:57:34'),
(1192, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 02:57:47', '2019-03-04 02:57:47'),
(1193, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 03:30:04', '2019-03-04 03:30:04'),
(1194, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 03:34:07', '2019-03-04 03:34:07'),
(1195, 14, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 03:40:47', '2019-03-04 03:40:47'),
(1196, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 03:44:58', '2019-03-04 03:44:58'),
(1197, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 03:55:21', '2019-03-04 03:55:21'),
(1198, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 03:55:45', '2019-03-04 03:55:45'),
(1199, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 04:30:02', '2019-03-04 04:30:02'),
(1200, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 07:15:55', '2019-03-04 07:15:55'),
(1201, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 07:16:08', '2019-03-04 07:16:08'),
(1202, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 07:19:18', '2019-03-04 07:19:18'),
(1203, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 07:29:40', '2019-03-04 07:29:40'),
(1204, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 07:48:32', '2019-03-04 07:48:32'),
(1205, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 07:49:52', '2019-03-04 07:49:52'),
(1206, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 22:42:04', '2019-03-04 22:42:04'),
(1207, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 22:52:37', '2019-03-04 22:52:37'),
(1208, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 22:53:18', '2019-03-04 22:53:18'),
(1209, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 22:56:59', '2019-03-04 22:56:59'),
(1210, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 22:58:03', '2019-03-04 22:58:03'),
(1211, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 22:58:10', '2019-03-04 22:58:10'),
(1212, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:00:38', '2019-03-04 23:00:38'),
(1213, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:01:34', '2019-03-04 23:01:34'),
(1214, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:02:42', '2019-03-04 23:02:42'),
(1215, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:02:46', '2019-03-04 23:02:46'),
(1216, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:06', '2019-03-04 23:03:06'),
(1217, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:07', '2019-03-04 23:03:07'),
(1218, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:07', '2019-03-04 23:03:07'),
(1219, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:07', '2019-03-04 23:03:07'),
(1220, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:07', '2019-03-04 23:03:07'),
(1221, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:07', '2019-03-04 23:03:07'),
(1222, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:07', '2019-03-04 23:03:07'),
(1223, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:08', '2019-03-04 23:03:08'),
(1224, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:08', '2019-03-04 23:03:08'),
(1225, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:08', '2019-03-04 23:03:08'),
(1226, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:08', '2019-03-04 23:03:08'),
(1227, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:03:32', '2019-03-04 23:03:32'),
(1228, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:05:09', '2019-03-04 23:05:09'),
(1229, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:09:21', '2019-03-04 23:09:21'),
(1230, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:09:22', '2019-03-04 23:09:22'),
(1231, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:09:56', '2019-03-04 23:09:56'),
(1232, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:10:45', '2019-03-04 23:10:45'),
(1233, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:10:52', '2019-03-04 23:10:52'),
(1234, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:10:55', '2019-03-04 23:10:55'),
(1235, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:14:43', '2019-03-04 23:14:43'),
(1236, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:15:18', '2019-03-04 23:15:18'),
(1237, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:15:23', '2019-03-04 23:15:23'),
(1238, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:15:31', '2019-03-04 23:15:31'),
(1239, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:16:17', '2019-03-04 23:16:17'),
(1240, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:20:40', '2019-03-04 23:20:40'),
(1241, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:58:07', '2019-03-04 23:58:07'),
(1242, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:58:26', '2019-03-04 23:58:26'),
(1243, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:59:05', '2019-03-04 23:59:05'),
(1244, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-04 23:59:20', '2019-03-04 23:59:20'),
(1245, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:06:25', '2019-03-05 00:06:25'),
(1246, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:09:44', '2019-03-05 00:09:44'),
(1247, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:09:44', '2019-03-05 00:09:44'),
(1248, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:10:40', '2019-03-05 00:10:40'),
(1249, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:10:47', '2019-03-05 00:10:47'),
(1250, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:10:48', '2019-03-05 00:10:48'),
(1251, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:11:10', '2019-03-05 00:11:10'),
(1252, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:15:37', '2019-03-05 00:15:37'),
(1253, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:15:40', '2019-03-05 00:15:40'),
(1254, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:16:16', '2019-03-05 00:16:16'),
(1255, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:19:01', '2019-03-05 00:19:01'),
(1256, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:19:37', '2019-03-05 00:19:37'),
(1257, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:20:05', '2019-03-05 00:20:05'),
(1258, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:25:25', '2019-03-05 00:25:25'),
(1259, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:26:21', '2019-03-05 00:26:21'),
(1260, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:26:21', '2019-03-05 00:26:21'),
(1261, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:26:29', '2019-03-05 00:26:29'),
(1262, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:27:54', '2019-03-05 00:27:54'),
(1263, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:27:54', '2019-03-05 00:27:54'),
(1264, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:29:30', '2019-03-05 00:29:30'),
(1265, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:29:30', '2019-03-05 00:29:30'),
(1266, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:29:55', '2019-03-05 00:29:55'),
(1267, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:29:56', '2019-03-05 00:29:56'),
(1268, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:31:27', '2019-03-05 00:31:27'),
(1269, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 00:39:50', '2019-03-05 00:39:50'),
(1270, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:00:31', '2019-03-05 01:00:31'),
(1271, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:11:00', '2019-03-05 01:11:00'),
(1272, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:11:24', '2019-03-05 01:11:24'),
(1273, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:37:38', '2019-03-05 01:37:38'),
(1274, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:38:25', '2019-03-05 01:38:25'),
(1275, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:38:41', '2019-03-05 01:38:41'),
(1276, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:42:05', '2019-03-05 01:42:05'),
(1277, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:50:33', '2019-03-05 01:50:33'),
(1278, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:52:23', '2019-03-05 01:52:23'),
(1279, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 01:52:55', '2019-03-05 01:52:55'),
(1280, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 02:03:11', '2019-03-05 02:03:11'),
(1281, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 02:35:47', '2019-03-05 02:35:47'),
(1282, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:08:46', '2019-03-05 03:08:46'),
(1283, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:08:51', '2019-03-05 03:08:51'),
(1284, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:10:21', '2019-03-05 03:10:21'),
(1285, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:11:08', '2019-03-05 03:11:08'),
(1286, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:11:22', '2019-03-05 03:11:22'),
(1287, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:11:31', '2019-03-05 03:11:31'),
(1288, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:11:56', '2019-03-05 03:11:56'),
(1289, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:11:57', '2019-03-05 03:11:57'),
(1290, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:20:12', '2019-03-05 03:20:12'),
(1291, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:20:19', '2019-03-05 03:20:19'),
(1292, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:20:52', '2019-03-05 03:20:52'),
(1293, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:21:14', '2019-03-05 03:21:14'),
(1294, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:21:28', '2019-03-05 03:21:28'),
(1295, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:21:33', '2019-03-05 03:21:33'),
(1296, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:21:57', '2019-03-05 03:21:57'),
(1297, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:22:19', '2019-03-05 03:22:19'),
(1298, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:22:40', '2019-03-05 03:22:40'),
(1299, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:22:58', '2019-03-05 03:22:58'),
(1300, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:23:04', '2019-03-05 03:23:04'),
(1301, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:41:38', '2019-03-05 03:41:38'),
(1302, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:44:14', '2019-03-05 03:44:14'),
(1303, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:44:26', '2019-03-05 03:44:26'),
(1304, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:47:17', '2019-03-05 03:47:17'),
(1305, 19, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:56:11', '2019-03-05 03:56:11'),
(1306, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 03:57:19', '2019-03-05 03:57:19'),
(1307, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 04:50:37', '2019-03-05 04:50:37'),
(1308, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 04:59:00', '2019-03-05 04:59:00'),
(1309, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 05:48:27', '2019-03-05 05:48:27'),
(1310, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 05:48:37', '2019-03-05 05:48:37'),
(1311, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 07:33:37', '2019-03-05 07:33:37'),
(1312, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 07:34:11', '2019-03-05 07:34:11'),
(1313, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-05 23:45:27', '2019-03-05 23:45:27'),
(1314, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 01:11:34', '2019-03-06 01:11:34'),
(1315, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 01:13:19', '2019-03-06 01:13:19'),
(1316, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 01:23:46', '2019-03-06 01:23:46'),
(1317, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 01:34:34', '2019-03-06 01:34:34'),
(1318, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:05:25', '2019-03-06 02:05:25'),
(1319, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:06:32', '2019-03-06 02:06:32'),
(1320, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:07:32', '2019-03-06 02:07:32'),
(1321, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:07:34', '2019-03-06 02:07:34'),
(1322, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:07:34', '2019-03-06 02:07:34'),
(1323, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:11:24', '2019-03-06 02:11:24'),
(1324, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:12:43', '2019-03-06 02:12:43'),
(1325, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:13:01', '2019-03-06 02:13:01'),
(1326, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:14:48', '2019-03-06 02:14:48'),
(1327, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 02:14:49', '2019-03-06 02:14:49'),
(1328, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:12:19', '2019-03-06 03:12:19'),
(1329, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:17:15', '2019-03-06 03:17:15'),
(1330, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:17:48', '2019-03-06 03:17:48'),
(1331, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:18:34', '2019-03-06 03:18:34'),
(1332, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:20:43', '2019-03-06 03:20:43'),
(1333, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:21:08', '2019-03-06 03:21:08'),
(1334, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:21:30', '2019-03-06 03:21:30'),
(1335, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:23:50', '2019-03-06 03:23:50'),
(1336, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:28:44', '2019-03-06 03:28:44'),
(1337, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:30:15', '2019-03-06 03:30:15'),
(1338, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:30:15', '2019-03-06 03:30:15'),
(1339, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:31:12', '2019-03-06 03:31:12'),
(1340, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:33:59', '2019-03-06 03:33:59'),
(1341, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 03:52:24', '2019-03-06 03:52:24'),
(1342, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 05:03:50', '2019-03-06 05:03:50'),
(1343, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 08:58:06', '2019-03-06 08:58:06'),
(1344, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 09:14:40', '2019-03-06 09:14:40'),
(1345, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 22:16:06', '2019-03-06 22:16:06'),
(1346, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-06 22:16:06', '2019-03-06 22:16:06'),
(1347, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 03:58:24', '2019-03-07 03:58:24'),
(1348, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 03:59:01', '2019-03-07 03:59:01'),
(1349, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:03:11', '2019-03-07 04:03:11'),
(1350, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:05:56', '2019-03-07 04:05:56'),
(1351, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:06:12', '2019-03-07 04:06:12'),
(1352, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:08:01', '2019-03-07 04:08:01'),
(1353, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:10:07', '2019-03-07 04:10:07'),
(1354, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:10:38', '2019-03-07 04:10:38'),
(1355, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:13:07', '2019-03-07 04:13:07'),
(1356, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:13:15', '2019-03-07 04:13:15'),
(1357, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:13:47', '2019-03-07 04:13:47'),
(1358, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:13:47', '2019-03-07 04:13:47'),
(1359, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:14:56', '2019-03-07 04:14:56'),
(1360, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:15:25', '2019-03-07 04:15:25'),
(1361, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:15:48', '2019-03-07 04:15:48'),
(1362, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:19:07', '2019-03-07 04:19:07'),
(1363, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:19:56', '2019-03-07 04:19:56'),
(1364, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:22:38', '2019-03-07 04:22:38'),
(1365, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:42:52', '2019-03-07 04:42:52'),
(1366, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:44:13', '2019-03-07 04:44:13'),
(1367, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:45:57', '2019-03-07 04:45:57'),
(1368, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:52:15', '2019-03-07 04:52:15'),
(1369, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:52:15', '2019-03-07 04:52:15'),
(1370, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:52:15', '2019-03-07 04:52:15'),
(1371, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:53:13', '2019-03-07 04:53:13'),
(1372, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:53:15', '2019-03-07 04:53:15'),
(1373, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:54:20', '2019-03-07 04:54:20'),
(1374, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:54:21', '2019-03-07 04:54:21'),
(1375, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 04:54:21', '2019-03-07 04:54:21'),
(1376, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:00:34', '2019-03-07 05:00:34'),
(1377, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:00:34', '2019-03-07 05:00:34'),
(1378, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:00:40', '2019-03-07 05:00:40'),
(1379, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:00:41', '2019-03-07 05:00:41'),
(1380, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:00:41', '2019-03-07 05:00:41'),
(1381, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:00:41', '2019-03-07 05:00:41'),
(1382, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:01:49', '2019-03-07 05:01:49'),
(1383, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:01:50', '2019-03-07 05:01:50'),
(1384, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:01:50', '2019-03-07 05:01:50'),
(1385, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:01:50', '2019-03-07 05:01:50'),
(1386, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:01:50', '2019-03-07 05:01:50'),
(1387, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:02:18', '2019-03-07 05:02:18'),
(1388, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:02:19', '2019-03-07 05:02:19'),
(1389, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:02:20', '2019-03-07 05:02:20'),
(1390, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:02:20', '2019-03-07 05:02:20'),
(1391, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:03:07', '2019-03-07 05:03:07'),
(1392, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:07:37', '2019-03-07 05:07:37'),
(1393, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:08:30', '2019-03-07 05:08:30'),
(1394, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:08:31', '2019-03-07 05:08:31'),
(1395, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:08:31', '2019-03-07 05:08:31'),
(1396, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:08:32', '2019-03-07 05:08:32'),
(1397, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:08:51', '2019-03-07 05:08:51'),
(1398, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:09:21', '2019-03-07 05:09:21'),
(1399, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:09:25', '2019-03-07 05:09:25'),
(1400, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:09:40', '2019-03-07 05:09:40'),
(1401, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:09:41', '2019-03-07 05:09:41'),
(1402, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:10:32', '2019-03-07 05:10:32'),
(1403, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:14:24', '2019-03-07 05:14:24'),
(1404, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:16:30', '2019-03-07 05:16:30'),
(1405, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:19:27', '2019-03-07 05:19:27'),
(1406, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 05:41:42', '2019-03-07 05:41:42'),
(1407, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 06:02:04', '2019-03-07 06:02:04'),
(1408, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 06:02:15', '2019-03-07 06:02:15'),
(1409, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 06:06:54', '2019-03-07 06:06:54'),
(1410, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 07:04:04', '2019-03-07 07:04:04'),
(1411, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 07:04:51', '2019-03-07 07:04:51'),
(1412, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 07:05:04', '2019-03-07 07:05:04'),
(1413, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 07:21:52', '2019-03-07 07:21:52'),
(1414, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 07:22:38', '2019-03-07 07:22:38'),
(1415, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 07:22:45', '2019-03-07 07:22:45'),
(1416, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 07:28:44', '2019-03-07 07:28:44'),
(1417, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 07:29:57', '2019-03-07 07:29:57'),
(1418, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 11:56:51', '2019-03-07 11:56:51'),
(1419, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 11:56:51', '2019-03-07 11:56:51'),
(1420, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 12:02:36', '2019-03-07 12:02:36'),
(1421, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 13:41:52', '2019-03-07 13:41:52'),
(1422, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 13:43:31', '2019-03-07 13:43:31'),
(1423, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 13:43:35', '2019-03-07 13:43:35'),
(1424, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 13:43:36', '2019-03-07 13:43:36'),
(1425, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 13:43:36', '2019-03-07 13:43:36'),
(1426, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 13:46:25', '2019-03-07 13:46:25'),
(1427, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 13:47:25', '2019-03-07 13:47:25'),
(1428, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 22:00:58', '2019-03-07 22:00:58'),
(1429, 27, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 22:00:58', '2019-03-07 22:00:58'),
(1430, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 23:04:17', '2019-03-07 23:04:17'),
(1431, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 23:34:01', '2019-03-07 23:34:01'),
(1432, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 23:47:12', '2019-03-07 23:47:12'),
(1433, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-07 23:58:15', '2019-03-07 23:58:15'),
(1434, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-08 00:03:39', '2019-03-08 00:03:39'),
(1435, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-08 00:09:39', '2019-03-08 00:09:39'),
(1436, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-08 00:40:22', '2019-03-08 00:40:22'),
(1437, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-08 00:40:42', '2019-03-08 00:40:42'),
(1438, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-08 01:11:23', '2019-03-08 01:11:23'),
(1439, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-08 03:00:37', '2019-03-08 03:00:37'),
(1440, 26, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-08 03:26:41', '2019-03-08 03:26:41'),
(1441, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-08 03:29:26', '2019-03-08 03:29:26'),
(1442, NULL, 'http://localhost/projects/mediusware/r3dev/public', '3', '2019-03-08 03:38:09', '2019-03-08 03:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `wish_list_items`
--

CREATE TABLE `wish_list_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_ship_methods`
--
ALTER TABLE `admin_ship_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `front_pages_user_id_index` (`user_id`) USING BTREE;

--
-- Indexes for table `block_users`
--
ALTER TABLE `block_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `body_sizes`
--
ALTER TABLE `body_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buyer_shipping_addresses`
--
ALTER TABLE `buyer_shipping_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_banners`
--
ALTER TABLE `category_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fabrics`
--
ALTER TABLE `fabrics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_images`
--
ALTER TABLE `item_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_views`
--
ALTER TABLE `item_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lengths`
--
ALTER TABLE `lengths`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_history`
--
ALTER TABLE `login_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `made_in_countries`
--
ALTER TABLE `made_in_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_colors`
--
ALTER TABLE `master_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_fabrics`
--
ALTER TABLE `master_fabrics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metas`
--
ALTER TABLE `metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meta_buyers`
--
ALTER TABLE `meta_buyers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meta_vendors`
--
ALTER TABLE `meta_vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packs`
--
ALTER TABLE `packs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `patterns`
--
ALTER TABLE `patterns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_methods`
--
ALTER TABLE `shipping_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_items`
--
ALTER TABLE `slider_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_credits`
--
ALTER TABLE `store_credits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_credit_transections`
--
ALTER TABLE `store_credit_transections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `styles`
--
ALTER TABLE `styles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `top_banners`
--
ALTER TABLE `top_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vendor_images`
--
ALTER TABLE `vendor_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wish_list_items`
--
ALTER TABLE `wish_list_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_ship_methods`
--
ALTER TABLE `admin_ship_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `block_users`
--
ALTER TABLE `block_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `body_sizes`
--
ALTER TABLE `body_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `buyer_shipping_addresses`
--
ALTER TABLE `buyer_shipping_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `category_banners`
--
ALTER TABLE `category_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `couriers`
--
ALTER TABLE `couriers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fabrics`
--
ALTER TABLE `fabrics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `item_images`
--
ALTER TABLE `item_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=272;

--
-- AUTO_INCREMENT for table `item_views`
--
ALTER TABLE `item_views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=309;

--
-- AUTO_INCREMENT for table `lengths`
--
ALTER TABLE `lengths`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `login_history`
--
ALTER TABLE `login_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;

--
-- AUTO_INCREMENT for table `made_in_countries`
--
ALTER TABLE `made_in_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `master_colors`
--
ALTER TABLE `master_colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `master_fabrics`
--
ALTER TABLE `master_fabrics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `metas`
--
ALTER TABLE `metas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `meta_buyers`
--
ALTER TABLE `meta_buyers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `meta_vendors`
--
ALTER TABLE `meta_vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `packs`
--
ALTER TABLE `packs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `patterns`
--
ALTER TABLE `patterns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `shipping_methods`
--
ALTER TABLE `shipping_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slider_items`
--
ALTER TABLE `slider_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `store_credits`
--
ALTER TABLE `store_credits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `store_credit_transections`
--
ALTER TABLE `store_credit_transections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `styles`
--
ALTER TABLE `styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `top_banners`
--
ALTER TABLE `top_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `vendor_images`
--
ALTER TABLE `vendor_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1443;

--
-- AUTO_INCREMENT for table `wish_list_items`
--
ALTER TABLE `wish_list_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
