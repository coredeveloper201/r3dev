<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\Availability;
use App\Enumeration\Role;
use App\Model\CartItem;
use App\Model\Category;
use App\Model\Color;
use App\Model\Fabric;
use App\Model\Item;
use App\Model\ItemImages;
use App\Model\ItemView;
use App\Model\MadeInCountry;
use App\Model\MasterColor;
use App\Model\MetaVendor;
use App\Model\Pack;
use App\Model\User;
use App\Model\WishListItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Uuid;
use DateTime;
use Image;
use File;
use Carbon\Carbon;
use Excel;
use GuzzleHttp\Client;
use URL;

class AdminNewOrderController extends Controller
{


    public function itemListAll(Request $request) {
        $activeItemsQuery = Item::query();
        $activeItemsQuery->where('status', 1)
            ->with('category', 'images');

        // Search
        if (isset($request->text) && $request->text != '') {
            $activeItemsQuery->where(function($q) use ($request){
                if (isset($request->style) && $request->style == '1') {
                    $q->where('style_no', 'like', '%' . $request->text . '%');
                }

                if (isset($request->des) && $request->des == '1') {
                    $q->orWhere('description', 'like', '%' . $request->text . '%');
                }

                if (isset($request->name) && $request->name == '1') {
                    $q->orWhere('name', 'like', '%' . $request->text . '%');
                }
            });

            /*if (isset($request->style) && $request->style == '1')
                $activeItemsQuery->where('style_no', 'like', '%' . $request->text . '%');

            if (isset($request->des) && $request->des == '1')
                $activeItemsQuery->where('description', 'like', '%' . $request->text . '%');

            if (isset($request->name) && $request->name == '1')
                $activeItemsQuery->where('name', 'like', '%' . $request->text . '%');*/

        }

        // Active Items Order
        if (isset($request->s1) && $request->s1 != '') {
            if ($request->s1 == '4')
                $activeItemsQuery->orderBy('price');
            else if ($request->s1 == '1')
                $activeItemsQuery->orderBy('updated_at', 'desc');
            else if ($request->s1 == '2')
                $activeItemsQuery->orderBy('created_at', 'desc');
            else if ($request->s1 == '3')
                $activeItemsQuery->orderBy('activated_at', 'desc');
            else if ($request->s1 == '5')
                $activeItemsQuery->orderBy('price', 'desc');
            else if ($request->s1 == '6')
                $activeItemsQuery->orderBy('style_no');
            else if ($request->s1 == '0') {
                $activeItemsQuery->orderBy('sorting');
                $activeItemsQuery->orderBy('activated_at', 'desc');
            }
        } else {
            $activeItemsQuery->orderBy('sorting');
            $activeItemsQuery->orderBy('updated_at', 'desc');
        }

        $activeItems = $activeItemsQuery->paginate(40, ['*'], 'p1');




        $defaultCategories = [];
        $vendorCategories = [];

        $customers = User::where('role', Role::$BUYER)->orderBy('created_at', 'desc')->get();

        $current_customer = session('order_customer_id' , '');

        $cart_items = $this->getcartItems();

        $selected_items_list = [];
        $items_list = CartItem::where('user_id', $current_customer)->get()->pluck('item_id');

        foreach ($items_list as $item){
            $selected_items_list[] = $item;
        }



        return view('admin.dashboard.new_order.index', compact( 'activeItems', 'defaultCategories', 'vendorCategories' , 'customers' , 'current_customer' , 'cart_items' , 'selected_items_list'))
            ->with('page_title', 'Create New Order');
    }

    public function set_new_customer(Request $request){
        $message = 'New customer is set where id is '.request('customerId');

        if(request('delete_old') == true){
            CartItem::where('user_id', session('order_customer_id'))->delete();

            session(['order_customer_id' => request('customerId')]);

        }

        session(['order_customer_id' => request('customerId')]);

        return response()->json(['success' => false, 'message' => $message]);
    }

   public function getcartItems(){
       $cartItems['total'] = 0;
       $sessionUser = session('order_customer_id');

       $items = CartItem::where('user_id', $sessionUser)->with('item')->get();
       $totalPrice = 0;
       $totalQty = 0;
       foreach($items as $item) {
           $qty = $item->quantity;
           $totalPrice += $item->quantity * $item->item->price;
           $totalQty += $item->quantity;

           if (array_key_exists($item->item->id, $cartItems))
               $qty = (int) $cartItems[$item->item->id]['qty'] + (int) $qty;

           $image_path = asset('images/no-image.png');

           if (sizeof($item->item->images) > 0)
               $image_path = asset($item->item->images[0]->thumbs_image_path);

           $cartItems[$item->item->id] = [
               'name' => $item->item->style_no,
               'qty' => $qty,
               'image_path' => $image_path,
               'price' => $item->item->price,
               'details_url' => route('item_details_page', ['item' => $item->item->id])
           ];
       }
       $cartItems['total'] = [
           'total_price' => $totalPrice,
           'total_qty' => $totalQty
       ];

       return $cartItems;
   }


}
