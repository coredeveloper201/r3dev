<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\PageEnumeration;
use App\Enumeration\SliderType;
use App\Enumeration\VendorImageType;
use App\Model\Banners;
use App\Model\Category;
use App\Model\Item;
use App\Model\SliderItem;
use App\Model\TopBanner;
use App\Model\VendorImage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Uuid;
use File;
use DB;

class BannerController extends Controller
{
    public function index(Request $request) {
        $parameters = [];
        $appends = array();

        if ($request->type){
            $parameters [] = array('type', '=', $request->type);
            $appends['type'] = $request->type;
        }

        if ($request->status){
            $status = ($request->status == '2') ? 0 : 1;
            $parameters [] = array('status', '=', $status);
            $appends['status'] = $request->status;
        }

        $images = VendorImage::where($parameters)
                    ->whereIn('type', [VendorImageType::$BIDDING_BIG_BANNER, VendorImageType::$BIDDING_SMALL_BANNER, VendorImageType::$MOBILE_MAIN_BANNER,
                    VendorImageType::$LOGO, VendorImageType::$SMALL_AD_BANNER, VendorImageType::$HOME_PAGE_BANNER])
                    ->paginate(10);


        $white = DB::table('settings')->where('name', 'logo-white')->first();
        $black = DB::table('settings')->where('name', 'logo-black')->first();

        return view('admin.dashboard.marketing_tools.banner.index', compact('images', 'appends', 'white', 'black'))->with('page_title', 'Banner Manager');
    }

    public function addPost(Request $request) {
        $request->validate([
            'logo' => 'nullable|mimes:jpeg,jpg,png,svg',
            'home_page_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=1713,height=441',
            'small_ad_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=448,height=206',
            'mobile_main_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=593,height=400',
            'bidding_big_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=1400,height=400',
            'bidding_small_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=376,height=270',
        ]);

        if ($request->logo) {
            $file = $request->file('logo');
            $this->uploadFile($file, VendorImageType::$LOGO);
        }

        if ($request->home_page_banner) {
            $file = $request->file('home_page_banner');
            $this->uploadFile($file, VendorImageType::$HOME_PAGE_BANNER);
        }

        if ($request->small_ad_banner) {
            $file = $request->file('small_ad_banner');
            $this->uploadFile($file, VendorImageType::$SMALL_AD_BANNER);
        }

        if ($request->mobile_main_banner) {
            $file = $request->file('mobile_main_banner');
            $this->uploadFile($file, VendorImageType::$MOBILE_MAIN_BANNER);
        }

        if ($request->bidding_big_banner) {
            $file = $request->file('bidding_big_banner');
            $this->uploadFile($file, VendorImageType::$BIDDING_BIG_BANNER);
        }

        if ($request->bidding_small_banner) {
            $file = $request->file('bidding_small_banner');
            $this->uploadFile($file, VendorImageType::$BIDDING_SMALL_BANNER);
        }

        return redirect()->back();
    }

    public function delete(Request $request) {
        $image = VendorImage::where('id', $request->id)->first();
        File::delete(public_path($image->image_path));
        $image->delete();
    }

    public function active(Request $request) {
        VendorImage::where('type', $request->type)->update(['status' => 0]);
        VendorImage::where('id', $request->id)->update(['status' => 1]);
    }

    public function uploadFile($file, $type) {
        $filename = Uuid::generate()->string;
        $ext = $file->getClientOriginalExtension();

        $destinationPath = '/images/banner';
        $file->move(public_path($destinationPath), $filename.".".$ext);
        $imagePath = $destinationPath."/".$filename.".".$ext;

        VendorImage::create([
            'type' => $type,
            'image_path' => $imagePath,
            'status' => 0
        ]);
    }

    public function bannerItems() {
        $items = Item::where('status', 1)
            ->get();

        $mainSliderItems = SliderItem::where('type', SliderType::$MAIN_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        $categoryTopSliderItems = SliderItem::where('type', SliderType::$CATEGORY_TOP_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        $categorySecondSliderItems = SliderItem::where('type', SliderType::$CATEGORY_SECOND_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        $newTopSliderItems = SliderItem::where('type', SliderType::$NEW_ARRIVAL_TOP_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        $newSecondSliderItems = SliderItem::where('type', SliderType::$NEW_ARRIVAL_SECOND_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        return view('admin.dashboard.marketing_tools.banner_items.index',
            compact('items', 'mainSliderItems', 'categoryTopSliderItems', 'categorySecondSliderItems', 'newTopSliderItems', 'newSecondSliderItems'))
            ->with('page_title', 'Banner Items');
    }

    public function bannerItemAdd(Request $request) {
        $query = SliderItem::query();

        if ($request->type == SliderType::$MAIN_SLIDER) {
            $query->where('type', SliderType::$MAIN_SLIDER);
            $count = $query->count();

            if ($count >= 8)
                return response()->json(['success' => false, 'message' => 'Already added 8 items']);
            else {
                $item = SliderItem::where([
                    ['item_id', $request->id],
                    ['type', SliderType::$MAIN_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['type', SliderType::$MAIN_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$MAIN_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }
        else if ($request->type == SliderType::$CATEGORY_TOP_SLIDER) {
            $query->where('type', SliderType::$CATEGORY_TOP_SLIDER);
            $count = $query->count();

            if ($count >= 6)
                return response()->json(['success' => false, 'message' => 'Already added 6 items']);
            else {
                $item = SliderItem::where([
                    ['item_id', $request->id],
                    ['type', SliderType::$CATEGORY_TOP_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['type', SliderType::$CATEGORY_TOP_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$CATEGORY_TOP_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }
        else if ($request->type == SliderType::$CATEGORY_SECOND_SLIDER) {
            $query->where('type', SliderType::$CATEGORY_SECOND_SLIDER);
            $count = $query->count();

            if ($count >= 6)
                return response()->json(['success' => false, 'message' => 'Already added 6 items']);
            else {
                $item = SliderItem::where([
                    ['item_id', $request->id],
                    ['type', SliderType::$CATEGORY_SECOND_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['type', SliderType::$CATEGORY_SECOND_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$CATEGORY_SECOND_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }
        else if ($request->type == SliderType::$NEW_ARRIVAL_TOP_SLIDER) {
            $query->where('type', SliderType::$NEW_ARRIVAL_TOP_SLIDER);
            $count = $query->count();

            if ($count >= 6)
                return response()->json(['success' => false, 'message' => 'Already added 6 items']);
            else {
                $item = SliderItem::where([
                    ['item_id', $request->id],
                    ['type', SliderType::$NEW_ARRIVAL_TOP_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['type', SliderType::$NEW_ARRIVAL_TOP_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$NEW_ARRIVAL_TOP_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }
        else if ($request->type == SliderType::$NEW_ARRIVAL_SECOND_SLIDER) {
            $query->where('type', SliderType::$NEW_ARRIVAL_SECOND_SLIDER);
            $count = $query->count();

            if ($count >= 6)
                return response()->json(['success' => false, 'message' => 'Already added 6 items']);
            else {
                $item = SliderItem::where([
                    ['item_id', $request->id],
                    ['type', SliderType::$NEW_ARRIVAL_SECOND_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['type', SliderType::$NEW_ARRIVAL_SECOND_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$NEW_ARRIVAL_SECOND_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }

        return response()->json(['success' => false, 'message' => '']);
    }

    public function bannerItemRemove(Request $request) {
        SliderItem::where('id', $request->id)->delete();
    }

    public function bannerItemsSort(Request $request) {
        $sort = 1;

        foreach ($request->ids as $id) {
            SliderItem::where('id', $id)->update(['sort' => $sort]);
            $sort++;
        }
    }

    public function mainSliderItems() {
        $images = VendorImage::where('type', VendorImageType::$MAIN_SLIDER)
            ->orderBy('sort')
            ->get();

        return view('admin.dashboard.marketing_tools.main_slider.index', compact('images'))->with('page_title', 'Main Slider');
    }

    public function mainSliderItemAdd(Request $request) {
        $request->validate([
            'photo' => 'required|mimes:jpg,jpeg,mp4',
            'link' => 'required',
        ]);

        $filename = Uuid::generate()->string;
        $file = $request->file('photo');
        $ext = $file->getClientOriginalExtension();

        $destinationPath = '/images/banner';
        $file->move(public_path($destinationPath), $filename.".".$ext);
        $imagePath = $destinationPath."/".$filename.".".$ext;

        $sort = VendorImage::where('type', VendorImageType::$MAIN_SLIDER)->max('sort');

        if ($sort == null || $sort == '')
            $sort = 0;

        $sort++;

        VendorImage::create([
            'type' => VendorImageType::$MAIN_SLIDER,
            'image_path' => $imagePath,
            'status' => 1,
            'url' => $request->link,
            'sort' => $sort,
            'color' => $request->color
        ]);


        return redirect()->back()->with('message', 'Successfully Added!.');
    }

    public function mainSliderItemsSort(Request $request) {
        $sort = 1;

        foreach ($request->ids as $id) {
            VendorImage::where('id', $id)->update(['sort' => $sort]);
            $sort++;
        }
    }

    public function mainSliderItemDelete(Request $request) {
        $image = VendorImage::where('id', $request->id)->first();

        if ($image->image_path != null){
            File::delete(public_path($image->image_path));
        }

        $image->delete();
    }

    public function frontPageBannerItems() {
        $images = VendorImage::where('type', VendorImageType::$FRONT_PAGE_BANNER)
            ->orderBy('sort')
            ->get();

        return view('admin.dashboard.marketing_tools.front_page_banner.index', compact('images'))->with('page_title', 'Front Page Banner');
    }

    public function frontPageBannerItemAdd(Request $request) {
        $request->validate([
            'photo' => 'required|mimes:jpg,jpeg',
            'link' => 'required',
        ]);

        $filename = Uuid::generate()->string;
        $file = $request->file('photo');
        $ext = $file->getClientOriginalExtension();

        $destinationPath = '/images/banner';
        $file->move(public_path($destinationPath), $filename.".".$ext);
        $imagePath = $destinationPath."/".$filename.".".$ext;

        $sort = VendorImage::where('type', VendorImageType::$FRONT_PAGE_BANNER)->max('sort');

        if ($sort == null || $sort == '')
            $sort = 0;

        $sort++;

        VendorImage::create([
            'type' => VendorImageType::$FRONT_PAGE_BANNER,
            'image_path' => $imagePath,
            'url' => $request->link,
            'status' => 1,
            'sort' => $sort
        ]);


        return redirect()->back()->with('message', 'Successfully Added!.');
    }

    public function editPost(Request $request) {
        VendorImage::where('id', $request->id)->update([
            'url' => $request->url,
            'color' => $request->color,
        ]);
    }

    public function topBanner() {
        $categories = Category::where('parent', 0)->get();
        $banners = TopBanner::all();

        return view('admin.dashboard.marketing_tools.top_banner.index', compact('categories', 'banners'))->with('page_title', 'Top Banner');
    }

    public function topBannerAdd(Request $request) {
        $request->validate([
            'photo' => 'required|mimes:jpg,jpeg',
            'link' => 'required',
            'page' => 'required'
        ]);

        $filename = Uuid::generate()->string;
        $file = $request->file('photo');
        $ext = $file->getClientOriginalExtension();

        $destinationPath = '/images/banner';
        $file->move(public_path($destinationPath), $filename.".".$ext);
        $imagePath = $destinationPath."/".$filename.".".$ext;

        $page = null;
        $category = null;
        $previous = null;

        if ($request->page == '-1') {
            $page = PageEnumeration::$NEW_ARRIVAL;
            $previous = TopBanner::where('page', $page)->first();
        } else if ($request->page == '-2') {
            $page = PageEnumeration::$BEST_SELLER;

            $previous = TopBanner::where('page', $page)->first();
        } else {
            $category = $request->page;

            $previous = TopBanner::where('category_id', $category)->first();
        }

        if ($previous) {
            unlink(ltrim($previous->image_path, '/'));
            $previous->delete();
        }

        TopBanner::create([
            'page' => $page,
            'category_id' => $category,
            'url' => $request->link,
            'image_path' => $imagePath
        ]);

        return redirect()->back();
    }

    public function topBannerDelete(Request $request) {
        TopBanner::where('id', $request->id)->delete();
    }

    public function topBannerEditPost(Request $request) {
        TopBanner::where('id', $request->id)->update([
            'url' => $request->url
        ]);
    }

    public function logoPost(Request $request) {
        $request->validate([
            'logo' => 'nullable|mimes:jpeg,jpg,png,svg',
            'logo2' => 'nullable|mimes:jpeg,jpg,png,svg',
        ]);

        if ($request->logo) {
            $file = $request->file('logo');
            $this->uploadLogo($file, 'logo-white');
        }

        if ($request->logo2) {
            $file = $request->file('logo2');
            $this->uploadLogo($file, 'logo-black');
        }

        return redirect()->route('admin_banner');
    }

    public function uploadLogo($file, $type) {
        $filename = Uuid::generate()->string;
        $ext = $file->getClientOriginalExtension();
        $destinationPath = 'images/logo';
        $file->move(public_path($destinationPath), $filename.".".$ext);
        $imagePath = $destinationPath."/".$filename.".".$ext;

        DB::table('settings')->where('name', $type)->update(['value' => $imagePath]);
    }


    //============================
    // Banner Items
    //============================
    public function frontBannerOne() {
        $banners = Banners::where('name', 1)->orderBy('id','asc')->get()->toArray();
        $bannerDetails = Banners::where('name', 111)->get()->first();
        return view('admin.dashboard.marketing_tools.front_page_banner.banner_one', compact('banners','bannerDetails'))->with('page_title', 'Banner One');
    }
    public function frontBannerTwo() {
        $banners = Banners::where('name', 2)->orderBy('id','asc')->get()->toArray();
        $bannerDetails = Banners::where('name', 222)->get()->first();
        return view('admin.dashboard.marketing_tools.front_page_banner.banner_two', compact('banners','bannerDetails'))->with('page_title', 'Banner Two');
    }
    public function uploadBanner(Request $request) {


        $input = $request->input();
        if($input['type'] > 100){

            $request->validate([
                'type' => 'required|integer',
                'value' => 'required',
            ]);

            $bannerDetails = Banners::where('name', $input['type'])->get()->first();
            if($bannerDetails == null){
                $bannerModel = new Banners();
                $bannerModel->name = $input['type']; // 1. banner One, 2. Banner Two, 111. banner One details, , 222. banner One details
                $bannerModel->value = $input['value'];
                $bannerModel->link = null;
                $bannerModel->status = 1;
                $bannerModel->user_id = Auth::user()->id;
                $bannerModel->created_at = Carbon::now();
                $bannerModel->save();
            } else {
                $bannerDetails->value = $input['value'];
                $bannerDetails->save();
            }


        } else {

            $request->validate([
                'type' => 'required|integer',
                'photo' => 'required|mimes:jpg,jpeg,mp4,png,gif',
            ]);


            $filename = Uuid::generate()->string;
            $file = $request->file('photo');
            $ext = $file->getClientOriginalExtension();

            $destinationPath = '/images/banner';
            $file->move(public_path($destinationPath), $filename.".".$ext);
            $input['value'] = $filename.".".$ext;

            if(isset($input['id'])){
                $bannerDetails = Banners::where('name', $input['type'])->where('id', $input['id'])->get()->first();
                $bannerDetails->value = $input['value'];
                $bannerDetails->save();
            } else {
                $bannerModel = new Banners();
                $bannerModel->name = $input['type']; // 1. banner One, 2. Banner Two, 111. banner One details, 222. banner One details
                $bannerModel->value = $input['value'];
                $bannerModel->link = $input['link'];
                $bannerModel->status = 1;
                $bannerModel->user_id = Auth::user()->id;
                $bannerModel->created_at = Carbon::now();
                $bannerModel->save();
            }
        }


        return redirect()->back();
    }
    public function removeBanner(Request $request) {
        $input = $request->input();

        $check = Banners::where('id', $input['id'])->get()->first();
        if($check != null){
            File::delete(public_path('images\banner\\'.$check->value));
            Banners::where('id', $input['id'])->delete();
        }
        return redirect()->back();
    }
}
