<?php

namespace App\Http\Controllers;

use App\Enumeration\PageEnumeration;
use App\Model\Page;
use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    public function aboutUs() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$ABOUT_US)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'Our Story');
    }

    public function contactUs() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$CONTACT_US)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'Contact Us');
    }

    public function privacyPolicy() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$PRIVACY_POLICY)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'Privacy Policy');
    }

    public function returnInfo() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$RETURN_INFO)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'Return Info');
    }

    public function billingShipping() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$BILLING_SHIPPING_INFO)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'Billing & Shipping Info');
    }

    public function largeQuantities() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$LARGE_QUANTITIES)->first();
        if ($page)
            $content = $page->content;

        return view('pages.static', compact('content'))->with('title', 'Large Quantities / Pre Orders');
    }

    public function refunds() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$REFUNDS)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'Refunds & Replacements');
    }

    public function faq() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$FAQ)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'FAQ');
    }

    public function our_story() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$ABOUT_US)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'Our Story');
    }

    public function contactUsPost(Request $request) {
        dd($request->all());
    }

    public function termsCondiditions() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$TERMS_CONDITIONS)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'Terms & Conditions');
    }

    public function cookiesPolicy() {
        $content = '';

        $page = Page::where('page_id', PageEnumeration::$COOKIES_POLICY)->first();
        if ($page)
            $content = $page->content;


        return view('pages.static', compact('content'))->with('title', 'Cookies Policy');
    }
}
