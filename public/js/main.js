$(document).ready(function () {

    /*$(".showhide").click(function () {
        $(".for_mobile .mobile_menu").addClass('menu_open');
        return false;
    });*/

    $('body').on('click','.showhide', function () {
        $('.for_mobile .mobile_menu').addClass('menu_open');
        $('.for_mobile .mobile_overlay').addClass('mobile_overlay_open');
        $('.for_mobile .ic_c_mb').addClass('ic_c_mb_show');
        return false;
    });
    $('.ic_c_mb').click(function () {
        $('.mobile_menu').removeClass('menu_open');
        $('.mobile_overlay').removeClass('mobile_overlay_open');
        $('.ic_c_mb').removeClass('ic_c_mb_show');
    });
    $('.mobile_overlay').click(function () {
        $('.mobile_menu').removeClass('menu_open');
        $('.mobile_overlay').removeClass('mobile_overlay_open');
        $('.ic_c_mb').removeClass('ic_c_mb_show');
    });


    $("#header_search_ic").click(function () {
        console.log("this is the click");
        $(".header_search").addClass("main");
    });

    $('.header_search_inner span').click(function () {
        $('.header_search').hide();
        return false;
    });


    /*
    =========================================================================================
        SHOW RIGHT REVIEW MENU
    =========================================================================================

    */
    $('.write_review span').click(function () {
        $('.review_rating_right_menu').addClass('review_rating_right_menu_open');
        $('.full_screen_overlay').addClass('overlay_open');
        return false;
    });

    $('.close_ic').click(function () {
        $('.review_rating_right_menu').removeClass('review_rating_right_menu_open');
        $('.full_screen_overlay').removeClass('overlay_open');
    });


    $(document).mouseup(function (e) {
        var container = $('.review_rating_right_menu');
        if (!container.is(e.target)
            && container.has(e.target).length === 0) {
            $('.review_rating_right_menu').removeClass('review_rating_right_menu_open');
            $('.full_screen_overlay').removeClass('overlay_open');
        }
    });

    /*
    =========================================================================================
        HOVER FUNCTION
    =========================================================================================

    */

    $('.home_coveted_right1').hover(function () {
        $(this).addClass("coveted_inner_hover");
    }, function () {
        $(this).removeClass("coveted_inner_hover");
    });
    $('.home_coveted_right2').hover(function () {
        $(this).addClass("coveted_inner_hover");
    }, function () {
        $(this).removeClass("coveted_inner_hover");
    });


    $("#menuzord").menuzord();

    $(window).scroll(function () {
        var scrollTop = $(this).scrollTop();

        $('.image_rounded').css({
            opacity: function () {
                var elementHeight = $(this).height(),
                    opacity = ((1 + (elementHeight - scrollTop) / elementHeight) * 0.8) + 0.2;

                return opacity;
            }
        });
    });


    $('.product_wishlist button').click(function (e) {
        $('.product_wishlist').hide();
    });
    $('.terms_menu ul li a').click(function (e) {
        e.preventDefault();
        var target = $($(this).attr('href'));
        if (target.length) {
            var scrollTo = target.offset().top;
            $('body, html').animate({scrollTop: scrollTo + 'px'}, 800);
        }
    });


    $(".scroll_down").click(function () {
        $('html,body').animate({scrollTop: $("#to_bottom").offset().top - 100}, 1000, 'swing');
    });


    $('.image-link').magnificPopup({type: 'inline'});
    $('.video').magnificPopup({
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">' +
            '<div class="mfp-close"></div>' +
            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
            '<div class="mfp-title">Some caption</div>' +
            '</div>'
        },
        callbacks: {
            markupParse: function (template, values, item) {
                values.title = item.el.attr('title');
            }
        }

    });

    $('.card_verification label span').click(function () {
        $('.chekout_pop_up').toggle();
    });

    // $('.popup-vimeo').magnificPopup({
    //     items: {
    //         src: 'https://vimeo.com/148198462'
    //     },
    //     type: 'iframe'
    // });
    // $('.popup-vimeo').magnificPopup({
    //     disableOn: 700,
    //     type: 'iframe',
    //     mainClass: 'mfp-fade',
    //     removalDelay: 160,
    //     preloader: false,

    //     fixedContentPos: false
    // });

    $('#datepicker').datepicker();

    $(".subscriptions").click(function () {
        $(this).parent().parent().addClass('selected').siblings().removeClass('selected');
        $(this).text('selected');
    });

    /*
    =========================================================================================
    5.  CART INCREASE DECREASE VALUE
    =========================================================================================
    */

    $('.btn-number').click(function (e) {
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {

                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if (type == 'plus') {

                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function () {
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function () {

        minValue = parseInt($(this).attr('min'));
        maxValue = parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if (valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });


    /*
    =========================================================================================
        BANNER SLIDER
    =========================================================================================
    */

    var banner_slider = jQuery("#banner_slider");
    banner_slider.owlCarousel({
        loop: false,
        margin: 0,
        lazyLoad: true,
        center: true,
        smartSpeed: 1500,
        autoplay: false,
        nav: true,
        animateOut: 'fadeOut',
        dots: true,
        navText: ["<img src='images/bannerarrowleft.png'>", "<img src='images/bannerarrowright.png'>"],
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });


    /*
    =========================================================================================
        BANNER SLIDER
    =========================================================================================
    */

    var product_slider = jQuery("#product_slider");
    product_slider.owlCarousel({
        loop: false,
        margin: 30,
        lazyLoad: true,
        smartSpeed: 500,
        autoplay: false,
        nav: false,
        mouseDrag: false,
        dots: true,
        responsive: {
            0: {
                items: 2
            },
            400: {
                items: 2
            },
            768: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });

    /*
    =========================================================================================
        BANNER SLIDER
    =========================================================================================
    */

    var product2_slider = jQuery("#product2_slider");
    product2_slider.owlCarousel({
        loop: false,
        margin: 30,
        lazyLoad: true,
        smartSpeed: 1500,
        autoplay: false,
        nav: false,
        mouseDrag: false,
        dots: true,
        responsive: {
            0: {
                items: 2
            },
            400: {
                items: 2
            },
            768: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });

    /*
    =========================================================================================
        BANNER SLIDER
    =========================================================================================
    */

    var product3_slider = jQuery("#product3_slider");
    product3_slider.owlCarousel({
        loop: false,
        margin: 30,
        lazyLoad: true,
        smartSpeed: 1500,
        autoplay: false,
        nav: false,
        mouseDrag: false,
        dots: true,
        responsive: {
            0: {
                items: 2
            },
            400: {
                items: 2
            },
            768: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });

    /*
    =========================================================================================
        HOME COVETED SLIDER
    =========================================================================================
    */

    var coveted_slider = jQuery("#coveted_slider");
    coveted_slider.owlCarousel({
        loop: false,
        margin: 0,
        lazyLoad: true,
        center: true,
        smartSpeed: 1000,
        autoplay: true,
        animateOut: 'fadeOut',
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });


    /*
    =========================================================================================
        HOME COVETED 2 SLIDER
    =========================================================================================
    */

    var coveted_slider2 = jQuery("#coveted_slider2");
    coveted_slider2.owlCarousel({
        loop: false,
        margin: 0,
        lazyLoad: true,
        center: true,
        smartSpeed: 1000,
        autoplay: true,
        animateOut: 'fadeOut',
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });

    /*
    =========================================================================================
        HOW IT WORK SLIDE 2
    =========================================================================================
    */

    var hiw_slider1 = jQuery("#hiw_slider1");
    hiw_slider1.owlCarousel({
        loop: false,
        margin: 0,
        lazyLoad: true,
        center: true,
        smartSpeed: 1000,
        autoplay: true,
        animateOut: 'fadeOut',
        navText: ["<img src='images/bannerarrowleft.png'>", "<img src='images/bannerarrowright.png'>"],
        nav: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });


    /*
    =========================================================================================
        HOW IT WORK SLIDE 2
    =========================================================================================
    */

    var hiw_slider2 = jQuery("#hiw_slider2");
    hiw_slider2.owlCarousel({
        loop: false,
        margin: 0,
        lazyLoad: true,
        center: true,
        smartSpeed: 1000,
        autoplay: true,
        animateOut: 'fadeOut',
        navText: ["<img src='images/bannerarrowleft.png'>", "<img src='images/bannerarrowright.png'>"],
        nav: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });

    /*
    =========================================================================================
        HOW IT WORK SLIDE 1
    =========================================================================================
    */

    var hiw_slider3 = jQuery("#hiw_slider3");
    hiw_slider3.owlCarousel({
        loop: false,
        margin: 0,
        lazyLoad: true,
        center: true,
        smartSpeed: 1000,
        autoplay: false,
        mouseDrag: false,
        animateOut: 'fadeOut',
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });

    /*
    =========================================================================================
        HOW IT WORK SLIDE 1
    =========================================================================================
    */

    var hiw_slider4 = jQuery("#hiw_slider4");
    hiw_slider4.owlCarousel({
        loop: false,
        margin: 20,
        lazyLoad: true,
        smartSpeed: 1000,
        autoplay: false,
        mouseDrag: false,
        animateOut: 'fadeOut',
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });


    /*
    =========================================================================================
        TEAM SLIDER
    =========================================================================================
    */

    var team_slider = jQuery("#team_slider");
    team_slider.owlCarousel({
        loop: false,
        margin: 40,
        lazyLoad: true,
        smartSpeed: 1000,
        autoplay: true,
        animateOut: 'fadeOut',
        navText: ["<img src='images/bannerarrowleft.png'>", "<img src='images/bannerarrowright.png'>"],
        nav: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });

    /*
    =========================================================================================
       CLEANSE INSIDE SLIDE
    =========================================================================================
    */

    var cleanse_inside_slide = jQuery("#cleanse_inside_slide");
    cleanse_inside_slide.owlCarousel({
        loop: false,
        margin: 20,
        lazyLoad: true,
        smartSpeed: 1000,
        autoplay: false,
        mouseDrag: false,
        animateOut: 'fadeOut',
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });

    /*
    =========================================================================================
       CLEANSE THUMBNAIL SLIDE
    =========================================================================================
    */

    var cleanse_thumbnail_slide = jQuery("#cleanse_thumbnail_slide");
    cleanse_thumbnail_slide.owlCarousel({
        loop: false,
        margin: 20,
        lazyLoad: true,
        smartSpeed: 1000,
        autoplay: false,
        mouseDrag: false,
        animateOut: 'fadeOut',
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });

    /*
    =========================================================================================
       CLEANSE THUMBNAIL SLIDE
    =========================================================================================
    */

    var blog_single_slide = jQuery("#blog_single_slide");
    blog_single_slide.owlCarousel({
        loop: false,
        margin: 20,
        lazyLoad: true,
        smartSpeed: 1000,
        autoplay: false,
        mouseDrag: false,
        navText: ["<img src='images/blog/blogarrowleft.png'>", "<img src='images/blog/blogarrowright.png'>"],
        nav: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });

    /*
    =========================================================================================
        MATCH HEIGHT
    =========================================================================================
    */
    $('.match_item').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });

});


var x, i, j, selElmnt, a, b, c;
x = document.getElementsByClassName("custom_select");
for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function (e) {
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function (e) {
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}

function closeAllSelect(elmnt) {
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

document.addEventListener("click", closeAllSelect);


// $(document).ready(function() {
//   $('#autoWidth').lightSlider({
//       autoWidth:true,
//       loop:true,
//       onSliderLoad: function() {
//           $('#autoWidth').removeClass('cS-hidden');
//       }
//   });
// });


$(document).ready(function () {
    $('#imageGallery').lightSlider({
        gallery: true,
        item: 1,
        loop: false,
        thumbItem: 9,
        slideMargin: 0,
        enableDrag: false,
        currentPagerPosition: 'center',
    });
});

// $('.btn_common_type2').click(function(){
//   $('.nav-tabs > .active').next('li').find('a').trigger('click');
// });
// $('.back').click(function(){
//   $('.nav-tabs > .active').prev('li').find('a').trigger('click');
// });

function myFunction() {
    if (confirm("Are you sure you want to remove this product from your wishlist?")) {
        console.log(12009);
    }
}
