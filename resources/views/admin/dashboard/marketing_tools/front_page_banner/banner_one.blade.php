@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')

    <div class="row {{ ($errors && sizeof($errors) > 0) ? 'd-none' : '' }}" id="addBtnRow">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNew">Add New Image</button>
        </div>
    </div>

    <div class="row {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="addEditRow">
        <div class="col-md-12" style="border: 1px solid black; padding: 15px;">
            <h3><span id="addEditTitle">Add New Image</span></h3>

            <form class="form-horizontal" enctype="multipart/form-data" id="form" method="post" action="{{ route('admin_upload_banners') }}">
                @csrf

                <div class="form-group row{{ $errors->has('photo') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="photo" class="col-form-label">Image *</label>
                    </div>

                    <div class="col-lg-5">
                        <input class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}"
                               type="file" id="photo" name="photo" accept="image/*">
                    </div>
                </div>

                @if ($errors->has('photo'))
                    <div class="form-control-feedback">{{ $errors->first('photo') }}</div>
                @endif

                <div class="form-group row{{ $errors->has('link') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="link" class="col-form-label">Link *</label>
                    </div>

                    <div class="col-lg-5">
                        <input class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}"
                               type="text" id="link" name="link" value="{{ old('link') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <input type="hidden" name="type" value="1">
                        <a class="btn btn-danger" href="{{route('admin_front_banner_one')}}">Cancel</a>
                        <button type="submit" id="btnSubmit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="EditBanRow">
        <div class="col-md-12" style="border: 1px solid black; padding: 15px;">
            <h3><span id="addEditTitle">Edit Image</span></h3>
            <br><br>

            <form class="form-horizontal" enctype="multipart/form-data" id="form" method="post" action="{{ route('admin_upload_banners') }}">
                @csrf

                <div class="form-group row{{ $errors->has('photo') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="photo" class="col-form-label">Image *</label>
                    </div>

                    <div class="col-lg-5">
                        <input class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}"
                               type="file" id="photo" name="photo" accept="image/*">
                    </div>
                </div>

                @if ($errors->has('photo'))
                    <div class="form-control-feedback">{{ $errors->first('photo') }}</div>
                @endif

                <div class="form-group row{{ $errors->has('link') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="link" class="col-form-label">Link *</label>
                    </div>

                    <div class="col-lg-5">
                        <input class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}"
                               type="text" id="link" name="link" value="{{ old('link') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="type" value="1">
                        <a class="btn btn-danger" href="{{route('admin_front_banner_one')}}">Cancel</a>
                        <button type="submit" id="btnSubmit" class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="banner-item-container">
                <ul id="HomePageSliderItems">
                    @foreach($banners as $banner)
                        <li class="text-center" data-id="{{ $banner['id'] }}">
                            <img src="{{ asset('/images/banner/'.$banner['value']) }}">
                            <a class="text-danger btnRemove" data-type="1" data-id="{{ $banner['id'] }}">Remove</a> |
                            <a class="text-info btnEdit" data-id="{{ $banner['id'] }}">Edit</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>


    <br>
    <hr>
    <br>

    <form method="POST" action="{{ route('admin_upload_banners') }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <h3>Details</h3>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <textarea class="d-none" name="value" id="page_editor" rows="2">{{ $bannerDetails != null ? $bannerDetails->value : null }}</textarea>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 text-right">
                <input type="hidden" name="type" value="111">
                <button type="submit" class="btn btn-primary">Save Changes</button>
            </div>
        </div>
    </form>

@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sortable/js/Sortable.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var selectedId;
            var message = '{{ session('message') }}';
            var images = <?php echo json_encode($banners); ?>;

            if (message != '')
                toastr.success(message);

            $('#btnAddNew').click(function () {
                $('#addEditRow').removeClass('d-none');
                $('#EditBanRow').addClass('d-none');
                $('#addBtnRow').addClass('d-none');
            });
            $('.btnEdit').click(function () {
                $('#EditBanRow').removeClass('d-none');
                $('#addEditRow').addClass('d-none');
                $('#addBtnRow').addClass('d-none');
                var id = $(this).attr('data-id');
                $('#EditBanRow').find('input[name="id"]').val(id);
            });

            $('#btnCancel').click(function (e) {
                e.preventDefault();

                $('#addEditRow').addClass('d-none');
                $('#addBtnRow').removeClass('d-none');

                // Clear form
                $('input').removeClass('is-invalid');
                $('.form-group').removeClass('has-danger');
            });

            $('.btnRemove').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
                if(confirm('Are you really want to remove this item?')){
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_remove_banners') }}",
                        data: { id: selectedId }
                    }).done(function( msg ) {
                        location.reload();
                    });
                }
            });


            var el = document.getElementById('HomePageSliderItems');
            Sortable.create(el, {
                animation: 150,
                dataIdAttr: 'data-id',
                onEnd: function () {
                    updateSort(this.toArray());
                },
            });

            function updateSort(ids) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_main_slider_items_sort') }}",
                    data: { ids: ids }
                }).done(function( msg ) {
                    toastr.success('Items sort updated!');
                });
            }

        })
    </script>

    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}?id={{ rand() }}"></script>--}}
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        $(function () {
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            var options = {
                filebrowserImageBrowseUrl: '{{ url('laravel-filemanager') }}?type=Images',
                filebrowserImageUploadUrl: '{{ route('unisharp.lfm.upload') }}?type=Images&_token=',
                filebrowserBrowseUrl: '{{ url('laravel-filemanager') }}?type=Files',
                filebrowserUploadUrl: '{{ url('laravel-filemanager') }}?type=Files&_token='
            };
            CKEDITOR.replace('page_editor', options);
        });
    </script>
@stop