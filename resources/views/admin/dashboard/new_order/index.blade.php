<?php
    use App\Enumeration\Availability;
?>

@extends('admin.layouts.main')

@section('additionalCSS')
<style>
    .cart_fixed_bottom {}
    .cart_fixed_bottom > span{
        text-decoration: underline;
        display: inline-block;
        cursor: pointer;
    }
    .cart_fixed_bottom ul{
        text-align: center;
    }
    .cart_fixed_bottom ul li{}
    .cart_fixed_bottom ul li a{
        position: relative;
        color: #fff;
        display: block;
        text-transform: uppercase;
        text-align: center;
    }
    .cart_fixed_bottom ul li a span{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        font-size: 20px;
        padding-top: 13px;
    }
    .cart_fixed_bottom ul li a span{}
    .cart_fixed_bottom ul li a img{
        width: 35px;
    }
    .cart_fixed_bottom ul li:nth-of-type(2) a{
        background: #c57ce7;
        padding: 15px 23px;
        margin: 8px 0px;
        font-size: 12px;
    }
    .cart_fixed_bottom ul li a img{}
</style>
@stop

@section('content')
    <div class="item_border">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="form-group row item_list_search">
                        {{--<label for="inputPassword3" class="col-1 col-form-label">Search</label>--}}
                        {{--<div class="col-10">--}}
                            {{--<div class="form-check custom_checkbox">--}}
                                {{--<input class="form-check-input" type="checkbox" id="searchStyleNo"--}}
                                        {{--{{ (request()->get('style') == '1' || request()->get('style') == null) ? 'checked' : '' }}>--}}
                                {{--<label class="form-check-label" for="searchStyleNo">--}}
                                    {{--Style No.--}}
                                {{--</label>--}}
                            {{--</div>--}}
                            {{--<div class="form-check custom_checkbox">--}}
                                {{--<input class="form-check-input" type="checkbox" id="searchDescription"--}}
                                        {{--{{ (request()->get('des') == '1') ? 'checked' : '' }}>--}}
                                {{--<label class="form-check-label" for="searchDescription">--}}
                                    {{--Full Description--}}
                                {{--</label>--}}
                            {{--</div>--}}
                            {{--<div class="form-check custom_checkbox">--}}
                                {{--<input class="form-check-input" type="checkbox" id="searchItemName"--}}
                                        {{--{{ (request()->get('name') == '1') ? 'checked' : '' }}>--}}
                                {{--<label class="form-check-label" for="searchItemName">--}}
                                    {{--Item Name--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="col-8">
                            <select class="form-control" id="selectedCustomer">
                                <option value="">Select Customer</option>
                                @foreach($customers as $customer)
                                    <option value="{{$customer->id}}" {{$customer->id == $current_customer ? 'selected' : ''}} >{{ $customer->first_name." ".$customer->last_name  }}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="col-3">
                            <input type="hidden" id="current_customer" value="{{$current_customer}}">
                            {{--<button class="btn btn-primary" id="selectCustomer">Select</button>--}}
                        </div>

                        <div class="col-1">
                            <div class="cartGlobal">
                                @if($cart_items['total']['total_qty'] > 0)
                                    <div class="cart_fixed_bottom">
                                        <ul>
                                            <li><a href="{{route('show_admin_cart')}}"><img src="{{asset('/images/shopping_bag.svg')}}" alt="">
                                                    <span>{{$cart_items['total']['total_qty']}}</span></a></li>
                                            {{--<li><a href="{{route('show_cart')}}">see my item</a></li>--}}
                                        </ul>
                                        {{--<span onclick="closeCart(this)">Close</span>--}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                    {{--<div class="form-group row item_list_search">--}}
                        {{--<div class="col-6">--}}
                            {{--<input type="text" class="form-control" placeholder="(Use commas(,) for multiple style search)" id="inputText" value="{{ request()->get('text') }}">--}}
                            {{--<button class="btn btn-primary" id="btnSearch">search</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="global_accordion">
        <div class="container-fluid no-padding">
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne">
                                Active Items - {{ sizeof($activeItems) }} Items
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show">
                        <div class="card-body">
                            <div class="accordion_content">
                                <div class="container-fluid no-padding">
                                    <div class="row accordion_filter">
                                        <div class="col-2 no-padding-left">
                                            <select class="form-control" id="selectSortActiveItems">
                                                <option value="1" {{ request()->get('s1') == '1' ? 'selected' : '' }}>Last Update</option>
                                                <option value="0" {{ request()->get('s1') == '0' ? 'selected' : '' }}>Sort Number</option>
                                                <option value="2" {{ request()->get('s1') == '2' ? 'selected' : '' }}>Upload Date</option>
                                                <option value="3" {{ request()->get('s1') == '3' ? 'selected' : '' }}>Activation Date</option>
                                                <option value="4" {{ request()->get('s1') == '4' ? 'selected' : '' }}>Price Low to High</option>
                                                <option value="5" {{ request()->get('s1') == '5' ? 'selected' : '' }}>Price High to Low</option>
                                                <option value="6" {{ request()->get('s1') == '6' ? 'selected' : '' }}>Style No.</option>
                                            </select>
                                        </div>
                                        <div class="col-6">

                                        </div>
                                        {{--<div class="col-4 no-padding-right">--}}
                                            {{--<div class="accordion_btn text-right">--}}
                                                {{--<button class="btn btn-secondary" id="btnSelectAllActive">Select All</button>--}}
                                                {{--<button class="btn btn-secondary" id="btnDeselectAllActive">Deselect All</button>--}}
                                                {{--<button class="btn btn-primary" id="btnDeactive">Deactivate</button>--}}
                                                {{--<button class="btn btn-primary" id="btnExportActive">Export To SP</button>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="single_product_wrapper">
                                                @foreach($activeItems as $item)
                                                    <div class="single_product">
                                                        <div class="form_checkbox">
                                                            {{--<input class="form-check-input checkbox-active-items" type="checkbox" id="productCB_{{ $item->id }}" data-id="{{ $item->id }}">--}}
                                                            {{--<label class="form-check-label" for="productCB_{{ $item->id }}">--}}
                                                            {{--</label>--}}
                                                        </div>
                                                        <div class="single_product_text">
                                                            <span class="single_img">
                                                                <a href="{{ route('admin_edit_item', ['item' => $item->id]) }}">
                                                                    @if (sizeof($item->images) > 0)
                                                                        <img src="{{ asset($item->images[0]->image_path) }}" alt="{{ $item->style_no }}">
                                                                    @else
                                                                        <img src="{{ asset('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                                                    @endif
                                                                </a>
                                                                {{--<a class="edit" href="{{ route('admin_edit_item', ['item' => $item->id]) }}"><i class="fas fa-edit"></i></a>--}}
                                                            </span>
                                                            <span class="single_product_desc">
                                                                <h2>{{ $item->style_no }}</h2>
                                                                {{--<h3><a href="{{ route('admin_edit_item', ['item' => $item->id]) }}">{{ $item->name }}</a></h3>--}}
                                                                <h2>
                                                                    @if ($item->orig_price != null)
                                                                        <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                                                    @endif
                                                                    ${{ number_format($item->price, 2, '.', '') }}
                                                                </h2>
                                                            </span>
                                                            <span class="single_product_desc">

                                                                <h3 id="added_to_cart_{{$item->id}}" style="{{in_array($item->id ,$selected_items_list) ? 'visibility : visible' : 'visibility : hidden'}}">Already Added to Cart</h3>

                                                                    <h3><a href="#" class="add_to_cart" id="add_to_cart_{{$item->id}}" style="{{!in_array($item->id ,$selected_items_list) ? 'visibility : visible' : 'visibility : hidden'}}">Add To Cart</a></h3>


                                                            </span>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="row accordion_filter accordion_filter_bottom">--}}
                                        {{--<div class="col-12 text-center">--}}
                                            {{--{{ $activeItems->appends($appends)->links() }}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>

<div class="modal fade" id="selectSPCategory" role="dialog" aria-labelledby="deleteModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="deleteModal">Select Category</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label for="d_parent_category" class="col-form-label">Category</label>
                    </div>

                    <div class="col-lg-3">
                        <select class="form-control" name="d_parent_category" id="d_parent_category">
                            <option value="">Select Category</option>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <select class="form-control" name="d_second_parent_category" id="d_second_parent_category">
                            <option value="">Sub Category</option>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <select class="form-control" name="d_third_parent_category" id="d_third_parent_category">
                            <option value="">Sub Category</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label for="d_parent_category" class="col-form-label">Vendor Category</label>
                    </div>

                    <div class="col-lg-4">
                        <select class="form-control" id="vendor_category">
                            <option value="">Select Category</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn  btn-default" data-dismiss="modal">Close</button>
                <button class="btn  btn-primary" id="modalBtnExport">Export</button>
            </div>
        </div>
    </div>
    <!--- end modals-->
</div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $('#selectSortActiveItems, #selectSortInactiveItems').change(function () {
                checkParameters();
            });

            $('#btnSearch').click(function () {
                search();
            });

            $('#inputText').keypress(function(e) {
                if(e.which == 13) {
                    search();
                }
            });

            $('#btnSelectAllActive').click(function () {
                $('.checkbox-active-items').prop('checked', false).trigger('click');
            });

            $('#btnDeselectAllActive').click(function () {
                $('.checkbox-active-items').prop('checked', true).trigger('click');
            });

            $('#btnSelectAllInactive').click(function () {
                $('.checkbox-inactive-items').prop('checked', false).trigger('click');
            });

            $('#btnDeselectAllInactive').click(function () {
                $('.checkbox-inactive-items').prop('checked', true).trigger('click');
            });

            $('#btnDeactive').click(function () {
                var ids = [];

                $('.checkbox-active-items').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_item_list_change_to_inactive') }}",
                        data: {ids: ids}
                    }).done(function (msg) {
                        window.location.reload();
                    });
                }
            });

            $('#btnActive').click(function () {
                var ids = [];

                $('.checkbox-inactive-items').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_item_list_change_to_active') }}",
                        data: {ids: ids}
                    }).done(function (msg) {
                        window.location.reload();
                    });
                }
            });

            $('#btnDelete').click(function () {
                var ids = [];

                $('.checkbox-inactive-items').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_item_list_delete') }}",
                        data: {ids: ids}
                    }).done(function (msg) {
                        window.location.reload();
                    });
                }
            });


            $('.add_to_cart').click(function (e) {
                e.preventDefault();

                if(document.getElementById('current_customer').value == ''){
                    alert('please select a customer');
                    return;
                }
                var actual_id = $(this).attr('id');
                var id = $(this).attr('id').split('_')[3];
                // console.log($(this).attr('id'));
                // console.log(id);

                    $.ajax({
                        method: "POST",
                        url: "{{ route('cart_item_add') }}",
                        data: {itemId: id , qty : 1}
                    }).done(function (res) {
                        console.log(res);

                        if (res.status === 2000) {
                            var th = `<div class="cart_fixed_bottom">
                                <ul>
                                    <li><a href="{{route('show_admin_cart')}}"><img src="{{asset('/images/shopping_bag.svg')}}" alt=""> <span>` + res.count + `</span></a></li>

                                </ul>

                            </div>`;
                            $('.cartGlobal').html(th);

                            document.getElementById(actual_id).style.visibility = 'hidden';
                            document.getElementById('added_to_cart_'+id).style.visibility = 'visible';

                           // $('#'+actual_id).attr('id');
                            //$('#cartCountNow').html(res.count);
                        }
                    });

            });

            $('#selectedCustomer').change(function () {
                var id = document.getElementById('selectedCustomer').value;

                if(id == document.getElementById('current_customer').value)
                    return;

                if(id == '')
                {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('set_new_customer') }}",
                        data: {customerId: id, delete_old : true}
                    }).done(function (msg) {
                        document.getElementById('current_customer').value = id;
                        console.log(msg);

                        $('.cartGlobal').html('');
                        window.location.reload();
                    });

                    return;
                }

                if(id !== document.getElementById('current_customer').value && document.getElementById('current_customer').value !== '' && id !== ''){
                    // var check = confirm('if you select this customer current cart items for the previous customer will be cleared');
                    //
                    // if(check){

                        $.ajax({
                            method: "POST",
                            url: "{{ route('set_new_customer') }}",
                            data: {customerId: id, delete_old : true}
                        }).done(function (msg) {
                            document.getElementById('current_customer').value = id;
                            console.log(msg);

                            $('.cartGlobal').html('');
                            window.location.reload();
                        });
                   // }

                    return;
                }


                $.ajax({
                method: "POST",
                url: "{{ route('set_new_customer') }}",
                data: {customerId: id, delete_old : false}
                }).done(function (msg) {
                    document.getElementById('current_customer').value = id;
                    console.log(msg);
                //window.location.reload();
                });

            })
            
            function checkParameters() {
                var s1 = $('#selectSortActiveItems').val();
                var s2 = $('#selectSortInactiveItems').val();

                var parameters = <?php echo json_encode(request()->all()); ?>;
                var url = '{{ route('admin_new_order_create') }}' + '?s1=' + s1 + '&s2=' + s2;

                $.each(parameters, function (key, value) {
                    if (key != 's1' && key != 's2' && key != 'p1' && key != 'p2') {
                        var val = '';

                        if (value != null)
                            val = value;

                        url += '&' + key + '=' + val;
                    }
                });
                window.location.replace(url);
            }
            
            function search() {
                var s1 = $('#selectSortActiveItems').val();
                var s2 = $('#selectSortInactiveItems').val();
                var text = $('#inputText').val();
                var searchStyleNo = ($('#searchStyleNo').is(':checked')) ? 1 : 0;
                var description = ($('#searchDescription').is(':checked')) ? 1 : 0;
                var name = ($('#searchItemName').is(':checked')) ? 1 : 0;

                var url = '{{ route('admin_item_list_all') }}' + '?s1=' + s1 + '&s2=' + s2 + '&text=' + text + '&style=' + searchStyleNo +
                    '&des=' + description + '&name=' + name;
                window.location.replace(url);
            }

            // Export to SP
            var selectActive = 0;
            var defaultCategories = [];
            function getDefaultCategories() {
                defaultCategories = <?php echo json_encode($defaultCategories); ?>;
                $('#d_parent_category').html('<option value="">Select Category</option>');

                $.each(defaultCategories, function (i, dc) {
                    $('#d_parent_category').append('<option value="'+dc.id+'" data-index="'+i+'">'+dc.name+'</option>');
                });
            }

            function getVendorCategories() {
                vendorCategories = <?php echo json_encode($vendorCategories); ?>;
                $.each(vendorCategories, function (i, cat) {
                    $('#vendor_category').append('<option value="'+cat.id+'">'+cat.name+'</option>');
                });
            }

            getDefaultCategories();
            getVendorCategories();

            $('#d_parent_category').change(function () {
                $('#d_second_parent_category').html('<option value="">Sub Category</option>');
                $('#d_third_parent_category').html('<option value="">Sub Category</option>');

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').data('index');
                    d_parent_index = index;

                    var childrens = defaultCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        $('#d_second_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }

                $('#d_second_parent_category').trigger('change');
            });

            $('#d_second_parent_category').change(function () {
                $('#d_third_parent_category').html('<option value="">Sub Category</option>');

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').attr('data-index');

                    var childrens = defaultCategories[d_parent_index].subCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        $('#d_third_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }
            });

            $('#btnExportActive').click(function () {
                selectActive = 1;
                $('#selectSPCategory').modal('show');
                /*var ids = [];

                $('.checkbox-active-items').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    window.location.replace('{{ route('admin_export_to_sp_view') }}' + '?ids=' + ids.join(','));
                }*/
            });

            $('#btnExportInactive').click(function () {
                selectActive = 0;
                $('#selectSPCategory').modal('show');
                /*var ids = [];

                $('.checkbox-inactive-items').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    window.location.replace('{{ route('admin_export_to_sp_view') }}' + '?ids=' + ids.join(','));
                }*/
            });

            $('#modalBtnExport').click(function () {
                if ($('#d_parent_category').val() == '') {
                    alert('Select Default category');
                    return;
                }

                if ($('#vendor_category').val() == '') {
                    alert('Select vendor category');
                    return;
                }


                var defaultCategory = $("#d_parent_category option:selected").text();
                var vendorCategory = $("#vendor_category option:selected").text();
                var ids = [];

                if ($('#d_second_parent_category').val() != '') {
                    defaultCategory += ',' + $("#d_second_parent_category option:selected").text();

                    if ($('#d_third_parent_category').val() != '') {
                        defaultCategory += ',' + $("#d_third_parent_category option:selected").text();
                    }
                }

                if (selectActive == 1) {
                    $('.checkbox-active-items').each(function () {
                        if ($(this).is(':checked')) {
                            ids.push($(this).data('id'));
                        }
                    });
                } else {
                    $('.checkbox-inactive-items').each(function () {
                        if ($(this).is(':checked')) {
                            ids.push($(this).data('id'));
                        }
                    });
                }

                if (ids.length > 0) {
                    window.location.replace('{{ route('admin_export_to_sp_view') }}' + '?ids=' + ids.join(',') + '&c=' + defaultCategory + '&v=' + vendorCategory);
                }
            });
        });
    </script>
@stop
