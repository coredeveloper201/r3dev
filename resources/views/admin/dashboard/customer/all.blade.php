@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    {{-- <th>Company Name</th> --}}
                    <th>Active</th>
                    <th>Verified</th>
                    <th>Block</th>
                    {{-- <th>Minimum Require</th> --}}
                    <th>Created At</th>
                    {{-- <th>Files</th> --}}
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($buyers as $buyer)
                    @if($buyer->user['role'] == 3)
                    <tr>
                        <td>{{ $buyer->user['first_name'].' '.$buyer->user['last_name'] }}</td>
                        {{-- <td>{{ $buyer->company_name }}</td> --}}
                        <td>
                            <div class="form-check custom_checkbox">
                                <input class="form-check-input status" type="checkbox" id="checkbox-status-{{ $buyer->id }}" value="1" data-id="{{ $buyer->id }}" {{ $buyer->active == 1 ? 'checked' : '' }}>
                                <label class="form-check-label" for="checkbox-status-{{ $buyer->id }}">
                                    &nbsp;
                                </label>
                            </div>
                            {{--<label class="custom-control custom-checkbox">
                                <input type="checkbox" data-id="{{ $buyer->id }}" class="custom-control-input status" value="1" {{ $buyer->active == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>--}}
                        </td>
                        <td>
                            <div class="form-check custom_checkbox">
                                <input class="form-check-input verified" type="checkbox" id="checkbox-verified-{{ $buyer->id }}" value="1" data-id="{{ $buyer->id }}" {{ $buyer->verified == 1 ? 'checked' : '' }}>
                                <label class="form-check-label" for="checkbox-verified-{{ $buyer->id }}">
                                    &nbsp;
                                </label>
                            </div>
                            {{--<label class="custom-control custom-checkbox">
                                <input type="checkbox" data-id="{{ $buyer->id }}" class="custom-control-input verified" value="1" {{ $buyer->verified == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>--}}
                        </td>
                        <td>
                            <div class="form-check custom_checkbox">
                                <input class="form-check-input block" type="checkbox" id="checkbox-block-{{ $buyer->id }}" value="1" data-id="{{ $buyer->id }}" {{ $buyer->block == 1 ? 'checked' : '' }}>
                                <label class="form-check-label" for="checkbox-block-{{ $buyer->id }}">
                                    &nbsp;
                                </label>
                            </div>
                            {{--<label class="custom-control custom-checkbox">
                                <input type="checkbox" data-id="{{ $buyer->id }}" class="custom-control-input block" value="1" {{ $buyer->block == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>--}}
                        </td>
                        {{-- <td>
                            <div class="form-check custom_checkbox">
                                <input class="form-check-input minOrder" type="checkbox" id="checkbox-minOrder-{{ $buyer->id }}" value="1" data-id="{{ $buyer->id }}" {{ $buyer->min_order == 1 ? 'checked' : '' }}>
                                <label class="form-check-label" for="checkbox-minOrder-{{ $buyer->id }}">
                                    &nbsp;
                                </label>
                            </div>
                            {{--<label class="custom-control custom-checkbox">
                                <input type="checkbox" data-id="{{ $buyer->id }}" class="custom-control-input minOrder" value="1" {{ $buyer->min_order == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>--}}
                        {{-- </td> --}}
                        <td>{{ date('m/d/Y g:i:s a', strtotime($buyer->created_at)) }}</td>
                        {{-- <td>
                            @if ($buyer->ein_path != null)
                                @if (pathinfo($buyer->ein_path, PATHINFO_EXTENSION) != 'pdf')
                                    <a class="show-image" data-href="{{ asset($buyer->ein_path) }}" href="#">EIN</a>
                                @else
                                    <a href="{{ asset($buyer->ein_path) }}" download>EIN</a> &nbsp;
                                @endif
                            @endif

                            @if ($buyer->sales1_path != null)
                                @if (pathinfo($buyer->sales1_path, PATHINFO_EXTENSION) != 'pdf')
                                    <a class="show-image" data-href="{{ asset($buyer->sales1_path) }}" href="#">Sales 1</a>
                                @else
                                    <a href="{{ asset($buyer->sales1_path) }}" download>Sales 1</a> &nbsp;
                                @endif
                            @endif

                            @if ($buyer->sales2_path != null)
                                @if (pathinfo($buyer->sales2_path, PATHINFO_EXTENSION) != 'pdf')
                                    <a class="show-image" data-href="{{ asset($buyer->sales2_path) }}" href="#">Sales 2</a>
                                @else
                                    <a href="{{ asset($buyer->sales2_path) }}" download>Sales 2</a> &nbsp;
                                @endif
                            @endif
                        </td> --}}
                        <td>
                            <a class="btnEdit" href="{{ route('admin_buyer_edit', ['buyer' => $buyer->id]) }}" style="color: blue">Edit</a> |
                            <a class="btnDelete" data-id="{{ $buyer->id }}" role="button" style="color: red">Delete</a>
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

            <div class="pagination">
                {{ $buyers->links() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalShowImage" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelLarge">Document</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="img" src="" width="100%">
                </div>

                <div class="modal-footer">
                    <a id="btnDownload" class="btn btn-light" download>Download</a>
                    <button class="btn btn-light" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.status').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_buyer_change_status') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

            $('.verified').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_buyer_change_verified') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

            $('.block').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_buyer_change_block') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

            $('.minOrder').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_buyer_change_min_order') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Min. Order Updated!');
                });
            });

            $('.show-image').click(function (e) {
                e.preventDefault();

                var url = $(this).data('href');

                $('#img').attr('src', url);
                $('#btnDownload').attr('href', url);
                $('#modalShowImage').modal('show');
            });

            var selectedId;

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_buyer_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        });
    </script>
@stop