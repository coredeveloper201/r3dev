@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row mb-50">
        <div class="col-12">
            <form class="form-inline" action="{{ route('admin_store_home_catone_page', ['id' => $sectionTitle->id]) }}" method="POST">
                @csrf
                <div class="col-md-1">
                    <div class="form-group">
                        <label for="col-form-label">Title</label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" name="title" class="form-control" style="width: 100%;" id="title" value="{{ $sectionTitle->title }}">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form=group">
                        <input type="hidden" name="id" value="{{$featured}}">
                        <input class="btn btn-primary" type="submit" class="form-control" value="SAVE">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br><br><br>
    <div class="row">
        <div class="col-12">
            <div class="single_product_wrapper">
                {{-- {{ dd($products) }} --}}
                @foreach($products as $product)
                {{-- {{ dd($product->images) }} --}}
                <div class="single_product">
                    <div class="single_product_text">
                        <span class="single_img">
                            @if( isset($product->images) )
                                <a href="{{ route('item_details_page', $product->id) }}"><img src="{{ asset('/'.$product->images[0]['image_path'])}}" alt=""
                                                 class="img-fluid"></a>
                            @else
                                <a href="{{ route('item_details_page', $product->id) }}"><img src="{{ asset('/images/no-image.png')}}" alt=""
                                                 class="img-fluid"></a>
                            @endif
                            <a href="{{ route('admin_edit_item', $product->id) }}" class="edit"><i class="fas fa-edit"></i></a>
                        </span>
                        <span class="single_product_desc">
                            <h2>{{ $product->style_no }}</h2>
                            <h3><a href="{{ route('admin_edit_item', $product->id) }}">{{ $product->name }}</a></h3>
                            <h2>${{ $product->price }}</h2>
                        </span>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="text-center">
                {{ $products->links() }}
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@stop