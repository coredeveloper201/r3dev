@extends('admin.layouts.main')

@section('content')
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Last Updated</th>
                {{-- <th>Company Name</th> --}}
                <th>Details</th>
                <th>Notification</th>
                <th>Amount</th>
            </tr>
            </thead>

            <tbody>

            @foreach($orders as $order)
                <tr>
                    <td>{{ date('F d, Y g:i:s a', strtotime($order['updated_at'])) }}</td>
                    {{-- <td>{{ $order['company']}}</td> --}}
                    <td>
                        <a class="text-primary" href="{{ route('admin_incomplete_order_detail', ['order' => $order['id']]) }}">View Detail</a>
                    </td>
                    <td>
                        <div id="mailbody{{$order['id']}}" style="display: none;">

                            <p> Hi {{$order['name']}}</p>

                            <p>Do you still want these?</p>

                            <p>Check out your shopping cart today and get 5% OFF. (Promo code: CQ50OFF)</p>
                            <p>*Offer valid for a limited time. Visit us at cqbycq.com for more details.</p>
                            <p>If you have any questions, feel free to contact me. Thanks you.</p>
                            <div class="row">
                            
                                
                            @foreach($order->items as $item)
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                            @if ($item->itemImages != null)
                                                <img src="{{ asset($item->itemImages->image_path) }}" alt="Product" width="200px" height="150px">
                                            @else
                                                <img src="{{ asset('images/no-image.png') }}"  alt="Product" width="200px" height="150px">
                                            @endif
                                            <div class="caption">
                                                <h5>{{$item->item->name}}</h5>
                                                <p>{{$item->item->style_no}}</p>
                                                <b>${{ sprintf('%0.2f', $item->amount) }}</b>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                              <hr>
                            @endforeach


                            </div>
                            <p>&nbsp;</p>
                            <p>from CQ orders@r3all.com</p>
                            <p>877-651-3292</p>
                            <p>r3</p>
                            <p>r3all.com</p>
                        </div>
                        <a data-userid="{{$order['id']}}" class="text-primary btnSend" href="#" data-recipient="{{ $order['email']}}" data-name="{{$order['company']}}"> <i class="fa fa-bell"></i> Send</a>
                    </td>
                    <td>${{ sprintf('%0.2f', $order['total']) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="pagination">
        {{ $orders->links() }}
    </div>

    <div class="modal fade" id="modalSendNotification" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelLarge">Mail Send  </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-12">

                            <textarea rows="2" id="page_editor" class="d-none form-control mailbody">

                            </textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Sender</label>

                        <div class="col-10">
                            <input type="email" id="sender" value="orders@r3all.com" name="sender" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">

                        <label class="col-2 col-form-label">Recipient</label>

                        <div class="col-10">
                            <input type="email" id="recipient" value="" name="recipient" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2">Subject</label>

                        <div class="col-10">
                            <input name="subject" id="subject" value="[r3all] You have items in your cart." class="form-control">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <a style="color: #000; border: 1px solid" id="modalBtnSendMail" class="btn btn-outline-primary" download>Send Confirm</a>
                    <button class="btn btn-light" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}?id={{ rand() }}"></script>--}}
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var selectedId;

            $('.btnSend').click(function () {
                var userId = $(this).data('userid');
                var mailBody = $('#mailbody'+userId).html();
                CKEDITOR.instances['page_editor'].setData(mailBody);
                $('#recipient').val($(this).data('recipient'));
                $('#modalSendNotification').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnSendMail').click(function () {
                var recipient =  $('#recipient').val();
                var subject =  $('#subject').val();
                var sender =  $('#sender').val();
                var mailbody =   CKEDITOR.instances['page_editor'].getData();
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_incomplete_order_send_mail') }}",
                    data: {sender: sender,recipient:recipient,subject:subject,mailbody:mailbody}
                }).done(function (msg) {
                    $('#modalSendNotification').modal('hide');

                    // location.reload();
                });
            });
        });
    </script>
    <script>
        $(function () {
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            //var pageEditor = CKEDITOR.replace( 'page_editor' );

            var options = {
                filebrowserImageBrowseUrl: '{{ url('laravel-filemanager') }}?type=Images',
                filebrowserImageUploadUrl: '{{ url('laravel-filemanager') }}type=Images&_token=',
                filebrowserBrowseUrl: '{{ url('laravel-filemanager') }}?type=Files',
                filebrowserUploadUrl: '{{ url('laravel-filemanager') }}?type=Files&_token='
            };

            CKEDITOR.replace('page_editor', options);
        });
    </script>
@stop