@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row {{ ($errors && sizeof($errors) > 0) ? 'd-none' : '' }}" id="addBtnRow">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNew">Add New Promo Code</button>
        </div>
    </div>

    <div class="row {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="addEditRow">
        <div class="col-md-12" style="border: 1px solid black">
            <h3><span id="addEditTitle">{{ old('inputAdd') == '0' ? 'Edit Promo Code' : 'Add Promo' }}</span></h3>

            <form class="form-horizontal" enctype="multipart/form-data" id="form"
                  method="post" action="{{ (old('inputAdd') == '1') ? route('admin_promo_codes_add') : route('admin_promo_codes_update') }}">
                @csrf

                <input type="hidden" name="inputAdd" id="inputAdd" value="{{ old('inputAdd') }}">
                <input type="hidden" name="promoCodeId" id="promoCodeId" value="{{ old('promoCodeId') }}">

                <div class="form-group row{{ $errors->has('title') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="title" class="col-form-label">Title *</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                               placeholder="Title" name="title" value="{{ old('title') }}">
                    </div>
                </div>                
                
                <div class="form-group row{{ $errors->has('promo_code') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="code" class="col-form-label">Promo Code *</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="code" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}"
                               placeholder="Promo Code" name="code" value="{{ old('code') }}">
                    </div>
                </div>
                
                <div class="form-group row{{ $errors->has('discount') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="discount" class="col-form-label">Discount *</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="discount" class="form-control{{ $errors->has('discount') ? ' is-invalid' : '' }}"
                               placeholder="Discount" name="discount" value="{{ old('discount') }}">
                    </div>
                </div>
                
                <div class="form-group row{{ $errors->has('start_date') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="start_date" class="col-form-label">Start Date *</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="start_date" class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }}"
                               placeholder="Start Date" name="start_date" value="{{ old('start_date') }}">
                    </div>
                </div>
                
                <div class="form-group row{{ $errors->has('end_date') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="end_date" class="col-form-label">End Date *</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="end_date" class="form-control{{ $errors->has('end_date') ? ' is-invalid' : '' }}"
                               placeholder="End Date" name="end_date" value="{{ old('end_date') }}">
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="status" class="col-form-label">Status</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="checkbox" name="status" id="status" value="1">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-default" id="btnCancel">Cancel</button>
                        <input type="submit" id="btnSubmit" class="btn btn-primary" value="{{ old('inputAdd') == '0' ? 'Update' : 'Add' }}">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Code</th>
                    <th>Discount</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($promoCodes as $item)
                    <tr>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->code }}</td>
                        <td>{{ $item->discount }}</td>
                        <td>{{ date('m/d/Y', strtotime($item->start_date)) }}</td>
                        <td>{{ date('m/d/Y', strtotime($item->end_date)) }}</td>
                        <td>
                            @if ($item->status == 1)
                                Active
                            @else
                                Inavtive
                            @endif
                        </td>
                        <td>
                            <a class="btnEdit" data-id="{{ $item->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                            <a class="btnDelete" data-id="{{ $item->id }}" role="button" style="color: red">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // start date
            $('#start_date').datepicker({
                autoclose: true,
                format: 'mm/dd/yyyy'
            });
            // end date
            $('#end_date').datepicker({
                autoclose: true,
                format: 'mm/dd/yyyy'
            });

            var promoCodes = <?php echo json_encode($promoCodes->toArray()); ?>;
            
            var selectedId;
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('#btnAddNew').click(function () {
                $('#addEditRow').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitle').html('Add Promo Code');
                $('#btnSubmit').val('Add');
                $('#inputAdd').val('1');
                $('#form').attr('action', '{{ route('admin_promo_codes_add') }}');
            });

            $('#btnCancel').click(function (e) {
                e.preventDefault();

                $('#addEditRow').addClass('d-none');
                $('#addBtnRow').removeClass('d-none');

                // Clear form
                $('#ship_method').val('');
                $('#courier').val('');

                $('input').removeClass('is-invalid');
                $('.form-group').removeClass('has-danger');
            });

            $('.btnEdit').click(function () {
                var id = $(this).data('id');
                var index = $(this).data('index');

                $('#addEditRow').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitle').html('Edit Promo Code');
                $('#btnSubmit').val('Update');
                $('#inputAdd').val('0');
                $('#form').attr('action', '{{ route('admin_promo_codes_update') }}');
                $('#promoCodeId').val(id);
                
                var promoCode = promoCodes[index];

                $('#title').val(promoCode.title);
                $('#code').val(promoCode.code);
                $('#discount').val(promoCode.discount);
                var promoStartDateObject = new Date(promoCode.start_date);
                var promoStartDate = (promoStartDateObject.getMonth()+1) + '/' + promoStartDateObject.getDate() + '/' + promoStartDateObject.getFullYear();
                $('#start_date').val(promoStartDate);
                var promoEndDateObject = new Date(promoCode.end_date);
                var promoEndDate = (promoEndDateObject.getMonth()+1) + '/' + promoEndDateObject.getDate() + '/' + promoEndDateObject.getFullYear();
                $('#end_date').val(promoEndDate);
                $('#status').val(promoCode.status);
                if ( promoCode.status == 1 ) {
                    $("#status").prop('checked', true);
                }
            });

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_promo_codes_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        })
    </script>
@stop