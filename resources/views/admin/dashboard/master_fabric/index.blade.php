@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/Nestable/style.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNew">Add New Fabric</button>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($fabrics as $fabric)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $fabric->name }}</td>

                        <td>
                            <a class="btnEdit" data-id="{{ $fabric->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                            <a class="btnDelete" data-id="{{ $fabric->id }}" role="button" style="color: red">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="addEditModal" role="dialog" aria-labelledby="addEditModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" id="form" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="" id="modalInputId">

                    <div class="modal-header bg-primary">
                        <h4 class="modal-title text-white" id="addEditModal">Add Master Fabric</h4>
                    </div>

                    <div class="modal-body">
                        <fieldset>
                            <div class="form-group row">
                                <div class="col-lg-5 text-lg-right">
                                    <label for="modalName" class="col-form-label">Name</label>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" id="modalName" class="form-control" placeholder="Fabric Name" name="name">
                                </div>
                            </div>
                        </fieldset>
                    </div>

                    <div class="modal-footer">
                        <button class="btn  btn-secondary" data-dismiss="modal">Close</button>
                        <input class="btn  btn-primary" type="submit" id="modalBtnAdd" value="Add">
                        <input class="btn  btn-primary" type="submit" id="modalBtnUpdate" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var fabrics = <?php echo json_encode($fabrics); ?>;
            var selectedID;
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('#btnAddNew').click(function () {
                $('#modalName').removeClass('is-invalid');
                $('#modalName').val('');
                $('#modalBtnAdd').show();
                $('#modalBtnUpdate').hide();
                $('#form').attr('action', '{{ route('admin_master_fabric_add') }}');

                $('#addEditModal').modal('show');
            });

            $('#modalBtnAdd').click(function (e) {
                e.preventDefault();
                var name = $('#modalName').val();

                if (name == '') {
                    $('#modalName').addClass('is-invalid');
                } else {
                    $('#form').submit();
                }
            });

            $('.btnDelete').click(function () {
                selectedID = $(this).data('id');
                $('#deleteModal').modal('show');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_master_fabric_delete') }}",
                    data: { id: selectedID }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            $('.btnEdit').click(function () {
                selectedID = $(this).data('id');
                var index = $(this).data('index');
                var fabric = fabrics[index];

                $('#modalName').removeClass('is-invalid');
                $('#modalName').val(fabric.name);

                $('#modalBtnAdd').hide();
                $('#modalBtnUpdate').show();

                $('#form').attr('action', '{{ route('admin_master_fabric_update') }}');
                $('#modalInputId').val(selectedID);
                $('#addEditModal').modal('show');
            });

            $('#modalBtnUpdate').click(function (e) {
                e.preventDefault();
                var name = $('#modalName').val();

                if (name == '') {
                    $('#modalName').addClass('is-invalid');
                } else {
                    $('#form').submit();
                }
            });
        })
    </script>
@stop