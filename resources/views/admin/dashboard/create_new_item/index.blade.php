<?php use App\Enumeration\Availability; ?>
@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/ezdz/jquery.ezdz.min.css') }}" rel="stylesheet">

    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

@stop

@section('content')
    <form class="form-horizontal" method="post" action="{{ route('admin_create_new_item_post') }}" id="form" enctype="multipart/form-data">
        @csrf

        <div class="global_accordion edit_item_list">
            <div class="container-fluid no-padding">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne">
                                Item Info
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="status" class="col-form-label">Status</label>
                                            </div>

                                            <div class="col-6">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="statusActive" name="status" class="custom-control-input"
                                                           value="1" {{ old('status') == '1' ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="statusActive">Active</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="statusInactive" name="status" class="custom-control-input"
                                                           value="0" {{ (old('status') == '0' || empty(old('status'))) ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="statusInactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="style_no" class="col-form-label">Style No.*</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="text" id="style_no" class="form-control{{ $errors->has('style_no') ? ' is-invalid' : '' }}"
                                                       name="style_no" value="{{ old('style_no') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row{{ $errors->has('item_name') ? ' has-danger' : '' }}">
                                            <div class="col-2">
                                                <label for="item_name" class="col-form-label">Item Name*</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="text" id="item_name" class="form-control{{ $errors->has('item_name') ? ' is-invalid' : '' }}"
                                                       name="item_name" value="{{ old('item_name') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="price" class="col-form-label">Price *</label>
                                            </div>

                                            <div class="col-3">
                                                <input type="text" id="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                                                       placeholder="$" name="price" value="{{ old('price') }}">
                                            </div>

                                            <div class="col-2">
                                                <label for="orig_price" class="col-form-label">Orig. Price</label>
                                            </div>

                                            <div class="col-3">
                                                <input type="text" id="orig_price" class="form-control{{ $errors->has('orig_price') ? ' is-invalid' : '' }}"
                                                       placeholder="$" name="orig_price" value="{{ old('orig_price') }}">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="description" class="col-form-label">Description</label>
                                            </div>

                                            <div class="col-8">
                                                <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                                          name="description" rows="4">{{ old('description') }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label class="col-form-label">Available On</label>
                                            </div>

                                            <div class="col-4">
                                                <input type="text" id="available_on" class="form-control{{ $errors->has('available_on') ? ' is-invalid' : '' }}"
                                                       name="available_on" value="{{ old('available_on') }}">
                                            </div>

                                            <div class="col-4">
                                                <select class="form-control" name="availability" id="availability">
                                                    <option value="{{ Availability::$IN_STOCK }}" {{ old('availability') == Availability::$IN_STOCK ? 'selected' : '' }}>In Stock</option>
                                                    <option value="{{ Availability::$ARRIVES_SOON }}" {{ old('availability') == Availability::$ARRIVES_SOON ? 'selected' : '' }}>Arrives Soon / Back Order</option>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="d_parent_category" class="col-form-label">Category *</label>
                                            </div>

                                            <div class="col-4">
                                                <select class="form-control{{ $errors->has('d_parent_category') ? ' is-invalid' : '' }}" name="d_parent_category" id="d_parent_category">
                                                    <option value="">Select Category</option>
                                                    @foreach($defaultCategories as $item)
                                                        <option value="{{ $item['id'] }}" data-index="{{ $loop->index }}" {{ old('d_parent_category') == $item['id'] ? 'selected' : '' }}>{{ $item['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-4">
                                                <select class="form-control{{ $errors->has('d_second_parent_category') ? ' is-invalid' : '' }}" name="d_second_parent_category" id="d_second_parent_category">
                                                    <option value="">Sub Category</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="form-group row{{ $errors->has('featured') ? ' has-danger' : '' }}">
                                            <div class="col-2">
                                                <label for="featured" class="col-form-label">Featured</label>
                                            </div>

                                            <div class="col-8">
                                                <select class="form-control" name="featured" id="featured">
                                                    <option value="">Select Featured</option>
                                                    <option value="{{  \App\Enumeration\Featured::$BANNER_ONE }}" {{ old('featured') == \App\Enumeration\Featured::$BANNER_ONE ? 'selected' : '' }}>Banner section one</option>
                                                    <option value="{{  \App\Enumeration\Featured::$BANNER_TWO }}" {{ old('featured') == \App\Enumeration\Featured::$BANNER_TWO ? 'selected' : '' }}>Banner section Two</option>
                                                    <option value="{{  \App\Enumeration\Featured::$BANNER_THREE }}" {{ old('featured') == \App\Enumeration\Featured::$BANNER_THREE ? 'selected' : '' }}>Banner section Three</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label class="col-form-label">Quantity</label>
                                            </div>

                                            <div class="col-4">
                                                <input type="text" id="min_qty" class="form-control{{ $errors->has('min_qty') ? ' is-invalid' : '' }}"
                                                       name="min_qty" value="{{ old('min_qty') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="advantage" class="col-form-label">Advantage</label>
                                            </div>

                                            <div class="col-8">
                                                <textarea class="form-control{{ $errors->has('advantage') ? ' is-invalid' : '' }}"
                                                          name="advantage" rows="4">{{ old('advantage') }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><div class="card">
                    <div class="card-header" id="cardDetail">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#detailEditor">
                            Detail
                        </button>
                    </div>
                    <div id="detailEditor" class="collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">

                                        <div class="col-12">
                                            <textarea class="d-none" name="details" id="page_editor" rows="2"> </textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree">
                            Colors
                        </button>
                    </div>
                    <div id="collapseThree" class="collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-2">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-search"></i></div>
                                                </div>
                                                <input class="form-control" type="text" id="color_search" placeholder="Type Color">
                                            </div>

                                            @if ($errors->has('colors'))
                                                <span class="text-danger">Color(s) is required.</span>
                                            @endif
                                        </div>

                                        <div class="col-6">
                                            <a class="btn btn-primary" role="button" id="btnAddColor">Add Color</a>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <ul class="colors-ul">
                                                @if (old('colors') != null)
                                                    @foreach($colors as $color)
                                                        @if (in_array($color->id, old('colors')))
                                                            <li>
                                                                <span class="name">{{ $color->name }}</span>
                                                                <a class="color-remove">X</a><br><br>
                                                                <div class="input-group">
                                                                    <label for="color_available_{{ $color->id }}" class="custom-control custom-checkbox color-available">
                                                                        <input type="checkbox" id="color_available_{{ $color->id }}" class="custom-control-input" value="1" name="color_available_{{ $color->id }}" checked>
                                                                        <span class="custom-control-indicator"></span>
                                                                        <span class="custom-control-description">Available</span>
                                                                    </label>
                                                                </div>
                                                                <input class="templateColor" type="hidden" name="colors[]" value="{{ $color->id }}">
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFourT">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourT">
                            Images
                        </button>
                    </div>
                    <div id="collapseFourT" class="collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-primary" id="btnUploadImages" type="button">Upload Images</button>
                                    <input type="file" class="d-none" multiple id="inputImages">
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col-12">
                                    <div style="width: 100%;height: auto;border: 3px dotted black;padding: 20px;">
                                        <div class="row" id="images" style="padding: 20px;">
                                            <span>Drag & Drop Images from your computer</span>
                                            <ul id="image-container" class="block__list block__list_tags">
                                                @if (old('imagesId') != null && sizeof(old('imagesId')) > 0)
                                                    @foreach(old('imagesId') as $img)
                                                        <li>
                                                            <div class="image-item" style="margin-right: 10px">
                                                                <img class="img-thumbnail img" style="margin-bottom: 10px"
                                                                     src="{{ asset(old('imagesSrc.'.$loop->index)) }}"><br>
                                                                <select class="image-color" name="imageColor[]">
                                                                    <option value="">Color [Default]</option>
                                                                </select><br>
                                                                <a class="btnRemoveImage">Remove</a>

                                                                <input class="inputImageId" type="hidden" name="imagesId[]" value="{{ $img }}">
                                                                <input class="inputImageSrc" type="hidden" name="imagesSrc[]" value="{{ old('imagesSrc.'.$loop->index) }}">
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-right">
                    <button class="btn btn-primary" id="btnSave">Save</button>
                </div>
            </div>
        </div>
    </form>

    <template id="imageTemplate">
        <li>
            <div class="image-item" style="margin-right: 10px">
                <img height="150px" width="100px" class="img-thumbnail img" style="margin-bottom: 10px"><br>
                <select class="image-color" name="imageColor[]">
                    <option value="">Color [Default]</option>
                </select><br>
                <a class="btnRemoveImage">Remove</a>

                <input class="inputImageId" type="hidden" name="imagesId[]">
                <input class="inputImageSrc" type="hidden" name="imagesSrc[]">
            </div>
        </li>
    </template>

    <template id="colorItemTemplate">
        <li>
            <div class="input-group">
                <div class="form-check custom_checkbox">
                    <input class="form-check-input"
                           type="checkbox"
                           value="1" checked>
                    <label class="form-check-label color-available">
                        <span class="name"></span>
                    </label>
                </div>
                <a class="color-remove">X</a>
            </div>
            <input class="templateColor" type="hidden" name="colors[]" value="">
        </li>
    </template>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sortable/js/Sortable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/ezdz/jquery.ezdz.min.js') }}"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>

    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

    <script>
        $(function () {
            var message = '{{ session('message') }}';

            if (message !== ''){
                toastr.success(message);
            }

            var options = {
                filebrowserImageBrowseUrl: '{{ url('laravel-filemanager') }}?type=Images',
                filebrowserImageUploadUrl: '{{ route('unisharp.lfm.upload') }}?type=Images&_token=',
                filebrowserBrowseUrl: '{{ url('laravel-filemanager') }}?type=Files',
                filebrowserUploadUrl: '{{ url('laravel-filemanager') }}?type=Files&_token='
            };
            CKEDITOR.replace('page_editor', options);
        });
    </script>
    <script>
        $(function () {
            var defaultCategories = <?php echo json_encode($defaultCategories); ?>;
            var packs = <?php echo json_encode($packs->toArray()); ?>;
            var colors = <?php echo json_encode($colors->toArray()); ?>;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#form').bind('submit', function () {
                $(this).find(':input').prop('disabled', false);
            });

            // Video
            $('#input-video').ezdz({
                previewImage: false
            });

            // Color select
            var availableColors = [];

            $.each(colors, function (i, color) {
                availableColors.push(color.name);
            });

            $('#color_search').autocomplete({
                source: function (request, response) {
                    var results = $.map(availableColors, function (tag) {
                        if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                            return tag;
                        }
                    });
                    response(results);
                },
                response: function(event, ui) {
                    if (ui.content.length === 0) {
                        $('#select_master_color').val('');
                        $('#select_master_color').removeClass('d-none');
                    } else {
                        $('#select_master_color').addClass('d-none');
                    }
                }
            });

            $('#color_search').keydown(function (e){
                if(e.keyCode == 13){
                    e.preventDefault();
                    addColor();
                }
            });

            $('#color_search').keyup(function (e) {
                if ($('#color_search').val().length == 0)
                    $('#select_master_color').addClass('d-none');
            });

            $('#btnAddColor').click(function () {
                addColor();
            });

            function addColor() {
                var text = $('#color_search').val();

                if (text != '') {
                    var color = '';

                    $.each(colors, function (i, c) {
                        if (c.name == text)
                            color = c;
                    });

                    if (color != '') {
                        var found = false;
                        $( "input[name*='colors']" ).each(function () {
                            if ($(this).val() == color.id)
                                found = true;
                        });

                        if (!found) {
                            var html = $('#colorItemTemplate').html();
                            row = $(html);

                            row.find('.name').html(color.name);
                            row.find('.templateColor').val(color.id);
                            row.find('.color-available').attr('name', 'color_available_'+color.id);
                            row.find('.color-available').attr('id', 'color_available_'+color.id);
                            row.find('.custom-checkbox').attr('for', 'color_available_'+color.id);

                            $('.colors-ul').append(row);
                            updateImageColors();
                        }
                        $('#color_search').val('');
                    } else {
                        $('#select_master_color').removeClass('d-none');
                    }
                }
            }

            function updateImageColors() {
                var ids = [];

                $( "input[name*='colors']" ).each(function () {
                    ids.push($(this).val());
                });

                $('.image-color').each(function () {
                    var selected = $(this).val();

                    $(this).html('<option value="">Color [Default]</option>');
                    $this = $(this);

                    $.each(ids, function (index, id) {
                        var color = colors.filter(function( obj ) {
                            return obj.id == id;
                        });
                        color = color[0];

                        if (color.id == selected)
                            $this.append('<option value="'+color.id+'" selected>'+color.name+'</option>');
                        else
                            $this.append('<option value="'+color.id+'">'+color.name+'</option>');
                    });
                });
            }
            updateImageColors();

            $(document).on('click', '.color-remove', function () {
                $(this).closest('li').remove();
            });


            // Available on
            $('#available_on').datepicker({
                autoclose: true,
                format: 'mm/dd/yyyy'
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            // Images
            var el = document.getElementById('image-container');
            Sortable.create(el, {
                group: "words",
                animation: 150,
            });

            $('#images').on({
                'dragover dragenter': function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                },
                'drop': function(e) {
                    var dataTransfer =  e.originalEvent.dataTransfer;
                    if( dataTransfer && dataTransfer.files.length) {
                        e.preventDefault();
                        e.stopPropagation();
                        $.each( dataTransfer.files, function(i, file) {
                            if (file.size > 3072000) {
                                alert('Max allowed image size is 3MB per image.')
                            } else if (file.type != 'image/jpeg' && file.type != 'image/png') {
                                alert('Only jpg and png file allowed.');
                            } else if ($(".image-container").length > 20) {
                                alert('Maximum 20 photos allows');
                            } else {
                                var xmlHttpRequest = new XMLHttpRequest();
                                xmlHttpRequest.open("POST", '{{ route('admin_item_upload_image') }}', true);
                                var formData = new FormData();
                                formData.append("file", file);
                                xmlHttpRequest.send(formData);

                                xmlHttpRequest.onreadystatechange = function() {
                                    if (xmlHttpRequest.readyState == XMLHttpRequest.DONE) {
                                        var response = JSON.parse(xmlHttpRequest.responseText);

                                        if (response.success) {
                                            var html = $('#imageTemplate').html();
                                            var item = $(html);
                                            item.find('.img').attr('src', response.data.fullPath);
                                            item.find('.inputImageId').val(response.data.id);
                                            item.find('.inputImageSrc').val(response.data.image_path);

                                            $('#image-container').append(item);
                                            updateImageColors();
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            });


            $('body').on('click', '.btnRemoveImage', function () {
                $(this).closest('li').remove();
            });

            var d_parent_index;
            var d_second_id = '{{ old('d_second_parent_category') }}';
            var d_third_id = '{{ old('d_third_parent_category') }}';

            $('#d_parent_category').change(function () {
                $('#d_second_parent_category').html('<option value="">Sub Category</option>');
                $('#d_third_parent_category').html('<option value="">Sub Category</option>');
                var parent_id = $(this).val();

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').data('index');
                    d_parent_index = index;

                    var childrens = defaultCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        if (value.id == d_second_id)
                            $('#d_second_parent_category').append('<option data-index="' + index + '" value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#d_second_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }

                $('#d_second_parent_category').trigger('change');
            });

            $('#d_parent_category').trigger('change');

            $('#d_second_parent_category').change(function () {
                $('#d_third_parent_category').html('<option value="">Sub Category</option>');

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').attr('data-index');

                    var childrens = defaultCategories[d_parent_index].subCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        if (value.id == d_third_id)
                            $('#d_third_parent_category').append('<option data-index="' + index + '" value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#d_third_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }

                var id = $(this).val();

                // Size
                $('#size').change(function () {
                    var index = $(this).find(':selected').data('index');

                    if (typeof index !== "undefined") {
                        var pack = packs[index];
                        var packDetails = pack.pack1;

                        if (pack.pack2 != null)
                            packDetails += '-' + pack.pack2;

                        if (pack.pack3 != null)
                            packDetails += '-' + pack.pack3;

                        if (pack.pack4 != null)
                            packDetails += '-' + pack.pack4;

                        if (pack.pack5 != null)
                            packDetails += '-' + pack.pack5;

                        if (pack.pack6 != null)
                            packDetails += '-' + pack.pack6;

                        if (pack.pack7 != null)
                            packDetails += '-' + pack.pack7;

                        if (pack.pack8 != null)
                            packDetails += '-' + pack.pack8;

                        if (pack.pack9 != null)
                            packDetails += '-' + pack.pack9;

                        if (pack.pack10 != null)
                            packDetails += '-' + pack.pack10;

                        $('#pack_details').html(packDetails);
                    } else {
                        $('#pack_details').html('Pack Details');
                    }
                });
            });

            $('#size').trigger('change');
            $('#d_second_parent_category').trigger('change');

            // Upload images button
            $('#btnUploadImages').click(function (e) {
                e.preventDefault();

                $('#inputImages').click();
            });

            $('#inputImages').change(function (e) {
                $.each(e.target.files, function (index, file) {
                    if (file.size > 614400) {
                        alert('Max allowed image size is 600Kb per image.')
                    } else if (file.type != 'image/jpeg' && file.type != 'image/png' && file.type != 'image/gif') {
                        alert('Only jpg and png file allowed.');
                    } else if ($(".image-container").length > 2) {
                        alert('Maximum 20 photos allows');
                    } else {
                        var xmlHttpRequest = new XMLHttpRequest();
                        xmlHttpRequest.open("POST", '{{ route('admin_item_upload_image') }}', true);
                        var formData = new FormData();
                        formData.append("file", file);
                        xmlHttpRequest.send(formData);

                        xmlHttpRequest.onreadystatechange = function() {
                            if (xmlHttpRequest.readyState == XMLHttpRequest.DONE) {
                                var response = JSON.parse(xmlHttpRequest.responseText);

                                if (response.success) {
                                    var html = $('#imageTemplate').html();
                                    var item = $(html);
                                    item.find('.img').attr('src', response.data.fullPath);
                                    item.find('.inputImageId').val(response.data.id);
                                    item.find('.inputImageSrc').val(response.data.image_path);

                                    $('#image-container').append(item);
                                    updateImageColors();
                                }
                            }
                        }
                    }
                });

                $(this).val('');
            });

            $('#btnSave').click(function (e) {
                e.preventDefault();

                $('#form').submit();
                /*var style_no = $('#style_no').val();

                $.ajax({
                    method: "POST",
                    url: "#",
                    data: { style_no: style_no }
                }).done(function( data ) {
                    if (data.success)
                        $('#form').submit();
                    else
                        alert(data.message);
                });*/
            });

            // Available
            $('#available_on').change(function () {
                var val = $(this).val();
                $('#availability').prop('disabled', false);

                if (val != '') {
                    if (moment(val, 'MM/DD/YYYY').isBefore(moment())) {
                        //console.log('before');
                    }else {
                        //console.log('after');
                        $('#availability').val('{{ Availability::$ARRIVES_SOON }}');
                        $('#availability').prop('disabled', true);
                    }
                } else {
                    $('#availability').val('{{ Availability::$IN_STOCK }}');
                }
            });

            $('#available_on').trigger('change');

            window.addEventListener("dragover",function(e){
                e = e || event;
                e.preventDefault();
            },false);
            window.addEventListener("drop",function(e){
                e = e || event;
                e.preventDefault();
            },false);
        });
    </script>
@stop