<?php
use App\Enumeration\Availability;
?>

@extends('admin.layouts.main')

@section('additionalCSS')
    <link rel="stylesheet" href="{{ asset('css/main.css?css=') }}{{ time() }}">
    <link rel="stylesheet" href="{{ asset('themes/back/css/main.css') }}?id={{ rand() }}">
@stop

@section('content')

    <!-- =========================
        START CART SECTION
    ============================== -->
    <section class="shopping_cart_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product_title cart_main_title checkout_main_title">
                        <h2>Checkout </h2>
                        @if($user == null)
                            {{--<p>Hi <a href="#">Sign out</a></p>--}}
                        @else
                            <p>Hi, {{$user->first_name.' '.$user->last_name}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =========================
        END CART SECTION
    ============================== -->

    <!-- =========================
        START CHECOUT SECTION
    ============================== -->
    <section class="checkout_area">
        <div class="container custom_container">
            <form id="payment-form-pp" method="post" action="{{route('admin_create_checkout')}}">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <p class="alert alert-danger cardError" style="display: none">Invalid Card Information. Please
                            check again.</p>
                    </div>
                    <div class="col-md-6 checkout-padding-right">
                        <div class="checkout_left_form">
                            <div class="shipping_info">
                                <h2>SHIPPING INFORMATION</h2>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>First name *</label>
                                            <input type="text" class="form-control"
                                                   name="shipping[first_name]" required
                                                   onkeyup="billingSameChangeAuto()"
                                                   value="@if($user != null){{$user->first_name}}@endif">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Last name *</label>
                                            <input type="text" class="form-control"
                                                   name="shipping[last_name]" required
                                                   onkeyup="billingSameChangeAuto()"
                                                   value="@if($user != null){{$user->last_name}}@endif">
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="form-group">
                                            <label>Address *</label>
                                            <input type="text" class="form-control" placeholder="Address"
                                                   name="shipping[address]" required
                                                   onkeyup="billingSameChangeAuto()"
                                                   value="@if($shippingAddresses != null){{$shippingAddresses->address}}@endif">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group address2">
                                            <input type="text" class="form-control" placeholder="Apt/Suite"
                                                   name="shipping[unit]"
                                                   onkeyup="billingSameChangeAuto()"
                                                   value="@if($shippingAddresses != null){{$shippingAddresses->unit}}@endif">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>City *</label>
                                            <input type="text" class="form-control"
                                                   name="shipping[city]" required
                                                   onkeyup="billingSameChangeAuto()"
                                                   value="@if($shippingAddresses != null){{$shippingAddresses->city}}@endif">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>State/Province *</label>
                                            <input type="text" class="form-control"
                                                   name="shipping[state_text]" required
                                                   onkeyup="billingSameChangeAuto()"
                                                   value="@if($shippingAddresses != null){{$shippingAddresses->state_text}}@endif">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Zip *</label>
                                            <input type="text" class="form-control"
                                                   name="shipping[zip]" required
                                                   onkeyup="billingSameChangeAuto()"
                                                   value="@if($shippingAddresses != null){{$shippingAddresses->zip}}@endif">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Country *</label>
                                            <select class="form-control" required name="shipping[country_id]"
                                                    onchange="billingSameChangeAuto()">
                                                @foreach($country as $c)
                                                    <option value="{{$c['id']}}"@if($shippingAddresses != null && $shippingAddresses->country_id == $c['id']) selected="selected" @endif >{{$c['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mobile Number *</label>
                                            <input type="text" class="form-control"
                                                   name="shipping[phone]" required
                                                   onkeyup="billingSameChangeAuto()"
                                                   value="@if($shippingAddresses != null){{$shippingAddresses->phone}}@endif">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="shipping_info billing_info">
                                <h2>BILLING INFORMATION</h2>
                                <div class="form-group form_checkbox">
                                    <input type="checkbox" id="billingSame" name="billingSame" checked
                                           onchange="billingSameChange()">
                                    <label for="billingSame">Billing address same as shipping</label>
                                </div>
                                <div class="billingView" style="display: none">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>First name *</label>
                                                <input type="text" class="form-control"
                                                       name="billing[first_name]" required
                                                       value="@if($user != null){{$user->first_name}}@endif">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Last name *</label>
                                                <input type="text" class="form-control"
                                                       name="billing[last_name]" required
                                                       value="@if($user != null){{$user->last_name}}@endif">
                                            </div>
                                        </div>
                                        <div class="col-8">
                                            <div class="form-group">
                                                <label>Address *</label>
                                                <input type="text" class="form-control" placeholder="Address"
                                                       name="billing[address]" required
                                                       value="@if($shippingAddresses != null){{$shippingAddresses->address}}@endif">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group address2">
                                                <input type="text" class="form-control" placeholder="Apt/Suite"
                                                       name="billing[unit]"
                                                       value="@if($shippingAddresses != null){{$shippingAddresses->unit}}@endif">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>City *</label>
                                                <input type="text" class="form-control"
                                                       name="billing[city]" required
                                                       value="@if($shippingAddresses != null){{$shippingAddresses->city}}@endif">
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label>State/Province *</label>
                                                <input type="text" class="form-control"
                                                       name="billing[state_text]" required
                                                       value="@if($shippingAddresses != null){{$shippingAddresses->state_text}}@endif">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Zip *</label>
                                                <input type="text" class="form-control"
                                                       name="billing[zip]" required
                                                       value="@if($shippingAddresses != null){{$shippingAddresses->zip}}@endif">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Country *</label>
                                                <select class="form-control" required name="billing[country_id]">
                                                    @foreach($country as $c)
                                                        <option value="{{$c['id']}}"@if($shippingAddresses != null && $shippingAddresses->country_id == $c['id']) selected="selected" @endif >{{$c['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Mobile Number *</label>
                                                <input type="text" class="form-control"
                                                       name="billing[phone]" required
                                                       value="@if($shippingAddresses != null){{$shippingAddresses->phone}}@endif">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if($user == null)
                                <div class="shipping_info"><h2>CREATE ACCOUNT</h2>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Email *</label>
                                                <input type="email" class="form-control" name="user[email]" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Password *</label>
                                                <input type="password" class="form-control" name="user[password]"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="checkout_radio_btn">
                                            <input type="radio" id="checkout1" checked name="payment_type" value="stripe" onchange="payment_type_change()">
                                            <label for="checkout1">
                                                <img src="{{asset('/card/visa.png')}}" alt="" class="img-fluid">
                                                <img src="{{asset('/card/mastercard.png')}}" alt="" class="img-fluid">
                                                <img src="{{asset('/card/american_exp.png')}}" alt="" class="img-fluid">
                                            </label>
                                        </div>
                                        <div class="checkout_radio_btn">
                                            <input type="radio"  id="checkout2" name="payment_type" value="paypal" onchange="payment_type_change()">
                                            <label for="checkout2">
                                                <img src="{{asset('/card/paypal.png')}}" alt="" class="img-fluid">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 __stripre">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Credit Card Number</label>
                                                <input type="text" id="#card-cvc" data-stripe="number" name="card[number]"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Expiration Date</label>
                                                <select class="form-control" data-stripe="exp_month" name="card[exp_month]">
                                                    <option value="01">January</option>
                                                    <option value="02">February</option>
                                                    <option value="03">March</option>
                                                    <option value="04">April</option>
                                                    <option value="05">May</option>
                                                    <option value="06">June</option>
                                                    <option value="07">July</option>
                                                    <option value="08">August</option>
                                                    <option value="09">September</option>
                                                    <option value="10">October</option>
                                                    <option value="11">November</option>
                                                    <option value="12">December</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group address2">
                                                <select class="form-control" data-stripe="exp_year" name="card[exp_year]">
                                                    <option value="2019">2019</option>
                                                    <option value="2020">2020</option>
                                                    <option value="2021">2021</option>
                                                    <option value="2022">2022</option>
                                                    <option value="2023">2023</option>
                                                    <option value="2024">2024</option>
                                                    <option value="2025">2025</option>
                                                    <option value="2026">2026</option>
                                                    <option value="2027">2027</option>
                                                    <option value="2028">2028</option>
                                                    <option value="2029">2029</option>
                                                    <option value="2030">2030</option>
                                                </select></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group card_verification">
                                                <div class="chekout_pop_up">
                                                    A CVV is the 3-digit code on the back of your Visa or MasterCard, or a
                                                    4-digit
                                                    code on the front of your American Express
                                                </div>
                                                <label>Card Verification Number (CVV)</label>
                                                <input type="text" class="form-control" data-stripe="cvc" name="card[cvc]">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 checkout-padding-left">
                        <div class="checkout_right_area">
                            <div class="shipping_info">
                                <h2>ORDER SUMMARY <a href="{{route('show_admin_cart')}}">Edit</a></h2>
                                @foreach($cartItems as $cart)
                                    <div class="checkout_product_info">
                                        @if (isset($cart['item']['images']) && count($cart['item']['images']) > 0 )
                                            <img src="{{asset('/' . $cart['item']['images'][0]['image_path'])}}" alt=""
                                                 height="80" width="80">
                                        @else
                                            <img src="{{asset('/images/no-image.png')}}" alt="" height="80" width="80">
                                        @endif
                                        <div class="checkout_product_info_text">
                                            <h3>
                                                <a href="{{ route('admin_edit_item', $cart['item']['id']) }}">{{$cart['item']['name']}}</a>
                                            </h3>
                                            <p>{{$cart['item']['style_no']}}</p>
                                            <ul>
                                                <li>Qty: {{$cart['quantity']}}</li>
                                                <li>Unit Price: ${{$cart['item']['price']}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <table>
                                <tr>
                                    <td>Subtotal</td>
                                    <td>${{$subTotal}}</td>
                                </tr>
                                <tr>
                                    <td>Discount</td>
                                    <td>-$<span id="__promo">{{ ($promo+$giftCard) }}</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select class="custom-select" required name="shipping_method" onchange="shippingMethodChange(this)">
                                            @foreach($shipping_methods as $M)
                                                <option value="{{$M['id']}}"
                                                        data-fee="{{$M['fee']}}">{{$M['name']}} (${{$M['fee']}})</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>$<span id="__shipping">0.00</span></td>
                                </tr>
                                <tr>
                                    <td><b>TOTAL</b></td>
                                    <td><b>$<span id="__Total" data-type="{{$total}}">{{$total}}</span></b></td>
                                </tr>
                            </table>

                            <div class="__paypal" style="display: none;width: 100%;">
                                <div id="paypalLinks"></div>
                            </div>
                            <div class="__stripre checkout_order_btn text-right">
                                <button class="btn_common_type2" type="submit">place order</button>
                                {{--                            <a href="#"><img src="{{asset('/images/checkout-btn1.gif')}}" alt=""></a>--}}
                                {{--                            <img src="{{asset('/images/checkout-btn2.jpg')}}" alt="">--}}
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- =========================
        START CHECOUT SECTION
    ============================== -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="https://www.paypal.com/sdk/js?client-id={{env('PAYPAL_CLIENT_ID')}}"></script>
    <script type="application/javascript">
        function billingSameChangeAuto() {
            var status = $('#billingSame').prop('checked');
            if (status) {
                $('[name="billing[first_name]"]').val($('[name="shipping[first_name]"]').val());
                $('[name="billing[last_name]"]').val($('[name="shipping[last_name]"]').val());
                $('[name="billing[address]"]').val($('[name="shipping[address]"]').val());
                $('[name="billing[unit]"]').val($('[name="shipping[unit]"]').val());
                $('[name="billing[city]"]').val($('[name="shipping[city]"]').val());
                $('[name="billing[state_text]"]').val($('[name="shipping[state_text]"]').val());
                $('[name="billing[zip]"]').val($('[name="shipping[zip]"]').val());
                $('[name="billing[country_id]"]').val($('[name="shipping[country_id]"]').val());
                $('[name="billing[phone]"]').val($('[name="shipping[phone]"]').val());
            }
        }

        function billingSameChange() {
            var status = $('#billingSame').prop('checked');
            if (status) {
                $('.billingView').fadeOut();
                billingSameChangeAuto();
            } else {
                $('.billingView').fadeIn();
            }
        }
        function payment_type_change() {
            let pt = $('[name="payment_type"]:checked').val();
            if(pt === 'stripe'){
                $('.__stripre').show();
                $('.__paypal').hide();
            } else {
                $('.__stripre').hide();
                $('.__paypal').show();
                if($('.paypal-buttons-context-iframe').length == 0){
                    paypal.Buttons({
                        createOrder: function(data, actions) {
                            return actions.order.create({
                                purchase_units: [{
                                    amount: {
                                        value: parseFloat($('#__Total').html())
                                    }
                                }]
                            });
                        },
                        onApprove: function(data, actions) {
                            return actions.order.capture().then(function(details) {
                                console.log(details);
                                $form.append($('<input type="hidden" name="paypal_res">').val(JSON.stringify(details)));
                                goTop();
                                setTimeout(function () {
                                    $form.submit();
                                }, 500)
                            });
                        }
                    }).render('#paypalLinks');
                }
            }
        }
        function shippingMethodChange(trigger){
            var id = $(trigger).val();
            @if($promoType == 3)
            if(id !== '' ){
                var fee = $(trigger).find('option[value="'+id+'"]').attr('data-fee');
                var total = parseFloat($('#__Total').attr('data-type'));
                $('#__promo').html(parseFloat(fee));
                $('#__shipping').html(parseFloat(fee));
            } else {
                var total = parseFloat($('#__Total').attr('data-type'));
                $('#__promo').html('0.00');
                $('#__shipping').html('0.00');
            }
            @else
            if(id !== '' ){
                var fee = $(trigger).find('option[value="'+id+'"]').attr('data-fee');
                var total = parseFloat($('#__Total').attr('data-type'));
                if(parseFloat(fee) > 0){
                    total = parseFloat(total) + parseFloat(fee);
                }
                $('#__shipping').html(parseFloat(fee));
                $('#__Total').html(total);
            } else {
                var total = parseFloat($('#__Total').attr('data-type'));
                $('#__shipping').html('0.00');
                $('#__Total').html(total);
            }
            @endif
        }
    </script>
    <script type="text/javascript">
        Stripe.setPublishableKey('{{env('STRIPE_KEY', 'pk_test_rcMGgXJR4c7CTKI1sTF8kOjh')}}');
        var $form = $('#payment-form-pp');
        $form.on('submit', function (e) {
            let pt = $('[name="payment_type"]:checked').val();
            if(pt === 'stripe'){
                e.preventDefault();
                $('.cardError').hide();

                $form.find('.submit').prop('disabled', true);
                Stripe.card.createToken($form, stripeResponseHandler);
                return false;
            } else {
                if($('input[name="paypal_res"]').length = 0){
                    e.preventDefault();
                }
            }
        });
        function stripeResponseHandler(status, response) {
            console.log(response);
            var $form = $('#payment-form-pp');
            if (response.error) {
                console.log(response.error.message);
                $form.find('.payment-errors').text(response.error.message).show();
                $form.find('.submit').prop('disabled', false);
                $('.cardError').show();
                goTop();
            } else {
                var token = response.id;
                $form.append($('<input type="hidden" name="card[stripeToken]">').val(token));
                $form.append($('<input type="hidden" name="card[card_brand]">').val(response.card.brand));
                $form.append($('<input type="hidden" name="card[card_last_four]">').val(response.card.last4));

                goTop();
                setTimeout(function () {
                    $form.submit();
                }, 500)

            }
        }
    </script>
@stop

@section('additionalJS')

@stop
