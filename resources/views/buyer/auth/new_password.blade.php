@extends('layouts.home_layout')
@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <section class="my_account_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my_accout_menu">
                        @include('buyer.profile.menu')

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="my_account_content">
                        <div class="myaccount_title">
                            <h2>Change Password</h2>
                        </div>
                        <div class="my_info_area">
                            <div class="row">
                                <div class="col-md-6 col-offset-3">
                                    <p>Please enter new password</p>
                                    <form class="login-box" method="post" action="{{ route('new_password_post_buyer') }}">
                                        @csrf

                                        <div class="form-group input-group">
                                            <input class="form-control" type="password" placeholder="Password" name="password" required><span class="input-group-addon"><i class="icon-lock"></i></span>
                                        </div>

                                        <div class="form-group input-group">
                                            <input class="form-control" type="password" placeholder="Re-enter Password" name="password_confirmation" required><span class="input-group-addon"><i class="icon-lock"></i></span>
                                        </div>

                                        @if ($errors->has('password'))
                                            <div class="form-group">
                                                <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                                            </div>
                                        @endif

                                        <div class="form-group">
                                            <div class="form-control-feedback">{{ session('message') }}</div>
                                        </div>

                                        <input type="hidden" name="token" value="{{ request()->get('token') }}">

                                        <div class="text-center text-sm-right">
                                            <button class="btn_common" type="submit">Reset Password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@stop