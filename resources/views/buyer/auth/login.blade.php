@extends('layouts.home_layout')

@section('content')


    <!-- =========================
            START  SIGN IN AREA SECTION
        ============================== -->
    <section class="sign_in_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-6 sign_up_right_padding">
                    <h2>Sign In</h2>
                    <div class="sign_in_inner">

                        <a href="javascript:void(0)" onclick="checkLoginFB()" class="facebook_connect"><i class="fab fa-facebook-f"></i> Login WITH FACEBOOK </a>
                        <p class="sign_p_small">We won't post anything without your permission</p>
                        <h3>OR JUST SIGN IN...</h3>

                        @if (session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif

                        <form method="post" action="{{route('buyer_login_post')}}">
                            @csrf
                            <div class="form-group">
                                <label>Email *</label>
                                <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Password *</label>
                                <input type="password" name="password" class="form-control"  placeholder="Password">
                            </div>
                            <button type="submit" class="btn_common">SIGN IN</button>
                            <a href="{{route('forgot_password_buyer')}}" class="forget_pword">Forgot Password?</a>
                        </form>
                        {{--<p class="p_required">* Required</p>--}}
                        <h1>Need Help?</h1>
                        <p>We're here Monday-Friday, 10 am-5 pm ET</p>
                        <br>
                        <p><b>+1-323-231-3600</b></p>
                        <a href="mailto:pjglobalr3@gmail.com" class="sign_email">pjglobalr3@gmail.com</a>
                    </div>
                </div>
                <div class="col-md-6 sign_up_left_border">
                    <h2>Sign Up</h2>
                    <div class="sign_in_inner">
                        <a href="javascript:void(0)" onclick="checkLoginFB()" class="facebook_connect"><i class="fab fa-facebook-f"></i> CONNECT WITH FACEBOOK </a>
                        <p class="sign_p_small">We won't post anything without your permission</p>
                        <h3>OR CREATE AN ACCOUNT</h3>

                        <form method="post" action="{{route('buyer_register_post')}}" id="register">
                            @csrf
                            <div class="form-group">
                                <label>First name *</label>
                                <span class="text-danger">{{$errors->has('first_name') ? $errors->first('first_name') : ''}}</span>
                                <input type="text" name="first_name" class="form-control" value="{{ old('first_name') }}" placeholder="First name">
                            </div>
                            <div class="form-group">
                                <label>Last name *</label>
                                <span class="text-danger">{{$errors->has('last_name') ? $errors->first('last_name') : ''}}</span>
                                <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control" placeholder="Last name">
                            </div>
                            <div class="form-group">
                                <label>Email *</label>
                                <span class="text-danger">{{$errors->has('email') ? $errors->first('email') : ''}}</span>
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Password *</label>
                                <span class="text-danger">{{$errors->has('password') ? $errors->first('password') : ''}}</span>
                                <input type="password" name="password" class="form-control"  placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label>Confirm password *</label>
                                <span class="text-danger">{{$errors->has('password_confirmation') ? $errors->first('password_confirmation') : ''}}</span>
                                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm password">
                            </div>
                            <div class="form-group form_checkbox">
                                <input type="checkbox" name="subscribe" id="input_check">
                                <label for="input_check">Receive Exclusive Deals</label>
                            </div>

                            <input type="hidden" name="origin">
                            <input type="hidden" name="provider_id">
                            <button type="submit" class="btn_common">Create Account</button>
                        </form>
                        {{--<p class="p_required">* Required</p>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =========================
        END SIGN IN AREA SECTION
    ============================== -->

    <script>
        // =======================
        // Social FB
        // =======================
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=173992223228367";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        function getUserSocialInfoFB() {
            var THIS = this;
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me', {fields: 'name,email,picture,first_name,last_name,birthday,gender'}, function(res) {

                var form = $('#register');
                form.find('input[name="first_name"]').val(res.first_name);
                form.find('input[name="last_name"]').val(res.last_name);
                form.find('input[name="email"]').val(res.email);

                form.append('<input type="hidden" name="origin" value="facebook">');
                form.append('<input type="hidden" name="provider_id" value="'+res.id+'">');
                setTimeout(function(){
                    $('#register').submit();
                }, 500);

            });
        }
        function OAuthChangeFB(response) {
            if (response.status === 'connected') {
                getUserSocialInfoFB();
            } else {
                FB.login(function(response) {
                    if (response.authResponse) {
                        getUserSocialInfoFB();
                    } else {
                        console.log('User cancelled login or did not fully authorize.');
                    }
                }, {scope: 'public_profile,email'});
            }
        }
        function checkLoginFB() {
            console.log('Checking Authentication');
            FB.init({
                appId      : '462515347618911',//'186501528565605',//'1431074556943850',//'1862542370669664',//'1431074556943850',
                cookie     : true,  // enable cookies to allow the server to access
                                    // the session
                xfbml      : true,  // parse social plugins on this page
                version    : 'v3.2' // use graph api version 2.8
            });
            FB.getLoginStatus(function(response) {
                OAuthChangeFB(response);
            });
        }
    </script>

@endsection