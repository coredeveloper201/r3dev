@extends('layouts.home_layout')

@section('content')
    <div class="container content">
        <div class="row">
            <div class="col-md-12">
                <section class="order_success_container">

                    <h3>Thank you for your order</h3>
                    <p>
                        <b>Your Order Number is: </b>

                        <a href="{{ route('buyer_show_orders_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a>&nbsp;
                    </p>

                    <p>We will proceed your order soon.</p>
                </section>
            </div>
        </div>

    </div>
@stop