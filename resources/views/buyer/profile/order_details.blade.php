@extends('layouts.home_layout')
@section('content')
    <section class="my_account_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my_accout_menu">
                        @include('buyer.profile.menu')

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="my_account_content">
                        <div class="myaccount_title"><h2>Order Details</h2></div>
                        <div class="my_info_area">
                            <div class="row">
                                <div class="content col-md-12 margin-bottom-1x">
                                    <h4>Vendor Order Details - {{  $orderDetails->order_number  }}</h4>
                                    <br><br><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    @if ($black_logo_path != '')
                                        <img height="100px" src="{{ $black_logo_path }}">
                                    @endif
                        {{--            @if ($orderDetails->user->buyer->avatar != '')
                                        <img style="width: 235px;height: 90px"  src="{{url('storage/'.$orderDetails->user->buyer->avatar)}}">
                                    @endif--}}
                                    <p>
                                        <b>{{ $orderDetails->user->buyer->company_name }}</b><br>
                                        {{ $orderDetails->user->buyer->billing_address }},<br>
                                        {{ $orderDetails->user->buyer->billing_city }},
                                        @if ($orderDetails->user->buyer->billingState == null)
                                            {{ $orderDetails->user->buyer->billing_state }},
                                        @else
                                            {{ $orderDetails->user->buyer->billingState->name }},
                                        @endif
                                        {{ $orderDetails->user->buyer->billingCountry->name }} - {{ $orderDetails->user->buyer->billing_zip }}
                                    </p>
                                </div>

                                <div class="col-md-2"></div>
                                <div class="col-md-5 text-right">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Order No.</th>
                                            <td>{{ $orderDetails->order_number }}</td>
                                        </tr>

                                        <tr>
                                            <th>Order Date</th>
                                            <td>{{ date('F d, Y', strtotime($orderDetails->created_at)) }}</td>
                                        </tr>

                                        <tr>
                                            <th>Status</th>
                                            <td>
                                                {{ $orderDetails->statusText() }}

                                                @if ($orderDetails->status == \App\Enumeration\OrderStatus::$BACK_ORDER && $orderDetails->rejected == 0)
                                                    <br>
                                                    <a href="" class="text-success" id="btnApprove">Approve</a>
                                                    <a href="" class="text-danger" id="btnDecline">Decline</a>
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class=" padding-bottom-1x">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Shipping Address</th>
                                        <th>Billing Address</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{ $orderDetails->shipping_address }}<br>
                                            {{ $orderDetails->shipping_state }}, {{ $orderDetails->shipping_city }}, {{ $orderDetails->shipping_country }} - {{ $orderDetails->shipping_zip }}</td>
                                        <td>{{ $orderDetails->billing_address }}<br>
                                            {{ $orderDetails->billing_state }}, {{ $orderDetails->billing_city }}, {{ $orderDetails->billing_country }} - {{ $orderDetails->billing_zip }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Phone: </b>{{ $orderDetails->shipping_phone }}</td>
                                        <td><b>Phone: </b>{{ $orderDetails->billing_phone }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Shipping Method</th>
                                        <td>{{ isset($orderDetails->shippingMethod) ? $orderDetails->shippingMethod->name : '' }}</td>
                                        <th>Tracking Number</th>
                                        <td><a href="https://www.ups.com/track?tracknum={{ $orderDetails->tracking_number }}&requester=WT/trackdetails" target="_blank">{{ $orderDetails->tracking_number }}</a></td>
                                        <th>Invoice Number</th>
                                        <td>{{ $orderDetails->invoice_number }}</td>
                                    </tr>
                                </table>
                            </div>

                            <h4>Items</h4>
                            <hr class="padding-bottom-1x">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Style No.</th>
                                        <th class="text-center">Qty</th>
                                        <th class="text-center">Unit Price</th>
                                        <th class="text-center">Amount</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($orderDetails->items as $item_id => $item)
                                      <tr>
                                          <td rowspan="">
                                              @if (count($item->item->images) > 0)
                                                  <img src="{{ asset($item->item->images[0]->image_path) }}" alt="Product" style="height: 100px">
                                              @else
                                                  <img src="{{ asset('images/no-image.png') }}" alt="Product">
                                              @endif
                                          </td>
                                          <td> {{ $item->style_no }}</td>
                                          <td> {{ $item->total_qty }}</td>
                                          <td>  ${{ sprintf('%0.2f', $item->per_unit_price) }}</td>
                                          <td>  ${{ sprintf('%0.2f', $item->amount) }}</td>
                                      </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-md-9"></div>
                                <div class="col-md-3">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Sub Total</th>
                                            <td>${{ sprintf('%0.2f', $orderDetails->subtotal) }}</td>
                                        </tr>

                                        <tr>
                                            <th>Discount</th>
                                            <td>${{ sprintf('%0.2f', $orderDetails->discount) }}</td>
                                        </tr>

                                        <tr>
                                            <th>Shipping Cost</th>
                                            <td>${{ sprintf('%0.2f', $orderDetails->shipping_cost) }}</td>
                                        </tr>

                                        <tr>
                                            <th>Total</th>
                                            <td><b>${{ sprintf('%0.2f', $orderDetails->total) }}</b></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <b>Note: </b>
                                        {{ $orderDetails->note }}
                                    </p>
                                </div>



                            </div>
                            <div class="row">
                                <div class="content col-md-12 margin-top-1x">
                                    <a style="float: left" class="btn btn-primary" href="{{ route('buyer_show_orders') }}">Back To Order List</a>

                                    <a style="float: right" class="btn btn-primary btnPrintInvoiceOrder" href="#">Print Invoice</a>
                                    {{--<button style="float: right" class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>--}}
                                </div>


                                <div class="modal fade" id="print-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall"
                                     aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="modalLabelSmall">Print</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>

                                            <div class="modal-body">
                                                <a class="btn btn-primary" href="" target="_blank" id="btnPrintWithImage">Print with Images</a><br><br>
                                                <a class="btn btn-primary" href="" target="_blank" id="btnPrintWithoutImage">Print without Images</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('additionalJS')
    <script>

        $(function () {



            function createOrderPdfUrl() {
                var url = '{{ route('buyer_print_pdf') }}' + '?order={{$orderDetails->id}}';
                var urlWithoutImage = '{{ route('buyer_print_pdf_without_image') }}' + '?order={{$orderDetails->id}}';

                $('#btnPrintWithImage').attr('href', url);
                $('#btnPrintWithoutImage').attr('href', urlWithoutImage);
            }

            $('.btnPrintInvoiceOrder').click(function (e) {
                e.preventDefault();
                createOrderPdfUrl();
                $('#print-modal').modal('show');

            });
        });
    </script>
@stop