<h2>MY ACCOUNT</h2>
<ul>
    <li class="{{ $profile_page == 'overview' ? 'active' : ''  }}"><a href="{{ route('buyer_show_overview') }}">My Dashboard</a></li>
    <li class="{{ $profile_page == 'myInformation' ? 'active' : ''  }}"><a href="{{ route('buyer_my_information') }}">My Information</a></li>
    <li class="{{ $profile_page == 'buyerBilling' ? 'active' : ''  }}"><a href="{{ route('buyer_billing') }}">Billing Information</a></li>
    <li class="{{ $profile_page == 'orders' ? 'active' : ''  }}"><a href="{{ route('buyer_show_orders') }}">Order History</a></li>
    <li class="{{ $profile_page == 'wishlist' ? 'active' : ''  }}"><a href="{{ route('view_wishlist') }}">My Wishlist</a></li>
{{--<li><a href="{{ route('buyer_beauty_bio') }}">My Beauty Bio</a></li>
    <li><a href="{{ route('buyer_gift_card') }}">Check Gift Card Balance</a></li>--}}
</ul>