@extends('layouts.home_layout')

@section('content')

    <section class="my_account_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my_accout_menu">
                        @include('buyer.profile.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="my_account_content">
                        <div class="myaccount_title">
                            <h2>Billing Information</h2>
                        </div>
                        <div class="my_info_area">
                            <div class="row">
                                <div class="col-md-12">
                                    <form method="post" action="{{route('buyer_update_shipping_info')}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                                                    <label for="small-rounded-input">First name <span
                                                                class="required">*</span></label>
                                                    <input class="form-control" type="text" id="first_name"
                                                           name="first_name"
                                                           value="{{ empty(old('first_name')) ? ($errors->has('first_name') ? '' : auth()->user()->first_name) : old('first_name') }}">

                                                    @if ($errors->has('first_name'))
                                                        <div class="has-error form-control-feedback">{{ $errors->first('first_name') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                                                    <label for="small-rounded-input">Last name <span
                                                                class="required">*</span></label>
                                                    <input class="form-control" type="text" id="last_name"
                                                           name="last_name"
                                                           value="{{ empty(old('first_name')) ? ($errors->has('	last_name') ? '' : auth()->user()->last_name) : old('last_name') }}">

                                                    @if ($errors->has('last_name'))
                                                        <div class="has-error form-control-feedback">{{ $errors->first('last_name') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group{{ $errors->has('	address') ? ' has-danger' : '' }}">
                                                    <label for="small-rounded-input">Address <span
                                                                class="required">*</span></label>
                                                    <textarea name="address" rows="5" id="address"
                                                              class="form-control">{{ empty(old('address')) ? ($errors->has('address') ? '' : $buyerBillingInfo->address) : old('address') }}</textarea>

                                                    @if ($errors->has('address'))
                                                        <div class="has-error form-control-feedback">{{ $errors->first('address') }}</div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group{{ $errors->has('city') ? ' has-danger' : '' }}">
                                                    <label for="small-rounded-input">City <span
                                                                class="required">*</span></label>
                                                    <input class="form-control" type="text" id="city" name="city"
                                                           value="{{ empty(old('city')) ? ($errors->has('city') ? '' : $buyerBillingInfo->city) : old('city') }}">
                                                    @if ($errors->has('city'))
                                                        <div class="has-error form-control-feedback">{{ $errors->first('city') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group{{ $errors->has('state_text') ? ' has-danger' : '' }}">
                                                    <label for="small-rounded-input">State <span
                                                                class="required">*</span></label>
                                                    <input class="form-control" type="text" id="state_text"
                                                           name="state_text"
                                                           value="{{ empty(old('state_text')) ? ($errors->has('state_text') ? '' : $buyerBillingInfo->state_text) : old('state_text') }}">
                                                    @if ($errors->has('state_text'))
                                                        <div class="has-error form-control-feedback">{{ $errors->first('state_text') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-4">

                                                <div class="form-group{{ $errors->has('zip') ? ' has-danger' : '' }}">
                                                    <label for="small-rounded-input">Zip <span class="required">*</span></label>
                                                    <input class="form-control" type="text" id="zip" name="zip"
                                                           value="{{ empty(old('zip')) ? ($errors->has('zip') ? '' : $buyerBillingInfo->zip) : old('zip') }}">
                                                    @if ($errors->has('zip'))
                                                        <div class="has-error form-control-feedback">{{ $errors->first('zip') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group{{ $errors->has('factoryCountry') ? ' has-danger' : '' }}">
                                                    <label for="small-rounded-input">Country <span
                                                                class="required">*</span></label>
                                                    <select class="form-control" id="country" name="country">
                                                        <option value="">Select Country</option>
                                                        @foreach($countries as $country)
                                                            <option data-code="{{ $country->code }}"
                                                                    value="{{ $country->id }}"
                                                                    {{ empty(old('country')) ? ($errors->has('country') ? '' : ($buyerBillingInfo->country_id == $country->id ? 'selected' : '')) :
                                                                        ($country->code  == 'US' ? 'selected' : '') }}>{{ $country->name }}</option>
                                                        @endforeach
                                                    </select>

                                                    @if ($errors->has('country'))
                                                        <div class="form-control-feedback">{{ $errors->first('country') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group" id="form-group-state-select">
                                                    <label for="small-rounded-input">State <span
                                                                class="required">*</span></label>
                                                    <select class="form-control form-control-rounded form-control-sm"
                                                            id="stateSelect" name="state">
                                                        <option value="">Select State</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                                                    <label for="small-rounded-input">Phone <span
                                                                class="required">*</span></label>
                                                    <input class="form-control" type="text" id="phone" name="phone"
                                                           value="{{ empty(old('phone')) ? ($errors->has('phone') ? '' : $buyerBillingInfo->phone) : old('phone') }}">
                                                    @if ($errors->has('phone'))
                                                        <div class="has-error form-control-feedback">{{ $errors->first('phone') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">

                                                    <button type="submit" class="btn_common float-right">Update</button>
                                                </div>
                                            </div>

                                        </div>


                                    </form>
                                </div>
                            </div>
                            {{-- <h2>Stored Payment Methods</h2>
                            <p>When editing or adding a payment method, please be sure to select "Set as Default Payment Method" if you would like to use that card for your Beauty Box subscription.</p> <br>

                            <h2>Email</h2> <br>
                            <p>You have no credit cards on file.</p>
                            <button class="btn_common_type2">ADD CREDIT CARD</button>
                            <button class="btn_common_type2">ADD PAYPAL</button> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
