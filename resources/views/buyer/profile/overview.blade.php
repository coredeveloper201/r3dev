@extends('layouts.home_layout')

@section('content')

    <section class="my_account_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my_accout_menu">
                        @include('buyer.profile.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="my_account_content">
                        <div class="myaccount_title">
                            <h2>My Dashboard</h2>
                        </div>
                        <div class="my_dashboard_title text-center">
                            <h2>Hello, {{$UserInfoData->first_name.' '.$UserInfoData->last_name}}</h2>
                            <p>Welcome to R3 All</p>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 my_dasboard_custom_padding">
                                <div class="my_dashboard_inner text-center my_info_bg_1">
                                    <h2>My Information</h2>
                                    <img src="{{asset('/images/my-account/myacc-ic1.svg')}}" alt="">
                                    <a href="{{route('buyer_my_information')}}">Update My Info</a>
                                </div>
                            </div>
                            <div class="col-lg-4 my_dasboard_custom_padding">
                                <div class="my_dashboard_inner text-center my_info_bg_2">
                                    <h2>My Beauty Bio</h2>
                                    <img src="{{asset('/images/my-account/myacc-ic2.svg')}}" alt="">
                                    <a href="#">Tell Us About You</a>
                                </div>
                            </div>
                            <div class="col-lg-4 my_dasboard_custom_padding">
                                <div class="my_dashboard_inner text-center my_info_bg_3">
                                    <h2>Order History</h2>
                                    <img src="{{asset('/images/my-account/myacc-ic3.svg')}}" alt="">
                                    <a href="{{ route('buyer_show_orders') }}">See My Orders</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 my_dasboard_custom_padding">
                                <div class="common_bottom_borter"></div>
                            </div>
                        </div>
                        {{--<div class="my_dashboard_title text-center ">--}}
                            {{--<h2>Join Julep Beauty Lab</h2>--}}
                            {{--<img src="{{asset('/images/my-account/myacc-img1.jpg')}}" alt="" class="img-fluid my_dashboard_img">--}}
                            {{--<p>Sign up and work with us to co-create amazing products. We'll ask you for your thoughts on new colors, products, trends and more. You can help determine what we create!</p>--}}
                            {{--<button class="btn_common">sign up</button>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-12 my_dasboard_custom_padding">--}}
                                {{--<div class="common_bottom_borter"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="my_dashboard_title text-center ">--}}
                            {{--<h2>Sign up for the Julep Beauty Box</h2>--}}
                            {{--<p>Enjoy a customizable Julep Beauty Box subscription that lets you choose full-size beauty and polish from brand-new collections every month.</p>--}}
                            {{--<img src="{{asset('/images/my-account/myacc-img2.jpg')}}" alt="" class="img-fluid">--}}
                            {{--<button class="btn_common">sign up</button>--}}
                        {{--</div>--}}
                        {{--<div class="my_dashboard_inner_type_2">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-lg-4">--}}
                                    {{--<div class="my_dashboard_inner text-center ">--}}
                                        {{--<img src="{{asset('/images/my-account/myacc-ic4.svg')}}" alt="">--}}
                                        {{--<h1>1</h1>--}}
                                        {{--<h2>Take the beauty quiz</h2>--}}
                                        {{--<p><a href="#">Answer a few fun questions</a>Answer a few fun questions,sign up, then get yourfirst beauty box.</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-4">--}}
                                    {{--<div class="my_dashboard_inner text-center ">--}}
                                        {{--<img src="{{asset('/images/my-account/myacc-ic5.svg')}}" alt="">--}}
                                        {{--<h1>2</h1>--}}
                                        {{--<h2>Customize your box</h2>--}}
                                        {{--<p>Make your picks from a new collection every month. Not feeling it? Skip a month!</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-4">--}}
                                    {{--<div class="my_dashboard_inner text-center">--}}
                                        {{--<img src="{{asset('/images/my-account/myacc-ic6.svg')}}" alt="">--}}
                                        {{--<h1>3</h1>--}}
                                        {{--<h2>Enjoy beauty box perks</h2>--}}
                                        {{--<p>You'll always get free U.S. shipping and exclusive offers</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="my_dashboard_title my_dashboard_title_type2 text-center ">--}}
                            {{--<h2>Ready to join the fun?</h2>--}}
                            {{--<button class="btn_common_type2">LETS GO</button>--}}
                            {{--<a href="#">Want to know more? Check out FAQs</a>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
