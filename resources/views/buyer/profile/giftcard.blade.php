@extends('layouts.home_layout')

@section('content')

    <section class="my_account_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my_accout_menu">
                        <h2>MY ACCOUNT</h2>
                        <ul>
                            <li><a href="{{ route('buyer_show_overview') }}">My Dashboard</a></li>
                            <li><a href="{{ route('buyer_my_information') }}">My Information</a></li>
                            <li><a href="{{ route('buyer_billing') }}">Billing Information</a></li>
                            <li><a href="{{ route('buyer_show_orders') }}">Order History</a></li>
                            {{-- <li><a href="#">My Auto Replenishments</a></li> --}}
                            <li><a href="{{ route('view_wishlist') }}">My Wishlist</a></li>
                            <li><a href="{{ route('buyer_beauty_bio') }}">My Beauty Bio</a></li>
                            <li class="active"><a href="{{ route('buyer_gift_card') }}">Check Gift Card Balance</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="my_account_content">
                        <div class="myaccount_title">
                            <h2>Check Gift Card Balance</h2>
                        </div>
                        <div class="my_gift_area">
                            <div class="form-group">
                                <label>Enter gift card code *</label>
                                <input type="text" class="form-control" placeholder="">
                                <button type="submit" class="btn_common">CHECK CARD BALANCE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
