<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="{{ $meta_description or '' }}">
    <title>{{ isset($page_title) ? $page_title : $meta_title }}</title>
    <link rel="icon" href="{{ $black_logo_path }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">     
    <link rel="stylesheet" href="{{ asset('fonts/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('css/lightslider.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('css/menuzord.css?css=') }}{{ time() }}">
    <link rel="stylesheet" href="{{ asset('css/main.css?css=') }}{{ time() }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('additionalCSS')
    <style>
        .has-error{
            color: red!important;
        }
    </style>
</head>
<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- <div class="image_rounded top_left_img"><img src="demo/top-left-img.svg" alt=""></div>
    <div class="image_rounded top_right_img"><img src="demo/top-right-img.svg" alt=""></div>
    <div class="image_rounded bottom_left_img"><img src="demo/bottom-left-img.svg" alt=""></div>
    <div class="image_rounded bottom_right_img"><img src="demo/bottom-right-img.svg" alt=""></div> -->
    
    <!-- =========================
        START HEADER SECTION
    ============================== -->
    @include('layouts.shared.header')
    <!-- =========================
        END HEADER SECTION
    ============================== -->

    @yield('content')

    <!-- =========================
        START FOOTER SECTION
    ============================== -->
    @include('layouts.shared.footer')
    <!-- =========================
        END FOOTER SECTION
    ============================== -->

    <script src="{{ asset('js/vendor/jquery-1.11.2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js"></script>
    <script src="{{ asset('js/vendor/bootstrap.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.js') }}"></script>
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
    {{-- <script src="{{ asset('js/lightslider.js') }}"></script> --}}
    <script src="{{ asset('js/lightslider.js') }}"></script>
    <script src="{{ asset('js/menuzord.js') }}"></script>
    <script src="{{ asset('js/main.js?js=') }}{{ time() }}"></script>

    @yield('additionalJS')

</body>
</html>