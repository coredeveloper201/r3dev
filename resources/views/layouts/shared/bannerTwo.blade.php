<section class="home_coveted_area banner_area clearfix">
    <div class="home_coveted_left match_item">
        @if(count($bannerTwo) > 0)
            <img src="{{asset('images/banner/'.$bannerTwo[0]['value'])}}" alt="" class="img-fluid">
        @else 
            <img src="{{asset('images/banner/no-image.png')}}" alt="" class="img-fluid">
        @endif
        <div class="home_coveted_text">
            {!! $bannerTwoDes->value !!}
        </div>
    </div>
    <div class="home_coveted_right home_coveted_right2 match_item">
        <div class="coveted_inner">

        </div>
    </div>
</section>