<?php
    use App\Enumeration\Role;
    use App\Enumeration\Availability;
?>

@extends('layouts.home_layout')

@section('additionalCSS')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
@stop

@section('content')
    <div class="shipping_cart_area">
        <!-- =========================
            START BREDCRUMB SECTION
        ============================== -->
        <section class="breadcrumbs_area">
            <div class="container custom_container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active">Products</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END BREDCRUMB SECTION
        ============================== -->

        <!-- =========================
            START  SHOP SECTION
        ============================== -->
        <section class="shop_filter">
            <div class="container custom_container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product_title">
                            <h2>Products</h2>
                            <span data-toggle="collapse" href="#collapseExample"><img src="/images/eye.svg" alt="" class="img-fluid"></span>
                            <div class="collapse " id="collapseExample">
                                <div class="card card-body filter_desc">
                                    Enchant and entrance with mesmerizing holiday beauty gifts that dance between the delicate and the daring. Created in collaboration with illustrator and textile designer Barbara Agnes, the Magic Meets Mania collection invites you to fall deeply in love—and spread it around. Eyes flutter, lips pulse, and life takes on new light in makeup gift sets and whimsy goes for a whirl in nail polish gift sets.
                                    Add your special touch with free artist-inspired printable gift tags. Download them here.
                                </div>
                            </div>
                        </div>
                        <div class="shop_filter_inner">
                            <div class="custom_select_box_wrapper">
                                <select class="custom_select_box form-control" dir="rtl" onchange="sortChange(this)">
                                    <option value="">Sort By</option>
                                    <option value="price|desc" @if(isset($_GET['sort_by']) && isset($_GET['order_by']) && $_GET['sort_by'].'|'.$_GET['order_by'] == 'price|desc') selected @endif>Highest price</option>
                                    <option value="price|asc" @if(isset($_GET['sort_by']) && isset($_GET['order_by']) && $_GET['sort_by'].'|'.$_GET['order_by'] == 'price|asc') selected @endif>Lowest price</option>
                                    <option value="featured|asc" @if(isset($_GET['sort_by']) && isset($_GET['order_by']) && $_GET['sort_by'].'|'.$_GET['order_by'] == 'featured|asc') selected @endif>Featured</option>
                                    <option value="created_at|desc" @if(isset($_GET['sort_by']) && isset($_GET['order_by']) && $_GET['sort_by'].'|'.$_GET['order_by'] == 'created_at|desc') selected @endif>Newest</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END SHOP SECTION
        ============================== -->

        <!-- =========================
            START PRODUCT SECTION
        ============================== -->
        <section class="prduct_area">
            <div class="container custom_container">
                <div class="row">
                    @if ( !$products->isEmpty() )
                        @foreach ( $products as $product )
                            <div class="col-6 col-md-4 col-lg-4">
                                <div class="product_inner text-center">
                                    @if ( isset($product->images) && count($product->images) > 0 )
                                        <a href="{{ route('item_details_page', $product->slug) }}"><img src="{{asset('/' . $product->images[0]['image_path'])}}" alt="" class="img-fluid"></a>
                                    @else
                                        <a href="{{ route('item_details_page', $product->slug) }}"><img src="{{asset('/images/no-image.png')}}" alt="" class="img-fluid"></a>
                                    @endif
                                    <div class="product_info">
                                        <h2 class="name">{{ $product->name }}</h2>
                                        <p class="description">{{ $product->description }}</p>
                                        <p class="price">
                                            @if($product->orig_price != null && $product->orig_price != '')
                                                <span>${{$product->orig_price}}</span>
                                            @endif
                                            {{ $product->price }}<span></span> 
                                        </p>
                                        <a href="javascript:void(0)" onclick="addToCart('{{$product->id}}')">Add to Bag</a>
                                        @if ( $product->rating['count'] > 0 )
                                            <div class="product_star_rating">
                                                <span class="customer_rateYo" data-type="{{ $product->rating['rate'] }}"></span>
                                                <span class="average_rating">({{ $product->rating['count'] }})</span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END PRODUCT SECTION
        ============================== -->
    </div>


    <script>
        function sortChange(trigger){
            var v = $(trigger).val();
            if(v !== ''){
                var url_string = window.location.href;
                var url = new URL(url_string);
                var c = url.searchParams.get("page");
                if(c === null){ c = 1}

                var vr = v.split('|');
                var sort_by = vr[0];
                var order_by = vr[1];
                var page = c;

                window.location.href = window.location.pathname+'?page='+c+'&sort_by='+sort_by+'&order_by='+order_by;
            } else {

                var url_string = window.location.href;
                var url = new URL(url_string);
                var c = url.searchParams.get("page");
                if(c === null){ c = 1}
                var page = c;

                window.location.href = window.location.pathname+'?page='+c;
            }
        }
    </script>

@stop

@section('additionalJS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<script>
    //Make sure that the dom is ready
    $(function () {
        
        $(".customer_rateYo").each(function(){
            var rating = $(this).attr('data-type');
            $(this).rateYo({
                rating: rating,
                starWidth: "17px",
                readOnly: true,
                normalFill: "#cfcfcf",
                ratedFill: "#000"
            });
        });

    });
</script>
@stop