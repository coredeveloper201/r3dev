<?php
    use App\Enumeration\Role;
    use App\Enumeration\Availability;
?>

@extends('layouts.home_layout')

@section('additionalCSS')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
@stop

@section('content')
    <!-- right side rating div -->
    <div class="full_screen_overlay" @click="closeReview()"></div>
        <div v-if="loggedIn" class="review_rating_right_menu">
            <form action="{{ route('item_rating', $item['id']) }}" method="POST">
                {{ csrf_field() }}
                <h2>WRITE YOUR REVIEW</h2>
                <span class="close_ic mr-3" @click="closeReview()">
                    <i class="fa fa-times-circle" style="font-size:28px"></i>
                </span>
                
                <div class="review_title clearfix">


                    @if(count($item['images']) > 0)
                        <img src="{{ asset($item['images'][0]['image_path']) }}" alt="No image" />
                    @else
                        <img src="{{ asset('/images/no-image.png') }}" />
                    @endif
                    <p>{{ $item['name'] }}</p>
                    {{-- <h2>currency+product.discount_price</h2> --}}
                </div>
                <div class="clearfix"></div>
                <div class="review_description clearfix">
                    <h2>Review Rating *</h2>
                    <div id="rateYo"></div>
                </div>
                <div class="form-group common_form">
                    <label>REVIEW TITLE</label>
                    <input type="text" class="form-control" name="title" required>
                </div>
                <div class="form-group common_form">
                    <label>WRITE REVIEW</label>
                    <textarea id="" cols="30" rows="10" class="form-control" name="comment" required></textarea>
                    <input type="hidden" id="rate_value" name="rate_value" value="">
                    <input type="hidden" name="product_id" value="{{ $item['id'] }}">
                </div>
                <p>Please avoid using any inappropriate language, personal information, HTML, references to other retailers or copyrighted comments.</p>
                <div class="form-group common_form">
                    <input type="submit" class="btn btn-primary btn-lg btn-block" value="SUBMIT">
                </div>
            </form>
        </div>
    </div>

    <div class="shipping_cart_area">
        <!-- =========================
        START PRODUCT SINGLE SECTION
        ============================== -->
        <section class="product_single_area">
            <div class="container custom_container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="product_single_left">
                            @if(count($item['images']) > 0)
                            <img src="{{ asset($item['images'][0]['image_path']) }}" class="img-fluid main-img-fluid"/>
                            @else
                                <img src="{{ asset('/images/no-image.png') }}" class="img-fluid main-img-fluid"/>
                                @endif
                            {{-- {{ dd($item['images']) }} --}}
                            @if ( isset($item['images']) )
                                <ul id="product_single_left_list">
                                    @foreach ( $item['images'] as $image )
                                    <li>
                                        <img src="{{ asset($image['thumbs_image_path']) }}" class="img-fluid"/>
                                    </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="single_product_description">
                            <h2>{{ $item['name'] }}</h2>
                            <p>{{ $item['style_no'] }}</p>
                            @if(count($review) > 0)
                            <div class="customer_rateYo block" data-type="{{ $average_rating }}"></div> {{$average_rating}} ({{ count($review)  }})
                            @endif
                            {{-- <p><span>$100</span> $24.99 Special</p> --}}
                            <p>
                                @if($item['orig_price'] != null && $item['orig_price'] != '')
                                    <span>${{$item['orig_price']}}</span>
                                @endif
                                ${{ $item['price'] }}
                            </p>
                            <div class="row">
                                <div class="col-lg-12 wishlist-custom-col">
                                    <button class="btn_common" onclick="addToCart('{{$item['id']}}')">ADD TO BAG</button>
                                    @if ( isset(Auth::user()->id) )
                                        @if ( $wishlistFlag == 0 ) 
                                            <span>
                                                <form action="{{ route('add_to_wishlist') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="item_id" value="{{$item['id']}}">
                                                    <input type="submit" id="submit-wishlist" value="Add to Wishlist">
                                                </form>
                                            </span>
                                        @else 
                                            <span>
                                                <form action="{{ route('remove_from_wishlist') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="item_id" value="{{$item['id']}}">
                                                    <input type="submit" id="remove-wishlist" value="Remove to Wishlist">
                                                </form>
                                            </span>
                                        @endif
                                    @else 
                                        <a href="{{ route('buyer_login') }}">Add to Wishlist</a>
                                    @endif
                                </div>
                            </div>
                            <div class="single_product_desc_collapse text-center">
                                <div class="accordion" id="readmoreara">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" id="current_product" type="button" data-toggle="collapse" data-target="#readmore3">
                                                    Description
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="readmore3" class="collapse" style="">
                                            <div class="card-body">{!! $item['description'] !!}</div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#readmore2">
                                                    WRITE A REVIEW
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="readmore2" class="collapse" style="">
                                            <div class="card-body">
                                                @if ( isset(Auth::user()->id) )
                                                    <div class="write_review ">
                                                        <span>Write a Review</span>
                                                    </div>
                                                @else
                                                    <div class="write_review">
                                                        <a  href="{{ route('buyer_login') }}">Write a Review</a>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END PRODUCT SINGLE SECTION
        ============================== -->

        <!-- =========================
            START PRODUCT SECTION
        ============================== -->
        <section class="product_desc_area">
            <div class="container custom_container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="related_product_title">
                            <h2>Details</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product_desc_content">
                            <p>{!! $item['details'] !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END PRODUCT SECTION
        ============================== -->

        <!-- =========================
            START PRODUCT REVIEW SECTION
        ============================== -->
        <section class="product_review">
            <div class="container custom_container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>What people say about us</h1>
                        <div class="product_review_inner">
                            {{-- {{ dd($review) }} --}}
                            @if ( count($review) > 0 ) 
                            <ul>
                                @foreach ($review as $review_data)
                                <li>
                                    <div class="product_review_inner_img">
                                        <img src="{{ asset(\App\Model\MetaBuyer::avatar_path().$review_data->avatar) }}" alt="">
                                        <h2>{{ $review_data->first_name }} {{ $review_data->last_name }}</h2>
                                        <p>{{ date('d M Y', strtotime($review_data->updated_at)) }}</p>
                                    </div>
                                    <h2>{{ $review_data->comment }}</h2>
                                    <div class="customer_rateYo" data-type="{{ $review_data->rating }}"></div>
                                </li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            START PRODUCT REVIEW SECTION
        ============================== -->

        <!-- =========================
            START PRODUCT SECTION
        ============================== -->
        <section class="prduct_area">
            <div class="container custom_container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="related_product_title">
                            <h2>Customers Also Bought</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @if ( isset($productByCatId) && count($productByCatId) > 0 )
                        @foreach ( $productByCatId as $product )
                        <div class="col-6 col-md-4 col-lg-4">
                            <div class="product_inner text-center">
                                @if ( isset($product->image_path) )
                                    <a href="{{ route('item_details_page', $product->slug) }}"><img src="{{asset('/' . $product->image_path)}}" alt="" class="img-fluid"></a>
                                @else
                                    <a href="{{ route('item_details_page', $product->slug) }}"><img src="{{asset('/images/no-image.png')}}" alt="" class="img-fluid"></a>
                                @endif
                                {{-- <a href="#"><img src="images/product/product-1.jpg" alt="" class="img-fluid"></a> --}}
                                <div class="product_info">
                                    <h2 class="name">{{ $product->name }}</h2>
                                    <p class="description">{{ $product->description }}</p>
                                    <p class="price">
                                        @if($product->orig_price != null && $product->orig_price != '')
                                            <span>${{$product->orig_price}}</span>
                                        @endif
                                         {{ $product->price }}
                                    </p>
                                    <a href="javascript:void(0)" onclick="addToCart('{{$product->id}}')">Add to Bag</a>
                                    @if ( $product->rating['count'] > 0 )
                                        <div class="product_star_rating">
                                            <span class="customer_rateYo" data-type="{{ $product->rating['rate'] }}"></span>
                                            {{-- <span class="average_rating">({{ $product->rating['count'] }})</span> --}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </section>
        <!-- =========================
            END PRODUCT SECTION
        ============================== -->
    </div>

@stop

@section('additionalJS')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <script>
        //Make sure that the dom is ready
        $(function () {
            // Gallery image replace from thumbnail
            $("#product_single_left_list .img-fluid").click(function(){
                var getImgUrl = $(this).attr('src');
                var getImgUrl = getImgUrl.replace('thumbs', 'original');
                $(".main-img-fluid").attr('src', getImgUrl);
            });

            $("#rateYo").rateYo({
                rating: 5,
                fullStar: true,
                onChange: function (rating, rateYoInstance) {
                    $('#rate_value').val(rating);
                }
            });
            
            $(".customer_rateYo").each(function(){
                var rating = $(this).attr('data-type');
                $(this).rateYo({
                    rating: rating,
                    starWidth: "17px",
                    readOnly: true,
                    normalFill: "#cfcfcf",
                    ratedFill: "#000"
                });
            });

            setTimeout(function(){
                $('#current_product').trigger('click');
            }, 200)

        });
    </script>
@stop