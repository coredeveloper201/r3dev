@extends('layouts.home_layout')

@section('additionalJS')
    <style>
        .home_coveted_right1{
            @if(count($bannerOne) > 1)
                background: url('{{asset('images/banner/' . $bannerOne[1]['value'])}}') no-repeat;
            @endif
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            -webkit-transition: all .3s;
            -o-transition: all .3s;
            transition: all .3s;
        }
        .home_coveted_right1.coveted_inner_hover{
            @if(count($bannerOne) > 2)
                background: url('{{asset('images/banner/'.$bannerOne[2]['value'])}}') no-repeat;
            @endif
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            -webkit-transition: all .3s;
            -o-transition: all .3s;
            transition: all .3s;
        }
        .home_coveted_right2{
            @if(count($bannerTwo) > 1)
                background: url('{{asset('images/banner/'.$bannerTwo[1]['value'])}}') no-repeat;
            @endif
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            -webkit-transition: all .3s;
            -o-transition: all .3s;
            transition: all .3s;
        }
        .home_coveted_right2.coveted_inner_hover{
            @if(count($bannerTwo) > 2)
                background: url('{{asset('images/banner/'.$bannerTwo[2]['value'])}}') no-repeat;
            @endif
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            -webkit-transition: all .3s;
            -o-transition: all .3s;
            transition: all .3s;
        }
    </style>
@endsection
@section('content')
    <!-- =========================
        START BANNER SECTION
    ============================== -->
    @include('layouts.shared.banner')
    <!-- =========================
        END BANNER SECTION
    ============================== -->  
    
    <!-- =========================
        START HOME SHOP SECTION
    ============================== -->
    @include('layouts.shared.sectionOneProduct')
    <!-- =========================
        END HOME SHOP SECTION
    ============================== -->

    <!-- =========================
        START HOME COVETED SECTION
    ============================== -->
    @include('layouts.shared.bannerOne')
    <!-- =========================
        END HOME COVETED SECTION
    ============================== -->

    <!-- =========================
        START HOME SHOP SECTION
    ============================== -->
    @include('layouts.shared.sectionTwoProduct')
    <!-- =========================
        END HOME SHOP SECTION
    ============================== -->

    <!-- =========================
        START HOME COVETED SECTION
    ============================== -->
    @include('layouts.shared.bannerTwo')
    <!-- =========================
        END HOME COVETED SECTION
    ============================== -->

    <!-- =========================
        START HOME SHOP SECTION
    ============================== -->
    @include('layouts.shared.sectionThreeProduct')
    <!-- =========================
        END HOME SHOP SECTION
    ============================== -->
@endsection