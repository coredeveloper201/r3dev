#Tabel Name : items
    	featured(int(11)) default 0
    	advantage (text) null
    	details (long text) null
    	
    	ALTER TABLE `items` ADD `featured` INT(11) NOT NULL DEFAULT '0' AFTER `description`, ADD `advantage` TEXT NULL DEFAULT NULL AFTER `featured`, ADD `details` LONGTEXT NULL DEFAULT NULL AFTER `advantage`;
    	
    	
#Tabel Name : users 	
    	ALTER TABLE `users` ADD `social` VARCHAR(50) NULL DEFAULT NULL AFTER `password`, ADD `provider_id` VARCHAR(50) NULL DEFAULT NULL AFTER `social`;


#Table Name : meta_buyers
        ALTER TABLE `meta_buyers`  
        ADD `avatar` VARCHAR(191) NOT NULL DEFAULT 'user.png'  AFTER `id`, 
        ADD `mobile` VARCHAR(50) NULL AFTER `avatar`, 
        ADD `address1` VARCHAR(191) NULL  AFTER `mobile`,  
        ADD `address2` VARCHAR(191) NULL  AFTER `address1`,  
        ADD `city` VARCHAR(191) NULL  AFTER `address2`,  
        ADD `state` VARCHAR(191) NULL  AFTER `city`,  
        ADD `zip` VARCHAR(191) NULL  AFTER `state`,  
        ADD `country` VARCHAR(191) NULL  AFTER `zip`;

#Table Name : meta_buyers
        ALTER TABLE `cart_items` CHANGE `user_id` `user_id` VARCHAR(255) NOT NULL;
        ALTER TABLE `cart_items` CHANGE `item_id` `item_id` BIGINT(191) NOT NULL;
        ALTER TABLE `cart_items` CHANGE `color_id` `color_id` INT(11) NULL DEFAULT NULL;
        ADD `country` VARCHAR(191) NULL  AFTER `zip`;
        ALTER TABLE `meta_buyers` CHANGE `avatar` `avatar` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user.png';
#Table Name: category
        ALTER TABLE `categories` ADD `slug` VARCHAR(255) NULL DEFAULT NULL AFTER `name`;
        ALTER TABLE `items` ADD `slug` VARCHAR(255) NULL DEFAULT NULL AFTER `name`